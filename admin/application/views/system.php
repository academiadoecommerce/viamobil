<!DOCTYPE html>
<html lang="pt-br">
    <head>
        

     <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <!-- <link rel="stylesheet" href="<?=base_url('assets/adminlte')?>/bower_components/bootstrap/dist/css/bootstrap.min.css"> -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('assets/adminlte')?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url('assets/adminlte')?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/adminlte')?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=base_url('assets/adminlte')?>/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<?php 
include "head.php"; 

// get user info if logged
$userinfo = $this->panamerico_model->details('admin_users', 'adm_use_id', $this->session->userdata('login'));

?>

</head>
<body class="hold-transition skin-blue sidebar-mini green">
<!-- Site wrapper -->
<div class="wrapper" style="overflow: hidden;">

  <header class="main-header border-0">
    <!-- Logo -->
    <a title="<?php echo NOME_SITE ?>" href="<?=base_url()?>" class="logo" target="_self">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <img class="logo-lg" width="120" style="margin-left: 35px;" src="<?=base_url('assets/img/logo.png')?>">
      <!-- <span class="logo-lg"><b>Admin</b>LTE</span> -->
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
<!--           <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                
                <ul class="menu">
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="http://image.habnetlife.com/i/user_m.png" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li> -->
          <!-- Notifications: style can be found in dropdown.less -->
          <li  class="notifications-menu">
            <a href="http://shopbras.net/" target="_blank" title="Visitar Site">
              <i class="fa  fa-mouse-pointer"></i>
            </a>
<!--             <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul> -->
          </li>
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=base_url('assets/img/'.$userinfo->adm_img)?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?=$userinfo->adm_nome?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=base_url('assets/img/'.$userinfo->adm_img)?>" class="img-circle" alt="User Image">

                <p>
                  <?=$userinfo->adm_nome?> - <?=$userinfo->adm_use_login?>
                  <small><?=$userinfo->adm_use_email?></small>
                </p>
              </li>
              <!-- Menu Body -->
<!--               <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
              </li> -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?=base_url('system/users_edit/'.$userinfo->adm_use_id)?>" class="btn btn-default btn-flat">Minha conta</a>
                </div>
                <div class="pull-right">
                  <a href="<?=base_url('login/out')?>" class="btn btn-default btn-flat">Sair</a>
                </div>
              </li>
            </ul>
          </li>

          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-gears"></i>
            </a>
            <ul class="dropdown-menu">
              <!-- <li class="header">Gerenciar Moderadores</li> -->
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  
                  <li>
                    <a href="<?=base_url('system/users')?>">
                      <h3 style="margin: 10px 0;">
                        Gerenciar Moderadores
                      </h3>
                    </a>
                  </li>

                  <li>
                    <a href="<?=base_url('system')?>">
                      <h3 style="margin: 10px 0;">
                        Configurações do sistema
                      </h3>
                    </a>
                  </li>

                  <li>
                    <a href="#">
                      <h3 style="margin: 10px 0;">
                        Suporte
                      </h3>
                    </a>
                  </li>
                
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all options</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url('assets/img/'.$userinfo->adm_img)?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p> <?=$userinfo->adm_nome;?> </p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
<!--       <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu Principal</li>

        <li <?= ((CURRENT_PAGE == "dashboard" || CURRENT_PAGE == "" ) ? 'class="active"' : '') ?>>
          <a href="<?=base_url('dashboard');?>">
            <i class="fa fa-dashboard"></i> 
            <span>Dashboard</span>
<!--             <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
<!--           <ul class="treeview-menu">
            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul> -->
        </li>
<!--         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="../layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li> -->

        <li <?= ((CURRENT_PAGE == "banner") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('banner') ?>"><i class="fa fa-fw fa-image"></i>
                <span>Banners</span>
            </a>
        </li>

        <li <?= ((CURRENT_PAGE == "ads") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('ads') ?>"><i class="fa fa-fw fa-tags"></i>
                <span>Anúncios</span>
            </a>
        </li>

        <li <?= ((CURRENT_PAGE == "variacoes") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('variacoes') ?>"><i class="fa fa-fw fa-tint"></i>
                <span>Variações</span>
            </a>
        </li>
                        
        <li <?= ((CURRENT_PAGE == "categories") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('categories') ?>"><i class="fa fa-fw fa-folder"></i>
                <span>Categorias</span>
            </a>
        </li>

        <li <?= ((CURRENT_PAGE == "shops") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('shops') ?>"><i class="fa fa-fw fa-shopping-cart"></i> 
                <span>Lojas</span>
            </a>
        </li>

        <li <?= ((CURRENT_PAGE == "reclamacoes") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('reclamacoes') ?>"><i class="fa fa-folder-open"></i> 
                <span>Reclamações</span>
            </a>
        </li>

        <!-- <li <?= ((CURRENT_PAGE == "users") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('users') ?>"><i class="fa fa-fw fa-users"></i> 
                <span>Usuários</span>
            </a>
        </li> -->

        <li class="treeview <?= ((CURRENT_PAGE == "users") ? 'active' : '') ?>">
          <a href="#">
            <i class="fa fa-users"></i> <span>Usuários</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right" ></i>
            </span>
          </a>
          <ul class="treeview-menu" >
            <li <?= ((CURRENT_PAGE == "users" && !$this->uri->segment(2)) ? 'class="active"' : '') ?>>
              <a href="<?= base_url('users') ?>">
                <i class="fa fa-circle-o"></i> 
                Clientes
              </a>
            </li>
            <li <?= (($this->uri->segment(2) == 'vendedores') ? 'class="active"' : '') ?> >
              <a href="<?= base_url('users/vendedores') ?>">
                <i class="fa fa-circle-o" ></i> 
                Vendedores
              </a>
            </li>
          </ul>
        </li>

        <li <?= ((CURRENT_PAGE == "images") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('images') ?>"><i class="fa fa-fw fa-file-image-o"></i> 
                <span>Imagens</span>
            </a>
        </li>

        <li <?= ((CURRENT_PAGE == "emails") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('emails') ?>"><i class="fa fa-fw fa-envelope"></i> 
                <span>E-mails</span>
            </a>
        </li>

        <li <?= ((CURRENT_PAGE == "pages") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('pages') ?>"><i class=" fa fa-fw fa-file-code-o"></i>
                <span>Páginas</span>
            </a>
        </li>

        <li <?= ((CURRENT_PAGE == "advertising") ? 'class="active"' : '') ?>>
            <a  href="<?= base_url('advertising') ?>"><i class="fa fa-fw fa-puzzle-piece"></i>
                <span>Publicidade</span>
            </a>
        </li>

        <li <?= ((CURRENT_PAGE == "returns") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('returns') ?>"><i class="fa fa-fw fa-undo"></i>
                <span>Retornos</span>
            </a>
        </li>

        <li <?= ((CURRENT_PAGE == "testimonials") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('testimonials') ?>"><i class="fa fa-fw fa-indent"></i>
                <span>Depoimentos</span>
            </a>
        </li>

        <li <?= ((CURRENT_PAGE == "system") ? 'class="active"' : '') ?>>
            <a href="<?= base_url('system') ?>"><i class="fa fa-fw fa-cog"></i>
                <span>Sistema</span>
            </a>
        </li>
       
      </ul>

    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper" style="padding: 20px">
        <section class="content box box-primary" style="
        background-color: white;
        -webkit-box-shadow: 0px 2px 10px 0px rgba(0,0,0,0.1);
        box-shadow: 0px 2px 10px 0px rgba(0,0,0,0.1)">
    <!-- <section class="content" style="background-color: white; -webkit-box-shadow: 0 0 10px 0 #BFBFBF;
    box-shadow: 0 0 10px 0px #BFBFBF;"> -->

           
            <div class="ac-content">
                <div class="container-fluid" id="janela_container">
                    
                    <?php
                    $return = $this->session->flashdata('return');

                    if ($return) {
                        if ($return == 'save') {?>
                          <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                            Registro salvo com sucesso.
                          </div>
                        <? }
                        if ($return == 'delete') { ?>
                          <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Apagado!</h4>
                            Registro apagado com sucesso.
                          </div>
                        <? }
                        if ($return['status'] == 'erro') { ?>
                          <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Erro!</h4>
                            <?=$return['msg']?>
                          </div>
                        <? }
                    }
                    echo $contents;
                    ?>

                </div>
            </div>
        </section>
    </div>




        <div class="modal fade" id="modal_ask" role="dialog" aria-labelledby="myModalLabel"> </div>
        <!-- modal -->
        <!-- <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"></div> -->

        <!-- lightbox -->
        <link href="<?= base_url('assets/lightbox/dist/css/lightbox.min.css') ?>" rel="stylesheet">
        <script src="<?= base_url('assets/lightbox/dist/js/lightbox.min.js') ?>"></script>
        <script type="text/javascript">

            $('[data-toggle="modal').on('click', function(){
              $('#modal_ask').modal('show').load($(this).data('modal'));
            });

            function loadingPaginacao(pagina, janela) {
                //$("#janela_" + janela).empty();
                $("#carregandoJanela_" + janela).show();

                setTimeout(function() {
                    $("#janela_" + janela).load(pagina);
                    $("#carregandoJanela_" + janela).hide();


                }, 1000);
            }

            function mudaPagina(url) {
                window.location = '<?php echo base_url(); ?>' + url;
            }
            
             
        </script>

  <!-- SlimScroll -->
  <script src="<?=base_url('assets/adminlte')?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?=base_url('assets/adminlte')?>/bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?=base_url('assets/adminlte')?>/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?=base_url('assets/adminlte')?>/dist/js/demo.js"></script>


  <script>
    $(document).ready(function () {
      $('.sidebar-menu').tree()
    })
  </script>


    </body>
    <!-- Sistema desenvolvido por Wiliam Joaquim, sua distribuição não é autorizada! -->
</html>