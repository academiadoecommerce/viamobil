<div class="row">
	<div class="medium-3 columns">
		<nav class="profile-menu">
			<ul>
				<li>
					<a href="<?=base_url('cliente/painel')?>" <?=(($page == "dashboard") ? 'class="active"' : '')?> target="_self">
						<span><i class="fa fa-fw fa-tags"></i></span>
						<span>Meus Anúncios</span>
					</a>
				</li>
				<li>
					<a href="<?=base_url('cliente/detalhes')?>" <?=(($page == "details") ? 'class="active"' : '')?> target="_self">
						<span><i class="fa fa-fw fa-cogs"></i></span>
						<span>Meu Cadastro</span>
					</a>
				</li>
				<li>
					<!--<a href="<?=base_url('chat')?>" <?=(($page == "chat") ? 'class="active"' : '')?> target="_self">
						<span><i class="fa fa-comments"></i></span>
						<span>Chat</span>
					</a>-->
					<a href="<?=base_url('cliente/loja')?>" <?=(($page == "shop" || $page == "shop_denied" || $page == "shop_create") ? 'class="active"' : '')?> target="_self">
						<span><i class="fa fa-fw fa-shopping-cart"></i></span>
						<span>Minha Loja</span>
					</a>
				</li>
				<li>
					<a href="<?=base_url('cliente/favoritos')?>" <?=(($page == "favorites") ? 'class="active"' : '')?> target="_self">
						<span><i class="fa fa-fw fa-heart"></i></span>
						<span>Favoritos</span>
					</a>
				</li>
				<li>
					<a href="<?=base_url('cliente/sair')?>" target="_self">
						<span><i class="fa fa-fw fa-times"></i></span>
						<span>Sair</span>
					</a>
				</li>
			</ul>
		</nav>

		<div class="profile-infos hide-for-small-only">
			<?php
				$products_published = $this->user_model->adsCount(2);
				$evaluating_products = $this->user_model->adsCount(1);
				$products_sold = $this->user_model->info('use_ads_sales');
			?>
			<ul>
				<li><i class="fa fa-fw fa-tags"></i>Você possui <strong><?=$products_published?></strong> anúncio(s) publicado(s).</li>
				<li><i class="fa fa-fw fa-eye"></i>Existe <strong><?=$evaluating_products?></strong> anúncio(s) à ser analisado.</li>
				<li><i class="fa fa-fw fa-usd"></i>Você já vendeu <strong><?=$products_sold?></strong> produto(s).</li>
			</ul>
		</div>
		
		<div class="hide-for-small-only">
		<?=$this->main_model->advertisingBox('side', '266px', '600px')?>
		</div>
	</div>
	<div class="medium-9 columns">
		<?=$this->main_model->advertisingBox('top', '100%', '90px')?>
		
		<?php
			$subFolder = 'profile/';
			if($page == 'chat'){
				$subFolder = '';
			}
			include_once($subFolder.$page.".php");
		?>
	</div>
</div>