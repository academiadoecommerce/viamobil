<?
// number_format(199.99, 2, ',', ' '); die;
// echo dinheiro(199.99, 2); die;
?>
<style type="text/css">
	li {list-style-type:none;}
	ul#submenu {
		float:left;
		width: 638px;	
	}
	.lists {
	    float:left;
	    width:33%;
	}
	@media (max-width: 640px) {
	   /*#grupo-cat, #ads_popul, #shop_popul {display: none;}*/
	}
	#idou {text-align: center; width: 100%; padding-top: 10px;}
</style>

<!-- ========================= RETURN ALERTS ========================= -->
<?php
$return = $this->session->flashdata('return');
$ret = $this->main_model->returnDetails($return);

if($ret){
	echo '
	<div id="msg-box" style="padding: 10px;">
		<div style="padding-left: 20px;" class="row alert alert-'.$ret->ret_type.'"><i class="fa fa-fw '.$ret->ret_icon.'"></i>'.$ret->ret_text.'</div>
	</div>';
} ?>
	

<!-- ========================= SECTION MAIN ========================= -->
<section class="section-main bg padding-y-sm" id="grupo-cat">
	<div class="container p-0-mobile">
		<div class="card rounded-0">
			<div class="card-body">
				<div class="row row-sm">
					<!-- menu de categorias -->
					<aside class=" col-md-3 d-none d-lg-block">
						<h5 class="text-uppercase mx-3 mt-2"><i class="fa fa-bars"></i> Categorias</h5>
						  <?php
						  $i = 0;
						  $r = 9;
						  $limite = $count_categories - $r; 
							 //  Exibe es categorias PAI e Filho na pagina principal
							 if($ads_categories){
							echo '<ul class="menu-category list-dots">';	

										$this->db->limit($r, 1);				
										$this->db->order_by('ads_cat_name', 'ASC');
										$this->db->where('ads_cat_parent', 0);
										$this->db->where('ads_cat_status', 1);
										$categorias = $this->db->get("ads_categories");
										$categorias = $categorias->result();
										$adulto = array_shift($categorias);
										
										
								foreach ($categorias as $key => $cat) { 
									$sub_categories = $this->ads_model->categories($cat->ads_cat_id);  // Pesquisa pela sub-categoria, passando o campo 'ads_cat_id' como parametro.

									echo '<li> <a target="_blank" href="'.base_url('anuncios/?categoria='.$cat->ads_cat_id).'">'.$cat->ads_cat_name.' </a></li> <hr class="m-0">';
									$i++;
								}
								// array_push($sub_categories, $adulto);								

										
								$this->db->limit($limite, $r+1);				
								$this->db->order_by('ads_cat_name', 'ASC');
								$this->db->where('ads_cat_parent', 0);
								$this->db->where('ads_cat_status', 1);
								$query = $this->db->get("ads_categories");
								$query = $query->result();		
								
								echo '<li class="has-submenu"> <a href="#">Mais categorias <i class="fas fa-angle-double-right"></i></a>						
										<ul class="submenu" id="submenu" style="bgcolor: #EFEFEF;">';
											foreach ($query as $chave => $cate) {
												echo '<div class="lists"><li id="linha"> <a href="'.base_url('anuncios/?categoria='.$cate->ads_cat_id).'">'.$cate->ads_cat_name.' </a></li></div>';
											}

								echo '<div class="lists"><li id="linha"> <a href="'.base_url('anuncios/?categoria='.$adulto->ads_cat_id).'">'.$adulto->ads_cat_name.' </a></li></div>';
								

								echo '</ul>							
									</li>';							
						
							  }
						   ?>
						</ul>
					</aside> <!-- col.// -->

					<!-- slider -->
					<div class="col-md-6">


						<!-- ================= main slide ================= -->

						<div class="owl-init slider-main owl-carousel" >
							<? 
							
							foreach ($imgbanner as $key) { ?>
								<div class="item-slide">
									<a target="_blank" href="<?=$key->banner_slug?>">
										<img alt="<?=$key->banner_name?>" src="<?= base_url('assets/img/banners/'.$key->banner_img)?>">
									</a>
								</div>
							<? } ?>
							<!-- <div class="item-slide">
								<img src="<?= base_url()?>assets/img/banners/slide1.jpg">
							</div> -->
						</div>

					    <style type="text/css">
					    	.owl-dots{
					    		display: flex;
    							justify-content: center;
					    	}
					    	button.owl-dot {
							    border: 1px solid #b5b5b5!important;
							    height: 14px;
							    margin: 8px;
							    border-radius: 10px;
							    background: transparent;
							}
							button.owl-dot.active {
							    border: 1px solid #2ebf77 !important;
							}
					    </style>
						
						<!-- ============== main slidesow .end // ============= -->
					</div> <!-- col.// -->

					<!-- publicidade -->
					<aside class=" col-md-3 d-none d-lg-block publicidade-home">
						<!-- <h6 class="title-bg bg-secondary">Publicidade</h6> -->
						<div style="height:100%;">
							<figure class="itemside has-bg border-bottom" style="margin-bottom: 4px;">
								<a target="_blank" href="<?=$img_publicidade[0]->adv_url?>">
									<img class="img-bg" src="<?=base_url('uploads/publicidade/'.$img_publicidade[0]->adv_img)?>" alt="<?=$img_publicidade[0]->adv_name?>">
								</a>
								<!-- <img class="img-bg" src="<?= base_url()?>assets/img/items/4.jpg"> -->
								<!-- <figcaption class="p-2 w-100">
									<h6 class="title">Espaço para publicidade </h6>
								</figcaption> -->
							</figure>
							<figure class="itemside has-bg" style="height: 50%;">
								<a target="_blank" href="<?=$img_publicidade[1]->adv_url?>">
									<img class="img-bg" src="<?=base_url('uploads/publicidade/'.$img_publicidade[1]->adv_img)?>" alt="<?=$img_publicidade[1]->adv_name?>">
								</a>
								<!-- <img class="img-bg" src="<?= base_url()?>assets/img/items/3.jpg"> -->
								<!-- <figcaption class="p-2 w-100">
									<h6 class="title">Espaço para publicidade </h6>
								</figcaption> -->
							</figure>
						</div>
					</aside>

				</div>
			</div>
		</div>
	</div> 
</section>


<!-- ========================= SECTION CTA ========================= -->
<section class="p-0">
	<div class="container">
		<div class="card rounded-0" style="background-color: #1d2547;">
			<h4 style="color: white;" class=" mt-2 text-center">Lorem Ipsum é simplesmente uma simulação</h4>
		</div>
	</div>
</section>


<!-- No modo mobile, exibe os botões de Vender e Comprar -->
<!-- <script src="assets/js/custom/main.js"></script>
<div class="show-for-small-only">
	<div class="medium-12 large-8 columns">		
		<div class="row">
			<div class="small-12 medium-6 columns">
				<a href="<?=base_url('anunciar')?>" class="btn btn-block btn-secondary"><i class="fa fa-fw fa-tag"></i> Vender grátis</a>
			</div>
			<p id="idou"> Ou </p>
			<div class="small-12 medium-6 columns" align="right">
				<a href="<?=base_url('anuncios')?>" class="btn btn-block btn-primary"><i class="fa fa-fw fa-usd"></i> Comprar agora</a>
			</div>
			<br>
		</div>		
	</div>
	<br> 
</div> -->
<!-- Final da Div Vender e Comprar -->





<!-- ========================= SECTION ANUNCIOS POPULARES 	// ========================= -->
<? require 'anuncios_populares_view.php'; ?>



<!-- ========================= SECTION LOJAS POPULARES 		// ========================= -->
<? require 'lojas_populares_view.php'; ?>
