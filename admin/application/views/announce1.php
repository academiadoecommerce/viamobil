
<div class="container page-add-ads">

    <h1 style="position: relative;top: 25px" class="text-center">Criar anuncio</h1>

    <form action="Announce/anunciar_prox" method="post">
        <div class="row pt-5">

                
                <div class="col-12 col-md-4">
                    <div class="form-group border rounded p-2 col-add shadow-sm bgwhite">
                        <ul id="categ">
                            <? foreach ($categories as $cat) { ?>
                                <li class=""  id="<?=$cat->ads_cat_id?>" ><?=$cat->ads_cat_name?></li>
                            <? } ?>
                            
                            <input type="text" name="categ" hidden>
                        </ul>
                      </div>                
                </div>

                <div class="col-12 col-md-4">
                    <div class="form-group border rounded p-2 col-add shadow-sm d-none colsub bgwhite">
                        <ul id="subcateg">
                            <? foreach($categoriafilho as $filho){ ?>
                                <li class="d-none" data-parent="<?=$filho->ads_cat_parent?>"  id="<?=$filho->ads_cat_id?>" ><?=$filho->ads_cat_name?></li>
                            <? } ?>
                        
                            <input type="text" name="subcateg" hidden>
                        </ul>
                      </div>                
                </div>

                <div class="col-12 col-md-4">
                    <div class="form-group border rounded p-2 col-add shadow-sm d-none colpronto bgwhite">
                        <label for="ads_cat_id"><font size="4">Crie uma titulo para o seu anúncio.</font></label>
                        
                        <input class="form-control" name="ads_cat_name" placeholder="Exemplo: camisa Lacoste  manga comprida" required="required"> 
                        <span class="helper">* Requerido, 6 caracteres ou mais</span>
                        <center>
                            <div style="height: 200px"><img width="100" style="display: none;" class="m-3" alt="pronto" src="http://soatacado.com/assets/img/vue/ready-0.png"> 
                            <h3 class="m-2" style="display: none;">Pronto!</h3> 
                            </div>
                            <button id="continuar" disabled type="submit" class="btn btn-primary w-75 green"> Continuar </button>
                        </center>
                    </div>                
                </div>

        </div>
        
    </form>

</div>


<!-- ========================= SECTION ANUNCIOS POPULARES   // ========================= -->
<? require 'anuncios_populares_view.php'; ?>


<!-- ========================= SECTION LOJAS POPULARES      // ========================= -->
<? require 'lojas_populares_view.php'; ?>




<script type="text/javascript">

    $(document).ready(function(){
        
        $('ul#categ li').click(function(){

           var categ_id = $(this).attr('id');

            $('#categ li').removeClass('selected');
            $(this).addClass('selected');

            $("ul#subcateg li").addClass('d-none') ;
            $("ul#subcateg").find("[data-parent='"+categ_id+"']").fadeIn(800).removeClass('d-none') ;
           
            $('div.colsub').fadeIn(800).removeClass('d-none');

           // adicionar este id em imput hidden
            $('ul#categ input').attr('value', categ_id);
        });
        

        $('#subcateg li').click(function(){
            var subcateg_id = $(this).attr('id');
            $('#subcateg li').removeClass('selected');
            $(this).addClass('selected');
            $('div.colpronto').fadeIn(800).removeClass('d-none');
           
           // adicionar este id em imput hidden
            $('ul#subcateg input').attr('value', subcateg_id);
        });

        $('[name = ads_cat_name]').keyup(function(){
            if(this.value.length > 5) {
                $('[alt = pronto]').fadeIn(800);
                $('.colpronto h3').fadeIn(800);
                $('#continuar').removeAttr('disabled');

            } else if(this.value.length < 6) {
                $('[alt = pronto]').fadeOut(800);
                $('.colpronto h3').fadeOut(800);
                $('#continuar').attr('disabled', 'true');
            }
        })

    });
</script>