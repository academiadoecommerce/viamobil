
<div class=" bgwhite p-4 rounded-4">
	<div class="row d-block">
		<div class="small-12 columns">
			<p><?=$text?></p>
			<br>
		</div>
	</div>

	<!-- <div class="divider divider-m-top-none"></div> -->

	<div class="row d-block">
		<div class="small-12 columns text-right">
			<button type="button" class="btn btn-link modal-close" onclick="close_app_modal()"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
			<a href="<?=$link?>" target="_self" class="btn btn-<?=$button['type']?>"><?=$button['text']?></a>
		</div>
	</div>
</div>

<script type="text/javascript">
	function close_app_modal(){
		$('#app-modal').fadeOut();
	}
</script>