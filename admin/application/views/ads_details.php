<? include_once('returns.php'); ?>

<div class="container px-0-mobile">
    <section class="breadcrumbs row hide-for-small-only">
        <?php
        if (@$breadcrumbs) {
            echo '
            <div class="row hide-for-small-only">
                <div class="medium-12 columns">
                    <div class="breadcrumbs hide-for-small-only">
                       <ul>
                          <li><a href="' . base_url() . '">Início</a></li>';

                         foreach ($breadcrumbs as $key => $bc) {
                           echo '<li>' . ((@$bc['link']) ? '<a href="' . $bc['link'] . '" target="_self">' . $bc['name'] . '</a>' : $bc['name']) . '</li>';
                        }
                  echo '
                       </ul>
                    </div>
                </div
            </div>'; }
                ?>
    </section>
<div class="medium-12 columns px-0-mobile">
    <div class="ads-page border-grey rounded-0">
    <h1 class="ap-title h3 text-center  d-sm-none"><?=$ad->ad_name; ?></h1>
        <div class="row">
            <div class="medium-8 columns">
                
                <?php if (isset($images) && !empty($images)) { ?>
                    <link rel="stylesheet" href="<?=base_url();?>assets/desoslide/animate/animate.min.css">
                    <link rel="stylesheet" href="<?=base_url();?>assets/desoslide/jquery.desoslide.min.css"> 

                    <div class="row">
                        <div class="col-lg-2 text-center slider-d-thumb-col">
                            <ul id="slideshow3_thumbs" class="desoslide-thumbs-vertical list-inline ml-1 slider-d-thumb" style="margin-bottom: -14px;">
                                <?php
                                $q=0;
                                foreach ($images as $key => $image) {

                                    $related_images = $image;
                                    $related_image = thumbnail($image->ads_img_file, "ads", 680, 410, 2);

                                    echo '
                                    <li style="" class="border-grey mb-3">
                                        <a href="'.$related_image.'">
                                            <img src="'.$related_image.'" >
                                        </a>
                                    </li>

                                ';
                                $i++;
                                if($i > 4) break;
                                }
                               
                          ?>
                           </ul>
                        </div>
                        <div id="slideshow3" class="col-lg-10 p-0"></div>
                    </div>

                    <script src="<?=base_url();?>assets/js/vendor/jquery.js"></script>
                    <script src="<?=base_url();?>assets/desoslide/jquery.desoslide.min.js"></script>
                    <script type="text/javascript">
                        $('#slideshow3').desoSlide({
                            thumbs: $('#slideshow3_thumbs li > a'),
                            auto: {
                                start: false
                            },
                            first: 1,
                            interval: 1000,
                            controls: {
                                show:false,
                                keys:true
                            },
                        });
                    </script>
                    <style type="text/css">
                     .desoslide-overlay {display: none;}
                    </style>

                <?php } else { ?> 

                    <div class="ap-images">
                        <div class="ap-i-master">
                            <img class="active" src="<?= thumbnail(false, false, 740, 400) ?>">
                        </div>
                    </div>

                <?php } ?>

                <div class="ap-desc"> <br>
                    <h3 id="show-descri-js">Descrição </h3>
                    <p><?= (($ad->ad_desc) ? descAnuncio($ad->ad_desc) : 'Esse anúncio não possui uma descrição.') ?></p>
                    

                    <br>

                    <!-- <h3>Detalhes</h3> -->
                    <div>
                        <!-- <div class="row"><div style="margin-top: 10px;" class="small-12 medium-12 large-12 end columns"><i class="fa fa-square" style="font-size: 12px;color: #2CC17B;margin-right: 7px;" aria-hidden="true"></i><strong>Publicado em:</strong> <?= string_date_time($ad->ad_timestamp) ?></div></div>
                        <div class="row"><div style="margin-top: 10px;" class="small-12 medium-12 large-12 end columns"><i class="fa fa-square" style="font-size: 12px;color: #2CC17B;margin-right: 7px;" aria-hidden="true"></i><strong>Categoria:</strong> <?= $category_parent->ads_cat_name . ' <i class="fa fa-angle-right" aria-hidden="true"></i> ' . $ad->ads_cat_name ?></div></div>
                        <div class="row"><div style="margin-top: 10px;" class="small-12 medium-12 large-12 end columns"><i class="fa fa-square" style="font-size: 12px;color: #2CC17B;margin-right: 7px;" aria-hidden="true"></i><strong>ID do Anúncio:</strong> <?= $ad->ad_id ?></div></div> -->

                        <?php
                        if ($custom_fields) {

                            foreach ($custom_fields as $key => $cf) {
                                
                                $value = $cf->ads_cus_value;

                                if ($cf->cat_fie_type == "textarea") {
                                    $value = nl2br($cf->ads_cus_value);
                                }

                                if ($cf->cat_fie_type == "select") {
                                    $option = $this->ads_model->customSelectOption($cf->ads_cus_value);

                                    $value = @$option->sel_opt_name;
                                }


                                if ($cf->cat_fie_type == "checkbox") {
                                    $options = $this->ads_model->customCheckboxOption($ad->ad_id, $cf->cat_fie_id);
                                    $value = "";

                                    if ($options) {
                                        $value .= '<div class="row">';

                                        foreach ($options as $key => $option) {
                                            $value .= '<div class="small-12 medium-6 large-6 end columns">' . $option->che_opt_name . '</div>';
                                        }

                                        $value .= '</div>';
                                    }
                                }

                                if ($value) {
                                    echo '<div class="row"><div style="margin-top: 10px;" class="small-12 medium-12 large-12 end columns"><i class="fa fa-square" style="font-size: 12px;margin-right: 7px; color: #2CC17B;" aria-hidden="true"></i><strong>' . $cf->cat_fie_name . ':</strong> ' . mudaCor($cf->cat_fie_name, $value) . '</div></div>
                                        ';
                                }
                            }
                        }
                        ?>

                        <!-- <h3 style="margin-top: 10px;">Endereço</h3>
                        <div class="row">
                            <?= ($state) ? ('<div class="small-12 medium-6 large-6 end columns"><i class="fa fa-square" style="font-size: 12px;color: #2CC17B;margin-right: 7px;" aria-hidden="true"></i><strong>Estado: </strong>' . $state . '</div>') : ('') ?>
                            <?= ($city) ? ('<div class="small-12 medium-6 large-6 end columns"><i class="fa fa-square" style="font-size: 12px;color: #2CC17B;margin-right: 7px;" aria-hidden="true"></i><strong>Município: </strong>' . $city . '</div>') : ('') ?>
                        </div> -->
                    </div>
                </div>

                <?php
                
                function descAnuncio($str){
                    if(strlen($str) >= 40){
                        $string = explode(' ', $str);
                        if(count($string) == 1){
                            return substr($str, 0, 40).'...';
                        }
                        
                        return nl2br($str);
                    }
                    
                    return nl2br($str);
                }
                
                function mudaCor($nome, $value){
                    $aspa = "'";
                    $http = 'http://';
                    if($nome != 'URL'){
                        return str_replace('<div class="small-12 medium-6 large-6 end columns">', '<div class="small-12 medium-6 large-6 end columns"><i class="fa fa-circle" style="font-size: 12px;color: #ccc;margin-right: 7px;" aria-hidden="true"></i> ', $value);
                    }else if($nome == 'URL'){
                        return ' <a href="'.$http.str_replace('http://','',$value).'" style="color:blue;" target="_blank">'.$value.'</a>';
                       
                    }
                    return $value;
                    
                }
                
                if ($related) {
                    echo '
                        <div class="row">
                            <div class="medium-12 columns">
                                <div class="ap-ads-related">
                                  <hr>
                                    <h2>Anúncios do vendedor</h2>
                                    <div class="row">
                        ';
                  
                    foreach ($related as $key => $rel) {
                        $related_images = $this->ads_model->images($rel->ad_id);
                        $related_image = thumbnail(@$related_images[0]->ads_img_file, "ads", 175, 120, 1);

                        // $textoCorte = substr($rel->ad_name, 0, 25);
                        // $textoLimitado = substr($textoCorte, 0, strrpos($textoCorte, ' '));

                                // <div class="small-6 medium-3 end columns">
                        echo '
                                <div class="p-2">
                                    <div class="ads-item-box rounded shadow-sm box-hover-shadow border-grey">
                                        <a href="' . base_url('anuncio/' . $rel->ad_slug) . '" title="' . $rel->ad_name . '" class="item text-left hover-grey">
                                            <div class="image p-0"><img alt="' . $rel->ad_name . '" src="' . $related_image . '"></div>
                                            <div class="m-2 hover-grey">

                                                <h4 class="m-0 p-0 hover-grey">' . truncate($rel->ad_name, 22) . ' </h4>
                                                <font size="2" class="">' . $rel->ad_condicao . ' </font>

                                                <div class="m-0 ad-price">' . (($rel->ad_service) ? 'Serviço' : string_money($rel->ad_price)) . '</div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            ';
                           
                    }
                    echo '
                                    </div>
                                </div>	
                            </div>
                        </div>
                        ';
                }
                ?>

               
            </div>
            
            <div class="medium-4 columns">
                
                
                        
                        

                <div class="ap-salesman-info rounded-0 shadow-sm">
                    <h1 class="ap-title"><?= descAnuncio($ad->ad_name); ?>  <?= (($ad->ad_verified) ? '<i class="fas fa-check-circle-o verify"></i>' : '') ?> 
                    <span style="float: right;"> <? $this->user_model->favorite_btn($ad->ad_id); ?> </span>
                    </h1>
                    <!--<span style="position: relative;top: -22px"><font size="1"><?=ucfirst($ad->ad_condicao)?></font></span>-->
                    <span class="views-count"><font size="1"> <i class="fa fa-eye"> </i> <?=$ad->ad_visits?> Visualizações</font></span> 
                    
                    <center class="mb-4">                        
                        <div class="mb-4">   <font size="3" >R$ <?=$ad->ad_price;?> Und.</font>
                        <div class="">   <!--<font size="2" >Quantidade mínima: <?=$ad->ad_qtdmin;?></font>-->
                    </center>
                        <!-- <font size='3'>R$ <? echo "<pre>";print_r($ad); ?> </font> -->
                    <!-- </div> -->

                        <?php /*
                    
                        if(isset($ad->ad_price) or !empty($ad->ad_price)){ ?> 
                        <font size="5">
                            <?= getDescAds($ad, $category_parent->ads_cat_id); ?>
                        </font>
                        
                        /<font size="2">Unidade</font>

                        <?php } */?>

                        <?/* if($ad->ad_trade) ?> 
                            <!-- <font style="margin-top: 8px;float: right;" size="4">Aceito troca</font>  -->
                        <? ; ?>
                    </div>

                    <br>

                     
                                   <?php   
                                    if(isset($ad->ad_price2) && !empty($ad->ad_price2)){ ?>
                                    <div id='precos' style='background-color: white;border: 1px solid #eee; margin-bottom: 7px;'>  
                                        <center>    
                                            <font size='3'>10 UND R$ <?=$ad->ad_price2; ?> </font>
                                        </center>
                                    </div>
                                    <? } ?>

                                    <?php if(isset($ad->ad_price2) && isset($ad->ad_price3) && $ad->ad_price2 != NULL && $ad->ad_price3 != NULL ){ ?>
                                        
                                        <div id='precos' style='background-color: white;border: 1px solid #eee;'> 
                                            <center>
                                                <font size="3" > Acima de 10 UND R$  <?=$ad->ad_price3;?> </font>
                                            </center>
                                        </div> 
                                        <br> 
                                    <? } */ ?>
                                    
                      
                     <!--<div class="ap-send-message">                       
                   
                        <form method="POST" id="mobile-message" action="<?= base_url('chat/novo_chat') ?>">
                     
                            <input type="hidden" name="sender_id" value="<?= $_SESSION['login']; ?>">
                            <input type="hidden" name="recipient_id" value="<?= $ad->use_id ?>">
                            <input type="hidden" name="ad_id" value="<?= $ad->ad_id ?>">
                            <input type="hidden" name="use_email" value="<?= $ad->use_email; ?>">
                            <input type="hidden" name="use_name" value="<?= $ad->use_name; ?>">
                            <input type="hidden" name="ad_name" value="<?= $ad->ad_name; ?>">
                            <?if ($this->session->userdata('login')) { ?>
                            <center><button  <?php if($_SESSION['login'] == $ad->use_id ){ echo 'disabled' ; } ?> type="submit" class="btn btn-primary w-100 btn-hover-green rounded-0"><i class="fa fa-comments"></i> Iniciar chat</button></center>
                        <? }else{ ?>
                            <a href="#" data-toggle="modal" data-target="#chat-required" class="btn btn-primary w-100 btn-hover-green rounded-0"><i class="fa fa-comments"></i> Iniciar chat </a>
                        <? } ?>
                        </form>
                    </div>-->

                    <hr>
                     <h4 >Informações do Vendedor</h4>


                    <ul class="p-0">
                        <li><i class="fa fa-fw fa-user"></i><?= $ad->use_name; ?></li>
                        <li class="px-1"><i class="fas fa-map-marker-alt"></i><?= $city; echo " - ".$state; ?></li>
                        <?//= (($ads_shop) ? '<li><i class="fa fa-shopping-cart"></i><a href="' . base_url('loja/' . $shop) . '" target="_self">' . $ads_shop . ' Anúncios na Loja</a></li>' : '') ?>
                    </ul>

                    <ul class="info-vendedor text-center px-0">
                        <? if(isset($shop) && !empty($shop)) echo '<li><a href="'.base_url("loja/".$shop).'">Ir para loja</a></li>' ; ?>
                        <li>
                            <a href="javascript:void(0)" data-toggle="popover" data-html="true" data-placement="top"
                        <? if($ad->use_phone){ ?> 
                            data-content='<i style="font-size: 18px;position: relative;top: 2px;left: -3px;" class="fas fa-phone"></i><?=$ad->use_phone;?>'
                        <? } else { ?>
                            data-content="Indisponível" 
                        <? } ?>  >Ver telefone</a>
                        </li>

                        <li>
                            <a href="javascript:void(0)" data-html="true" data-toggle="popover" data-placement="top" 
                        <? if($ad->use_whatsapp){ ?> 
                            data-content='<i style="font-size: 18px;position: relative;top: 2px;left: -3px;" class="fab fa-whatsapp"></i><?=$ad->use_whatsapp;?>'
                        <? } else { ?>
                            data-content="Indisponível" 
                        <? } ?>>WhatsApp</a></li>

                        <!-- <li><a href="">Ver WhatsApp</a></li> -->
                    </ul>

                    <div class="row justify-content-center">
                        <div class="share-button m-3">
                          <font size="2"><a href="javascript:void()" class="social-toggle"><i class="fas fa-share-alt"></i> Compartilhar</a></font>
                            <div class="social-networks">
                              <ul>
                                    <li class="social-mail">
                                      <a href="javascript:void()" data-toggle="modal" data-target="#modal-share-mail"><i class="fa fa-envelope"></i></a>
                                    </li>

                                    <li class="social-facebook">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?=current_url();?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    </li>

                                    <li class="social-whatsapp">
                                        <a href="https://web.whatsapp.com/send?text=<?=current_url();?>" data-action="share/whatsapp/share" target="_blank"><i style="font-size: 22px;" class="fab fa-whatsapp"></i></a>
                                        <!-- <a href="whatsapp://send?text=<?= $ad->ad_name ?> <?= $link ?>"><i style="font-size: 22px;" class="fab fa-whatsapp"></i></a> -->
                                    </li>

                                </ul>
                             </div>
                           </div>   

                           <div class="m-3">
                        <?php
                        if ($this->session->userdata('login')) { ?>

                        <font size="2"><a href="javascript:void()" data-toggle="modal" data-target="#moda-report" class="ap-a-report modal-open"><i class="fa fa-bullhorn"></i> Denunciar</a> </font>

                        <? } else { ?>

                            <font size="2"><a href="javascript:void()" data-toggle="modal" data-target="#modal-required" class="ap-a-report modal-open"><i class="fa fa-bullhorn"></i> Denunciar</a></font>

                        <? } ?>
                        </div>
                    </div>

                    <hr>

                     <h3>Detalhes</h3>
                    <div class="text-left">
                        <div class="row"><div style="margin-top: 10px;" class="small-12 medium-12 large-12 end columns"><i class="fa fa-square" style="font-size: 12px;color: #2CC17B;margin-right: 7px;" aria-hidden="true"></i><strong>Publicado em:</strong> <?= string_date_time($ad->ad_timestamp) ?></div></div>
                        <div class="row"><div style="margin-top: 10px;" class="small-12 medium-12 large-12 end columns"><i class="fa fa-square" style="font-size: 12px;color: #2CC17B;margin-right: 7px;" aria-hidden="true"></i><strong>Categoria:</strong> <?= $category_parent->ads_cat_name . ' <i class="fa fa-angle-right" aria-hidden="true"></i> ' . $ad->ads_cat_name ?></div></div>
                        <div class="row"><div style="margin-top: 10px;" class="small-12 medium-12 large-12 end columns"><i class="fa fa-square" style="font-size: 12px;color: #2CC17B;margin-right: 7px;" aria-hidden="true"></i><strong>ID do Anúncio:</strong> <?= $ad->ad_id ?></div></div>

                        <?php
                        if ($custom_fields) {

                            foreach ($custom_fields as $key => $cf) {
                                
                                $value = $cf->ads_cus_value;

                                if ($cf->cat_fie_type == "textarea") {
                                    $value = nl2br($cf->ads_cus_value);
                                }

                                if ($cf->cat_fie_type == "select") {
                                    $option = $this->ads_model->customSelectOption($cf->ads_cus_value);

                                    $value = @$option->sel_opt_name;
                                }


                                if ($cf->cat_fie_type == "checkbox") {
                                    $options = $this->ads_model->customCheckboxOption($ad->ad_id, $cf->cat_fie_id);
                                    $value = "";

                                    if ($options) {
                                        $value .= '<div class="row">';

                                        foreach ($options as $key => $option) {
                                            $value .= '<div class="small-12 medium-6 large-6 end columns">' . $option->che_opt_name . '</div>';
                                        }

                                        $value .= '</div>';
                                    }
                                }

                                if ($value) {
                                    echo '<div class="row"><div style="margin-top: 10px;" class="small-12 medium-12 large-12 end columns"><i class="fa fa-square" style="font-size: 12px;margin-right: 7px; color: #2CC17B;" aria-hidden="true"></i><strong>' . $cf->cat_fie_name . ':</strong> ' . mudaCor($cf->cat_fie_name, $value) . '</div></div>
                                        ';
                                }
                            }
                        }
                        ?>

                        <!-- <h3 style="margin-top: 10px;">Endereço</h3>
                        <div class="row">
                            <?= ($state) ? ('<div class="small-12 medium-6 large-6 end columns"><i class="fa fa-square" style="font-size: 12px;color: #2CC17B;margin-right: 7px;" aria-hidden="true"></i><strong>Estado: </strong>' . $state . '</div>') : ('') ?>
                            <?= ($city) ? ('<div class="small-12 medium-6 large-6 end columns"><i class="fa fa-square" style="font-size: 12px;color: #2CC17B;margin-right: 7px;" aria-hidden="true"></i><strong>Município: </strong>' . $city . '</div>') : ('') ?>
                        </div> -->
                    </div>

                    
                </div>


                <?php /*
                if ($ad->ad_mercado_livre || $ad->ad_elo7) {
                    echo '
                    <div class="ap-links">
                        <h4>Publicado também em</h4>

                        <ul>
                    ';

                    if ($ad->ad_mercado_livre) {
                        echo '<li><a href="' . $ad->ad_mercado_livre . '" target="_blank" title="Ir para o Mercado Livre"><img alt="Mercado Livre" src="' . base_url('assets/img/mercadolivre-logo.png') . '"></a></li>';
                    }

                    if ($ad->ad_elo7) {
                        echo '<li><a href="' . $ad->ad_elo7 . '" target="_blank" title="Ir para o Elo7" title="Elo7"><img alt="Elo7" src="' . base_url('assets/img/elo7-logo.png') . '"></a></li>';
                    }

                    echo '
                        </ul>
                    </div>
                    ';
                } */
                ?>

                <div class="col-12 shadow-sm">
                    <a target="_blank" href="<?=$img_publicidade->adv_url ?>">
                        <img src="<?=base_url('uploads/publicidade/'.$img_publicidade->adv_img)?>">
                    </a>
                </div>

                <?= $this->main_model->advertisingBox('top', '95%', '300px') ?>
                <div class="hide-for-small-only"><?= $this->main_model->advertisingBox('top', '95%', '300px') ?></div>
            </div>
        </div>
         <div class="hide-for-small-only"><?=$this->main_model->advertisingBox('top', '100%', '90px') ?></div>
    </div>
</div>
</div>


       

<script src="<?= base_url('assets/js/custom/ads_details.js') ?>"></script>
<script type="text/javascript">
    $('.social-toggle').on('click', function() {
  $('.social-networks').toggleClass('open-menu');
});

    $(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});
</script>

<? 
require_once 'modal/ads_email_share.php';
require_once 'modal/ads_report.php'; 
?>
