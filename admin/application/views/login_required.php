<!-- MODEL DO LOGIN  -->
    <div class="modal fade" id="modal-required" tabindex="-1" role="dialog">
        <div class="modal-dialog login1"  style="margin-top: 130px;">
            <div class="modal-content">
                <div class="user-box">
                    <div class="page-header" style="margin: 0 0 20px;">
                        <h3>Ops, você precisa efetuar o login</h3>
                    </div>

					<div class="form-group row">
						<div class="col-lg-12">
							<p align="center">Para fazer isto você precisa antes criar uma conta grátis, se você já possuir basta fazer o login :)</p>
							<br><br>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-6">							
							<a href="#" data-toggle="modal" data-target="#form-registro" class="btn btn-primary btn-full myBtn" style="margin-bottom: 10px;"><i class="fa fa-user-plus" aria-hidden="true"></i>Cadastrar-se</a>
						</div>
						<div class="col-lg-6">							
							<a href="#" data-toggle="modal" data-target="#modal-login" class="btn btn-secondary btn-full myBtn"><i class="fa fa-sign-in" aria-hidden="true"></i>Entrar</a>
						</div>
					</div>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
	
<script>
    // Hide the Modal
    $(".myBtn").click(function(){
        $("#modal-required").modal("hide");
    });
</script>	


