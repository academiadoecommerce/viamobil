<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y-sm bg card-home-destaque lojap">
	<div class="container">
	
	<header class="section-heading heading-line">
		<h4 class="title-section bg"> LOJAS POPULARES </h4>
	</header>
	
		<div class="card rounded-0">
			<div class="row no-gutters">
				<div class="col-md-2" id="shop_popul">					
					<article href="#" class="card-banner h-100 bg2 rounded-0">
						<div class="card-body zoom-wrap">
							<img src="<?= base_url();?>assets/img/items/anuciospopularesazul.jpg" class="img-bg zoom-in">
							<!-- <h5>Lorem Ipsum é simplesmente uma simulação</h5> -->
							<a target="_blank" href="<?=base_url('lojas')?>" class="btn btn-vermais shadow-sm w-75" style="color: #0f4786 !important;">Ver Todas</a>
						</div>
					</article>
				</div> <!-- col.// -->
				
				<div class="col-md-10">
					<ul class="row no-gutters">
					<?php
						foreach ($queryShop->result() as $rowShop){ 
						echo '<li class="col-6 col-md-2">
							<a href="'.base_url("loja/".$rowShop->shop_slug).'" class="itembox"> 
								<div class="card-body" style="text-align: center; height: 100%;">
									<img class="img-sm" src="'.base_url().'admin/uploads/shops/'.$rowShop->shop_img_file.'">
									<p class="word-limit" style="font-size: 18px;">'.$rowShop->shop_name.'</p>
									<p class="word-limit" style="font-size: 14px; color: orange;">Ver Loja</p>
								</div>
							</a>
						</li>';
						}?>			
					</ul>
				</div> <!-- col.// -->
			</div> <!-- row.// -->
			
		</div> <!-- card.// -->

	</div> <!-- container .//  -->
</section>