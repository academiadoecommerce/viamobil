
<?php
/* Login Verify */
if ($this->session->userdata('login')) {
    $user = $this->user_model->info();
}?>

<style>
	@media (max-width: 640px) {
	  #col-search {display: none;}  
	  #logo {text-align: center;}
	  #navbarTop {text-align: center; font-size: 18px;}
	  .navbar-toggler {background-color: #FFAE00;}
	  div.navbar-nav:hover {background-color: #CCCCCC;}
	  div.navbar-nav {border-bottom: 1px solid #000000;}
	  div#navbar-nav-top {border-top: 1px solid #000000; }
	}	
</style>

<script>
    function somenteNumeros(num) {
        var er = /[^0-9.]/;
        er.lastIndex = 0;
        var campo = num;
        if (er.test(campo.value)) {
          campo.value = "";
        }
    }
 </script>
<div class="fluid-container">
<header class="section-header shadow-sm" style="padding-bottom: 20px; position: relative; z-index: 99;">

		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-2">
					<a href="<?= base_url() ?>"><img class="logo my-3" id="logo" src="<?= base_url()?>assets/img/logo.png" title="ViamoBil"></a>
					<a href="javascript:void()" class="menu-open h2 float-right d-inline d-lg-none btn-mobile-btn"><i class="fas fa-bars"></i></a>
					<a class="nav-link d-none d-lg-block" href="<?= base_url('categorias');?>"> <i class="fas fa-list-ul"></i> Categorias</a>
				</div>	
				<div class="col-12 d-lg-none" id=""> 
							<form method="get" action="http://soatacado.com/anuncios">
								<div class="input-group w-100 rounded" style="border: 1px solid #1e2648">
									<select class="rounded bo border-0" name="category_name" style="width: 90px;">
										<option value="anuncios">Anúncios</option>
										<option value="lojas">Lojas</option>
									</select>
									<input style="border-left: 1px solid #dbdde6 !important;" name="search" type="text" class="form-control border-0 rounded-0" placeholder="Pesquisar por palavra" value="">
									<div class="input-group-append">
									  <button class="btn btn-primary rounded-0 border-0" type="submit">
										<i class="fa fa-search" style="
    margin: 6px;
"></i></button>
									</div>
								</div>
							</form> <!-- search-wrap .end// -->
						</div>
				<div class="col-md-10 d-none d-lg-block">	
					<div class="row my-2 d-flex justify-content-between">
						<!-- <nav class="navbar navbar-expand-sm navbar-light h-mobile-menu"> -->

							<div class="col-md-2 navbar-nav"></div>

		                <?php if ($this->session->userdata('login')) { ?>
							<div class="col-md-2 navbar-nav"><a class="nav-link h-mm-inserir modal-open" href="<?= base_url('anunciar') ?>"><i class="fas fa-clipboard-check"></i> Inserir Anúncio</a></div>
		                <?php } else { ?>
							<div class="col-md-2 navbar-nav"><a class="nav-link" href="#" data-toggle="modal" data-target="#modal-required"><i class="fa fa-tag"> </i> Inserir Anúncio</a></div>                  
		                <?php } ?>
						
						<!--<?php if ($this->session->userdata('login')) { ?>
							<div class="col-md-2 navbar-nav"><a class="nav-link" href="<?= base_url('chat') ?>"><i class="fa fa-comments"></i> Chat</a></div>
						<?php } else { ?>
							<div class="col-md-2 navbar-nav"><a class="nav-link" href="#" data-toggle="modal" data-target="#chat-required"><i class="fa fa-comments"></i> Chat</a></div>
						<?php } ?>-->

		                <?php if ($this->session->userdata('login')) { ?>
		                    <div class="col-md-2 navbar-nav"><a class="nav-link" href="<?= base_url('ajuda/duvidas-frequentes') ?>"><i class="fa fa-question"></i> Ajuda</a></div>
		                <?php } ?>				
					
						<div class="col-md-2 navbar-nav"><a class="nav-link h-mm-lojas" href="<?= base_url('lojas') ?>"><i class="fa fa-shopping-cart"></i> Lojas</a></div>

						<div class="col-md-2 navbar-nav"><a class="nav-link h-mm-lojas" href="<?= base_url('cart') ?>"><i class="fa fa-shopping-cart"></i> Carrinho</a></div>

						<div class="col-md-2 navbar-nav"><a class="nav-link h-mm-lojas" href="<?= base_url('anuncios') ?>"><i class="fa fa-tag"> </i> Anúncios</a></div>
					<!-- </div> -->
					<!-- </nav> -->


					</div>		
					<div class="row d-flex justify-content-between">
						<div class="col-md-9" id="col-search"> 
							<form method="get" action="<?=base_url('anuncios/');?>">
								<div class="input-group w-100 rounded" style="border: 1px solid #1e2648">
									<select class="rounded bo border-0" name="category_name" style="width: 130px;">
										<option value="anuncios" <? if($this->uri->segment(1) == 'anuncios') echo 'selected';?> >Anúncios</option>
										<option value="lojas" <? if($this->uri->segment(1) == 'lojas') echo 'selected';?> >Lojas</option>
									</select>
									<input style="border-left: 1px solid #dbdde6 !important;" name="search" type="text" class="form-control border-0 rounded-0" placeholder="Pesquisar por palavra" value="<?=@$_GET['search'];?>">
									<div class="input-group-append">
									  <button class="btn btn-primary rounded-0 border-0" type="submit">
										<i class="fa fa-search"></i> Pesquisar 
									  </button>
									</div>
								</div>
							</form> <!-- search-wrap .end// -->
						</div> <!-- col.// -->

		           		<div class="col-lg-3 navbar-nav" id="navbar-nav-top">
							<? if ($this->session->userdata('login')) { ?>
			               			<span>
										<div class="d-flex justify-content-center">
											<a style="font-size: 13px;" class="" href="<?= base_url('cliente/painel') ?>">Olá,  <b><?= first_name($user->use_name) ?></b></a>
											<span style="color: #d4d4d4;margin: 0 4px; font-size: 12px;"> | </span>
											<a style="font-size: 13px;" class="" href="<?= base_url('cliente/sair') ?>">Sair</a>
										</div>
										<div class="d-flex justify-content-center">
											<a href="<?= base_url('cliente/painel') ?>"><i class="fas fa-user" style="color: #848484;position: absolute;font-size: 30px;left: 32px;top: 7px;"></i>Minha Conta</a>
										</div>
									</span>               			
			          		<?php } else { ?>
									<span>
										<div class="d-flex justify-content-center">
											<a style="font-size: 13px;" href="#" data-toggle="modal" data-target="#modal-login">Entrar </a>
											<span style="color: #d4d4d4;margin: 0 4px; font-size: 12px;"> | </span>
											<a style="font-size: 13px;" href="#" data-toggle="modal" data-target="#form-registro"> Cadastre-se</a>
										</div>
										<div class="d-flex justify-content-center">
											<a href="#" data-toggle="modal" data-target="#modal-login" ><i class="fas fa-user" style="color: #848484;position: absolute;font-size: 30px;left: 32px;top: 7px;"></i>Minha Conta</a>
										</div>
									</span>						
			            	<? } ?>
		           		</div>
	            	</div>
           		</div>

			</div> <!-- collapse.// -->
		</div>
</header> <!-- section-header.// -->

<div class="menu-overlay"></div>
<!-- <a href="#" class="menu-open">Open Menu</a> -->

<div class="side-menu-wrapper" style="display: none;">
<a href="#" class="menu-close ">×</a>
<ul>
	<li class="side-menu-wrapper-header"><i style="font-size: 30px" class="fas fa-user"></i> <br>
	<? if ($this->session->userdata('login')) { ?>
		<div><span><a href="<?= base_url('cliente/painel') ?>">Olá,  <b><?= first_name($user->use_name) ?></b></a></span> | <span><a href="<?= base_url('cliente/sair') ?>">Sair</a></span></div></li>
	<? }else{ ?>
		<div><span><a href="#" data-toggle="modal" data-target="#modal-login">Entrar</a></span> | <span><a data-toggle="modal" data-target="#form-registro"  href="#">Cadastrar-se</a></span></div></li>
	<? } ?>
	<li><a href="<?=base_url()?>" ><i class="fa fa-home"></i> Home</a></li>
	<li><a href="<?=base_url('categorias')?>"> <i class="fas fa-list-ul"></i> Categorias</a></li>
	<li><a href="<?=base_url('anuncios')?>"><i class="fa fa-tag"> </i> Anúncios</a></li>
	<li><a href="<?=base_url('lojas')?>" ><i class="fa fa-shopping-cart"></i> Lojas</a></li>
	
	<? if ($this->session->userdata('login')) { ?>

	<!--<li><a href="<?=base_url('chat')?>"><i class="fa fa-comments"></i> Chat</a></li>-->
	<li><a href="<?=base_url('anunciar') ?>"><i class="fas fa-clipboard-check"></i> Inserir Anúncio</a></li>
   
	<li><a href="<?=base_url('cliente/painel')?>"><i class="fas fa-bars"></i> Minha conta</a>
		<nav class="menu-dashboard">
			<!-- <h5 class="color-grey"><i class="fas fa-bars ml-4"></i>Minha conta</h5> -->
			<ul class="ml-0">
				<li>
					<a href="<?=base_url('cliente/painel')?>" <?=(($page == "dashboard") ? 'class="active"' : '')?> target="_self">
						<span><i class="fa fa-fw fa-tags"></i></span>
						<span>Meus Anúncios</span>
					</a>
				</li>

				<li>
					<a href="<?=base_url('cliente/detalhes')?>" <?=(($page == "details") ? 'class="active"' : '')?> target="_self">
						<span><i class="fa fa-fw fa-cogs"></i></span>
						<span>Meu Cadastro</span>
					</a>
				</li>

				<!--<li>
					<a href="<?=base_url('chat')?>" <?=(($page == "chat") ? 'class="active"' : '')?> target="_self">
						<span><i class="fa fa-comments"></i></span>
						<span>Chat</span>
					</a>
				</li>-->

				<li>
					<a href="<?=base_url('cliente/loja')?>" <?=(($page == "shop" || $page == "shop_denied" || $page == "shop_create") ? 'class="active"' : '')?> target="_self">
						<span><i class="fa fa-fw fa-shopping-cart"></i></span>
						<span>Minha Loja</span>
					</a>
				</li>

				<li>
					<a href="<?=base_url('cliente/favoritos')?>" <?=(($page == "favorites") ? 'class="active"' : '')?> target="_self">
						<span><i class="fa fa-fw fa-heart"></i></span>
						<span>Favoritos</span>
					</a>
				</li>

				<li>
					<a href="<?=base_url('cliente/sair')?>" target="_self">
						<span><i class="fa fa-fw fa-times"></i></span>
						<span>Sair</span>
					</a>
				</li>
			</ul>
		</nav>
	</li>
	<? } else { ?>
			<!--<li><a href="#" data-toggle="modal" data-target="#modal-required"><i class="fa fa-comments"></i> Chat</a></li>-->

			<li><a href="#" data-toggle="modal" data-target="#modal-required"><i class="fas fa-clipboard-check"></i> Inserir Anúncio</a></li>
   
			<li><a href="#" data-toggle="modal" data-target="#modal-required"><i class="fas fa-bars"></i> Minha conta</a>

	<? }?>
<br><br><br>
</ul>
</div>
	
  