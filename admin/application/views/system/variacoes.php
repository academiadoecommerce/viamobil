<div class="row">
	<div class="col-md-8">
		<h1>Variações</h1>
	</div>
	<div class="col-md-4" align="right">
		<div class="btn-group btn-group-lg" role="group">
			<a href="<?=base_url('variacoes/insert')?>" class="btn btn-primary">Adicionar Novo</a>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
            <?php  paginacao()->filtro('variacoes', FALSE); ?>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th align="center">ID</th>
					<th align="center">Cor</th>
					<th align="center">Status</th>
					<th align="center">Ações</th>
				</tr>
			</thead>
			<tbody>
			<?php 
				if($listing){
					foreach ($listing as $key => $item) {
						$categories = $this->panamerico_model->count('ads_categories', 'ads_cat_parent', $item->ads_cat_id);

						echo '
						<tr>
							<td align="center">'.$item->variacoes_id.'</td>
							<td align="center">'.$item->variacoes_nome.'</td>
							<td align="center">'.status($item->variacoes_status).'</td>
							<td align="center">
								<div class="btn-group" role="group">
									<a href="'.base_url('variacoes/edit/'.$item->variacoes_id).'" title="Editar" class="btn btn-primary"><i class="fa fa-fw fa-edit"></i></a>
									<button type="button" data-toggle="modal" data-modal="'.base_url('variacoes/delete/modal/'.$item->variacoes_id).'" data-target="#modal" class="btn btn-danger"><i class="fa fa-fw fa-close"></i></button>
								</div>
							</td>
						</tr>
						';
					}
				}else{
					echo no_results();
				}
			?>
			</tbody>
		</table>
            <?php  paginacao()->exibirPaginacao(paginacao()->getPagina(), paginacao()->getTotalPagina($total), 'categories', $total, FALSE); ?>
	</div>
</div>