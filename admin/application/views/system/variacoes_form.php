<div class="row">
	<div class="col-md-12">
		<h1>Adicionar Variações</h1>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<form action="<?=base_url('variacoes/save')?>" enctype="multipart/form-data" method="POST" accept-charset="utf-8">
			<input type="hidden" name="e" value="<?=(($e) ? $item->variacoes_id : '')?>">

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Nome</label>
						<input type="text" class="form-control" name="name" required value="<?=(($e) ? $item->variacoes_nome : '')?>">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">

						<label>Cor</label>
						<input type="color" id="hexadecimal" name="hexadecimal" value="<?=(($e) ? $item->variacoes_hexadecimal : '')?>">
					</div>
				</div>
			</div>

			<div class="row">
				<!-- <div class="col-md-6">
					<div class="form-group">
						<label>Ícone</label>
						<input type="text" required class="form-control" name="icon" value="<?=(($e) ? $item->ads_cat_icon : '')?>">
						<small><a href="http://fontawesome.io/icons/" target="_blank">http://fontawesome.io/icons/</a></small>
					</div>
				</div> -->
				<div class="col-md-6">
					<div class="form-group">
						<label>Status</label>
						<select class="form-control" name="status">
							<option <?=(($e && $item->variacoes_status == '1') ? 'selected' : '')?> value="1">Ativado</option>
							<option <?=(($e && $item->variacoes_status == '0') ? 'selected' : '')?> value="0">Desativado</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<a href="<?=base_url('variacoes')?>" class="btn btn-default btn-lg">Cancelar</a>
					<button type="submit" class="btn btn-primary btn-lg">Salvar</button>
				</div>
			</div>
		</form>
	</div>
</div>