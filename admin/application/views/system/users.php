<?php

if (!function_exists('selected')) {

    function selected($bd, $value) {
        if ($bd == $value) {
            return 'selected="selected"';
        }
    }

}
?>
<div class="row">
	<div class="col-md-12">
		<h1>Usuários</h1>
	</div>
</div>

<style>.nav-tabs>li>a{cursor: pointer;}</style>

<div class="row">
    <ul class="nav nav-tabs">
        <li <?php echo (!isset($_GET['status'])) ? 'class="active"' : ''; ?>><a data-toggle="tab" onclick="mudaPagina('users<?=(($this->uri->segment(2) == 'vendedores') ? '/'.$this->uri->segment(2) : '' )?>');"><i class="fa fa-tags"></i> Todos</a></li>
        <li <?php echo ($_GET['status'] == '1') ? 'class="active"' : ''; ?>><a data-toggle="tab" onclick="mudaPagina('users<?=(($this->uri->segment(2) == 'vendedores') ? '/'.$this->uri->segment(2) : '' )?>/?status=1');"><i class="fa fa-check"></i> Ativos</a></li>
        <li <?php echo ($_GET['status'] == '2') ? 'class="active"' : ''; ?>><a data-toggle="tab" onclick="mudaPagina('users<?=(($this->uri->segment(2) == 'vendedores') ? '/'.$this->uri->segment(2) : '' )?>/?status=2');"><i class="fa fa-hourglass-half"></i> Aguardando aprovação</a></li>
        <li <?php echo ($_GET['status'] == '0') ? 'class="active"' : ''; ?>><a data-toggle="tab" onclick="mudaPagina('users<?=(($this->uri->segment(2) == 'vendedores') ? '/'.$this->uri->segment(2) : '' )?>/?status=0');"><i class="fa fa-pause"></i> Desativados</a></li>
        <!-- <li <?php echo ($_GET['status'] == '5') ? 'class="active"' : ''; ?>><a data-toggle="tab" onclick="mudaPagina('users/?status=5');"><i class="fa fa-close"></i> Excluídos</a></li> -->
    </ul>
    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <div class="row" style="margin-top: 20px;">
                <div class="col-md-12">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
                        <!-- <div class="dataTables_length" id="DataTables_Table_0_length" style="width: 30%;"><label>Mostrar <select style="width: 20%;" onchange="mudaPagina('users/?offset=' + this.value + '<?php echo ($_GET['status'] > 0) ? '&status=' . $_GET['status'] : null; ?>' + '<?php echo ($_GET['search'] > 0) ? '&search=' . $_GET['search'] : null; ?>');" name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-control"><option value="50" <?php echo selected($_GET['offset'], 50); ?>>50</option><option value="100" <?php echo selected($_GET['offset'], 100); ?>>100</option><option value="200" <?php echo selected($_GET['offset'], 200); ?>>200</option><option value="300" <?php echo selected($_GET['offset'], 300); ?>>300</option></select> registros</label></div> -->
                        <!-- <div id="DataTables_Table_0_filter" class="dataTables_filter" style="width: 25%;"><label>Pesquisar:<input type="search" name="search" value="<?php echo $_GET['search']; ?>" onkeypress="if (event.keyCode == 13 || event.which == 13) {
                                    mudaPagina('ads/?search=' + this.value + '<?php echo ($_GET['status'] > 0) ? '&status=' . $_GET['status'] : null; ?>' + '<?php echo ($_GET['offset'] > 0) ? '&offset=' . $_GET['offset'] : null; ?>');
                                }" class="form-control" placeholder="Digite alguma coisa e aperte a tecla Enter" aria-controls="DataTables_Table_0"></label></div> -->
                        <div style="float:left">
                            <a href="javascript:void(0);" onclick="javascript:marcaTodosAds();" data-toggle="tooltip" title="Marcar Todos" data-placement="left" class="btn btn-default"><i class="fa fa-check-circle"></i> Marcar Todos</a>

                            <button type="button" data-toggle="modal" data-modal="<?php echo base_url(); ?>users/modalStatus/1/Aprovar/2" data-target="#modal" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Aprovar</button>

                            <button type="button" data-toggle="modal" data-modal="<?php echo base_url(); ?>users/modalStatus/0/Suspender/<?=($_GET['status'] > 0) ? $_GET['status'] : 0; ?>" data-target="#modal" class="btn btn-warning"><i class="fa fa-thumbs-down"></i> Suspender</button>

                            <button type="button" data-toggle="modal" data-modal="<?php echo base_url(); ?>users/modalStatus/5/Excluir/<?php echo ($_GET['status'] > 0) ? $_GET['status'] : 0; ?>" data-target="#modal" class="btn btn-danger"><i class="fa fa-close"></i> Excluir Permanente</button>

                            <a href="<?=base_url('/users/exportCSV/'.(($this->uri->segment(2) == 'vendedores') ? 'lojista' : 'cliente'))?>" id="exportar" class="btn bg-purple"><i class="fa fa-download"></i> Exportar lista</a>

                           
                        </div>
                    </div> <br> <br>
             <?php  paginacao()->filtro('users', FALSE); ?>
		<table class="table table-striped table-bordered" >
			<thead>
				<tr>
					<th class="center"><input type="checkbox" id="todos" disabled="disabled"/></th>
					<th align="center">ID</th>
					<th>Nome</th>
					<th>E-mail</th>
					<th align="center">Anúncios</th>
					<th align="center">Loja</th>
					<th align="center">Data de Registro</th>
					<th align="center">Último Acesso</th>
					<!-- <th align="center">Último Acesso</th> -->
					<th align="center">Status</th>
					<th align="center">Ações</th>
				</tr>
			</thead>
			<tbody>
			<?php 
		
				if($listing){
					foreach ($listing as $key => $item) { 
						$ads = $this->panamerico_model->countAdsByUser($item->use_id);
						$shop = $this->panamerico_model->details("shops", 'use_id', $item->use_id);

						if(!empty($item->use_email)):
							//$emailCorte    = substr($data->use_email, 0, 20);
							if(strlen($item->use_email) <= 34){
								$emailLimitado = substr($item->use_email, 0, 34);		
							}else{
								$emailLimitado = substr($item->use_email, 0, 28)."...";
							}	
						
						else:
							$emailLimitado = "";
						endif;


						echo '
						<tr>
							<td class="center"><input name="item_id[]" type="checkbox" class="idads" value="' . $item->use_id . '"  /></td>
							<td align="center">'.$item->use_id.'</td>
							<td>'.$item->use_name.'</td>
							<td>'.$emailLimitado.'&nbsp;<i data-link="'.$item->use_email.'" class="fa fa-fw fa-clipboard copy-link" style="cursor:pointer;"></i></td>
							<td align="center">'.$ads.'</td>
							<td align="center">'.(($shop) ? 'Sim' : 'Não').'</td>
							<td align="center">'.string_date_time($item->use_date_register).'</td>
							<td align="center">'.string_date_time($item->use_date_login).'</td>
							<!--<td align="center">'.$item->use_last_ip.'</td>-->
							<td align="center">'.status($item->use_status).' '.status($item->use_type).'</td>
							<td align="center" width="150">
								<div class="btn-group" role="group">
									'.(($item->use_status == 0 || $item->use_status == 2) ? '<button type="button" data-toggle="modal" data-modal="'.base_url('users/unsuspend_user/modal/'.$item->use_id.'/?status='.$_GET['status']).'" data-target="#modal" class="btn btn-success"><i class="fa fa-fw fa-thumbs-up"></i></button>' 
										:'<button type="button" data-toggle="modal" data-modal="'.base_url('users/suspend_user/modal/'.$item->use_id.'/?status='.$_GET['status']).'" data-target="#modal" class="btn btn-danger"><i class="fa fa-fw fa-thumbs-down"></i></button>').'

									<a href="'.base_url('users/details/'.$item->use_id).'" title="Visualizar" class="btn btn-warning"><i class="fa fa-fw fa-eye"></i></a>
									<a href="'.base_url('users/edit/'.$item->use_id).'" title="Editar" class="btn btn-primary"><i class="fa fa-fw fa-edit"></i></a>
									<a href="#" data-toggle="modal" data-modal="'.base_url('users/delete/modal/'.$item->use_id).'" data-target="#modal" title="Apagar" class="btn btn-danger"><i class="fa fa-fw fa-close"></i></a>
								</div>
							</td>
						</tr>
						';
					}
				}else{
					echo no_results();
				}
			?>
			</tbody>
		</table>
            <?php  paginacao()->exibirPaginacao(paginacao()->getPagina(), paginacao()->getTotalPagina($total), 'users', $total, FALSE); ?>
	</div>
</div>


<script type="text/javascript">


    function marcaTodosAds() {
        $('.idads').each(function() {
            if ($(this).prop("checked")) {
                $(this).prop("checked", false);
            } else {
                $(this).prop("checked", true);
            }
        });
    }
    $(document).ready(function() {
        $("#a-aprovar-anuncios").on('click', function(event) {
            $("#DataTables_Table_0_filter input[type='search']").val('Aprovação Pendente');
            $("#DataTables_Table_0_filter input[type='search']").keyup();
        });
    }
    );

    function enviaForm(status_id, status_atual) {
        if (!$('input[type=checkbox]').is(':checked')) {
            alert('Selecione pelo menos um usuário.');
        } else {
            verificaChecado(status_id, status_atual);
        }
    }

    function pegaIds() {
        var ids = '';
        // percorrer todos os elementos checados...
        $(".idads:checked").each(function() {
            //Verifica se checkbox está marcado
            if (this.checked) {
                //pegar os id do checkbox selecionado
                ids = ids + ', ' + this.value;
            }
        });
        return ids = ids.substr(1); //eleminar a primeira virgula (',')
    }

    function verificaChecado(status_id, status_atual)
    {
        var ids = pegaIds();
        var acao = 'ok';
        if (status_id == 5) {
            acao = 'excluir';
            status_id = 0;
        }

        $.post('<?=base_url('users/status/'); ?>', {use_id: ids, status: status_id, status_atual: status_atual, acao: acao},
        function(dados)
        {
            if (dados != null && status_id > 0)
            {

                mudaPagina('users/?status=' + status_id);

            } else if (status_id == 0) {
                mudaPagina('users');
            }
        }, "json");


    }

    function acaoEmMassa(status_id, status_atual)
    {

        var acao = 'massa';
        if (status_id == 0 && status_atual == 5) {
            acao = 'excluirmassa';
        }

        if (confirm('Deseja executar ação em massa?')) {
            $.post('<?=base_url('users/status/')?>', {status_id: status_id, status_atual: status_atual, acao: acao},
            function(dados)
            {
                if (dados != null && status_id > 0)
                {

                    mudaPagina('users/?status=' + status_id);

                } else if (status_id == 0) {
                    mudaPagina('users');
                }
            }, "json");
        }
    }

</script>