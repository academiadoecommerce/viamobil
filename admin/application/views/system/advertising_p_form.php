<div class="row">
	<div class="col-md-12">
		<h1>Editar Bloco de Publicidade</h1>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<form action="<?=base_url('advertising/save')?>" enctype="multipart/form-data" method="POST" accept-charset="utf-8">
			<input type="hidden" name="e" value="<?=(($e) ? $item->adv_id : '')?>">

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Nome</label>
						<input type="text" class="form-control" name="name" required value="<?=(($e) ? $item->adv_name : '')?>">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Link</label>
						<input type="text" class="form-control" name="url" required value="<?=(($e) ? $item->adv_url : '')?>" placeholder="http://exemplo.com">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Posição</label>
						<input type="text" class="form-control" disabled readonly name="position" value="<?=$item->adv_position?>">
							<!-- <option <?=(($e && $item->adv_position == 'top') ? 'selected' : '')?> value="top">Topo</option> -->

					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Imagem </label> <br>
						<?php if($e){ ?>
                        	<img  class="img-thumbnail" src="<?=base_url('../uploads/publicidade/'.$item->adv_img);?>"> <br> <br>
                        <? } ?>

                        <label class="btn btn-default" for="image-input" id="image-label">
                            <i class="fa fa-image"></i> Selecionar uma Imagem
                        </label> 
                        <i style="font-size: 27px;position: relative;top: 5px;left: 10px;color: green; display: none;" class="fa fa-check-circle img-check"></i> 
                        <br>

					   <input type="file" name="img" style="display: none;" onchange="$('.img-check').fadeIn()" id="image-input" accept="image/*" capture="camera"> <br>
					   <input type="text" name="img" hidden value="<?=$item->adv_img?>">

                        <div class="alert-warning" style="padding: 10px; width: 400px; border-radius: 5px">
                            <ul>
                                <li><strong>Tamanho máximo:</strong> 2MB</li>
                                <li><strong>Altura e largura recomendada (px): </strong><?=$item->adv_size?></li>
                                <li><strong>Tipos arquivo permitido:</strong> JPG e PNG</li>
                            </ul>
                        </div>
                    </div>

				</div>
			</div>
			<!-- <div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Conteúdo</label>
						<small>(Permite HTML)</small>
						<textarea class="form-control" rows="10" name="content"><?=(($e) ? $item->adv_content : '')?></textarea>
					</div>
				</div>
			</div> -->
			<div class="row">
				<div class="col-md-12">
					<a href="<?=base_url('advertising')?>" class="btn btn-default btn-lg">Cancelar</a>
					<button type="submit" class="btn btn-primary btn-lg">Salvar</button>
				</div>
			</div>
		</form>
	</div>
</div>