<div class="row">
    <div class="col-md-8">
        <h1>Dashboard</h1>
    </div>
</div>

<div class="row">
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-aqua">
	    <div class="inner">
	      <h3><?=$total_lojas?></h3>

	      <p>Total de lojas<br>cadastradas</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-bag"></i>
	    </div>
	    <a href="<?=base_url('shops');?>" class="small-box-footer">Ver Lojas <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-green">
	    <div class="inner">
	      <h3><?=$total_ads?></h3>  
	      <!-- <h3>53<sup style="font-size: 20px">%</sup></h3>  -->

	      <p>Total de anúncios<br>cadastrados</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-stats-bars"></i>
	    </div>
	    <a href="<?=base_url('ads');?>" class="small-box-footer">Ver Anuncios <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-yellow">
	    <div class="inner">
	      <h3><?=$user_pendente?></h3>

	      <p>Usuários aguardando<br>aprovação</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-person-add"></i>
	    </div>
	    <a href="<?=base_url('users/vendedores/?status=2');?>" class="small-box-footer">Ver usuários <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>

	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-red">
	    <div class="inner">
	      <h3><?=$ad_pendente?></h3>

	      <p>Anúncios aguardando<br>aprovação</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-ios-pricetags"></i>
	    </div>
	    <a href="<?=base_url('ads/?status=1');?>" class="small-box-footer">Ver anúncios <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>

	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-purple">
	    <div class="inner">
	      <p>Acessar o <br>Wirecard</p>
	    </div>
	    <div class="icon">
	      <i class="ion ion-at"></i>
	    </div>
	    <a href="https://connect.wirecard.com.br" target="_blank" class="small-box-footer">Ir para wirecard <i class="fa fa-arrow-circle-right"></i></a>
	  </div>
	</div>
	<!-- ./col -->
</div>