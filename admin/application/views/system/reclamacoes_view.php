

<div class="row">
	<div class="col-md-12">
		<h1>Reclamações</h1>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
           <?php  paginacao()->filtro('reclamacoes', FALSE); ?>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Cliente</th>
					<th>Vendedor</th>
					<th>Pedido</th>
					<th>Data da reclamação</th>
					<th>Status</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php if($listing){ ?>

					<?php foreach($reclamacoes as $recl): ?>
						<tr>
							<td><?php echo $recl['nome_cliente']?></td>
							<td><?php echo $recl['nome_vendedor']?></td>
							<td><?php echo $recl['reclama_pedido']?></td>
							<td><?php echo date("d-m-Y", strtotime($recl["reclama_data"]))?></td>
							
							<?php if($recl['reclama_encerrado'] == "2"){?>
								<td>Encerrado</td>
							<?php }else{?>
								<td>Em aberto</td>
							<?php }?>
				
							<td> 
								<a href="https://conta-sandbox.wirecard.com.br/orders/<?php echo $recl['reclama_pedido']?>"> 
									<i class="fa fa-hand-paper-o"></i> 
								 </a> 
							</td>

							<td> 
								<a href="<?= base_url("Reclamacao/chat/{$recl['reclama_pedido']}")?>"> 
									<i class="fa fa-info"></i> 
								 </a> 
							</td>
						</tr>
					<?php endforeach?>

				<?php }else {?>

					<?php echo no_results(); ?>

				<?php }?>

			</tbody>
		</table>
            <?php  paginacao()->exibirPaginacao(paginacao()->getPagina(), paginacao()->getTotalPagina($total), 'reclamacoes', $total, FALSE); ?>
	</div>
</div>
