<div class="row">
	<div class="col-md-12">
		<h1><?=(($e) ? 'Editar Banner' : 'Adicionar Banner');?></h1>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<form action="<?=base_url('banner/save')?>" enctype="multipart/form-data" method="POST" accept-charset="utf-8">
			<input type="hidden" name="e" value="<?=(($e) ? $item->banner_id : '')?>">

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Nome</label>
						<input type="text" class="form-control" name="name" value="<?=(($e) ? $item->banner_name : '')?>">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Link</label>
						<input type="text" class="form-control" name="slug" value="<?=(($e) ? $item->banner_slug : '')?>">
					</div>
				</div>
			</div>
			<div class="row">
				<!-- <div class="col-md-6">
					<div class="form-group">
						<label>Ícone</label>
						<input type="text" class="form-control" name="icon" value="<?=(($e) ? $item->banner_icon : '')?>">
						<small><a href="http://fontawesome.io/icons/" target="_blank">http://fontawesome.io/icons/</a></small>
					</div>
				</div> -->
				<div class="col-md-6">
					<div class="form-group">
						<label>Status</label>
						<select class="form-control" name="status">
							<option <?=(($e && $item->banner_status == '1') ? 'selected' : '')?> value="1">Ativado</option>
							<option <?=(($e && $item->banner_status == '0') ? 'selected' : '')?> value="0">Desativado</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Imagem </label> <br>
						<?php if($e){
	                            // $image = thumbnail(@$item->banner_img, "shops", 190, 190, 2);
	                            ?>
	                            	<img style="width: 600px;" class="img-thumbnail" src="<?=APP_URL.'assets/img/banners/'.$item->banner_img;?>"> <br> <br>
	                            <?

	                            // echo '<img id="pre-img" src="http://www.donaiara.com.br/assets/img/no-image.png"> <br>';
	                        }
	                            ?>

						<!-- <textarea class="form-control" id="textarea-editor" rows="10" name="content"><?=(($e) ? $item->banner_content : '')?></textarea> -->
	                        <label class="btn btn-default" for="image-input" id="image-label">
	                            <i class="fa fa-image"></i> Selecionar uma Imagem
	                        </label> 
	                        <i style="font-size: 27px;position: relative;top: 5px;left: 10px;color: green; display: none;" class="fa fa-check-circle img-check"></i> 
	                        <br>

						   <input type="file" name="image" <?=((!$e) ? 'required' : '')?> style="display: none;" onchange="$('.img-check').fadeIn()" id="image-input" accept="image/*" capture="camera"> <br>
						   <input type="text" name="image" hidden value="<?=(($e) ? $item->banner_img : '')?>">

	                        <div class="alert-warning" style="padding: 10px; width: 400px; border-radius: 5px">
	                            <ul>
	                                <li><strong>Tamanho máximo:</strong> 2MB</li>
	                                <li><strong>Altura e largura recomendada (px):</strong> 800 x 450</li>
	                                <li><strong>Tipos arquivo permitido:</strong> JPG e PNG</li>
	                            </ul>
	                        </div>
	                    </div>

					</div>
				</div>
			</div>

                     

<!-- 			<div class="row">
				<div class="col-md-12">
					<div class="faq-box">
						<button type="button" id="fb-add" class="btn btn-success">Adicionar Pergunta e Resposta</button>

						<div class="fb-listing">
							<?php
								if($e && $faq){
									foreach ($faq as $key => $f) {
										echo '<div class="item"><div class="h"><button type="button" class="btn btn-danger btn-remove">Remover</button></div><div class="c"><input type="text" class="form-control" required name="faq_q[]" placeholder="Pergunta" value="'.$f->banner_faq_question.'"><textarea required class="form-control" rows="5" name="faq_a[]" placeholder="Resposta">'.$f->banner_faq_answer.'</textarea></div></div>';
									}
								}
							?>
						</div>
					</div>
				</div>
			</div> --></div>
			<div class="row">
				<div class="col-md-12">
					<a href="<?=base_url('banners')?>" class="btn btn-default btn-lg">Cancelar</a>
					<button type="submit" class="btn btn-primary btn-lg">Salvar</button>
				</div>
			</div>
		</form>
	</div>
</div>
