<section class="section-content padding-y-sm bg card-home-destaque">
    <div class="container">
        <header class="section-heading heading-line">
            <h4 class="title-section bg"> ANÚNCIOS POPULARES </h4>
        </header> 
        <?php   
            // Seleciona os 6 últimos anúncios ativados. 

            $this->db->select('ads.ad_id,ads.ad_slug,ads.ad_name,ads.ad_price,ads_images.ads_img_file');
            $this->db->from('ads');
            $this->db->join('ads_images','ads.ad_id=ads_images.ad_id');
            $this->db->where("ads.ad_status", 2);   
            $this->db->order_by('ads.ad_id', 'DESC');           
            $this->db->group_by('ads.ad_id', 'DESC');           
            $this->db->limit(6);
            $query = $this->db->get();
        ?>      
        <div class="card rounded-0">
            <div class="row no-gutters">
                <div class="col-md-2" id="ads_popul">
                    <article href="#" class="card-banner h-100 bg2 rounded-0">
                        <div class="card-body zoom-wrap">   
                            <img src="<?= base_url();?>assets/img/items/lojaspopulares.jpg" class="img-bg zoom-in">
                            <!-- <h5>Lorem Ipsum é simplesmente uma simulação</h5> -->
                            <a href="#" class="btn btn-vermais shadow-sm w-75" style="color: #65be68 !important;">Ver Mais</a>                  
                            <!-- <img src="<?= base_url();?>assets/img/items/anuncios_populares.jpg" class="img-bg zoom-in"> -->
                        </div>
                    </article>
                </div> <!-- col.// -->
            
                <div class="col-md-10 px-mobile">
                    <ul class="row no-gutters">
                        <?php

                            foreach ($query->result() as $row){ 
                                echo '<li class="col-6 col-md-2">
                                    <a href="'.base_url().'anuncio/'.$row->ad_slug.'" class="itembox"> 
                                        <div class="card-body" style="text-align: center;">
                                           <img class="img-sm" src="'.base_url().'admin/uploads/ads/'.$row->ads_img_file.'">
                                            <p class="word-limit" style="padding-top: 15px;">'.$row->ad_name.'<span style="font-weight: bold;"><br/>R$ '.number_format($row->ad_price, 2, ",", ".").'</span> </p>
                                            
                                        </div>
                                    </a>
                                </li>';
                            }   
                        ?>                  
                    </ul>
                </div> <!-- col.// -->
            </div> <!-- row.// -->          
        </div> <!-- card.// -->
    </div> <!-- container .//  -->
</section>