<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include "head.php"; ?>
	</head>
	<body>
		<div class="app-login" style="">
				<div class="row">

					<div class="ap-box">
					<div class="al-logo">
						<img src="<?=base_url('assets/img/logo.png')?>" alt="Panamérico" width="200">
					</div>
					<?php
						echo $contents;
					?>
					</div>
				</div>
		</div>
	</body>
</html>