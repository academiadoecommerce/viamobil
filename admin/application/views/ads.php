
<style>
    li {list-style-type:none;}
    .book-tab-dec {background-color: #ffffff; position: relative; float: right; border: 1px solid #E6E6E6; border-radius: 5px;}
    @media (max-width: 640px) {
       #grupo-cat {display: none;}
    }
    .ad-price{ color: green; font-weight: bold; }   
</style>

<div class="container ">
    
    
        <?php
        include_once('returns.php');

        if (@$breadcrumbs) {
            echo '
            <div class="row hide-for-small-only d-block">
                <div class="medium-12 columns my-3">
                    <div class="breadcrumbs">
                       <ul>
                          <li><a href="' . base_url() . '">Início</a></li>';

                         foreach ($breadcrumbs as $key => $bc) {
                           echo '<li>' . ((@$bc['link']) ? '<a href="' . $bc['link'] . '" target="_self">' . $bc['name'] . '</a>' : $bc['name']) . '</li>';
                        }
                  echo '
                       </ul>
                    </div>
                </div>
            </div>'; }
                ?>
    
</div>

<div class="container d-flex bgwhite rounded border p-0 pt-2">
    <div class="row m-0 d-flex w-100">
        <div class="col-md-2 p-0">
            
                <div class="card-body p-0 shadow-none" id="grupo-cat">
                        <aside style="width: 100%;">
                            <h5 class="text-uppercase ml-3 my-2 h6">
                                <!-- <i class="fa fa-bars"></i> -->
                                Categorias
                            </h5> 
                            
                              <?php
                              $i = 0;
                              $r = 8;
                              $limite = $count_categories - $r; // Total das categorias PAI - 8
                              
                                 //  Exibe es categorias PAI e Filho na pagina principal
                                 if($categories){
                                echo '<ul class="menu-category second-menu" style="width: 100%;">'; 
                                            
                                    foreach ($categories as $key => $cat) { 
                                        echo '
                                            <li> <a class="p-0 py-1 pl-4" href="'.base_url('anuncios/?categoria='.$cat->ads_cat_id).'">'.$cat->ads_cat_name.' </a></li>';               
                                        $i++;
                                        if($i == $r) break;
                                    } ?>

                                        <li> 
                                            <a href="javascript:void();" data-toggle="modal" data-target=".modal-ver-mais-categ">
                                                Ver mais
                                            </a>
                                        </li>
                                    </ul> 
                                    <br>

                                <? } ?>
                        </aside>

                        <aside class="as-box mb-5">
                            <div class="text-uppercase ml-3 my-2 h6"><!-- <i class="fa fa-filter" aria-hidden="true"></i> --> Filtrar pelo Preço</div>
                            <!-- <h5 class="text-uppercase ml-3 my-2"><i class="fa fa-filter" aria-hidden="true"></i>Filtrar pelo Preço</h5> -->
                            <div class="as-b-content">
                                <ul class="as-b-options menu-category filtro-preco-menu second-menu">
                                    <li class="p-0 py-1 pl-4" data-value="0"  onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?preco='; ?>&categoria=<?php echo $_GET['categoria']; ?>&estado=<?php echo $_GET['estado']; ?>&tipo=<?php echo $_GET['tipo']; ?>&search=<?php echo $_GET['search']; ?>&condicao=<?php echo $_GET['condicao']; ?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');" <?php echo ($_GET['preco'] == '') ? 'class="active"' : ''; ?>>Todos os preços</li>
                                    <li class="p-0 py-1 pl-4" data-value="1_50" onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?preco=1_50'; ?>&categoria=<?php echo $_GET['categoria']; ?>&estado=<?php echo $_GET['estado']; ?>&condicao=<?php echo $_GET['condicao']; ?>&tipo=<?php echo $_GET['tipo']; ?>&search=<?php echo $_GET['search']; ?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');" <?php echo ($_GET['preco'] == '1_50') ? 'class="active"' : ''; ?>>R$ 1 - R$ 50</li>
                                    <li class="p-0 py-1 pl-4" data-value="51_150" onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?preco=51_150'; ?>&categoria=<?php echo $_GET['categoria']; ?>&estado=<?php echo $_GET['estado']; ?>&tipo=<?php echo $_GET['tipo']; ?>&search=<?php echo $_GET['search']; ?>&condicao=<?php echo $_GET['condicao']; ?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');" <?php echo ($_GET['preco'] == '51_150') ? 'class="active"' : ''; ?>>R$ 51 - R$ 150</li>
                                    <li class="p-0 py-1 pl-4" data-value="151_300" onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?preco=151_300'; ?>&categoria=<?php echo $_GET['categoria']; ?>&estado=<?php echo $_GET['estado']; ?>&tipo=<?php echo $_GET['tipo']; ?>&search=<?php echo $_GET['search']; ?>&condicao=<?php echo $_GET['condicao']; ?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');" <?php echo ($_GET['preco'] == '151_300') ? 'class="active"' : ''; ?>>R$ 151 - R$ 300</li>
                                    <li class="p-0 py-1 pl-4" data-value="301_800" onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?preco=301_800'; ?>&categoria=<?php echo $_GET['categoria']; ?>&estado=<?php echo $_GET['estado']; ?>&tipo=<?php echo $_GET['tipo']; ?>&search=<?php echo $_GET['search']; ?>&condicao=<?php echo $_GET['condicao']; ?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');" <?php echo ($_GET['preco'] == '301_800') ? 'class="active"' : ''; ?>>R$ 301 - R$ 800</li>
                                    <li class="p-0 py-1 pl-4" data-value="801_10000000000" onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?preco=801_10000000000'; ?>&categoria=<?php echo $_GET['categoria']; ?>&estado=<?php echo $_GET['estado']; ?>&tipo=<?php echo $_GET['tipo']; ?>&search=<?php echo $_GET['search']; ?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>&condicao=<?php echo $_GET['condicao']; ?>');" <?php echo ($_GET['preco'] == '801_10000000000') ? 'class="active"' : ''; ?>>R$ 801 - R$ ~</li>
                                </ul>
                            </div>
                        </aside>

                        <aside class="as-box mb-5">
                            <div class="text-uppercase ml-3 my-2 h6"><!-- <i class="fa fa-filter" aria-hidden="true"></i> --> Condição</div>
                            <!-- <h5 class="text-uppercase ml-3 my-2"><i class="fa fa-filter" aria-hidden="true"></i>Filtrar pelo Preço</h5> -->
                            <div class="as-b-content">
                                <ul class="as-b-options menu-category filtro-preco-menu second-menu">
                                    <li class="p-0 py-1 pl-4" data-value="novo"  onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?condicao='; ?>&categoria=<?php echo $_GET['categoria']; ?>&estado=<?php echo $_GET['estado']; ?>&tipo=<?php echo $_GET['tipo']; ?>&search=<?php echo $_GET['search']; ?>&preco=<?=$_GET['preco']?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');" <?php echo ($_GET['preco'] == '') ? 'class="active"' : ''; ?>>Todos</li>
                                    <li class="p-0 py-1 pl-4" data-value="novo"  onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?condicao=novo'; ?>&categoria=<?php echo $_GET['categoria']; ?>&estado=<?php echo $_GET['estado']; ?>&tipo=<?php echo $_GET['tipo']; ?>&search=<?php echo $_GET['search']; ?>&preco=<?=$_GET['preco']?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');" <?php echo ($_GET['preco'] == '') ? 'class="active"' : ''; ?>>Novo</li>
                                    <li class="p-0 py-1 pl-4" data-value="usado" onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?condicao=usado'; ?>&categoria=<?php echo $_GET['categoria']; ?>&estado=<?php echo $_GET['estado']; ?>&tipo=<?php echo $_GET['tipo']; ?>&search=<?php echo $_GET['search']; ?>&preco=<?php echo $_GET['preco']; ?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');" <?php echo ($_GET['condicao'] == 'usado') ? 'class="active"' : ''; ?>>Usado</li>
                                    </ul>
                            </div>
                        </aside> 

                        <aside style="width: 100%;">
                            <!-- <h6 class="title-bg bg-secondary">Publicidade</h6> -->
                            <div style="height:200px;">
                                <figure class="itemside has-bg border-0" style="height: 100%;">
                                    <a target="_blank" href="<?=$img_publicidade->adv_url ?>">
                                        <img class="img-bg" src="<?=base_url('uploads/publicidade/'.$img_publicidade->adv_img)?>" height="100" style="max-height: 170px;">
                                    </a>
                                    <!-- <figcaption class="p-2">
                                        <h6 class="title">Espaço para publicidade </h6>
                                    </figcaption> -->
                                </figure>
                            </div>
                        </aside>


                </div> 
            <!--
            <button type="button" class="show-for-small-only" id="show-filtros"><i class="fa fa-filter" aria-hidden="true"></i> Mostrar filtros</button>        
            <div class="ads-sidebar hide-for-small-only" id="mobile-filtros" style="border: 1px solid red;">

                <div class="as-search" style="border: 1px solid blue;">
                    <div class="input-group">
                        <input class="input-group-field" type="text"  onkeyup="if (event.keyCode == 13 || event.which == 13) {
                                    filtroAnuncio('<?php echo base_url(); ?>anuncios/?search=' + this.value);
                                }" id="as-s-input" placeholder="Procure pelo nome..." value="<?php echo (strlen($_GET['search']) > 0) ? $_GET['search'] : ''; ?>">

                        <div class="input-group-button"  style="border: 1px solid green;">
                            <button type="button" onclick="filtroAnuncio('<?php echo base_url(); ?>anuncios/?search=' + document.getElementById('as-s-input').value);" class="btn btn-primary btn-just-icon" id="as-s-button"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                
                <div id="fixo-categoria">
                <a id="top_ads"></a>
                
                <div class="as-box">
                    <div class="as-b-title"><i class="fa fa-folder" aria-hidden="true"></i> Buscar pela Categoria</div>
                    <div class="as-b-content">
                        <ul class="as-b-categories" id="as-b-categories">
                            <?php
                            if ($categories) {
                                foreach ($categories as $key => $cat) {
                                    ?>
                                    <li>
                                        <a href="javascript:void(0);" onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?categoria=' . $cat->ads_cat_id; ?>&estado=<?php echo $_GET['estado']; ?>&preco=<?php echo $_GET['preco']; ?>&tipo=<?php echo $_GET['tipo']; ?>&search=<?php echo $_GET['search']; ?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');"  <?php echo (((int) $_GET['categoria'] == $cat->ads_cat_id) ? 'class="active"' : ''); ?> >
                                            <span><i class="fa <?php echo $cat->ads_cat_icon; ?>"></i></span>
                                            <span><?php echo $cat->ads_cat_name; ?></span>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                </div> -->
     

                <!--
                <div class="as-box">
                    <div class="as-b-title"><i class="fa fa-filter" aria-hidden="true"></i> Filtrar pelo Tipo</div>
                    <div class="as-b-content">
                        <ul class="as-b-options" id="as-b-type">
                            <li data-value="all" onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?tipo=all'; ?>&estado=<?php echo $_GET['estado']; ?>&preco=<?php echo $_GET['preco']; ?>&categoria=<?php echo $_GET['categoria']; ?>&search=<?php echo $_GET['search']; ?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');" <?php echo ($_GET['tipo'] == '') ? 'class="active"' : ''; ?>>Todos</li>
                            <li data-value="service" onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?tipo=service'; ?>&estado=<?php echo $_GET['estado']; ?>&preco=<?php echo $_GET['preco']; ?>&categoria=<?php echo $_GET['categoria']; ?>&search=<?php echo $_GET['search']; ?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');" <?php echo ($_GET['tipo'] == 'service') ? 'class="active"' : ''; ?>>Apenas Serviços</li>
                            <li data-value="trade" onclick="filtroAnuncio('<?php echo base_url() . 'anuncios/?tipo=trade'; ?>&estado=<?php echo $_GET['estado']; ?>&preco=<?php echo $_GET['preco']; ?>&categoria=<?php echo $_GET['categoria']; ?>&search=<?php echo $_GET['search']; ?>&estado_nome=<?php echo $_GET['estado_nome']; ?>&regiao=<?php echo $_GET['regiao']; ?>&cidade=<?php echo $_GET['cidade']; ?>');" <?php echo ($_GET['tipo'] == 'trade') ? 'class="active"' : ''; ?>>Aceita Troca</li>
                        </ul>
                    </div>
                </div>
            </div> -->
        
            
        </div>

        <!-- ========================= SECTION MAIN END// ========================= -->
        
            <div class="col-md-10">
                <div class="row">
                    <div class="col">
                        <a href="javascript:void();" data-toggle="modal" data-target=".modal-ver-mais-categ" class="float-right px-4"><i class="fa fa-bars"></i> Categorias </a>
                    </div>
                </div>
                <div class="card border-0">         
                    <?php
                    if ($ads) {
                        echo '<div class="col-md-12">';
                        echo '<ul class="row no-gutters">';
                        foreach ($ads as $key => $ad) { ?>
                           <li class='col-sm-6 col-md-4 col-lg-3'>

                            <?=$this->ads_model->ads_item($ad)?>

                            <!--<div class="chat-icon-ads ap-send-message">

                                <?php if ($this->session->userdata('login')) { ?>
                                     <form method="POST" target="_blank" id="mobile-message" action="<?= base_url('chat/novo_chat') ?>">
                             
                                        <input type="hidden" name="sender_id" value="<?= $_SESSION['login']; ?>">
                                        <input type="hidden" name="recipient_id" value="<?= $ad->use_id ?>">
                                        <input type="hidden" name="ad_id" value="<?= $ad->ad_id ?>">
                                        <input type="hidden" name="use_email" value="<?= $ad->use_email; ?>">
                                        <input type="hidden" name="use_name" value="<?= $ad->use_name; ?>">
                                        <input type="hidden" name="ad_name" value="<?= $ad->ad_name; ?>">

                                        <button  <? if($_SESSION['login'] == $ad->use_id ){ echo 'disabled' ; } ?> type="submit" class=""><i class="fa fa-comments"></i> chat</button>
                                    </form>

                                <?php } else { ?>
                                    <button type="submit" data-toggle="modal" data-target="#chat-required"><i class="fa fa-comments"></i> chat</button>

                                    
                                <?}?>

                            </div>-->

                            </li>
                        <? }
                        echo '</ul>';                       
                        echo '<div class="pagination-box"></div>';
                        echo '</div>';
                    } else {
                        echo $this->main_model->advertisingBox('top', '100%', '90px');
                        echo '<div class="ads-listing" id="ads-listing">';
                        echo '<div align="center"><strong>Opss!<br>Nenhum anúncio encontrado!
                        <br>
                        Tente buscar uma palavra diferente, ou use os filtros!</strong></div>';
                        echo '</div>';
                        //  echo '</div>';
                    } 
                    echo paginacao()->exibirPaginacao(paginacao()->getPagina(), paginacao()->getTotalPagina($total), 'anuncios', $total, false);
                    ?>          
                </div>
            </div>
     </div>  
</div>


<!-- modal categorias -->

<div class="modal fade modal-ver-mais-categ" tabindex="-1" role="dialog" aria-labelledby="ModalVermaisCateg" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content p-5">
        <div class="modal-header py-0 border-0">
        <!-- <h5 class="modal-title">Modal title</h5> -->
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
            <h5 class="ml-4 h6">Escolher Categorias</h5>

            <?php

                $c = 0;
                $r = 0;


                if($categories){ ?>
                
                <div class="row">
                    <div class="col-12 col-sm-6 col-lg-4">
                        <ul class="menu-category second-menu"> 

                            <? foreach ($categories as $key => $cat) { 
                                    echo '
                                        <li> <a class="p-0 py-1 pl-4" href="'.base_url('anuncios/?categoria='.$cat->ads_cat_id).'">'.$cat->ads_cat_name.' </a></li>';               
                                    $c++;
                                    if($c == 4){ 
                                        ?>
                                        </ul>
                                        </div> 
                                        <div class="col-12 col-sm-6 col-lg-4"> 
                                        <ul class="menu-category second-menu">
                                        <? 
                                        $c=0; 
                                    }
                                }   
                            ?>
            
                        </ul>   
                    </div>  
                </div>

                <? } ?>
    
      </div>
    </div>
  </div>
</div>



<script src="<?= base_url('assets/js/custom/select2.min.js') ?>"></script>
<script>
    $(".shophover").hover(function() {
    $(this).find('h3').css("text-decoration", "underline");

    }, function() {
        $(this).find('h3').css("text-decoration", "none");
    });

</script>

<script type="text/javascript">
    if (!isMobile()) {
        $('#estado').select2();
        $('#regiao').select2();
        $('#cidade').select2();
                                }
        if (isMobile()) {
            $('.e').css('width', '100%');
        }
</script>
