
<style type="text/css">
li {list-style-type:none;}

ul#submenu {
	float:left;
	width: 600px;
	
}
.lists {
    float:left;
    width:50%;
}
@media (max-width: 640px) {
   #grupo-cat, #ads_popul, #shop_popul {display: none;}
}
</style>

<?php
$return = $this->session->flashdata('return');

	$ret = $this->main_model->returnDetails($return);

	if($ret){
		echo '
		<div id="msg-box" style="padding: 10px;">
			<div style="padding-left: 20px;" class="row alert alert-'.$ret->ret_type.'"><i class="fa fa-fw '.$ret->ret_icon.'"></i>'.$ret->ret_text.'</div>
		</div>	
		';
	}
	?>		

<!-- ========================= SECTION MAIN ========================= -->
<section class="section-main bg padding-y-sm" id="grupo-cat">
	<div class="container">
		<div class="card">
			<div class="card-body">
				<div class="row row-sm">
					<aside class="col-md-3">
						<h5 class="text-uppercase"><i class="fa fa-bars"></i> Categorias</h5>
						  <?php
						  $i = 0;
						  $r = 8;
						  $limite = $count_categories - $r; 
							 //  Exibe es categorias PAI e Filho na pagina principal
							 if($ads_categories){
							echo '<ul class="menu-category list-dots">';	

										$this->db->limit($r, 1);				
										$this->db->order_by('ads_cat_name', 'ASC');
										$this->db->where('ads_cat_parent', 0);
										$this->db->where('ads_cat_status', 1);
										$categorias = $this->db->get("ads_categories");
										$categorias = $categorias->result();	
										
								foreach ($categorias as $key => $cat) { 
									$sub_categories = $this->ads_model->categories($cat->ads_cat_id);  // Pesquisa pela sub-categoria, passando o campo 'ads_cat_id' como parametro.

									echo '<li> <a href="'.base_url('anuncios/?categoria='.$cat->ads_cat_id).'">'.$cat->ads_cat_name.' </a></li>';						
									$i++;
								}
										
								$this->db->limit($limite, $r+1);				
								$this->db->order_by('ads_cat_name', 'ASC');
								$this->db->where('ads_cat_parent', 0);
								$this->db->where('ads_cat_status', 1);
								$query = $this->db->get("ads_categories");
								$query = $query->result();		
								
								echo '<li class="has-submenu"> <a href="#">Mais categorias <i class="fas fa-angle-double-right"></i></a>						
										<ul class="submenu" id="submenu" style="bgcolor: #EFEFEF;">';
											foreach ($query as $chave => $cate) {
												echo '<div class="lists"><li id="linha"> <a href="'.base_url('anuncios/?categoria='.$cate->ads_cat_id).'">'.$cate->ads_cat_name.' </a></li></div>';
											}
								echo '</ul>							
									</li>';							
						
							  }
						   ?>
						</ul>
						
					</aside> <!-- col.// -->

					<div class="col-md-6">
						<!-- ================= main slide ================= -->
						<div class="owl-init slider-main owl-carousel" data-items="1" data-nav="true" data-dots="false">
							<div class="item-slide">
								<img src="<?= base_url()?>assets/img/banners/slide1.jpg">
							</div>
							<div class="item-slide">
								<img src="<?= base_url()?>assets/img/banners/slide2.jpg">
							</div>
							<div class="item-slide">
								<img src="<?= base_url()?>assets/img/banners/slide3.jpg">
							</div>
						</div>
						<!-- ============== main slidesow .end // ============= -->
					</div> <!-- col.// -->

					<aside class="col-md-3">
						<h6 class="title-bg bg-secondary">Publicidade</h6>
						<div style="height:280px;">
							<figure class="itemside has-bg border-bottom" style="height: 50%;">
								<img class="img-bg" src="<?= base_url()?>assets/img/items/item-sm.png">
								<figcaption class="p-2">
									<h6 class="title">Espaço para publicidade </h6>
								</figcaption>
							</figure>
							<figure class="itemside has-bg border-bottom" style="height: 50%;">
								<img class="img-bg" src="<?= base_url()?>assets/img/items/2.jpg" height="80">
								<figcaption class="p-2">
									<h6 class="title">Espaço para publicidade </h6>
								</figcaption>
							</figure>
						</div>
					</aside>
				</div> <!-- row.// -->
			</div>  <!--  card-body -->
		</div>
	</div> 
</section>
<!-- ========================= SECTION MAIN END// ========================= -->

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y-sm bg">
	<div class="container">
		<header class="section-heading heading-line">
			<h4 class="title-section bg"> ANÚNCIOS POPULARES </h4>
		</header> 

		<?php	
			// Seleciona os 6 últimos anúncios ativados. 
			$this->db->select('ads.ad_id,ads.ad_name,ads.ad_status,ads.ad_price,ads_images.ad_id,ads_images.ads_img_file');
			$this->db->from('ads');
			$this->db->join('ads_images','ads_images.ad_id=ads.ad_id');
			$this->db->where("ads.ad_status", 2);			
			$this->db->limit(6);
			$query = $this->db->get();
		?>
		
		<div class="card">
		
			<div class="row no-gutters">
				<div class="col-md-2" id="ads_popul">
					<article href="#" class="card-banner h-100 bg2">
						<div class="card-body zoom-wrap">						
							<img src="<?= base_url();?>assets/img/items/anuncios_populares.jpg" class="img-bg zoom-in">
						</div>
					</article>
				</div> <!-- col.// -->
			
				<div class="col-md-10">
					<ul class="row no-gutters border-cols">
						<?php
							foreach ($query->result() as $row){ 
								echo '<li class="col-6 col-md-2">
									<a href="#" class="itembox"> 
										<div class="card-body" style="text-align: center;">
										   <img class="img-sm" src="'.base_url().'admin/uploads/ads/'.$row->ads_img_file.'">
											<p class="word-limit" style="padding-top: 10px;">'.$row->ad_name.'<br/><span style="font-weight: bold;">R$ '.number_format($row->ad_price, 2, ",", ".").'</span> </p>
											
											
										</div>
									</a>
								</li>';
							}	
						?>					
					</ul>
				</div> <!-- col.// -->
			</div> <!-- row.// -->			
		</div> <!-- card.// -->
	</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->

		<?php	
			// Seleciona as 6 últimas Lojas ativadas. 
			$this->db->select('*');
			$this->db->from('shops');
			$this->db->where("shop_status", 1);			
			$this->db->limit(6);
			$queryShop = $this->db->get();
		?>

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y-sm bg">
	<div class="container">
	
	<header class="section-heading heading-line">
		<h4 class="title-section bg"> LOJAS POPULARES </h4>
	</header>
	
		<div class="card">
			<div class="row no-gutters">
				<div class="col-md-2" id="shop_popul">					
					<article href="#" class="card-banner h-100 bg2">
						<div class="card-body zoom-wrap">
							<img src="<?= base_url();?>assets/img/items/lojas_populares.jpg" class="img-bg zoom-in">
						</div>
					</article>
				</div> <!-- col.// -->
				
				<div class="col-md-10">
					<ul class="row no-gutters border-cols">
					<?php
						foreach ($queryShop->result() as $rowShop){ 
						echo '<li class="col-6 col-md-2">
							<a href="#" class="itembox"> 
								<div class="card-body" style="text-align: center;">
									<p class="word-limit" style="font-size: 18px;">'.$rowShop->shop_name.'</p>
									<img class="img-sm" src="'.base_url().'admin/uploads/shops/'.$rowShop->shop_img_file.'">
								</div>
							</a>
						</li>';
						}?>			
					</ul>
				</div> <!-- col.// -->
			</div> <!-- row.// -->
			
		</div> <!-- card.// -->

	</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->

 <!-- Ação para ocultar a div depois de 5 segundos -->
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function(){ 
            setTimeout(function() {
                $("#msg-box").fadeOut().empty();
            }, 5000);
        }, false);
    </script>