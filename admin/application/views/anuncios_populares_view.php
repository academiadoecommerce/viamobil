<section class="p-0 mt-5">

	<div class="container px-mobile">
		<div class="row m-0">
			<header class="section-heading heading-line">
				<h4 class="title-section bg pl-0"> ANÚNCIOS POPULARES </h4> <!-- <span class="float-right">Ver Mais</span> -->
			</header> 
		</div>
		<div class="row">
			<div class="col-12 p-0 px-2">
				<div class="card border-0 bgnone">         
				    <?php
				    $ads = $this->ads_model->get_anuncios_populares();
				    if ($ads) {
				        echo '<div class="col-md-12 p-0">';
				        echo '<ul class="row no-gutters">';
				        foreach ($ads as $key => $ad) { ?>
				           <li class='col-sm-6 col-md-3 col-lg-2 bgwhite-child'>

				            <?=$this->ads_model->ads_item($ad)?>

				            <!--<div class="chat-icon-ads ap-send-message">

				                <?php if ($this->session->userdata('login')) { ?>
				                     <form method="POST" target="_blank" id="mobile-message" action="<?= base_url('chat/novo_chat') ?>">
				             
				                        <input type="hidden" name="sender_id" value="<?= $_SESSION['login']; ?>">
				                        <input type="hidden" name="recipient_id" value="<?= $ad->use_id ?>">
				                        <input type="hidden" name="ad_id" value="<?= $ad->ad_id ?>">
				                        <input type="hidden" name="use_email" value="<?= $ad->use_email; ?>">
				                        <input type="hidden" name="use_name" value="<?= $ad->use_name; ?>">
				                        <input type="hidden" name="ad_name" value="<?= $ad->ad_name; ?>">

				                        <button  <? if($_SESSION['login'] == $ad->use_id ){ echo 'disabled' ; } ?> type="submit" class=""><i class="fa fa-comments"></i> chat</button>
				                    </form>

				                <?php } else { ?>
				                    <button type="submit" data-toggle="modal" data-target="#chat-required"><i class="fa fa-comments"></i> chat</button>

				                    
				                <?}?>

				            </div>-->

				            </li>
				        <? }
				        echo '</ul>';                       
				        // echo '<div class="pagination-box"></div>';
				        echo '</div>';
				    } else {
				        echo $this->main_model->advertisingBox('top', '100%', '90px');
				        echo '<div class="ads-listing" id="ads-listing">';
				        echo '<div align="center"><strong>Opss!<br>Nenhum anúncio encontrado!
				        <br>
				        Tente buscar uma palavra diferente, ou use os filtros!</strong></div>';
				        echo '</div>';
				        //  echo '</div>';
				    } ?>
				</div>
			</div>
		</div>
	</div>
</section>