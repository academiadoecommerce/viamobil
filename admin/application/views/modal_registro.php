	
<!-- MODEL DO CADASTRO   -->
    <div class="modal fade" id="form-registro" tabindex="-1" role="dialog">
        <div class="modal-dialog login1"  style="margin-top: 30px;">
            <div class="modal-content">
                <div class="user-box">
                    <div class="page-header" style="margin: 0 0 20px;">
                        <h3>Cadastre-se grátis</h3>
                    </div>
					
					<form method="POST" action="<?= base_url('register/insert') ?>" id="form-register" class="form form-simple">
					
						<div class="form-group row my-1">
						  <div class="col-12">
							 <span id="msgNome" style="color: red; font-size: 12px; font-family: arial; display: none;">Por favor, preencha nome e sobrenome. Ex. Maria silva</span>
						  	 <input class="form-control my-1" type="text" required name="name" autocomplete="off" id="nome" placeholder="Nome Completo">
						  </div>
						</div>
						
						<div class="form-group row my-1">
						  <div class="col-12">
								<input class="form-control my-1" type="email" required name="email" id="input-email" placeholder="E-mail (exemplo@exemplo.com.br)">
						  </div>
						</div>

						<div class="form-group row my-1">
							<div class="col-8">
								<input class=" my-1 form-control" type="text" name="cpfCnpj" id="cpfCnpj" maxlength="14" onkeyup="somenteNumeros(this);" placeholder="Número do CPF/CNPJ">
							</div>
							<div class="col-4">
								<input type="text" required class="my-1 form-control input-cep" name="cep" id="ai-cep" maxlength="9" placeholder="CEP">
							</div>							
						</div>				
						
                        <select class="form-group form-control my-1" name="state" id="ai-state" required >
                            <option>Selecione seu Estado</option>
                        </select>									
						
                        <select class="form-group form-control my-1" name="city" id="ai-city" required>
                            <option value="">Selecione um município</option>
                        </select>	
						
						<div class="form-group row my-1" id="box-address">
							<div class="col-12">
								<input class=" my-1 form-control" type="text" name="address" id="ai-address" placeholder="Nome da rua" value="">
							</div>
						</div>

						<div class="form-group row my-1" id="box-address-number">
							<div class="col-12">
								<input class=" my-1 form-control" type="text" name="number_address" id="ai-address-number" placeholder="número">
							</div>
						</div>	
						
						<div class="form-group  row my-1" id="box-neighborhood" style="display:none;">
							<div class="col-12">
								<div id="label-neighborhood" class="d-none" style="margin-bottom: 10px;">...</div>
								<input class=" my-1 form-control" type="text" name="neighborhood" id="ai-neighborhood" value="">
							</div>
						</div>	
						
						<div class="form-group row my-1">
							<div class="col-12 mb-1">
								<input class="form-control my-1" type="text" name="urlSite" id="urlSite" placeholder="URL do Site">
								<small id="emailHelp " class="form-text text-muted"> Obrigatório para logistas.</small>
							</div>
						</div>	
						
						<div class="form-group row my-1">
						  <div class="col-12">
							<input class="form-control my-1" type="password" required onkeyup="passwordStrength(this.value)" id="senha" name="password" placeholder="Senha (escolha um senha forte)">
							  <div class="passwordForce">
								  <div class="no-strength"></div>
							  </div>
						  </div>
						</div>

						<div id="register-return"></div>

						<div class="form-group row my-1">
							<div class="col-lg-12">
								<button type="submit" class="btn btn-primary  btn-lg btn-block" id="cadastrar"> <i class="fa fa-user-plus" aria-hidden="true"></i> Cadastrar</button>
							</div>
						</div>						
						
					</form>
					
					<script src="<?= base_url("assets/js/custom/register.js") ?>"></script>	
					<script src="<?= base_url("assets/js/custom/modal_registro.js") ?>"></script>
															
                </div>
            </div>           
        </div>
    </div>

