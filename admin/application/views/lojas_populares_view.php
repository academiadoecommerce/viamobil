<?php	
			// Seleciona as 6 últimas Lojas ativadas. 
			$this->db->select('*');
			$this->db->from('shops');
			$this->db->where("shop_status", 1);	
			$this->db->order_by('shop_id', 'DESC');			
			$this->db->limit(6);
			$queryShop = $this->db->get();
			$shops = $queryShop->result();
		
		?>

		<section class="p-0 mt-4">

	<div class="container px-mobile">
		<div class="row m-0">
			<header class="section-heading heading-line">
				<h4 class="title-section bg pl-0"> LOJAS POPULARES </h4> <!-- <span class="float-right">Ver Mais</span> -->
			</header> 
		</div>
		<div class="row">
			<div class="col-12 p-0 px-2">
				<div class="card border-0 bgnone">  

			        <div class="ads-shops">
			            <?php
			            if ($shops) {		
								echo '<ul class="row no-gutters ">';
								foreach ($shops as $key => $shop) {
									$shop->ads_cat_name = $this->ads_model->categoriesDetails($shop->ads_cat_id)->ads_cat_name;
									echo "<li class='col-sm-6 col-md-2 border-0 bgwhite-child'>".$this->shops_model->shop_item($shop)."</li>";
								}
								echo '</ul>';						
							
			            } else {
			                echo '<div align="center"><strong>Opss!<br>Nenhuma loja cadastrada!
			            <br>';
			            } ?>

			        </div>

	        	</div>
			</div>
		</div>
</section>