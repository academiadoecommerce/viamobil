<!-- MODEL DO LOGIN  -->
    <div class="modal fade" id="modal-password" tabindex="-1" role="dialog">
        <div class="modal-dialog login1" style="margin-top: 130px;">
            <div class="modal-content">
                <div class="user-box">				
                    <div class="page-header" style="margin: 0 0 20px;">
                        <h3>Esqueceu a senha?</h3>
                    </div>
					
					<form method="POST" action="<?=base_url('login/password/send')?>" id="form-login" class="form form-simple">
						<div class="form-group row">
							<div class="col-lg-12">
								<p>Digite seu endereço de e-mail. Você receberá um link para criar uma nova senha via email.</p>
							</div>
						</div>
						<div class="form-group row">
						    <div class="col-lg-2">
								<label class="form-check-label">Email<span class="required">*</span></label>
							</div>
							<div class="col-lg-10">
								<input class="form-control" type="email" required name="email" id="input-email" placeholder="Digite seu email">
							</div>
						</div>

						<div id="login-return"></div>

						<div class="divider divider-m-top-none"></div>

						<div class="row">
							<div class="col-lg-12 text-right">								
								<button type="submit" class="btn btn-primary">Recuperar</button>
								<button type="button" class="btn btn-link modal-open myBtn"><a href="#" data-toggle="modal" data-target="#modal-login">Voltar</a></button>
							</div>
						</div>
					</form>		
					
                </div>
            </div>
        </div>
    </div>
	
<script>
    // Hide the Modal
    $(".myBtn").click(function(){
        $("#modal-password").modal("hide");
    });
</script>	


