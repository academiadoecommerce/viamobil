<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Variacoes extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

 

    public function index() {
       
        $data['listing'] = $this->panamerico_model->listing('variacoes', 'variacoes_id', 'DESC', paginacao()->getQtd(), paginacao()->getInicio(), array(
            'variacoes_nome' => $this->input->get('search'),
                ), array(
            'variacoes_status' => '0'
        ));

        $total_rows = $this->panamerico_model->contall('variacoes', array(
            'variacoes_nome' => $this->input->get('search'),
        ), array(
            'variacoes_status' => '0'
        ));
        $data['total'] = $total_rows;
        $this->template->load('system', 'variacoes', $data);


    }

    public function insert() {
        $data['e'] = false;

        $this->template->load('system', 'variacoes_form', $data);
    }

    public function save() {
        $e = $this->input->post("e");

        $name = $this->input->post("name");
        $hexadecimal = $this->input->post("hexadecimal");
        $status = $this->input->post("status");

        $data = array(
            'variacoes_nome' => $name,
            'variacoes_status' => $status,
            'variacoes_hexadecimal' => $hexadecimal
        );

        if ($e) {
            $this->panamerico_model->update('variacoes', 'variacoes_id', $e, $data);
        } else {
            $e = $this->panamerico_model->insert('variacoes', $data);
        }

        $this->session->set_flashdata('return', 'save');

        redirect("variacoes");
    }

    public function edit($code) {
        $data['e'] = true;

        $data['item'] = $this->panamerico_model->details('variacoes', 'variacoes_id', $code);

        $this->template->load('system', 'variacoes_form', $data);
    }

    public function delete($view = "modal", $code) {
        if ($view == "modal") {
            $data['modal_title'] = "Apagar a Cor";
            $data['modal_text'] = "Você tem certeza que deseja apagar essa cor?";
            $data['modal_link_href'] = base_url('variacoes/delete/action/' . $code);
            $data['modal_link_type'] = 'danger';
            $data['modal_link_text'] = 'Apagar';

            $this->template->load('modal', 'default', $data);
        }

        if ($view == "action") {
            $this->panamerico_model->delete('variacoes', 'variacoes_id', $code);

            $this->session->set_flashdata('return', 'delete');

            redirect('variacoes');
        }
    }
}