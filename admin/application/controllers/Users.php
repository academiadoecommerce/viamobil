<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends MY_Controller {

    public function __construct() { 
        parent::__construct();
    }

    public function index() {

        $data['listing'] = $this->panamerico_model->listing('users', 'use_id', 'DESC', paginacao()->getQtd(), paginacao()->getInicio(), array(
            'use_name' => $this->input->get('search'),
            'use_id' => $this->input->get('search'),
                ), array(
                    'use_type' => 'cliente',
            'use_status' => $this->input->get('status')
        ));

        $total_rows = $this->panamerico_model->contall('users', array(
            'use_name' => $this->input->get('search'),
            'use_id' => $this->input->get('search'),
                ), array(
            'use_status' => $this->input->get('status')
        ));

        $data['total'] = $total_rows;
        $this->template->load('system', 'users', $data);
    }

    public function vendedores() {

        $data['listing'] = $this->panamerico_model->listing('users', 'use_id', 'DESC', paginacao()->getQtd(), paginacao()->getInicio(), array(
            'use_name' => $this->input->get('search'),
            'use_id' => $this->input->get('search'),
                ), array(
                    'use_type' => 'lojista',
            'use_status' => $this->input->get('status')
        ));

        $total_rows = $this->panamerico_model->contall('users', array(
            'use_name' => $this->input->get('search'),
            'use_id' => $this->input->get('search'),
                ), array(
            'use_status' => $this->input->get('status')
        ));

        $data['total'] = $total_rows;
        $this->template->load('system', 'users', $data);
    }

    public function modalStatus($status_id, $btn, $status_atual) {

        $data['modal_title'] = $btn . ' cadastro';
        $data['modal_text'] = "Você tem certeza que deseja $btn cadastro?";
        $data['status_id'] = $status_id;
        $data['status_atual'] = $status_atual;
        $data['modal_link_type'] = 'success';
        $data['modal_link_text'] = $btn;

        $this->template->load('modal', 'status', $data);
    }

    public function status() {

       
        if ($this->input->post('acao') == 'ok') {
            $this->panamerico_model->alteraStatus2($this->input->post('use_id'), $this->input->post('status'), $this->input->post('status_atual'));
            echo json_encode(array('sucess' => 'ok'));
        } elseif ($this->input->post('acao') == 'massa') {
            $this->panamerico_model->alteraStatusEmMassa2($this->input->post('status_id'), $this->input->post('status_atual'));
            echo json_encode(array('sucess' => 'ok'));
        } elseif ($this->input->post('acao') == 'excluir') {
            //exclue permanentemente os usuarioss selecionados..
            $this->panamerico_model->excluirUser($this->input->post('use_id'), $this->input->post('status'));
            echo json_encode(array('sucess' => 'ok'));
        } elseif ($this->input->post('acao') == 'excluirmassa') {
            //exclue permanentemente os usuarios com status = 5
            $this->panamerico_model->excluirUserEmMassa($this->input->post('status_id'), $this->input->post('status_atual'));
            echo json_encode(array('sucess' => 'ok'));
        }
    }
    
      public function ajax() {

        $data['listing'] = $this->panamerico_model->listing('users', 'use_name', 'ASC', paginacao()->getQtd(), paginacao()->getInicio(), array(
            'use_name' => $this->input->get('search'),
        ));

        $total_rows = $this->panamerico_model->contall('users', array(
            'use_name' => $this->input->get('search'),
        ));
        $data['total'] = $total_rows;
        $this->load->view('system/users', $data);
    }

    public function details($code) {
        $data['item'] = $this->panamerico_model->details('users', 'use_id', $code);

        $data['ads'] = $this->panamerico_model->listingByWhere('ads', 'use_id', $code);

        $this->template->load('system', 'users_details', $data);
    }

    public function insert() {
        $data['e'] = false;

        $data['states'] = $this->panamerico_model->listing('states', 'sta_name', 'ASC');

        $this->template->load('system', 'users_form', $data);
    }

    public function edit($code) {
        $data['e'] = true;

        $data['item'] = $this->panamerico_model->details('users', 'use_id', $code);

        $regions = $this->main_model->regionsDetails($data['item']->use_region);

        $data['region'] = $regions;

        $data['states'] = $this->panamerico_model->listing('states', 'sta_name', 'ASC');

        $this->template->load('system', 'users_form', $data);
    }

    public function save() {
        $e = $this->input->post("e");

        $name = $this->input->post("name");
        $status = $this->input->post("status");
        $email = $this->input->post("email");
        $cpf = $this->input->post("cpf");
        $phone = $this->input->post("phone");
        $celular = $this->input->post("celular");
        $whatsapp = $this->input->post("whatsapp");
        $website = $this->input->post("website");
        $facebook = $this->input->post("facebook");
        $instagram = $this->input->post("instagram");
        $elo7 = $this->input->post("elo7");
        $mercado_livre = $this->input->post("mercado_livre");
        $cep = $this->input->post("cep");
        $address = $this->input->post("address");
        $address_number = $this->input->post("address_number");
        $region = $this->input->post("region");
        $city = $this->input->post("city");
        $state = $this->input->post("state");
        $neighborhood = $this->input->post("neighborhood");

        $data = array(
            'use_name' => $name,
            'use_status' => $status,
            'use_email' => $email,
            'use_cpf' => $cpf,
            'use_phone' => $phone,
            'use_celular' => $celular,
            'use_whatsapp' => $whatsapp,
            'use_website' => $website,
            'use_facebook' => $facebook,
            'use_instagram' => $instagram,
            'use_elo7' => $elo7,
            'use_mercado_livre' => $mercado_livre,
            'use_cep' => $cep,
            'use_address' => $address,
            'use_address_number' => $address_number,
            'use_region' => $region,
            'use_city' => $city,
            'use_state' => $state,
            'use_neighborhood' => $neighborhood
        );

        if ($e) {
            $this->panamerico_model->update('users', 'use_id', $e, $data);
        } else {
            $e = $this->panamerico_model->insert('users', $data);
        }

        $this->session->set_flashdata('return', 'save');

        redirect("users");
    }

    public function delete($view = "modal", $code) {
        if ($view == "modal") {
            $data['modal_title'] = "Apagar Usuário";
            $data['modal_text'] = "Você tem certeza que deseja apagar esse usuário?";
            $data['modal_link_href'] = base_url('users/delete/action/' . $code);
            $data['modal_link_type'] = 'danger';
            $data['modal_link_text'] = 'Apagar';

            $this->template->load('modal', 'default', $data);
        }

        if ($view == "action") {
            $this->panamerico_model->delete('users', 'use_id', $code);

            $this->session->set_flashdata('return', 'delete');

            redirect('users');
        }
    }

    public function suspend_user($view = "modal", $code) {
        if ($view == "modal") {
            $data['modal_title'] = "Suspender Usuário";
            $data['modal_text'] = "Você tem certeza que deseja Suspender esse usuário?";
            $data['modal_link_href'] = base_url('users/suspend_user/action/' . $code);
            $data['modal_link_type'] = 'danger';
            $data['modal_link_text'] = 'Suspender';

            $this->template->load('modal', 'default', $data);
        }

        if ($view == "action") {

             $user = $this->panamerico_model->details('users', 'use_id', $code);

            /* Email Variables */
            $name = $user->use_name;
            $email = $user->use_email;
            $ad_name = $item->ad_name;
            $ad_link = "http://www.soatacado.com" . str_replace(ADMIN_PATH, "", base_url('anuncio/' . $item->ad_slug));
            $datetime = date('d/m/Y H:i:s');

            /* Email Details */
            $email_details = $this->panamerico_model->details('emails', 'email_id', 16);

            /* Message */
            $content = $email_details->email_content;

            if (preg_match_all('/({\$+\w+})/', $content, $matches)) {

                foreach ($matches[0] as $key => $value) {
                    $variable = str_replace('{', '', $value);
                    $variable = str_replace('}', '', $variable);
                    $string = eval('return ' . $variable . ';');

                    $content = str_replace($value, $string, $content);
                }
            }

            $email_message = $content;

            /* Subject */
            $email_subject = $email_details->email_subject;

            /* To */
            $email_to = $email;

            /* Send Email */
            $this->panamerico_model->email($email_to, $email_subject, $email_message);


            $this->panamerico_model->suspend_user('users', 'use_id', $code);
            $this->session->set_flashdata('return', 'save');

            redirect('users');
        }
    }

    public function unsuspend_user($view = "modal", $code) {
        if ($view == "modal") {
            $data['modal_title'] = "Liberar Usuário";
            $data['modal_text'] = "Você tem certeza que deseja Liberar esse usuário?";
            $data['modal_link_href'] = base_url('users/unsuspend_user/action/' . $code);
            $data['modal_link_type'] = 'success';
            $data['modal_link_text'] = 'Liberar';

            $this->template->load('modal', 'default', $data);
        }

        if ($view == "action") {


            // $item = $this->panamerico_model->details('ads', 'ad_id', $code);
            $user = $this->panamerico_model->details('users', 'use_id', $code);

            /* Email Variables */
            $name = $user->use_name;
            $email = $user->use_email;
            $ad_name = $item->ad_name;
            $ad_link = "http://www.soatacado.com" . str_replace(ADMIN_PATH, "", base_url('anuncio/' . $item->ad_slug));
            $datetime = date('d/m/Y H:i:s');

            /* Email Details */
            $email_details = $this->panamerico_model->details('emails', 'email_id', 7);

            /* Message */
            $content = $email_details->email_content;

            if (preg_match_all('/({\$+\w+})/', $content, $matches)) {

                foreach ($matches[0] as $key => $value) {
                    $variable = str_replace('{', '', $value);
                    $variable = str_replace('}', '', $variable);
                    $string = eval('return ' . $variable . ';');

                    $content = str_replace($value, $string, $content);
                }
            }

            $email_message = $content;

            /* Subject */
            $email_subject = $email_details->email_subject;

            /* To */
            $email_to = $email;

            /* Send Email */
            $this->panamerico_model->email($email_to, $email_subject, $email_message);



            $this->panamerico_model->unsuspend_user('users', 'use_id', $code);

            $this->session->set_flashdata('return', 'save');

            redirect('users');
        }
    }

    public function cities($state) {
        $cities = $this->panamerico_model->listingByWhere('cities', 'sta_id', $state);

        foreach ($cities as $key => $city) {
            echo '<option value="' . $city->cit_id . '">' . $city->cit_name . '</option>';
        }
    }

    public function exportCSV($type){ 

       // file name 
       $filename = 'users-'.date('d-m-y').'.csv'; 
       header("Content-Description: File Transfer"); 
       header("Content-Disposition: attachment; filename=$filename"); 
       header("Content-Type: application/csv; ");
       
       // get data 
       $usersData = $this->db->select('use_name, use_email')->from('users')->where('use_type', $type)->get()->result_array();

       // file creation 
       $file = fopen('php://output', 'w');
     
       $header = array("Name", "Email"); 
       fputcsv($file, $header);
       foreach ($usersData as $key=>$line){ 
         fputcsv($file,$line); 
       }
       fclose($file);
       redirect('/users');
       exit; 
  }

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */