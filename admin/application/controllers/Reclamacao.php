<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reclamacao extends CI_Controller {


	public function inicia(){
		$this->load->model("pedido_model");
		$id_pedido = $this->input->post("id_pedido");
		$assunto   = $this->input->post("assunto");
		$descricao = $this->input->post("descricao");
		//echo "PEDIDO: ".$id_pedido;
		$pedido    = $this->pedido_model->getByOrder($id_pedido);
		foreach($pedido as $recl):
			$reclamacao = array(
				'reclama_pedido'    => $recl['order_num'],
				'reclama_cliente'   => $recl['order_cliente'],
				'reclama_vendedor'  => $recl['order_vendedor'],
				'reclama_data'      => date("Y-m-d"),
				'reclama_status'    => 0,
				'reclama_encerrado' => 0,
			);
		endforeach;
		$this->pedido_model->addReclamacoes($reclamacao);
		$conteudo = array(
			"reclama_content_order"     => $id_pedido,
			"reclama_content_descricao" => $descricao,
			"reclama_content_assunto"   => $assunto,
			"reclama_hora" => date('h:i:s'),
			"reclama_content_tipo" => $this->session->userdata('login_tipo')
		);
		$this->pedido_model->updateStatus($id_pedido);
		$this->pedido_model->reclamacoesContent($conteudo);
		$this->session->set_flashdata('success', 'Sua reclamação foi iniciada');
      echo json_encode(array(
         'status' => 'success',
         'redirect' => base_url("Reclamacao/chat/{$id_pedido}")
      ));
		//$this->output->enable_profiler(true);
	}

	public function encerra()
   {
		$id_pedido = $this->input->post("order");
		echo "ID: ".$id_pedido;
		$this->load->model("pedido_model");
		$this->pedido_model->encerra($id_pedido);
		$this->pedido_model->encerraReclamacaoPedido($id_pedido);
	}

	public function chat($pedido)
   {
		// $order = $this->input->post("chat");
		$order = $pedido;
		$this->load->model("reclamacoes_model");

		$data['conteudo'] = $this->reclamacoes_model->getContent($order);
		$data['order'] = $order;

		/* View */
		$data['page'] = 'chat_pedido';
		# $this->template->load('app', 'profile', $data);
		//$this->load->view('app');
		//$this->load->view('header');
		$this->template->load('system', 'chat_pedido', $data);
		//$this->load->view('footer');
	
	}

	public function message()
   {
		$this->load->model("pedido_model");
		$message = $this->input->post("message");
		$order = $this->input->post("order");
		$conteudo = array(
			"reclama_content_order"     => $order,
			"reclama_content_descricao" => $message,
			"reclama_content_assunto"   => "",
			"reclama_hora" => date('h:i:s'),
			"reclama_content_tipo" => $this->session->userdata('login_tipo')
		);
		$this->pedido_model->reclamacoesContent($conteudo);
      redirect("Reclamacao/chat/{$order}");
	}

}
