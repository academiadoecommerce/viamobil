<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['modal_title'] = "Cadastre-se grátis";
		$data['modal_size'] = "small";

		# $this->template->load('modal', 'register', $data);
	}

	public function insert()
	{
		
		$name = validate_name($this->input->post('name'));
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));		
		$cpfCnpj  = $this->input->post('cpfCnpj');
		$cep 	  = $this->input->post('cep');
		$endereco = $this->input->post('address');
		$number_address = $this->input->post('number_address');
		$bairro   = $this->input->post('neighborhood');
		$cidade   = $this->input->post('city');	
		$estado   = $this->input->post('state');
		$site   = $this->input->post('urlSite');

		$data = array(
			'use_name' => $name, 
			'use_email' => $email, 
			'use_password' => $password,
			'use_cpf' => $cpfCnpj,
			'use_cep' => $cep,
			'use_address' => $endereco,
			'use_address_number' => $number_address,
			'use_neighborhood' => $bairro,
			'use_city' => $cidade,
			'use_state' => $estado,
			'use_website' => $site,
			'use_status' => 2
		);

		$user = $this->user_model->insert($data);

		if($user){
			/* Email Details */
			$email_details = $this->main_model->emailsDetails(14);

			/* Message */
			$content = $email_details->email_content;

			if (preg_match_all('/({\$+\w+})/', $content, $matches)){

				foreach ($matches[0] as $key => $value) {
					$variable = str_replace('{', '', $value);
					$variable = str_replace('}', '', $variable);
					$string = eval('return '. $variable . ';');

					$content = str_replace($value, $string, $content);
				}

			}

			$message = $content;

			/* Subject */
			$email_subject = $email_details->email_subject;

			/* To */
			$to = $email;

			/* Send Email */
			$this->main_model->email($to, $email_subject, $message);

			/* Login */
			// $this->session->set_userdata('login', $user);

			/* Return Msg */

			$this->session->set_flashdata('register_modal', 'pendente');
		}

		redirect('/');
	}

	public function emailVerify()
	{
		$email = $this->input->post('email');

		$verifyEmail = $this->user_model->emailVerify($email);

		if($verifyEmail == true){
			echo 'email_registered';
		}else{
			echo 'email_free';
		}
	}

}

/* End of file Register.php */
/* Location: ./application/controllers/Register.php */