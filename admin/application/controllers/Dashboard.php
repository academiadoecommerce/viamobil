<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array();

		$data['user_pendente'] = $this->panamerico_model->contall('users', array(
            'use_status' => 2,
        ));
        $data['ad_pendente'] = $this->panamerico_model->contall('ads', array(
            'ad_status' => 1,
        ));
        $data['total_lojas'] = $this->panamerico_model->contall('shops', array(
            'shop_status' => 1,
        ));
        $data['total_ads'] = $this->panamerico_model->contall('ads', array(
            'ad_status' => 2,
        ));

		$this->template->load('system', 'dashboard', $data);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */