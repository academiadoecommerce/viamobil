<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ads extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $data['listing'] = $this->panamerico_model->listing('ads', 'ad_id', 'DESC', paginacao()->getQtd(), paginacao()->getInicio(), array(
            'ad_name' => $this->input->get('search'),
            'ad_id' => $this->input->get('search'),
                ), array(
            'ad_status' => $this->input->get('status')
        ));

        $total_rows = $this->panamerico_model->contall('ads', array(
            'ad_name' => $this->input->get('search'),
            'ad_id' => $this->input->get('search'),
                ), array(
            'ad_status' => $this->input->get('status')
        ));
        $data['total'] = $total_rows;

        // $data['user'] = $this->panamerico_model->details('admin_users', 'adm_use_id', $this->session->userdata('login'));

        // $this->template->load('system', 'ads', $data);
        $this->template->load('system', 'ads', $data);
        
    }

    public function status() {
        if ($this->input->post('acao') == 'ok') {
            $tmp = $this->panamerico_model->alteraStatus($this->input->post('ad_id'), $this->input->post('status'), $this->input->post('status_atual'));
            echo json_encode(array('sucess' => 'ok'));
        } elseif ($this->input->post('acao') == 'massa') {
            $this->panamerico_model->alteraStatusEmMassa($this->input->post('status_id'), $this->input->post('status_atual'));
            echo json_encode(array('sucess' => 'ok'));
        } elseif ($this->input->post('acao') == 'excluir') {
            //exclue permanentemente os anuncios selecionados..
            $this->panamerico_model->excluirAds($this->input->post('ad_id'), $this->input->post('status'));
            echo json_encode(array('sucess' => 'ok'));
        } elseif ($this->input->post('acao') == 'excluirmassa') {
            //exclue permanentemente os anuncios com status = 5
            $this->panamerico_model->excluirAdsEmMassa($this->input->post('status_id'), $this->input->post('status_atual'));
            echo json_encode(array('sucess' => 'ok'));
        }
    }

    public function edit($code) {
        $data['e'] = 1;

        $data['item'] = $this->panamerico_model->details('ads', 'ad_id', $code);

        if ($data['item']->ad_use_info) {

            $regions = $this->main_model->regionsDetails($data['item']->use_region);
            $regions = @$regions->regiao_nome;
            $data['region'] = $regions;
        } else {

            $regions = $this->main_model->regionsDetails($data['item']->ad_region);
            $regions = @$regions->regiao_nome;
            $data['region'] = $regions;
        }
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
        $data['categories'] = $this->panamerico_model->listingByWhere('ads_categories', 'ads_cat_parent', 0);

        $data['categoriafilho'] = $this->db->where('ads_cat_parent >', 0)->where('ads_cat_grau', null)->get('ads_categories')->result();

        $data['categ_tertiary'] = $this->db->where('ads_cat_grau', 3)->get('ads_categories')->result();

        $categ = $this->db->where('ads_cat_id', $data['item']->ads_cat_id)->get('ads_categories')->row();

        if($categ->ads_cat_grau == 3){
            $data['categ'] = [
                'ter' => $categ->ads_cat_id,
                'seg' => $categ->ads_cat_parent,
                'pri' => $this->db->where('ads_cat_id', $categ->ads_cat_parent)->get('ads_categories')->row()->ads_cat_parent
            ];
        } else{
            $data['categ'] = [
                'seg' => $categ->ads_cat_id,
                'pri' => $categ->ads_cat_parent
            ];
        }

        $data['images'] = $this->panamerico_model->listingByWhere('ads_images', 'ad_id', $code);

        $data['areas'] = $this->panamerico_model->listing('areas', 'area_name', 'ASC');

        $data['states'] = $this->panamerico_model->listing('states', 'sta_name', 'ASC');
        
 
        $this->template->load('system', 'ads_form', $data);
    }

    public function save() {

        $e = $this->input->post("e");
      

        $name = $this->input->post("name");
        $status = $this->input->post("status");
        $category = $this->input->post("category");
        $desc = $this->input->post("desc");
        $price = $this->input->post("price");
        $price2 = $this->input->post("price2");
        $desc_atacado = $this->input->post("desc_atacado");
        $areas = $this->input->post("areas[]");
        $video = $this->input->post("video");
        $peso  = $this->input->post("peso"); 
        $altura = $this->input->post("altura");
        $largura = $this->input->post("largura");
        $diametro = $this->input->post("diametro");
        $comprimento = $this->input->post("comprimento");
        $qtdmin = $this->input->post("qtdmin");
        $qtdmax = $this->input->post("qtdmax");

        $data = array(
            'ad_name' => $name,
            'ad_desc' => $desc,
            'ad_status' => $status,
            'ads_cat_id' => $category,
            'ad_price' => $price,
            'ad_video' => $video,
            'ad_use_info' => 1,
            'ad_price2' => $price2,
            'ad_descricao_atacado' => $desc_atacado,
            'ad_peso' => $peso,
            'ad_altura' => $altura,
            'ad_largura' => $largura,
            'ad_diametro' => $diametro,
            'ad_comprimento' => $comprimento,
            'ad_qtdmin' => $qtdmin,
            'ad_qtdmax' => $qtdmax
        );

        if ($e) {
            $this->panamerico_model->update('ads', 'ad_id', $e, $data);
        } else {
            $e = $this->panamerico_model->insert('ads', $data);
        }

        $this->panamerico_model->delete('ads_areas', 'ad_id', $code);

        if ($areas) {
            foreach ($areas as $key => $area) {
                $data = array('area_id' => $area, 'ad_id' => $e);

                $this->panamerico_model->insert('ads_areas', $data);
            }
        }
        
        
        if($e){
			
			$this->ads_model->cleanCustomFields($e);

			if($custom){
				foreach ($custom as $key => $custom_item) {
					$custom_data = array(
						'ad_id' => $e, 
						'cat_fie_id' => $key, 
						'ads_cus_value' => $custom_item
					);

					$this->ads_model->insertCustomField($custom_data);
				}
			}

			$this->ads_model->cleanCustomCheckbox($e);

			if($custom_checkbox){
				foreach ($custom_checkbox as $key_1 => $checkbox_item) {
					foreach ($checkbox_item as $key_2 => $custom_item) {
						$custom_data = array(
							'ad_id' => $e, 
							'cat_fie_id' => $key_1, 
							'che_opt_id' => $key_2
						);

						$this->ads_model->insertCustomCheckbox($custom_data);
					}
				}
			}
                        
                       
                       

		}
        

        $this->session->set_flashdata('return', 'save');

        redirect("ads");
    }

    public function modalStatus($status_id, $btn, $status_atual) {

        $data['modal_title'] = $btn . ' Anúncio';
        $data['modal_text'] = "Você tem certeza que deseja $btn anúncio?";
        $data['status_id'] = $status_id;
        $data['status_atual'] = $status_atual;
        $data['modal_link_type'] = 'success';
        $data['modal_link_text'] = $btn;

        $this->template->load('modal', 'status', $data);
    }

    public function approve($view = "modal", $code) {
        if ($view == "modal") {
            $data['modal_title'] = "Aprovar Anúncio";
            $data['modal_text'] = "Você tem certeza que deseja aprovar esse anúncio?";
            $data['modal_link_href'] = base_url('ads/approve/action/' . $code.'/?status='.$_GET['status']);
            $data['modal_link_type'] = 'success';
            $data['modal_link_text'] = 'Aprovar';

            $this->template->load('modal', 'default', $data);
        }

        if ($view == "action") {
            $item = $this->panamerico_model->details('ads', 'ad_id', $code);
            $user = $this->panamerico_model->details('users', 'use_id', $item->use_id);

            /* Email Variables */
            $name = $user->use_name;
            $email = $user->use_email;
            $ad_name = $item->ad_name;
            $ad_link = "http://www.seusite.com.br" . str_replace(ADMIN_PATH, "", base_url('anuncio/' . $item->ad_slug));
            $datetime = date('d/m/Y H:i:s');

            /* Email Details */
            $email_details = $this->panamerico_model->details('emails', 'email_id', 12);

            /* Message */
            $content = $email_details->email_content;

            if (preg_match_all('/({\$+\w+})/', $content, $matches)) {

                foreach ($matches[0] as $key => $value) {
                    $variable = str_replace('{', '', $value);
                    $variable = str_replace('}', '', $variable);
                    $string = eval('return ' . $variable . ';');

                    $content = str_replace($value, $string, $content);
                }
            }

            $email_message = $content;

            /* Subject */
            $email_subject = $email_details->email_subject;

            /* To */
            $email_to = $email;

            /* Send Email */
            $this->panamerico_model->email($email_to, $email_subject, $email_message);

            $data = array('ad_status' => 2);

            $this->panamerico_model->update('ads', 'ad_id', $code, $data);

            $this->session->set_flashdata('return', 'save');
            if($this->input->get('status') > 0){
              redirect('ads/?status='.$this->input->get('status'));
            }
            redirect('ads');
            
        }
    }


    public function reprove($view = "modal", $code) {
        if ($view == "modal") {
            $data['modal_title'] = "Suspender/Reprovar Anúncio";
            $data['modal_text'] = "Você tem certeza que deseja suspender/reprovar esse anúncio?";
            $data['modal_link_href'] = base_url('ads/reprove/action/' . $code.'/?status='.$_GET['status']);
            $data['modal_link_type'] = 'danger';
            $data['modal_link_text'] = 'Continuar';

            $this->template->load('modal', 'default', $data);
        }

        if ($view == "action") {
            $item = $this->panamerico_model->details('ads', 'ad_id', $code);
            $user = $this->panamerico_model->details('users', 'use_id', $item->use_id);

            /* Email Variables */
            $name = $user->use_name;
            $email = $user->use_email;
            $ad_name = $item->ad_name;
            $ad_link = "http://www.seusite.com.br" . str_replace(ADMIN_PATH, "", base_url('anuncio/' . $item->ad_slug));
            $datetime = date('d/m/Y H:i:s');

            /* Email Details */
            $email_details = $this->panamerico_model->details('emails', 'email_id', 17);

            /* Message */
            $content = $email_details->email_content;

            if (preg_match_all('/({\$+\w+})/', $content, $matches)) {

                foreach ($matches[0] as $key => $value) {
                    $variable = str_replace('{', '', $value);
                    $variable = str_replace('}', '', $variable);
                    $string = eval('return ' . $variable . ';');

                    $content = str_replace($value, $string, $content);
                }
            }

            $email_message = $content;

            /* Subject */
            $email_subject = $email_details->email_subject;

            /* To */
            $email_to = $email;

            /* Send Email */
            $this->panamerico_model->email($email_to, $email_subject, $email_message);

            $data = array('ad_status' => 0);

            $this->panamerico_model->update('ads', 'ad_id', $code, $data);

            $this->session->set_flashdata('return', 'save');
            if($this->input->get('status') > 0){
              redirect('ads/?status='.$this->input->get('status'));
            }
            redirect('ads');
            
        }
    }

    public function delete($view = "modal", $code) {
        if ($view == "modal") {
            $data['modal_title'] = "Apagar Anúncio";
            $data['modal_text'] = "Você tem certeza que deseja apagar esse anúncio?";
            $data['modal_link_href'] = base_url('ads/delete/action/' . $code);
            $data['modal_link_type'] = 'danger';
            $data['modal_link_text'] = 'Apagar';

            $this->template->load('modal', 'default', $data);
        }

        if ($view == "action") {
            $item = $this->panamerico_model->details('ads', 'ad_id', $code);
            $user = $this->panamerico_model->details('users', 'use_id', $item->use_id);

            /* Email Variables */
            $name = $user->use_name;
            $email = $user->use_email;
            $ad_name = $item->ad_name;
            $datetime = date('d/m/Y H:i:s');

            /* Email Details */
            $email_details = $this->panamerico_model->details('emails', 'email_id', 10);

            /* Message */
            $content = $email_details->email_content;

            if (preg_match_all('/({\$+\w+})/', $content, $matches)) {

                foreach ($matches[0] as $key => $value) {
                    $variable = str_replace('{', '', $value);
                    $variable = str_replace('}', '', $variable);
                    $string = eval('return ' . $variable . ';');

                    $content = str_replace($value, $string, $content);
                }
            }

            $email_message = $content;

            /* Subject */
            $email_subject = $email_details->email_subject;

            /* To */
            $email_to = $email;

            /* Send Email */
            $this->panamerico_model->email($email_to, $email_subject, $email_message);

            $this->panamerico_model->delete('ads', 'ad_id', $code);
            $this->panamerico_model->deleteIMG($code);

            $this->session->set_flashdata('return', 'delete');

            redirect('ads');
        }
    }

    public function setDeleted($view = "modal", $code) {
        if ($view == "modal") {
            $data['modal_title'] = "Apagar Anúncio";
            $data['modal_text'] = "Você tem certeza que deseja excluir esse anúncio?";
            $data['modal_link_href'] = base_url('ads/setDeleted/action/' . $code);
            $data['modal_link_type'] = 'danger';
            $data['modal_link_text'] = 'Apagar';

            $this->template->load('modal', 'default', $data);
        }

        if ($view == "action") {
            $item = $this->panamerico_model->details('ads', 'ad_id', $code);
            $user = $this->panamerico_model->details('users', 'use_id', $item->use_id);

            /* Email Variables */
            $name = $user->use_name;
            $email = $user->use_email;
            $ad_name = $item->ad_name;
            $datetime = date('d/m/Y H:i:s');

            /* Email Details */
            $email_details = $this->panamerico_model->details('emails', 'email_id', 10);

            /* Message */
            $content = $email_details->email_content;

            if (preg_match_all('/({\$+\w+})/', $content, $matches)) {

                foreach ($matches[0] as $key => $value) {
                    $variable = str_replace('{', '', $value);
                    $variable = str_replace('}', '', $variable);
                    $string = eval('return ' . $variable . ';');

                    $content = str_replace($value, $string, $content);
                }
            }

            $email_message = $content;

            /* Subject */
            $email_subject = $email_details->email_subject;

            /* To */
            $email_to = $email;

            /* Send Email */
            $this->panamerico_model->email($email_to, $email_subject, $email_message);

            $this->panamerico_model->setDeleted('ads', 'ad_id', $code);
            // $this->panamerico_model->deleteIMG($code);

            $this->session->set_flashdata('return', 'delete');

            redirect('ads');
        }
    }

    public function categoriesSecondary($category) {
        $categories = $this->panamerico_model->listingByWhere('ads_categories', 'ads_cat_parent', $category);

        foreach ($categories as $key => $cat) {
            echo '<option value="' . $cat->ads_cat_id . '">' . $cat->ads_cat_name . '</option>';
        }
    }

    public function images_delete($view = "modal", $code) {
        if ($view == "modal") {
            $data['modal_title'] = "Apagar Imagem";
            $data['modal_text'] = "Você tem certeza que deseja apagar essa imagem?<br><br><strong>Atenção!</strong> Essa ação irá apagar a imagem e não terá como voltar a ação.";
            $data['modal_link_href'] = base_url('ads/images_delete/action/' . $code);
            $data['modal_link_type'] = 'danger';
            $data['modal_link_text'] = 'Apagar';

            $this->template->load('modal', 'default', $data);
        }

        if ($view == "action") {
            $image = $this->panamerico_model->details('ads_images', 'ads_img_id', $code);

            unlink($image->ads_img_path);

            $this->panamerico_model->delete('ads_images', 'ads_img_id', $code);

            $this->session->set_flashdata('return', 'delete');

            redirect('ads/edit/' . $image->ad_id);
        }
    }

    public function areas() {
        $data['listing'] = $this->panamerico_model->listing('areas', 'area_name', 'ASC');

        $this->template->load('system', 'ads_areas', $data);
    }

    public function areas_insert() {
        $data['e'] = false;

        $this->template->load('system', 'ads_areas_form', $data);
    }

    public function areas_edit($code) {
        $data['e'] = true;

        $data['item'] = $this->panamerico_model->details('areas', 'area_id', $code);

        $this->template->load('system', 'ads_areas_form', $data);
    }

    public function areas_save() {
        $e = $this->input->post("e");

        $name = $this->input->post("name");
        $status = $this->input->post("status");

        $data = array(
            'area_name' => $name,
            'area_status' => $status
        );

        if ($e) {
            $this->panamerico_model->update('areas', 'area_id', $e, $data);
        } else {
            $e = $this->panamerico_model->insert('areas', $data);
        }

        $this->session->set_flashdata('return', 'save');

        redirect("ads/areas");
    }

    public function areas_delete($view = "modal", $code) {
        if ($view == "modal") {
            $data['modal_title'] = "Apagar Área";
            $data['modal_text'] = "Você tem certeza que deseja apagar essa área?";
            $data['modal_link_href'] = base_url('ads/areas_delete/action/' . $code);
            $data['modal_link_type'] = 'danger';
            $data['modal_link_text'] = 'Apagar';

            $this->template->load('modal', 'default', $data);
        }

        if ($view == "action") {
            $this->panamerico_model->delete('areas', 'area_id', $code);

            $this->session->set_flashdata('return', 'delete');

            redirect('ads/areas');
        }
    }

    public function cities($state) {
        $cities = $this->panamerico_model->listingByWhere('cities', 'sta_id', $state);

        foreach ($cities as $key => $city) {
            echo '<option value="' . $city->cit_id . '">' . $city->cit_name . '</option>';
        }
    }

}

/* End of file Ads.php */
/* Location: ./application/controllers/Ads.php */