<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Banner extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['listing'] = $this->panamerico_model->listing('banner', 'banner_id', 'DESC', paginacao()->getQtd(), paginacao()->getInicio(), array(
            'banner_name' => $this->input->get('search'),
        ));

        $total_rows = $this->panamerico_model->contall('banner', array(
            'banner_name' => $this->input->get('search'),
        ));
        $data['total'] = $total_rows;
        $this->template->load('system', 'banner', $data);
    }

    public function ajax()
    {

        $data['listing'] = $this->panamerico_model->listing('banner', 'banner_name', 'ASC', paginacao()->getQtd(), paginacao()->getInicio(), array(
            'banner_name' => $this->input->get('search'),
        ));

        $total_rows = $this->panamerico_model->contall('banner', array(
            'banner_name' => $this->input->get('search'),
        ));
        $data['total'] = $total_rows;
        $this->load->view('system/banner', $data);
    }

    public function insert()
    {
        $data['e'] = false;

        $this->template->load('system', 'banner_form', $data);
    }

    public function edit($code)
    {
        $data['e'] = true;

        $data['item'] = $this->panamerico_model->details('banner', 'banner_id', $code);

        // $data['faq'] = $this->panamerico_model->listingByWhere('banner_faq', 'banner_id', $code);

        $this->template->load('system', 'banner_form', $data);
    }

    public function uploadimg()
    {
        $config['upload_path'] = '../assets/img/banners/';
        $config['allowed_types'] = 'gif|jpg|png';
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('image')) {
            return $this->upload->display_errors();
        } else {
            return $this->upload->data();
        }
    }

    public function save()
    {
        $e = $this->input->post("e");
        if ($e && $_FILES['image']['error']) {
            $image = $this->input->post('image');
        } elseif (isset($_FILES['image']) && !empty($_FILES['image'])){
            $image = $this->uploadimg()['file_name'];
        } else{
            $this->session->set_flashdata('return', ['status' => 'erro', 'msg'=>'Parece que não foi enviada uma imagem!']);
            redirect($_SERVER['HTTP_REFERER']);
            exit;
        }

        $name = $this->input->post("name");
        $slug = $this->input->post("slug");
        $status = $this->input->post("status");

        $data = array(
            'banner_name' => $name,
            'banner_slug' => $slug,
            'banner_img' => $image,
            'banner_status' => $status
        );

        if ($e) {
            $this->panamerico_model->update('banner', 'banner_id', $e, $data);
        } else {
            $e = $this->panamerico_model->insert('banner', $data);
        }

        $this->session->set_flashdata('return', 'save');

        redirect("banner");
    }

    public function delete($view = "modal", $code)
    {
        if ($view == "modal") {
            $data['modal_title'] = "Apagar Banner";
            $data['modal_text'] = "Você tem certeza que deseja apagar esse banner?";
            $data['modal_link_href'] = base_url('banner/delete/action/' . $code);
            $data['modal_link_type'] = 'danger';
            $data['modal_link_text'] = 'Apagar';

            $this->template->load('modal', 'default', $data);
        }

        if ($view == "action") {
            $this->panamerico_model->delete('banner', 'banner_id', $code);

            $this->session->set_flashdata('return', 'delete');

            redirect('banner');
        }
    }

}

/* End of file banner.php */
/* Location: ./application/controllers/banner.php */
