<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reclamacoes extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index(){
    	$this->load->model("pedido_model");
    	$data['reclamacoes'] = $this->pedido_model->getReclamacoes();

    	$data['listing'] = $this->panamerico_model->listing('reclamacoes', 'reclama_id', 'DESC', paginacao()->getQtd(), paginacao()->getInicio(), array(
            'reclama_pedido' => $this->input->get('search'),
        ));

        $total_rows = $this->panamerico_model->contall('reclamacoes', array(
            'reclama_pedido' => $this->input->get('search'),
        ));

        $data['total'] = $total_rows;

        $this->template->load('system', 'reclamacoes_view', $data);
    }

    public function ajax() {
       
        $data['listing'] = $this->panamerico_model->listing('reclamacoes', 'reclama_pedido', 'ASC', paginacao()->getQtd(), paginacao()->getInicio(), array(
            'reclama_pedido' => $this->input->get('search'),
        ));

        $total_rows = $this->panamerico_model->contall('reclamacoes', array(
            'reclama_pedido' => $this->input->get('search'),
        ));

        $data['total'] = $total_rows;
        
        $this->load->view('system/reclamacoes_view', $data);
    }

}