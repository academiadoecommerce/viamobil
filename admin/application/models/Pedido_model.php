<?php
/**
 * Created by PhpStorm.
 * User: Vinícius
 * Date: 05/10/2016
 * Time: 21:42
 */

/**
 * @property CI_DB_query_builder $db
 * @property Main_model          $main_model
 */
class Pedido_Model extends CI_Model{
	
	public function addOrder($data)
	{
		$this->db->insert('panamerico_orders', $data);
		return $this->db->insert_id();
	}

	public function addOrderDetais($detalhes){
		$this->db->insert('panamerico_orders_details', $detalhes);
        return $this->db->insert_id();
	}


	public function getOrderByCliente($id)
	{
		$this->db->select("SUM(details.orders_qtd) AS quantidade, order.order_valor as soma, user.use_name, order.order_num, order.order_situacao, order.order_data, order.order_valor, order.order_frete as frete, order.order_tipo as tipo, ads.ad_name, order.order_reclamacao, ads.ad_tipo as order_tipo, img.ads_img_file as img")
			->from("panamerico_orders as order")
			->join("panamerico_users as user","ON user.use_id = order.order_vendedor")
			->join("panamerico_orders_details as details", "ON details.orders_num = order.order_num")
			->join("panamerico_ads as ads", "ON details.orders_produto = ads.ad_id")
			->join("panamerico_ads_images as img", "ON details.orders_produto = img.ad_id")
			->where("order.order_cliente", $id)
			->order_by("order.order_data", "desc")
		->group_by("order.order_num");
		$query = $this->db->get()->result_array();
		return $query;
	}


	public function getOrderByVendedor($id)
	{
		$this->db->select("SUM(details.orders_qtd) AS quantidade, order.order_valor as soma, user.use_name, order.order_num, order.order_situacao, order.order_valor_comissao AS comissao ,order.order_data, order.order_valor, order.order_frete as frete, order.order_tipo as tipo, ads.ad_name, order.order_reclamacao, ads.ad_tipo as order_tipo, img.ads_img_file as img")
			->from("panamerico_orders as order")
			->join("panamerico_users as user","ON user.use_id = order.order_vendedor")
			->join("panamerico_orders_details as details", "ON details.orders_num = order.order_num")
			->join("panamerico_ads as ads", "ON details.orders_produto = ads.ad_id")
			->join("panamerico_ads_images as img", "ON details.orders_produto = img.ad_id")
			->where("order.order_vendedor", $id)
			->order_by("order.order_data", "desc")
		->group_by("order.order_num");
		$query = $this->db->get()->result_array();
		return $query;
    }

	public function getAllOrder()
	{
		$this->db->select("order_num, order_situacao");
		$this->db->order_by("order_data", "desc");
		$this->db->group_by("order_num");
		return $this->db->get("panamerico_orders")->result_array();
	}

	public function getByOrder($order)
	{
		$this->db
		->select("P.ads_img_path, P.ads_img_file, P.ad_id, use_name, order_num, order_situacao, order_data, order_valor, order_frete, details.orders_qtd as qtd, details.orders_valor_unit as unitario, ad_name, order_cliente, order_vendedor, order_reclamacao, Y.ad_slug")
				->join("panamerico_users","ON use_id = order_vendedor")
				->join("panamerico_orders_details AS details","ON details.orders_num = order_num")
				->join("panamerico_ads Y","ON Y.ad_id = details.orders_produto")
				->join("panamerico_ads_images AS P","ON P.ad_id = details.orders_produto")
			->where("order_num",$order)
			->group_by("P.ad_id")
		->order_by("order_data", "desc");
		$query = $this->db->get("panamerico_orders")->result_array();
		return $query;
	}

	public function addReclamacoes($reclamacao){
		$this->db->insert("panamerico_reclamacoes", $reclamacao);
	}

	public function getReclamacoes(){
		$this->db->select("reclama_pedido, reclama_cliente, x.use_name AS nome_cliente, y.use_name AS nome_vendedor, reclama_vendedor, reclama_data, reclama_encerrado");
		$this->db->join("panamerico_users x", "ON x.use_id = reclama_cliente");
		$this->db->join("panamerico_users y", "ON y.use_id = reclama_vendedor");
		$this->db->order_by("reclama_data", "desc");
		return $this->db->get("panamerico_reclamacoes")->result_array();
	}
}