 <?php if ($this->session->userdata('login')) { ?>
 	
	<form action="<?= base_url('Duvidas/perguntar')?>" method="POST">
		<div class="form-group"> <!-- Message input !-->
			<label class="control-label " for="message_id"><h3 id="show-descri-js">Pergunte ao vendedor</h3></label>
			<p>Escreva sua pergunta diretamente ao vendedor<p>
			<input type="hidden" name="produto" value="<?php echo $ad->ad_id ?>">
			<input type="hidden" name="vendedor" value="<?php echo $ad->use_id ?>">
			<textarea placeholder="Escreva sua pergunta..." class="form-control" id="message_id" name="message" rows="5"></textarea>
		</div>
		
		<div class="form-group"> <!-- Submit button !-->
			<button class="btn btn-primary " name="submit" type="submit">Perguntar</button>
		</div>
	</form>

 <?php } else { ?>

		<div class="form-group"> <!-- Message input !-->
			<label class="control-label " for="message_id"><h3 id="show-descri-js">Pergunte ao vendedor</h3></label>
			<p>Escreva sua pergunta diretamente ao vendedor<p>
			<input type="hidden" name="produto" value="<?php echo $ad->ad_id ?>">
			<input type="hidden" name="vendedor" value="<?php echo $ad->use_id ?>">
			<textarea placeholder="Escreva sua pergunta..." class="form-control" id="message_id" name="message" rows="5"></textarea>
		</div>
		
		<div class="form-group"> <!-- Submit button !-->
			<button data-toggle="modal" data-target="#modal-required" class="btn btn-primary " name="submit" type="submit">Perguntar</button>
		</div>

<?php }?>