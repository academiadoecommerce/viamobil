<style>
        .carousel {
            margin: 50px auto;
            padding: 0 70px;
        }
        .carousel .item {
            color: #747d89;
            min-height: 350px;
            text-align: center;
            overflow: hidden;
        }
        .carousel .thumb-wrapper {
            padding: 25px 15px;
            background: #fff;
            border-radius: 6px;
            text-align: center;
            position: relative;
            box-shadow: 0 2px 3px rgba(0,0,0,0.2);
        }
        .carousel .item .img-box {
            height: 120px;
            margin-bottom: 20px;
            width: 100%;
            position: relative;
        }
        .carousel .item img {
            max-width: 100%;
            max-height: 100%;
            display: inline-block;
            position: absolute;
            bottom: 0;
            margin: 0 auto;
            left: 0;
            right: 0;
        }
        .carousel .item h4 {
            height: 55px;
            font-size: 18px;
        }
        .carousel .item h4, .carousel .item p, .carousel .item ul {
            margin: auto;
        }
        .carousel .thumb-content .btn {
            color: #7ac400;
            font-size: 11px;
            text-transform: uppercase;
            font-weight: bold;
            background: none;
            border: 1px solid #7ac400;
            padding: 6px 14px;
            margin-top: 5px;
            line-height: 16px;
            border-radius: 20px;
        }
        .carousel .thumb-content .btn:hover, .carousel .thumb-content .btn:focus {
            color: #fff;
            background: #7ac400;
            box-shadow: none;
        }
        .carousel .thumb-content .btn i {
            font-size: 14px;
            font-weight: bold;
            margin-left: 5px;
        }
        .carousel .carousel-control {
            height: 44px;
            width: 40px;
            background: #3b4bc4;
            margin: auto 0;
            border-radius: 4px;
            opacity: 0.8;
        }
        .carousel .carousel-control:hover {
            background: #3435bf;
            opacity: 1;
        }
        .carousel .carousel-control i {
            font-size: 36px;
            position: absolute;
            top: 50%;
            display: inline-block;
            margin: -19px 0 0 0;
            z-index: 5;
            left: 0;
            right: 0;
            color: #fff;
            text-shadow: none;
            font-weight: bold;
        }
        .carousel .item-price {
            font-size: 13px;
            padding: 2px 0;
        }
        .carousel .item-price strike {
            opacity: 0.7;
            margin-right: 5px;
        }
        .carousel .carousel-control.left i {
            margin-left: -2px;
        }
        .carousel .carousel-control.right i {
            margin-right: -4px;
        }
        .carousel .carousel-indicators {
            bottom: -50px;
        }
        .carousel-indicators li, .carousel-indicators li.active {
            width: 10px;
            height: 10px;
            margin: 4px;
            border-radius: 50%;
            border-color: transparent;
        }
        .carousel-indicators li {
            background: rgba(0, 0, 0, 0.2);
        }
        .carousel-indicators li.active {
            background: rgba(0, 0, 0, 0.6);
        }
        .carousel .wish-icon {
            position: absolute;
            right: 10px;
            top: 10px;
            z-index: 99;
            cursor: pointer;
            font-size: 16px;
            color: #abb0b8;
        }
        .carousel .wish-icon .fa-heart {
            color: #ff6161;
        }
        .star-rating li {
            padding: 0;
        }
        .star-rating i {
            font-size: 14px;
            color: #ffc000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".wish-icon i").click(function(){
                if($(this).hasClass("far")){
                    $(this).removeClass("far fa-heart");
                    $(this).addClass("fa fa-heart");
                } else {
                    $(this).removeClass("fa fa-heart");
                    $(this).addClass("far fa-heart");
                }

            });
        });
    </script>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4 class="mt-5">ANÚNCIOS EM DESTAQUE</h4>
            <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">
                    <? //for($i = 0; $i < 3; $i++): ?>
                        <div class="item carousel-item <?=$i==0 ? 'active' : ''?>">
                        <div class="row">
                            <? //for($i2 = 0; $i2 < 4; $i2++):
                            foreach($bestseller as $item):?>
                            <div class="col-sm-3">
                                <div class="thumb-wrapper">
                                    <span class="wish-icon"><i class="far fa-heart"></i></span>
                                    <div class="img-box">
                                        <img src="/admin/uploads/ads/fd78aca5f665864c3c91f2f9f9ac64f1.jpg" class="img-responsive img-fluid" alt="">
                                    </div>
                                    <div class="thumb-content">
                                        <h4>Mochila Teste Produto com Paçoca</h4>
                                        <div class="star-rating">
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            </ul>
                                        </div>
                                        <p class="item-price"><strike>R$315,00</strike> <b>R$250,00</b></p>
                                        <a href="#" class="btn btn-primary">Ver detalhes</a>
                                    </div>
                                </div>
                            </div>
                            <? endfor ?>
                        </div>
                    </div>
                    <? endfor ?>
                </div>
                <!-- Carousel controls -->
                <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
