<div class="container">
    <div class="row center mt-4 p-2 shadow-sm bg-white">

        <div class="col-md-4">
            <i class="far fa-credit-card m-3" style=" float: left; font-size: 40px;"></i>
            <div class="info-subb-item mt-3">
                <h3 class="porto-sicon-title" style="font-weight:600;font-size:14px;color:#465157;">PAGUE COM CARTÃO OU BOLETO</h3>
                <p style="font-weight:300;font-size:13px;color:#839199;">Com o Wirecard você paga parcelado ou à vista.</p>
            </div>
        </div>

        <div class="col-md-4">
            <i class="fas fa-truck m-3" style=" float: left; font-size: 40px;"></i>
            <div class="info-subb-item mt-3">
                <h3 class="porto-sicon-title" style="font-weight:600;font-size:14px;color:#465157;">ENVIO PARA TODO O BRASIL</h3>
                <p style="font-weight:300;font-size:13px;color:#839199;">Consulte os valores com o vendedor.</p>
            </div>
        </div>

        <div class="col-md-4">
            <i class="fa fa-box m-3" style=" float: left; font-size: 40px;"></i>
            <div class="info-subb-item mt-3">
                <h3 class="porto-sicon-title" style="font-weight:600;font-size:14px;color:#465157;">COMPRE NO VAREJO OU ATACADO</h3>
                <p style="font-weight:300;font-size:13px;color:#839199;">Pague menos comprando em mais quantidade</p>
            </div>
        </div>

    </div>
</div>
