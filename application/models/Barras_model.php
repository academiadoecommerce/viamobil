<?php
/**
 * Created by PhpStorm.
 * User: Vinícius
 * Date: 05/10/2016
 * Time: 21:42
 */

/**
 * @property CI_DB_query_builder $db
 * @property Main_model          $main_model
 */
class Barras_model extends CI_Model
{

	public function get($cep){
		$fino = 1;
		$largo = 3;
		$altura = 50;
		
		$barcodes[0] = '00110';
		$barcodes[1] = '10001';
		$barcodes[2] = '01001';
		$barcodes[3] = '11000';
		$barcodes[4] = '00101';
		$barcodes[5] = '10100';
		$barcodes[6] = '01100';
		$barcodes[7] = '00011';
		$barcodes[8] = '10010';
		$barcodes[9] = '01010';
		
		for($f1 = 9; $f1 >= 0; $f1--){
			for($f2 = 9; $f2 >= 0; $f2--){
				$f = ($f1*10)+$f2;
				$texto = '';
				for($i = 1; $i < 6; $i++){
					$texto .= substr($barcodes[$f1], ($i-1), 1).substr($barcodes[$f2] ,($i-1), 1);
				}
				$barcodes[$f] = $texto;
			}
		}
		$barras .= "<center>";
		$barras  = '<img src="https://v2.soatacado.com/assets/img/p.png"  width="'.$fino.'" height="'.$altura.'" border="0" />';
		$barras .= '<img  src="https://v2.soatacado.com/assets/img/b.png"  width="'.$fino.'" height="'.$altura.'" border="0" />';
		$barras .= '<img  src="https://v2.soatacado.com/assets/img/p.png"  width="'.$fino.'" height="'.$altura.'" border="0" />';
		$barras .= '<img  src="https://v2.soatacado.com/assets/img/b.png"  width="'.$fino.'" height="'.$altura.'" border="0" />';
		
		$barras .= '<img ';
		
		$texto = $cep;
		
		if((strlen($texto) % 2) <> 0){
			$texto = '0'.$texto;
		}
		
		while(strlen($texto) > 0){
			$i = round(substr($texto, 0, 2));
			$texto = substr($texto, strlen($texto)-(strlen($texto)-2), (strlen($texto)-2));
			
			if(isset($barcodes[$i])){
				$f = $barcodes[$i];
			}
			
			for($i = 1; $i < 11; $i+=2){
				if(substr($f, ($i-1), 1) == '0'){
  					$f1 = $fino ;
  				}else{
  					$f1 = $largo ;
  				}
  				
  				$barras .= 'src="https://v2.soatacado.com/assets/img/p.png" width="'.$f1.'" height="'.$altura.'" border="0">';
  				$barras .= '<img ';
  				
  				if(substr($f, $i, 1) == '0'){
					$f2 = $fino ;
				}else{
					$f2 = $largo ;
				}
				
				$barras .= 'src="https://v2.soatacado.com/assets/img/b.png" width="'.$f2.'" height="'.$altura.'" border="0">';
				$barras .= '<img ';
			}
		}
		$barras .= 'src= "https://v2.soatacado.com/assets/img/p.png" width="'.$largo.'" height="'.$altura.'" border="0" />';
		$barras .= '<img src="https://v2.soatacado.com/assets/img/b.png" width="'.$fino.'" height="'.$altura.'" border="0" />';
		$barras .= '<img src="https://v2.soatacado.com/assets/img/p.png" width="1" height="'.$altura.'" border="0" />';
		$barras .= "</center>";
		$barras .= '<br>';

		return $barras;
	}
}