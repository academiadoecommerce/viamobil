<?php

require 'vendor/autoload.php';

use Moip\Moip;
use Moip\Auth\OAuth;

/**
 * Lida com comunicação com o sistema do wirecard, realizando cadastro de usuario
 */
class Vendedor_Model extends CI_Model
{
	protected $moip;

	public function __construct()
	{
		parent::__construct();
		$this->moip = new Moip(new OAuth(accessToken), Moip::ENDPOINT_PRODUCTION);
	}

	public function getVendedor($id)
	{
		$this->db->select('use_name, use_email, use_cpf, use_phone, use_celular, use_cep, use_address, use_address_number, cit_name, use_region, use_neighborhood, sta_initials, use_nascimento');
		$this->db->join('panamerico_states', 'sta_id = use_state');
		$this->db->join('panamerico_cities', 'cit_id = use_city');
		$this->db->where('use_id', $id);
		return $this->db->get("panamerico_users")->result_array();
	}

	public function setMoipId($id, $moipId)
	{
		$this->db->set('use_moip_id_vend', $moipId);
		$this->db->where('use_id', $id);
		return $this->db->update("panamerico_users");
	}

	public function getMoip($id)
	{
		$this->db->select('use_moip_id_vend');
		$this->db->where('use_id', $id);
		$query = $this->db->get('panamerico_users');
		$query = $query->result();

		if ($query) {
			return $query[0]->use_moip_id_vend;
		} else {
			return false;
		}
	}

	public function cadastrar_vendedor_moip($id)
	{
		$vendedores = $this->vendedor_model->getVendedor($id);

		foreach ($vendedores as $vend) {
			$nome_vendedor = $vend['use_name'];
			$cpf_vendedor = $vend['use_cpf'];
			$email_vendedor = $vend['use_email'];
			$cep_vendedor = $vend['use_cep'];
			$estado_vendedor = $vend['sta_initials'];
			$cidade_vendedor = $vend['cit_name'];
			$endereco_vendedor = $vend['use_address'];
			$endereco_number_vendedor = $vend['use_address_number'];
			$bairro_vendedor = $vend['use_neighborhood'];
			$data_nascimento = $vend['use_nascimento'];

			if (!empty($vend['use_phone'])) {
				$fone_vendedor = str_replace(['-', ' ', '(', ')'], '', $vend['use_phone']);
				$fone_vendedor = substr($fone_vendedor,2,9);
				$ddd_vendedor = substr($fone_vendedor, 0, 2);
			}

			if (!empty($vend['use_celular'])) {
				$celular_vendedor = $vend['use_celular'];
			}
		}

		try {
			$account = $this->moip->accounts()
				->setName($nome_vendedor)
				->setLastName('De Tal')
				->setEmail($email_vendedor)
				->setBirthDate($data_nascimento)
				->setTaxDocument($cpf_vendedor)
				->setType('MERCHANT')
				->setPhone($ddd_vendedor, $fone_vendedor, "55")
				->addAddress($endereco_vendedor, $endereco_number_vendedor, "teste", $cidade_vendedor, $estado_vendedor, $cep_vendedor, $bairro_vendedor)
				->setTransparentAccount(false)
				//  ->setCompanyName('Empresa Teste', 'Teste Empresa ME')
				//  ->setCompanyOpeningDate('2011-01-01')
				//  ->setCompanyPhone(11, 66558899, 55)
				//  ->setCompanyTaxDocument('69086878000198')
				//  ->setCompanyAddress('Rua de teste 2', 123, 'Bairro Teste', 'Sao Paulo', 'SP', '01234567', 'Apt. 23', 'BRA')
				// ->setCompanyMainActivity('82.91-1/00', 'Atividades de cobranças e informações cadastrais')
				->create();
		} catch (Exception $e) {
			 printf($e->__toString());
			$this->session->set_flashdata("msg", "Não foi possível completar seu cadastro, verifique se seu e-mail ou cpf já foram cadastrado no Wirecard!");
			redirect($_SERVER['HTTP_REFERER']);
			exit;
		}

		$this->vendedor_model->setMoipId($id, $account->getId());
		return $account->getPasswordLink();
	}
}