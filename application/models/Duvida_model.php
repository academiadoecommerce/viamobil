<?php

class Duvida_Model extends CI_Model{

	public function getDuvidas($id, $tipo){
		//funcao responsavel por listar as duvidas que o vendedor possui
		$this->db->select("ad_slug, ad_name, ad_price, duvida_id,use_name, duvida_cliente, duvida_data_inicio, duvida_status, duvida_id_produto, y.ads_img_file as img");

		if($tipo == "lojista"){
			$this->db->join("panamerico_users",'duvida_cliente = use_id');
		}else{
			$this->db->join("panamerico_users",'duvida_vendedor = use_id');
		}

		$this->db->join("panamerico_ads",'ad_id = duvida_id_produto');
		$this->db->join("panamerico_ads_images AS y", "ON duvida_id_produto = y.ad_id");
		$this->db->join("panamerico_duvidas_content", "ON duvida_id = duvidas_content_fk");

		if($tipo == "lojista"){
			$this->db->where("duvida_vendedor",$id);
			$this->db->where("duvida_status !=","2");
			//Se for igual a lojista entao o STATUS encerrado não deve aparecer (IGUAL AO MERCADO LIVRE)
		}else{
			$this->db->where("duvida_cliente",$id);
		}

		$this->db->group_by("duvida_id");
		$this->db->order_by("duvida_data_inicio", "desc");
		return $this->db->get("panamerico_duvidas")->result_array();
	}

	public function validaChat($order, $id, $tipo){
		$this->db->select("*");

		if($tipo == "cliente"){
			$this->db->where("duvida_cliente",$id);
		}else{
			$this->db->where("duvida_vendedor",$id);
		}
		$this->db->where('duvida_id', $order);
        return $this->db->get("panamerico_duvidas")->result();

	}

	public function getContent($order){
		$this->db->select('duvida_id, duvidas_content_message, duvidas_content_tipo, duvidas_content_hora, duvida_cliente, duvida_vendedor, duvida_data_inicio, x.use_name AS cliente, y.use_name AS vendedor');
		$this->db->join('panamerico_duvidas', 'duvidas_content_fk = duvida_id');
		$this->db->join('panamerico_users AS x', 'x.use_id = duvida_cliente');
		$this->db->join('panamerico_users AS y', 'y.use_id = duvida_vendedor');
        $this->db->where('duvidas_content_fk', $order);
        return $this->db->get("panamerico_duvidas_content")->result_array();

	}

	public function duvidasContent($content){
		$this->db->insert("panamerico_duvidas_content", $content);
	}

	public function validaDuvida($cliente, $id){
		$this->db->select("duvida_id");
		$this->db->where("duvida_cliente",$cliente);
		$this->db->where("duvida_id",$id);
		return $this->db->get("panamerico_duvidas")->result();
	}

	public function encerraDuvida($id){
		$this->db->set('duvida_status',2);
		$this->db->set('duvida_data_fim',date('Y-m-d'));
		$this->db->where('duvida_id',$id);
    	return $this->db->update("panamerico_duvidas");
	}

	public function setDuvida($content){
		$this->db->insert("panamerico_duvidas", $content);
		return $this->db->insert_id();
	}

	public function setMessageDuvida($content){
		$this->db->insert("panamerico_duvidas_content", $content);
	}

	public function getDuvidaCliente($id){
		$this->db->select('use_name, use_email, ad_name');
		$this->db->join('panamerico_users', 'duvida_cliente = use_id');
		$this->db->join('panamerico_ads', 'duvida_id_produto = ad_id');
        $this->db->where('duvida_id', $id);
        return $this->db->get("panamerico_duvidas")->result_array();

	}

	public function getAll($id){
		$this->db->select("duvidas_content_message, duvidas_content_tipo, duvidas_content_fk");
		$this->db->join("panamerico_duvidas", "ON duvida_id = duvidas_content_fk");
		$this->db->where("duvida_cliente",$id);
		$this->db->order_by("duvida_data_inicio", "desc");
		return $this->db->get("panamerico_duvidas_content")->result_array();
	}

	public function getByProduto($id, $start, $page){
		$this->db->select("duvidas_content_message, duvidas_content_tipo, duvidas_content_fk, duvida_cliente, duvidas_content_hora, duvida_data_fim");
		$this->db->join("panamerico_duvidas", "ON duvida_id = duvidas_content_fk");
		$this->db->where("duvida_id_produto",$id);
		$this->db->limit($start, $page);
		$this->db->order_by("duvida_data_inicio", "desc");
		return $this->db->get("panamerico_duvidas_content")->result_array();//->num_rows();
	}

	public function getByProdutoCount($id, $start, $page){
		$this->db->select("duvidas_content_message, duvidas_content_tipo, duvidas_content_fk, duvida_cliente, duvidas_content_hora, duvida_data_fim");
		$this->db->join("panamerico_duvidas", "ON duvida_id = duvidas_content_fk");
		$this->db->where("duvida_id_produto",$id);
		$this->db->limit($start, $page);
		$this->db->order_by("duvida_data_inicio", "desc");
		return $this->db->get("panamerico_duvidas_content")->num_rows();
	}

	public function getByProdutoPage($id, $start, $page){
		$this->db->select("duvidas_content_message, duvidas_content_tipo, duvidas_content_fk, duvida_cliente, duvidas_content_hora, duvida_data_fim");
		$this->db->join("panamerico_duvidas", "ON duvida_id = duvidas_content_fk");
		$this->db->where("duvida_id_produto",$id);
		$this->db->limit($start, $page);
		$this->db->order_by("duvida_data_inicio", "desc");
		return $this->db->get("panamerico_duvidas_content")->result();
	}
}

?>
