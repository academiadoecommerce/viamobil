<?php
/**
 * Created by PhpStorm.
 * User: Vinícius
 * Date: 05/10/2016
 * Time: 21:42
 */

/**
 * @property CI_DB_query_builder $db
 * @property Main_model          $main_model
 */
class Checkout_Model extends CI_Model{

	public function getComissao(){
		$this->db->select('cfg_comissao');
		$query = $this->db->get('panamerico_config');
        $query = $query->result();

        if ($query) {
            return $query[0]->cfg_comissao;
        } else {
            return false;
        }
	}


	public function getIdMoipGerente(){
		$this->db->select('cfg_idmoip');
		$query = $this->db->get('panamerico_config');
        $query = $query->result();

        if ($query) {
            return $query[0]->cfg_idmoip;
        } else {
            return false;
        }
	}
}

