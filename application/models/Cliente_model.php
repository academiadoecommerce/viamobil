<?php
/**
 * Created by PhpStorm.
 * User: Vinícius
 * Date: 05/10/2016
 * Time: 21:42
 */

/**
 * @property CI_DB_query_builder $db
 * @property Main_model          $main_model
 */
class Cliente_Model extends CI_Model{

	public function getCliente($id){
 		$this->db->select('use_name, use_email, use_cpf, use_phone, use_celular, use_cep, use_address, use_address_number, cit_name, use_region, use_neighborhood, sta_name, use_nascimento');
 		$this->db->join('panamerico_states', 'sta_id = use_state');
 		$this->db->join('panamerico_cities', 'cit_id = use_city');
        $this->db->where('use_id', $id);
        return $this->db->get("panamerico_users")->result_array();
	}

	public function setMoipId($id, $moipId){
		$this->db->set('use_moip_id_cli',$moipId);
		$this->db->where('use_id',$id);
    	return $this->db->update("panamerico_users");
	}

	public function getMoip($id){
		$this->db->select('use_moip_id_cli');
		$this->db->where('use_id', $id);
		$query = $this->db->get('panamerico_users');
        $query = $query->result();

        if ($query) {
            return $query[0]->use_moip_id_cli;
        } else {
            return false;
        }
	}
}

