<?php
/**
 * Created by PhpStorm.
 * User: Vinícius
 * Date: 05/10/2016
 * Time: 21:42
 */

/**
 * @property CI_DB_query_builder $db
 * @property Main_model          $main_model
 */
class Reclamacoes_model extends CI_Model{


	public function getContent($order){
		$this->db->select('reclama_content_descricao, reclama_content_tipo, reclama_hora, reclama_cliente, reclama_vendedor, x.use_name AS cliente, y.use_name AS vendedor');
		$this->db->join('panamerico_reclamacoes', 'reclama_pedido = reclama_content_order');
		$this->db->join('panamerico_users AS x', 'x.use_id = reclama_cliente');
		$this->db->join('panamerico_users AS y', 'y.use_id = reclama_vendedor');
        $this->db->where('reclama_content_order', $order);
        return $this->db->get("panamerico_reclamacoes_content")->result_array();

	}

	public function validaChat($order, $id, $tipo){
		$this->db->select("*");
		
		if($tipo == "cliente"){
			$this->db->where("reclama_cliente",$id);
		}else{
			$this->db->where("reclama_vendedor",$id);
		}
		$this->db->where('reclama_pedido', $order);
        return $this->db->get("panamerico_reclamacoes")->result();
		
	}

}