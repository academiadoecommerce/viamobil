<?php

class Pedido_Model extends CI_Model{

	public function addOrder($data)
	{
		$this->db->insert('panamerico_orders', $data);
		return $this->db->insert_id();
	}

	public function addOrderDetais($detalhes){
		$this->db->insert('panamerico_orders_details', $detalhes);
        return $this->db->insert_id();
	}

	public function addBoleto($link, $order){
		$this->db->where("order_num", $order);
		$this->db->set("order_boleto_link",$link);
		$this->db->update("panamerico_orders");
	}

	public function getOrderByCliente($id)
	{
		$this->db->select("SUM(details.orders_qtd) AS quantidade, order.order_valor as soma, order.order_vendedor as id_vendedor, order.order_cliente as id_cliente, vendedor.use_name as nome_vendedor, vendedor.use_email as email_vendedor, cliente.use_name as nome_cliente, cliente.use_email as email_cliente, order.order_num, order.order_situacao, order.order_data, order.order_valor, order.order_frete as frete, order.order_tipo as tipo, ads.ad_name, order.order_reclamacao, ads.ad_tipo as order_tipo, img.ads_img_file as img, details.orders_produto as id_produto, details.cor, details.tamanho")
			->from("panamerico_orders as order")
			->join("panamerico_users as vendedor","ON vendedor.use_id = order.order_vendedor")
			->join("panamerico_users as cliente","ON cliente.use_id = order.order_cliente")
			->join("panamerico_orders_details as details", "ON details.orders_num = order.order_num")
			->join("panamerico_ads as ads", "ON details.orders_produto = ads.ad_id")
			->join("panamerico_ads_images as img", "ON details.orders_produto = img.ad_id")
			->where("order.order_cliente", $id)
			->order_by("order.order_data", "desc")
		->group_by("order.order_num");
		$query = $this->db->get()->result_array();
		return $query;
	}


	public function getOrderByVendedor($id)
	{
		$this->db->select("SUM(details.orders_qtd) AS quantidade, details.orders_produto as id_produto, order.order_valor as soma, order.order_cliente as id_usuario, order.order_vendedor as id_vendedor, vendedor.use_name as nome_vendedor, vendedor.use_email as email_vendedor, cliente.use_id as id_cliente, cliente.use_name as nome_cliente, cliente.use_email as email_cliente, order.order_num, order.order_situacao, order.order_valor_comissao AS comissao ,order.order_data, order.order_valor, order.order_frete as frete, order.order_tipo as tipo, ads.ad_name, order.order_reclamacao, ads.ad_tipo as order_tipo, img.ads_img_file as img, details.cor, details.tamanho")
			->from("panamerico_orders as order")
			->join("panamerico_users as vendedor","ON vendedor.use_id = order.order_vendedor")
			->join("panamerico_users as cliente","ON cliente.use_id = order.order_cliente")
			->join("panamerico_orders_details as details", "ON details.orders_num = order.order_num")
			->join("panamerico_ads as ads", "ON details.orders_produto = ads.ad_id")
			->join("panamerico_ads_images as img", "ON details.orders_produto = img.ad_id")
			->where("order.order_vendedor", $id)
			->order_by("order.order_data", "desc")
		->group_by("order.order_num");
		$query = $this->db->get()->result_array();
		return $query;
    }

	public function getAllOrder()
	{
		$this->db->select("order_num, order_situacao");
		$this->db->order_by("order_data", "desc");
		$this->db->group_by("order_num");
		return $this->db->get("panamerico_orders")->result_array();
	}

	public function getByOrder($order)
	{
		$this->db
		->select("order_boleto_link, order_tipo, P.ads_img_path, P.ads_img_file, P.ad_id, vendedor.use_name as nome_vendedor, vendedor.use_email as email_vendedor, order_num, order_situacao, order_data, order_valor, order_frete, details.orders_qtd as qtd, details.orders_valor_unit as unitario, ad_name, order_cliente, order_vendedor, order_reclamacao, Y.ad_slug, order_valor_comissao AS comissao, cliente.use_name as nome_cliente, cliente.use_email as email_cliente")
				->join("panamerico_users as vendedor", "ON vendedor.use_id = order_vendedor")
				->join("panamerico_users as cliente","ON cliente.use_id = order_cliente")
				->join("panamerico_orders_details AS details","ON details.orders_num = order_num")
				->join("panamerico_ads Y","ON Y.ad_id = details.orders_produto")
				->join("panamerico_ads_images AS P","ON P.ad_id = details.orders_produto")
			->where("order_num",$order)
			->group_by("P.ad_id")
		->order_by("order_data", "desc");
		$query = $this->db->get("panamerico_orders")->result_array();
		return $query;
	}

	public function getSoma($order)
	{
		$this->db
			->select("order_valor as soma")
				->join("panamerico_users","ON use_id = order_vendedor")
				->join("panamerico_orders_details AS details","ON details.orders_num = order_num")
				->join("panamerico_ads","ON ad_id = details.orders_produto")
			->where("order_num",$order)
		->order_by("order_data", "desc");
		$query = $this->db->get("panamerico_orders")->result_array();
		return $query;
	}

	/*public function getByOrder($order){
		$this->db->select("use_name, order_num, order_situacao, order_data, order_valor, order_item_qtd, order_valor_unitario, ad_name, order_cliente, order_vendedor, order_reclamacao");
      	$this->db->join("panamerico_users","ON use_id = order_vendedor");
      	$this->db->join("panamerico_ads","ON ad_id = order_item_cod");
		$this->db->where("order_num",$order);
		$this->db->order_by("order_data", "desc");
		return $this->db->get("panamerico_orders")->result_array();
	}*/

	public function addReclamacoes($reclamacao)
	{
		$this->db->insert("panamerico_reclamacoes", $reclamacao);
	}

	public function reclamacoesContent($content)
	{
		$this->db->insert("panamerico_reclamacoes_content", $content);
	}

	public function updateStatus($id)
	{
		$this->db->set('order_reclamacao',1);
		$this->db->where('order_num',$id);
    	return $this->db->update("panamerico_orders");
	}

	public function updateSituacao($id, $situacao)
	{
		$this->db->set('order_situacao',$situacao);
		$this->db->where('order_num',$id);
    	return $this->db->update("panamerico_orders");
	}

	public function encerra($id)
	{
		$this->db->set('reclama_encerrado',2);
		$this->db->where('reclama_pedido',$id);
    	return $this->db->update("panamerico_reclamacoes");
	}

	public function encerraReclamacaoPedido($id)
	{
		$this->db->set('order_reclamacao',2);
		$this->db->where('order_num',$id);
    	return $this->db->update("panamerico_orders");
	}

	public function getReclamacoes($id)
	{
		$this->db->select("reclama_pedido, reclama_data, reclama_encerrado");
		$this->db->where("reclama_cliente",$id);
		$this->db->order_by("reclama_data", "desc");
		return $this->db->get("panamerico_reclamacoes")->result_array();
	}

	public function getReclamacoesVend($id)
	{
		$this->db->select("reclama_pedido, reclama_data, reclama_encerrado");
		$this->db->where("reclama_vendedor",$id);
		$this->db->order_by("reclama_data", "desc");
		return $this->db->get("panamerico_reclamacoes")->result_array();
	}

	public function validaOrder($id, $pedido){
		$this->db->select("order_num");
		$this->db->where("order_cliente",$id);
		$this->db->where("order_num",$pedido);
		return $this->db->get("panamerico_orders")->result();
	}

	public function getInfoOrders($order){
		$this->db->select("C.use_city, C.use_name AS cliente, C.use_neighborhood AS bairro_cliente, C.use_cep AS cep_cliente, C.use_address AS endereco_cliente, C.use_address_number AS endereco_numero_cliente, O.cit_name AS cidade_cliente, A.sta_id, A.sta_initials as estado_cliente,
			V.use_city, V.use_name AS vendedor, V.use_neighborhood AS bairro_vendedor,V.use_cep AS cep_vendedor, V.use_address AS endereco_vendedor, V.use_address_number AS endereco_numero_vendedor,Z.cit_name AS cidade_vendedor, B.sta_id, B.sta_initials AS estado_vendedor, 
			order_num, order_situacao, order_data, order_valor,
			order_frete, order_reclamacao");
      	$this->db->join("panamerico_users V","ON order_vendedor = V.use_id");
      	$this->db->join("panamerico_users C","ON order_cliente = C.use_id");
      	$this->db->join("panamerico_cities O","ON C.use_city = O.cit_id");
      	$this->db->join("panamerico_cities Z","ON V.use_city = Z.cit_id");
      	$this->db->join("panamerico_states A","ON O.sta_id = A.sta_id");
      	$this->db->join("panamerico_states B","ON Z.sta_id = B.sta_id");
		//$this->db->join("panamerico_ads","ON ad_id = order_item_cod");
		$this->db->where("order_num",$order);
		$this->db->order_by("order_data", "desc");
		return $this->db->get("panamerico_orders")->result_array();
	}

	public function getPeso($order){
		$this->db->select("ad_peso, orders_qtd");
		$this->db->join("panamerico_ads", "ON ad_id = orders_produto");
		$this->db->where("orders_num", $order);
		return $this->db->get("panamerico_orders_details")->result_array();
	}

}

