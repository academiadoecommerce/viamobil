<?php


class Correios_model extends CI_Model{


	public function freteCart($cep_cliente){
		$itens = $this->cart->contents();

		$cep_destino = $cep_cliente;
		//36.5 SC CEP 88070-740

		$peso = 0;
		$comprimento = 0;
		$altura = 0;
		$largura = 0;
		$diametro = 0;
		$volumeTotal = 0;
		$volume = 0;

		foreach($itens as $item){
			$peso += $item['peso'];
			$qtd = $item['qty'];
			$comprimento = $item['comprimento'];
			$altura = $item['altura'];
			$largura = $item['largura'];
			$diametro += $item['diametro'];
			$id_vendedor = $item['vendedor_id'];
			$cep_origem = $item['cep_vendedor'];
			$volume = $comprimento * $largura * $altura;
			$volumeTotal += $volume * $qtd;
		}

		$grauraiz = 3;
		$cubicoVol  = pow($volumeTotal, (1/$grauraiz));

		$soma = $cubicoVol * 3;
		if($soma > 200){
			return " A quantidade definida não é válida";
		}
		//$soma = $comprimento + $altura + $largura;

		/*if($soma > 200){
			$comprimento = 66;
			$altura	= 66;
			$largura = 66;
		}

		if($diametro > 90){
			$diametro = 90;
		}

		if($peso > 40){
			$peso = 40;
		}*/

		$url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?';
		$url .= 'nCdEmpresa=delescorreios&';
		$url .= 'sDsSenha=//spsp2019//&';
		$url .= 'nCdServico=41211&'; //41211  PAC com contrato
		$url .= 'sCepOrigem='.$cep_origem.'&';
		$url .= 'sCepDestino='.$cep_destino.'&';
		$url .= 'nVlPeso='.$peso.'&';
		$url .= 'nCdFormato=1&';
		$url .= 'nVlComprimento='.$cubicoVol.'&';
		$url .= 'nVlAltura='.$cubicoVol.'&';
		$url .= 'nVlLargura='.$cubicoVol.'&';
		$url .= 'nVlDiametro='.$diametro.'&';
		$url .= 'sCdMaoPropria=S&';
		$url .= 'nVlValorDeclarado=0&';
		$url .= 'sCdAavisoRecebimento=N&';
		$url .= 'StrRetorno=xml&';
		$url .= 'nIndicaCalculo=3';

		$xml = simplexml_load_file($url);
		$frete = $xml->cServico;
		$valor = $frete->Valor;
		return $valor;
	}

	public function getCep($id){
 		$this->db->select('use_cep');
        $this->db->where('use_id', $id);
		$query = $this->db->get('panamerico_users')->row();
        if ($query) {
            return $query->use_cep;
        } else {
            return false;
        }
	}

}
