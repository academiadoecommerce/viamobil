<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'main';
$route['404_override'] = 'errors/e404';
$route['translate_uri_dashes'] = FALSE;
$route['quem-somos'] = 'pages/route/quem-somos';

/* Anúncios */
$route['anuncios'] = 'ads';
$route['anuncios/categoria/(:num)'] = 'ads/category/$1';
$route['anuncios/estado/(:any)'] = 'ads/state/$1';
$route['anuncio/(:any)'] = 'ads/route/$1';
$route['anunciar'] = 'announce';

/* Lojas */
$route['lojas'] = 'shops';
$route['loja/(:any)'] = 'shops/route/$1';
$route['loja/categoria/(:num)'] = 'shops/category/$1';
$route['reclamacoes'] = 'Reclamacoes/index';

/* Area do Cliente */
$route['cliente'] = 'profile';
$route['cliente/painel'] = 'profile/dashboard';
$route['cliente/detalhes'] = 'profile/details';
$route['cliente/loja'] = 'profile/shop/edit';
$route['cliente/favoritos'] = 'profile/favorites';
$route['cliente/sair'] = 'login/out';
$route['cliente/depoimento'] = 'profile/testimony';
$route['cliente/chat'] = 'profile/chat/0';

/* Outros */
$route['ajuda/(:any)'] = 'pages/route/$1';
$route['contato'] = 'pages/contact';
$route['depoimentos'] = 'testimonials';
$route['depoimentos/page'] = 'testimonials';
$route['depoimentos/page/(:num)'] = 'testimonials';
$route['manutencao'] = 'maintenance';

/** Chat */
$route['chat'] = 'profile/chat/0';
$route['chat/getChatsByAd/(:num)*'] = 'profile/chat/$1';
$route['chat/listMessages/(:num)'] = 'chat/listMessages/$1';
$route['chat/listChatsByAd/(:num)'] = 'chat/listChatsByAd/$1';
$route['chat/sendMessage'] = 'chat/sendMessage';
$route['registrar'] = 'register/page';

$route['chat_pedido'] = 'Reclamacao/chat';

$route['Cart'] = 'Cart/index';
$route['Finalizar'] = 'Cart/finalzia';
$route['Pedido'] = 'Checkout/criar_pedido/$1';
$route['Sucesso'] = 'Checkout/successOrder';
$route['pedidos'] = 'profile/pedidos';
$route['vendas'] = 'profile/vendas';
$route['minhas_reclamacoes'] = 'profile/reclamacoes';

/** Endereço */
$route['address/getByCep/(:num){8}'] = 'address/getAddressByCep/$1';
$route['reclamacoes_recebidas'] = 'profile/reclamacoesVend';


/*CRON*/
$route['cron'] = 'Cron/updateStatus';

//SUCESSO PEDIDO
$route['checkout/redirect'] = 'Checkout/sucessoPedido';

//chamada de url e redirect para permissao conta moip
$route['cliente/moip-permissao'] = 'profile/getAuthApp';
$route['cliente/moip-permissao/(:any)'] = 'profile/getAuthApp/$1';

$route['duvidas'] = 'Profile/duvidas';
$route['dev'] = 'Correios/precoPrazo';
// RESERVEDS
// $route['default_controller'] = 'welcome';
// $route['404_override'] = 'errors/page_missing';
// $route['translate_uri_dashes'] = FALSE;
