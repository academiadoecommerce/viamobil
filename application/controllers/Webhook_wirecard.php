<?php
require 'vendor/autoload.php';
use Moip\Moip;
use Moip\Auth\OAuth;

class Webhook_wirecard extends CI_Controller {

	protected $moip;

    public function __construct()
    {
        parent::__construct();
    	$this->moip = new Moip(new OAuth(accessToken));
    	// $this->moip = new Moip(new OAuth(token, key), Moip::ENDPOINT_SANDBOX);
    }

	public function index()
    {

	}

	public function cria_webhook()
    {
		$moip = $this->moip;
		try {
		  $notification = $moip->notifications()
		    ->addEvent('ORDER.*')
		    ->addEvent('PAYMENT.*')
		    ->addEvent('REFUND.*')
		    ->setTarget('http://v2.soatacado.com/webhook_wirecard/receber')
		    ->create();
		    echo "<pre>";
		  print_r($notification);
		} catch (Exception $e) {
		  printf($e->__toString());
		}
	}

	public function consulta_preferencia()
    {
		$notification = $this->moip->notifications()->getList();
		echo "<pre>";
		print_r($notification);
	}

	public function receber()
   {
        $get_post = [ "post" => $_POST, "get" => $_GET ];        
        $json = file_get_contents('php://input');
        $json = json_decode( $json, true );
        $insert = json_encode( [ "json" => $json, "array" => $get_post ] );
		// $teste = $this->moip->webhooks()->get();
        $this->db->insert( "webhook", array("webhook_json" => $insert ) );
	}

	public function teste()
   {
		$name = 'teste.html';
		$file = fopen($name, 'a');
		fwrite($file, json_encode($_POST));
		fclose($file);
	}
}

?>

<!-- [data:protected] => stdClass Object
        (
            [events] => Array
                (
                    [0] => ORDER.*
                    [1] => PAYMENT.*
                    [2] => REFUND.*
                )

            [media] => WEBHOOK
            [target] => http://v2.soatacado.com/webhook_wirecard/receber
            [token] => f81de5a5426447ca9e0094459943a5b8
            [id] => NPR-E8HXKICXX6ZU
        ) -->
