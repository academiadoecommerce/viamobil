<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Correios extends CI_Controller {


	public function frete(){

		$cep_destino = $this->input->post("cep_destino");
		$comprimento = $this->input->post("comprimento");
		$largura = $this->input->post("largura");
		$altura = $this->input->post("altura");
		$diametro = $this->input->post("diametro");
		$cep_origem = $this->input->post("cep_origem");
		$peso = $this->input->post("peso");

		$url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?';
		$url .= 'nCdEmpresa=0&';
		$url .= 'sDsSenha=0&';
		$url .= 'nCdServico=41106&';
		$url .= 'sCepOrigem='.$cep_origem.'&';
		$url .= 'sCepDestino='.$cep_destino.'&';
		$url .= 'nVlPeso='.$peso.'&';
		$url .= 'nCdFormato=1&';
		$url .= 'nVlComprimento='.$comprimento.'&';
		$url .= 'nVlAltura='.$altura.'&';
		$url .= 'nVlLargura='.$largura.'&';
		$url .= 'nVlDiametro='.$diametro.'&';
		$url .= 'sCdMaoPropria=S&';
		$url .= 'nVlValorDeclarado=0&';
		$url .= 'sCdAavisoRecebimento=N&';
		$url .= 'StrRetorno=xml&';
		$url .= 'nIndicaCalculo=3';

		$xml = simplexml_load_file($url);
		$frete = $xml->cServico;
		$valor = $frete->Valor;

		echo " Valor: R$".$valor;

	}

}