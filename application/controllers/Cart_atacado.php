<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//require 'vendor/PhpSigep';

class Cart extends CI_Controller
{

    public function index($page = false)
    {
        $this->load->model('correios_model');
        $data = array();
        //$data['valorFrete'] = $this->correios_model->freteCart();
        $data['cep_cliente'] = $this->correios_model->getCep($this->session->userdata('login'));
        if ($this->cart->total_items() > 0)
        $data['produtos'] = $this->cart->contents();

        $this->load->view('app');
        $this->load->view('header');
        $this->load->view("cart", $data);
        $this->load->view('footer');
    }

    public function calcular()
    {
        $this->load->model('correios_model');
        $cep_cliente = $this->input->post("cep_cliente");
        $data['valorFrete'] = $this->correios_model->freteCart($cep_cliente);
        echo $data['valorFrete'];
        $this->session->set_userdata('cep', $cep_cliente);
    }

    public function add($id, $frete, $qtd = 1, $cor, $tamanho, $page = false)
    {
        $prod = $this->ads_model->get_prod_to_cart($id);

        $qtd = $this->input->get('qtd') ?? $qtd ?? 1;

        if ($prod->ad_tipo == "atacado" && $qtd > $prod->ad_qtdmin) {
            $this->session->set_flashdata('danger', 'Para seguir com a compra clique em Comprar em atacado.');
            redirect($this->agent->referrer());
            die();
        }

        $preco = $prod->ad_price;

        if ($this->cart->total_items() > 0) {
            $session = $this->session->userdata('primeiro-vendedor');
            if ($prod->vendedor_id == $session) {

                //popula array de dados do produto
                $data = array(
                    'id' => $prod->ad_id,
                    'qty' => $qtd,
                    'price' => $preco,
                    'name' => $prod->ad_name,
                    'qtd_min' => $prod->ad_qtdmin,
                    'vendedor_id' => $prod->vendedor_id,
                    'img' => $prod->img,
                    'vendedor' => $prod->vendedor_name,
                    'peso' => $prod->ad_peso,
                    'altura' => $prod->ad_altura,
                    'largura' => $prod->ad_largura,
                    'comprimento' => $prod->ad_comprimento,
                    'diametro' => $prod->ad_diametro,
                    'cep_vendedor' => $prod->use_cep,
                    'tipo' => $prod->ad_tipo,
                    'max' => $prod->ad_qtdmax,
                    "cor" => $this->input->get('cor') ?? null,
                    "tamanho" => $this->input->get('tamanho') ?? null
                );

                //adiciona produto ao carrinho de session
                $this->cart->insert($data);

                redirect('/cart');
            } else {
                $this->session->set_flashdata('danger', 'Não é  possível adicionar ao carrinho produtos de vendedores diferentes');
                redirect($this->agent->referrer());
            }

        } else {
            $data = array(
                'id' => $prod->ad_id,
                'qty' => $qtd,
                'price' => $preco,
                'name' => $prod->ad_name,
                'qtd_min' => $prod->ad_qtdmin,
                'vendedor_id' => $prod->vendedor_id,
                'img' => $prod->img,
                'vendedor' => $prod->vendedor_name,
                'peso' => $prod->ad_peso,
                'altura' => $prod->ad_altura,
                'largura' => $prod->ad_largura,
                'comprimento' => $prod->ad_comprimento,
                'diametro' => $prod->ad_diametro,
                'cep_vendedor' => $prod->use_cep,
                'tipo' => $prod->ad_tipo,
                'max' => $prod->ad_qtdmax,
                "cor" => $this->input->get('cor') ?? null,
                "tamanho" => $this->input->get('tamanho') ?? null
                //'options' => array('qtdmin' => 'L', 'Color' => 'Red')
            );
            $this->cart->insert($data);
            $this->session->set_userdata('primeiro-vendedor', $prod->vendedor_id);
            redirect('/cart');
        }
    }

    public function atualizar()
    {
        $this->load->model('correios_model');
        $carrinho = array();
        $cars = $this->input->post();
        foreach ($cars as $item) {

            /*if($item['quantidade_item'] > $item['maximo']){
                echo "erro";
                die();
            }*/

            $carrinho[] = array(
                'rowid' => $item['rowid'],
                'qty' => $item['quantidade_item']
            );
        }
        $this->cart->update($carrinho);

        if ($this->input->post('json_req')) {
            $data['total_qtd'] = $this->cart->total_items();
            header('Content-Type: application/json');
            if ($this->session->userdata('login'))
                $data['cep'] = $this->correios_model->getCep($this->session->userdata('login'));
            elseif ($this->get_cep_flashdata())
                $data['cep'] = $this->get_cep_flashdata();
            // else return json_encode(array('status' => 'error', 'type' => 'cep'));
            $data['status'] = "success";
            $data['valor_total'] = $this->cart->total();
            echo json_encode($data);
            exit;
        }
        redirect('Cart');
    }

    public function finaliza()
    {
        $this->load->view('app');
        $this->load->view('header');
        $this->load->view("finalizar");
    }

    public function limpar()
    {
        $this->cart->destroy();
        redirect('/cart');
        exit;
    }

    public function remove($rowid)
    {
        $this->cart->remove($rowid);
        redirect('/cart');
        exit;
    }

    public function get_cep_flashdata()
    {
        return $this->session->userdata('cep');
    }

}
