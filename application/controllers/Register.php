<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Register. Handle with user register and swap of type of
 * account client / seller
 */

class Register extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
	}

	public function index()
	{
		$data['modal_title'] = "Cadastre-se grátis";
		$data['modal_size'] = "small";
	}

	/**
	 * method para quando o usuário mudar de cliente para lojista 
	 * @param method post
	 * @return redirect to home page
	 */
	public function lojista()
	{
		$data = array(
			'use_cpf' => $this->input->post('cpfCnpj'),
			'use_website' => $this->input->post('urlSite'),
			'use_type' => 'lojista',
			'use_status' => 2
		);
		$this->user_model->update($data);
		$email = $this->user_model->info("use_email");
		$user = $this->session->userdata('login');
		$this->email_registro($user, "lojista", $email);

		$this->session->set_flashdata('register_modal', 'pendente');
		redirect('/', 'location');
	}

	/**
	 * Registra usuários
	 * @param required post
	 */
	public function insert()
	{
		$this->emailVerify($this->input->post('email'));
		$name 			 =	validate_name($this->input->post('name'));
		$email 			 = 	$this->input->post('email');
		$password 		 =	md5($this->input->post('password'));
		$cpfCnpj  		 = 	$this->input->post('cpfCnpj');
		$cep 	  		 = 	$this->input->post('cep');
		$endereco 		 = 	$this->input->post('address');
		$number_address  = 	$this->input->post('number_address');
		$bairro   		 = 	$this->input->post('neighborhood');
		$cidade   		 = 	$this->input->post('city');
		$estado   		 = 	$this->input->post('state');
		$site   		 = 	$this->input->post('urlSite');
		$tipo			 =	$this->input->post('use_type');
		$telefone		 =	$this->input->post('telefone');
		$data_nascimento =  $this->input->post('data_nascimento');
		$id_wirecard 	 =	$this->input->post('wirecard_id');
		$possuiconta	 =	$this->input->post('possui_conta_wirecard');

		$data = array(
			'use_name' 			 => 	$name,
			'use_email' 		 => 	$email,
			'use_phone' 		 => 	$telefone,
			'use_password' 		 => 	$password,
			'use_cep' 			 => 	$cep,
			'use_address' 		 => 	$endereco,
			'use_address_number' => 	$number_address,
			'use_neighborhood'	 => 	$bairro,
			'use_city' 			 => 	$cidade,
			'use_state' 		 => 	$estado,
			'use_type'			 =>	    $tipo,
			'use_nascimento'     =>     $data_nascimento,
			'use_cpf' 		     =>  	$cpfCnpj
		);
		if ($id_wirecard && $possuiconta)
			$data['use_moip_id_vend'] = $id_wirecard;

		if ($tipo == 'lojista') {

			if ($this->input->post('sobre')) $data['use_sobre'] = $this->input->post('sobre');
			$data['use_website'] 	=	$site;
			$data['use_status']		=	2;
			$data['use_loj_tip'] = $this->input->post('tip_vend');
		} elseif ($tipo == 'cliente') {
			$data['use_status']		=	1;
		}

		$user = $this->user_model->insert($data);
		/** caso seja um cadastro de vendedor, realiza o cadastro no moip na sequencia */
		//$link_acesso = '';

		/*if ($tipo == 'lojista') {
			if($id_wirecard == NULL) {
				$this->load->model('vendedor_model');
				$link_acesso = $this->vendedor_model->cadastrar_vendedor_moip($user);
				//$link_acesso += "acesse o link a seguir para completar sua conta no wirecard: \n $link_acesso";
			}
		}*/

		if ($user) {
			/** Envia e-mail de boas vindas ou aviso de moderação */
			$this->email_registro($user, $tipo, $email);

			/** Realiza login após o cadastro caso seja um cliente comum
			 * Caso vendedor, redireciona para a home e com uma mensagem 
			 * de aviso de pendencia de moderação
			 */
			if ($tipo == "cliente") {
				$this->session->set_userdata(array('login' => $user, 'login_tipo' => $tipo));
				$refferer = $this->session->userdata('refferer');
				if (isset($referrer) && !empty($refferer)) {
					$this->session->unset_userdata('refferer');
					redirect($referrer);
				} else
					redirect('/');
			} else {
				$this->session->set_flashdata('register_modal', 'pendente');
				redirect('/');
			}
		}
	}

	/** method que lida com o envio de e-mail de notificação logo após o cadastro */
	protected function email_registro($user, $tipo, $email)
	{
		/* Email Details */
		$email_details = $this->main_model->emailsDetails((($tipo == 'lojista') ? 14 : 7));

		/* Message */
		$content = $email_details->email_content;

		if (preg_match_all('/({\$+\w+})/', $content, $matches)) {

			foreach ($matches[0] as $key => $value) {
				$variable = str_replace('{', '', $value);
				$variable = str_replace('}', '', $variable);
				$string = eval('return ' . $variable . ';');
				$content = str_replace($value, $string, $content);
			}
		}

		$message = $content;
		//$message .= "Link: " . $conteudo;

		/* Subject */
		$email_subject = $email_details->email_subject;

		/* To */
		$to = $email;

		/* Send Email */
		$this->main_model->email($to, $email_subject, $message);
	}

	/** valida se o e-mail usado para o cadastro já foi utilizado (ajax) */
	public function emailVerify($email)
	{
		$verifyEmail = $this->user_model->emailVerify($email);
		if ($verifyEmail == true) {
			$this->session->set_flashdata('msg', 'O e-mail informado já se encontra cadastrado em nossa base de dados!');
			redirect($_SERVER['HTTP_REFERER']);
			exit;
		} else {
			return true;
		}
	}

	public function page()
	{
		$referrer = explode('.com/', $this->agent->referrer());
		if ($referrer == 'loja' || $referrer == 'lojas' || $referrer == 'anuncios' || $referrer == 'anuncio')
			$this->session->set_userdata('referrer', $referrer);
		$data = array();
		if (isset($_GET['c'])) $data['c'] = true;
		else $data['c'] = false;

		$this->load->view('app');
		$this->load->view('header');
		$this->load->view('register_view', $data);
		$this->load->view('footer');
	}
}

/* End of file Register.php */
/* Location: ./application/controllers/Register.php */
