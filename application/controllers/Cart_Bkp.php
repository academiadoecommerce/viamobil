<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart extends CI_Controller {



	public function index($page = false) {
		$data = array();
		if($this->cart->total_items() > 0)
        	$data['produtos'] = $this->cart->contents();

		$this->load->view('app');
		$this->load->view('header');
		$this->load->view("cart", $data);
		//$this->load->view('footer');   
	}

	public function add($id, $page = false) {
		$prod = $this->ads_model->get_prod_to_cart($id);

        $data = array(
            'id'      => $prod->ad_id,
            'qty'     => 1,
            'price'   => $prod->ad_price,
            'name'    => $prod->ad_name,
            'vendedor_id' => $prod->vendedor_id
            //'img' => $prod->img
            //'options' => array('qtdmin' => 'L', 'Color' => 'Red')
        );

        $this->cart->insert($data);  
        redirect('/cart');
	}

	public function atualizar(){
			$carrinho = array();
			$cars = $this->cart->contents();
			foreach ($cars as $itens) {
				$carrinho = array(
					'rowid' => $itens['rowid'],
					'qty' => $this->input->post('quantidade-item')
			
				);
			}
		$this->cart->update($carrinho);	
	
		redirect('index.php/Cart/index');
		
	}


	public function finaliza(){
		$this->load->view('app');
		$this->load->view('header');
		$this->load->view("finalizar");
	}


	public function limpar(){
		$this->cart->destroy();
		redirect('/cart');
		exit;
	}


	public function remove($rowid){
		$this->cart->remove($rowid);
		redirect('/cart');
		exit;
	}


	/*public function update($rowid, $qtd){
		$data = array(
        'rowid' => $rowid,
        'qty'   => $qtd
		);
		$this->cart->update($data);
		redirect('/cart');
		exit;
	}	*/

}