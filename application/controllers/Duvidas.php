<?php

class Duvidas extends CI_Controller{


	public function chat($id){
        if( !$id ) redirect($_SERVER['HTTP_REFERER']);
		$order = $id;
		$this->load->model("duvida_model");
		$valida = $this->duvida_model->validaChat($order,$this->session->userdata('login'),$this->session->userdata('login_tipo'));

		if($valida = 1) {
			$data['conteudo'] = $this->duvida_model->getContent($order);
			$data['order'] = $order;
			/* Breadcrumbs */
			$data['breadcrumbs'][] = array('name' => $this->seo_title);
			$data['breadcrumbs'][] = array('name' => 'Chat');
			/* SEO */
			$data['seo_title'] = "Chat - ".$this->seo_title;
			/* View */
			$data['page'] = 'chat_duvidas';
			# $this->template->load('app', 'profile', $data);
			$this->load->view('app');
			$this->load->view('header');
			$this->load->view('profile', $data);
			$this->load->view('footer');
			//$this->output->enable_profiler(TRUE);
		} else {
			echo "Sem acesso";
		}

		//$this->output->enable_profiler(true);
	}

	public function perguntar(){
		
		if(!$this->session->userdata('login')){
			$this->session->set_flashdata('danger', 'Favor efetuar o login');
			redirect($this->agent->referrer());
			die();
		}

		$this->load->model("duvida_model");


		$cliente = $this->session->userdata('login');
		$produto = $this->input->post("produto");
		$vendedor = $this->input->post("vendedor");
		$duvida = $this->input->post("message");
		
		$conteudo = array(
			"duvida_cliente"     => $cliente,
			"duvida_vendedor"    => $vendedor,
			"duvida_status"      => 1,
			"duvida_data_inicio" => date("Y-m-d"),
			"duvida_id_produto"  => $produto
		);

		$id_duvida = $this->duvida_model->setDuvida($conteudo);

		$mensagem = array(

			"duvidas_content_message" => $duvida,
			"duvidas_content_fk"      => $id_duvida,
			"duvidas_content_hora"    => date('H:i'),
			"duvidas_content_tipo" => "cliente"

		);

		$this->duvida_model->setMessageDuvida($mensagem);
		$this->notificar(99,$vendedor,$produto);
		redirect($this->agent->referrer());
	}

	public function message(){
		//Metodo responsavel por realizar a resposta 
		$this->load->model("duvida_model");
		$message = $this->input->post("message");
		$id = $this->input->post("duvida_id");

		$tipo = $this->duvida_model->validaDuvida($this->session->userdata('login'), $id);

		if($tipo){
			$tipo = "cliente";
		}else{
			$tipo = "lojista";
		}

		$conteudo = array(
			"duvidas_content_fk"     => $id,
			"duvidas_content_message" => $message,
			//"reclama_content_assunto"   => "",
			"duvidas_content_hora" => date('H:i'),
			"duvidas_content_tipo" => $tipo
		);

		$this->duvida_model->duvidasContent($conteudo);
      	$this->duvida_model->encerraDuvida($id);
      	$this->notificarCliente(22,$id);
      	redirect("duvidas");
	}

	public function notificar($id, $pessoa, $produto){
		$this->load->model("main_model");
		$this->load->model("user_model");
		$this->load->model("ads_model");

		 /* Variables */
        $nome_vendedor = $this->user_model->getNome($pessoa);
        $nome_produto = $this->ads_model->getNome($produto);;

		/* Email Details */
        $email_details = $this->main_model->emailsDetails($id);
        /* Message */
        $content = $email_details->email_content;

        if (preg_match_all('/({\$+\w+})/', $content, $matches)) {
        	
            foreach ($matches[0] as $key => $value) {
                $variable = str_replace('{', '', $value);
                $variable = str_replace('}', '', $variable);
                $string = eval('return ' . $variable . ';');

                $content = str_replace($value, $string, $content);
            }
        }

        $email_message = $content;
        /* Subject */
        $email_subject = $email_details->email_subject;
        /* To */
        $email_to = $this->user_model->getMail($pessoa);//retorna o email do vendedor passando o ID de seu usuario
        /* Send Email */
        $this->main_model->email($email_to, $email_subject, $email_message);

	}

	public function notificarCliente($id, $duvida){
		$this->load->model("main_model");
		$this->load->model("user_model");
		$this->load->model("duvida_model");

		$dados = $this->duvida_model->getDuvidaCliente($duvida);
		
		/* Variables */
		foreach ($dados as $reg){
			$nome_cliente  = $reg["use_name"];
			$email_to = $reg["use_email"];
			$nome_produto  = $reg["ad_name"];
		} 
        
		/* Email Details */
        $email_details = $this->main_model->emailsDetails($id);
        /* Message */
        $content = $email_details->email_content;

        if (preg_match_all('/({\$+\w+})/', $content, $matches)) {
        	
            foreach ($matches[0] as $key => $value) {
                $variable = str_replace('{', '', $value);
                $variable = str_replace('}', '', $variable);
                $string = eval('return ' . $variable . ';');

                $content = str_replace($value, $string, $content);
            }
        }

        $email_message = $content;
        /* Subject */
        $email_subject = $email_details->email_subject;

        /* Send Email */
        $this->main_model->email($email_to, $email_subject, $email_message);

	}


}	
