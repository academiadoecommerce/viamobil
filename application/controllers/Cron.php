<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

require 'vendor/autoload.php';
use Moip\Moip;
use Moip\Auth\OAuth;

class Cron extends CI_Controller {

	public function updateStatus()
   {
		$moip = new Moip(new OAuth(accessToken), Moip::ENDPOINT_PRODUCTION);
		$this->load->model("pedido_model");
		$pedidos = $this->pedido_model->getAllOrder();

		foreach($pedidos as $ped){
			$novaSituacao = traducao($moip->orders()->get($ped["order_num"])->getStatus());

			if($novaSituacao != $ped["order_situacao"]){
				$this->pedido_model->updateSituacao($ped["order_num"], $novaSituacao);
			}
		}
	}
}
