<?php


/**
 * Controller Correios
 * 
 * Controller responsável pela integração com sigepweb, calculo de frete, solicitação
 * de etiquetas e impressão das mesmas com plp
 * 
 * @package Sigep Web
 * @author Rafael Affonso <hrafaelaf@gmail.com>
 * @author Matheus Affonso <matheuspradoaffonso@gmail.com>
 * @since 2019
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

require 'vendor/autoload.php';

use \PhpSigep\Config;
use \PhpSigep\Bootstrap;
use \PhpSigep\Services\SoapClient\Real;
//require_once("vendor/PhpSigep/Pdf/CartaoDePostagem2016.php");
//use \PhpSigep\Pdf\CartaoDePostagem2016;

class Correios extends CI_Controller {

	/**
	 * Inicia configurações do sistema Sigep
	 */
	protected function sigep_start()
	{
		$config = new Config();
		$phpSigep = new Real();
		$config->setEnv(Config::ENV_DEVELOPMENT);
		Bootstrap::start($config);
	}

	/**
	 * Dados de acesso do sistema Sigep
	 * @return object
	 */
	private function sigep_access_data()
	{
		self::sigep_start();
		$accessDataDeHomologacao = new \PhpSigep\Model\AccessDataHomologacao();
		$usuario = trim((isset($_GET['usuario']) ? $_GET['usuario'] : $accessDataDeHomologacao->getUsuario()));
		$senha = trim((isset($_GET['senha']) ? $_GET['senha'] : $accessDataDeHomologacao->getSenha()));
		$cnpjEmpresa = $accessDataDeHomologacao->getCnpjEmpresa();
		
		$accessData = new \PhpSigep\Model\AccessData();
		$accessData->setUsuario($usuario);
		$accessData->setSenha($senha);
		$accessData->setCnpjEmpresa($cnpjEmpresa);
		return $accessData;
	}

	/**
	 * Solicita código pré postagem no sistema dos correios
	 * com base nos dados de contrato
	 *  
	 * @return string Contendo um codigo de pré postagem
	 */
	public function solicita_etiquetas()
	{	
		$accessData = self::sigep_access_data();
	
		$params = new \PhpSigep\Model\SolicitaEtiquetas();
		$params->setQtdEtiquetas(1);
		$params->setServicoDePostagem(\PhpSigep\Model\ServicoDePostagem::SERVICE_PAC_CONTRATO_AGENCIA);
		$params->setAccessData($accessData);
		$phpSigep = new Real();
		$etiqueta = ( $phpSigep->solicitaEtiquetas($params)->getResult()[0]->getEtiquetaSemDv() );

		return $etiqueta;
	}
	/**
	 * Gera a etiqueta em A4 no formato pdf
	 * 
	 * @return document pdf
	 * Etiquetas em pdf para impressão em A4
	 */
	public function gerar_etiqueta ($order)
	{	
		$this->sigep_start();
		$this->load->model("pedido_model");
		$result = $this->pedido_model->getInfoOrders($order);
		$peso = $this->pedido_model->getPeso($order);
		$pesoTotal = 0;

		foreach($peso as $pesoProduto){
			$soma = $pesoProduto["orders_qtd"] * $pesoProduto["ad_peso"];
			$pesoTotal += $soma; 
		}

		foreach($result as $info){
		
			$dados = array(
				'nome_cliente'             => $info["cliente"],
				'bairro_cliente'           => $info["bairro_cliente"],
				'endereco_cliente'         => $info["endereco_cliente"],
				'endereco_cliente_numero'  => $info["endereco_numero_cliente"],
				'cep_cliente'              => $info["cep_cliente"],
				'cidade_cliente'           => $info["cidade_cliente"],
				'estado_cliente'           => $info["estado_cliente"],
				'nome_vendedor'            => $info["vendedor"],
				'bairro_vendedor'          => $info["bairro_vendedor"],
				'endereco_vendedor'        => $info["endereco_vendedor"],
 				'endereco_vendedor_numero' => $info["endereco_numero_vendedor"],
				'cep_vendedor'             => $info["cep_vendedor"],
				'cidade_vendedor'          => $info["cidade_vendedor"],
				'estado_vendedor'          => $info["estado_vendedor"],
				'numero_pedido'            => $info["order_num"],
				'valor_pedido'             => $info["order_valor"],
				'peso_total'               => $pesoTotal

			);
		}

		$params = self::helper_cria_plp($dados);
		$logoFile = base_url( "assets/img/shopbras%20165x43.png" );
		$layoutChancela = array();
		$pdf = new \PhpSigep\Pdf\CartaoDePostagem2016($params, time(), $logoFile, $layoutChancela);
		$pdf->render();

		/*$phpSigep = new PhpSigep\Services\SoapClient\Real();
		$result = $phpSigep->fechaPlpVariosServicos($params);
		fechaPlpVariosServicos (xml, idPlpCliente, cartaoPostagem, listaEtiquetas, usuário, senha)

		echo '<pre>';
		print_r((array)$result);
		echo '</pre>';*/
		//$pdf  = new \PhpSigep\Pdf\ListaDePostagem($params, time());
		//$pdf->render('I'); //GERAR PLP
	}



	public function gerar_plp ($order)
	{
		$this->load->model("pedido_model");
		$result = $this->pedido_model->getInfoOrders($order);
		$peso = $this->pedido_model->getPeso($order);
		$pesoTotal = 0;

		foreach($peso as $pesoProduto){
			$soma = $pesoProduto["orders_qtd"] * $pesoProduto["ad_peso"];
			$pesoTotal += $soma; 
		}

		foreach($result as $info){
		
			$dados = array(
				'nome_cliente'             => $info["cliente"],
				'bairro_cliente'           => $info["bairro_cliente"],
				'endereco_cliente'         => $info["endereco_cliente"],
				'endereco_cliente_numero'  => $info["endereco_numero_cliente"],
				'cep_cliente'              => $info["cep_cliente"],
				'cidade_cliente'           => $info["cidade_cliente"],
				'estado_cliente'           => $info["estado_cliente"],
				'nome_vendedor'            => $info["vendedor"],
				'bairro_vendedor'          => $info["bairro_vendedor"],
				'endereco_vendedor'        => $info["endereco_vendedor"],
 				'endereco_vendedor_numero' => $info["endereco_numero_vendedor"],
				'cep_vendedor'             => $info["cep_vendedor"],
				'cidade_vendedor'          => $info["cidade_vendedor"],
				'estado_vendedor'          => $info["estado_vendedor"],
				'numero_pedido'            => $info["order_num"],
				'valor_pedido'             => $info["order_valor"],
				'peso_total'               => $pesoTotal

			);
		}

		//$params = self::helper_cria_plp($dados);
		$logoFile = base_url( "assets/img/shopbras%20165x43.png" );
		$layoutChancela = array();

		$pdf  = new \PhpSigep\Pdf\ListaDePostagem($params, time());
		$pdf->render('I'); //GERAR PLP
	}

	/**
	 * Monta a plp necessária para gerar etiqueta
	 * 
	 * @return object $plp com dados da encomenda, remetente,
	 * destinatário, serviços, etc
	 */
		private function helper_cria_plp(array $dados)
	{
		/** DADOS DA ENCOMENDA QUE SERÁ DESPACHADA */
		extract($dados);
		$dimensao = new \PhpSigep\Model\Dimensao();
		$dimensao->setAltura(20);
		$dimensao->setLargura(20);
		$dimensao->setComprimento(20);
		$dimensao->setDiametro(0);
		$dimensao->setTipo(\PhpSigep\Model\Dimensao::TIPO_PACOTE_CAIXA);
	
		$destinatario = new \PhpSigep\Model\Destinatario();
		$destinatario->setNome((String)$nome_cliente);
		$destinatario->setLogradouro($endereco_cliente);
		$destinatario->setNumero($endereco_cliente_numero);
		$destinatario->setComplemento("");
	
		$destino = new \PhpSigep\Model\DestinoNacional();
		$destino->setBairro($bairro_cliente);
		$destino->setCep($cep_cliente);
		$destino->setCidade($cidade_cliente." ");
		$destino->setUf($estado_cliente);
		//$destino->setNumeroNotaFiscal(1234567890);
		//$destino->setNumeroPedido(12345678);
	
		$etiqueta = new \PhpSigep\Model\Etiqueta();
		$numeroEtiqueta = self::solicita_etiquetas();
		$etiqueta->setEtiquetaSemDv($numeroEtiqueta);
	
		$servicoAdicional = new \PhpSigep\Model\ServicoAdicional();
		$servicoAdicional->setCodigoServicoAdicional(\PhpSigep\Model\ServicoAdicional::SERVICE_REGISTRO);
		/** Se não tiver valor declarado informar 0 (zero) */
		$servicoAdicional->setCodigoServicoAdicional(\PhpSigep\Model\ServicoAdicional::SERVICE_AVISO_DE_RECEBIMENTO);
	
		$servicoAdicional2 = new \PhpSigep\Model\ServicoAdicional();
		$servicoAdicional2->setCodigoServicoAdicional(\PhpSigep\Model\ServicoAdicional::SERVICE_REGISTRO);
		$servicoAdicional2->setCodigoServicoAdicional(\PhpSigep\Model\ServicoAdicional::SERVICE_VALOR_DECLARADO_PAC);
		$servicoAdicional2->setValorDeclarado((float)$valor_pedido);
	
		$encomenda = new \PhpSigep\Model\ObjetoPostal();
		$encomenda->setServicosAdicionais(array($servicoAdicional, $servicoAdicional2));
		$encomenda->setDestinatario($destinatario);
		$encomenda->setDestino($destino);
		$encomenda->setDimensao($dimensao);
		$encomenda->setEtiqueta($etiqueta);
		$encomenda->setPeso($peso_total);// 500 gramas
		$encomenda->setObservacao('Obrigado por comprar no ShopBras');
		$encomenda->setServicoDePostagem(new \PhpSigep\Model\ServicoDePostagem(\PhpSigep\Model\ServicoDePostagem::SERVICE_PAC_CONTRATO_AGENCIA));
		/** FIM DOS DADOS DA ENCOMENDA QUE SERÁ DESPACHADA */
	
		/** DADOS DO REMETENTE */
		$remetente = new \PhpSigep\Model\Remetente();
		$remetente->setNome((String)$nome_vendedor);
		$remetente->setLogradouro($endereco_vendedor);
		$remetente->setNumero($endereco_vendedor_numero);
		//$remetente->setComplemento('3º andar');
		$remetente->setBairro($bairro_vendedor);
		$remetente->setCep($cep_vendedor);
		$remetente->setUf($estado_vendedor);
		$remetente->setCidade($cidade_vendedor." ");
		/** FIM DOS DADOS DO REMETENTE */
	
		$plp = new \PhpSigep\Model\PreListaDePostagem();
		$plp->setAccessData(new \PhpSigep\Model\AccessDataHomologacao());
		$plp->setEncomendas([$encomenda]);
		$plp->setRemetente($remetente);
		
		return $plp;
	}

	/**
	 * Calcula o prazo e valor do frete com base nos nos dados informados
	 * realizando requisição no webservice dos correios
	 * 
	 * @uses string, int, float post method variables
	 * @return string json contendo dados concatenados de prazo e custo de envio
	 */
	public function frete()
	{
		$cep_destino = $this->input->post("cep_destino");
		$comprimento = $this->input->post("comprimento");
		$largura = $this->input->post("largura");
		$altura = $this->input->post("altura");
		$diametro = $this->input->post("diametro");
		$cep_origem = $this->input->post("cep_origem");
		$peso = $this->input->post("peso");
		$qtd = $this->input->post("qtd");

		$volumeTotal = $comprimento * $altura * $largura;
		$volume += $volumeTotal * $qtd;

		$cubicoVol  = pow($volume, (1/3));

		$url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?';
		$url .= 'nCdEmpresa=delescorreios&';
		$url .= 'sDsSenha=//spsp2019//&';
		$url .= 'nCdServico=41211&'; //41211  PAC com contrato
		$url .= 'sCepOrigem='.$cep_origem.'&';
		$url .= 'sCepDestino='.$cep_destino.'&';
		$url .= 'nVlPeso='.$peso.'&';
		$url .= 'nCdFormato=1&';
		$url .= 'nVlComprimento='.$cubicoVol.'&';
		$url .= 'nVlAltura='.$cubicoVol.'&';
		$url .= 'nVlLargura='.$cubicoVol.'&';
		$url .= 'nVlDiametro='.$diametro.'&';
		$url .= 'sCdMaoPropria=S&';
		$url .= 'nVlValorDeclarado=0&';
		$url .= 'sCdAavisoRecebimento=N&';
		$url .= 'StrRetorno=xml&';
		$url .= 'nIndicaCalculo=3';

		$xml = simplexml_load_file($url);
		$frete = $xml->cServico;

		$valor =  (string) $frete->Valor;
		$prazo =  (string) $frete->PrazoEntrega;
		$prazo = date('d', strtotime("+$prazo days"));
		$prazo = $prazo." e ".(strtotime($prazo) + date('d', strtotime("+12 days")));
      	$preco = $frete->Valor;
		// echo json_encode($retorno; die('oi');
		echo '{"valor" : "'.$valor.'", "prazo":"'.$prazo.'", "preco":"'.$preco.'" }';
	}

	public function precoPrazo(){
		$this->sigep_start();
		$dimensao = new \PhpSigep\Model\Dimensao();
		$dimensao->setTipo(\PhpSigep\Model\Dimensao::TIPO_PACOTE_CAIXA);
		$dimensao->setAltura(15); // em centímetros
		$dimensao->setComprimento(17); // em centímetros
		$dimensao->setLargura(12); // em centímetros

		$params = new \PhpSigep\Model\CalcPrecoPrazo();
		$params->setAccessData(new \PhpSigep\Model\AccessDataHomologacao());
		$params->setCepOrigem('30170-010');
		$params->setCepDestino('86300-000');
		$params->setServicosPostagem(\PhpSigep\Model\ServicoDePostagem::getAll(04510));
		$params->setAjustarDimensaoMinima(true);
		$params->setDimensao($dimensao);
		$params->setPeso(0.150);// 150 gramas


		$phpSigep = new PhpSigep\Services\SoapClient\Real();
		$result = $phpSigep->calcPrecoPrazo($params);
		echo "<pre>";
		print_r((array)$result);

	}

}
