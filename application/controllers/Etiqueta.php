<?php

class Etiqueta extends CI_Controller{

	public function gerarEtiqueta(){
		include("mpdf60/mpdf.php");
		$this->load->model("cliente_model");
		$this->load->model("vendedor_model");
		$this->load->model("barras_model");

		$cliente = $this->cliente_model->getCliente("127");//DESTINATARIO
		$vendedor = $this->vendedor_model->getVendedor("167");//REMETENTE

		foreach ($cliente as $cli) {
			$nome_cli =   $cli["use_name"];
			$fone     =   $cli["use_celular"];
			$endereco =   $cli["use_address"];
			$numero   =   $cli["use_address_number"];
			$bairro   =   $cli["use_neighborhood"];
			$cidade   =   $cli["cit_name"];
			$cep      =   $cli["use_cep"];
			$estado   =   $cli["sta_name"];
			$pais     =   "Brasil";
		}

		foreach ($vendedor as $vend){
			$nome_vend     =   $vend['use_name'];
			$fone_vend     =   $vend["use_celular"];
			$endereco_vend =   $vend["use_address"];
			$numero_vend   =   $vend["use_address_number"];
			$bairro_vend   =   $vend["use_neighborhood"];
			$cidade_vend   =   $vend["cit_name"];
			$cep_vend      =   $vend["use_cep"];
			$estado_vend   =   $vend["sta_name"];
			$pais_vend     =   "Brasil";
		}
		
		$barras = $this->barras_model->get($cep);
		$barrasVend = $this->barras_model->get($cep_vend);

		$html = " <div id='destinatario' style='padding:10px; border: 1px solid black;'> <h3>DESTINATÁRIO</h3>";
		$html .= "<img src='https://soatacado.com/assets/img/logo_correios.png' alt='Correios' width='20%' height='5%' style='float: right; bottom: 50;'>";
		$html .= "".$nome_cli."";
		$html .= "<br>";
		$html .= "Rua".$endereco.", "."N: ".$numero."";
		$html .= "<br>";
		$html .= "".$cidade." - ".$estado." ";
		$html .= "<br>";
		$html .= "Cep: ".$cep;
		
		$html .= "<div id='codebar' style=\"text-align:center\">";
			$html .= $barras;
		$html .= "</div>";
		$html .= "</div>";

		$html .= "<br><br>";

		$html .= " <div id=\"remetente\" style='width=100%; border: 1px solid black; padding:10px'> <h3>REMETENTE</h3>";
		$html .= "".$nome_vend."";
		$html .= "<br>";
		$html .= "Rua".$endereco_vend.", "."N: ".$numero_vend."";
		$html .= "<br>";
		$html .= "".$cidade_vend." - ".$estado_vend." ";
		$html .= "<br>";
		$html .= "Cep: ".$cep_vend;

		$html .= "<div id=\"codebarVend\" style=\"text-align:center\">";
		// $html .= "<div id=\"codebarVend\" style=\"width: 100%; display: flex; justify-content: center; \">";
			$html .= $barrasVend;
		$html .= "</div>";

		$html .= "</div>";

		$mpdf = new mPDF();
		// Define um Cabeçalho para o arquivo PDF
		$mpdf->defaultheaderfontsize=10;
		// Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
		// página através da pseudo-variável PAGENO
		$mpdf->SetFooter('{PAGENO}');
		// Insere o conteúdo da variável $html no arquivo PDF
		// Cria uma nova página no arquivo
		$mpdf->AddPage();
		// Insere o conteúdo na nova página do arquivo PDF
		$mpdf->WriteHTML($html,2);
		date_default_timezone_set('America/Sao_Paulo');
		$mpdf->SetHTMLFooter("Gerado em: ".date("d/m/Y")." ás: ".date("H:i"));
		
		// Gera o arquivo PDF
		$mpdf->Output();
	}

}
