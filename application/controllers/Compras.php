<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Compras extends CI_Controller {

	public function __construct()
   {
        parent::__construct();
	}

	public function checkout(){
		$this->load->view('app');
		$this->load->view('header');
		$this->load->view("checkout");
	}
}
