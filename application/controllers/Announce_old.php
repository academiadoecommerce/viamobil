<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Announce extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function listar($id , $limite){
		 $lista = array();


        $this->db->where('ads_cat_parent', $id);
        $query = $this->db->get('ads_categories');
        $lista = $query->result_array();

        if(count($lista) > 0){
            foreach($lista as $key => $categoria){
            $lista[$key]['filhos'] = array();

            if($limite > 0){
                $lista[$key]['filhos'] = $this->listar($categoria['ads_cat_id'] , $limite-1);
            }
          }
          return $lista;
        }

	}

    public function index() {
        $data['categories'] = $this->ads_model->categories(0);
        $data['categoriafilho'] = $this->CategoriaFilho();
        $data['categ_tertiary'] = $this->CategoriaTer();
        $this->load->view('app');
        $this->load->view('header');
        $this->load->view('announce1', $data);
        $this->load->view('footer');
    }

    public function CategoriaFilho() {
        $this->db->where('ads_cat_parent > ', 0);
        $this->db->where('ads_cat_status', 1);
        $this->db->where('ads_cat_grau', null);
        $query = $this->db->get("ads_categories");
        return $query->result();
    }

    public function CategoriaTer() {
        $this->db->where('ads_cat_parent > ', 0);
        $this->db->where('ads_cat_status', 1);
        $this->db->where('ads_cat_grau', 3);
        $query = $this->db->get("ads_categories");
        return $query->result();
    }

	public function puxar_dados(){
		$query = $this->db->query("SELECT * from panamerico_ads_categories WHERE ads_cat_parent = 0");
        $resultado = $query->result();

        $max = count($resultado);
        $i   = 1;
        foreach($resultado as $res => $value){

                if($value->ads_parent_id == 0){
                 $data[$value->ads_cat_id]  = json_decode (json_encode ($this->listar($value->ads_cat_id , 3)), FALSE);
                 $dados[$i]['ads_cat_id'] = $value->ads_cat_id;
                 $dados[$i]['ads_cat_name'] = $value->ads_cat_name;
                 $dados[$i]['ads_cat_icon'] = $value->ads_cat_icon;
                 $dados[$i]['ads_cat_icon'] = $value->ads_cat_parent;
                 $dados[$i]['ads_cat_icon'] = $value->ads_cat_status;
                 $dados[$i]['filhos'] = $data[$value->ads_cat_id];
                 $dados[$i] = array($dados[$i]);
                 $i++;
                }

        }

		$data['tudo']      =  $dados;
        $this->load->view("json" , $data);
    }


    public function anunciar_prox(){

        $data['id_categoria'] = $this->input->post('categ');

        if($this->input->post('ads_cat_name'))
            $data['nome_categoria'] = $this->input->post('ads_cat_name');

        $ip = $_SERVER['REMOTE_ADDR'];
        $data['hash'] = md5($ip . rand(1, 999));

        /* opcionais */
        $data['options'] = $this->ads_model->getOptions();

        /* Areas */
        $data['user'] = $this->user_model->info();

        /* Breadcrumbs */
        $data['breadcrumbs'][] = array('name' => 'Inserir Anúncio');

        /* SEO */
        $data['seo_title'] = "Inserir Anúncio";

        /*Variacoes*/
        $data['variacoes_cores'] = $this->ads_model->getVariacoes();


        if (empty($data['user']->use_cep)){
            $data['modal_alert'] = base_url('announce/ads_action/modal/user');
            $data['form_disabled'] = true;
        }
        
		$this->load->view('app');
		$this->load->view('header');
		$this->load->view('announce', $data);
		$this->load->view('footer');
    }

    public function ads_action($view, $action) {

        if ($action == "user") {
            $data['modal_title'] = "<p style=\"text-align: center; font-size: 30px; padding-left: 80px;\">Ops!</p>";
            $data['text'] = 'Para inserir um anúncio você precisa completar seu cadastro!<br>É rapidinho ;)';
            $data['link'] = base_url('cliente/detalhes/');
            $data['button'] = array('type' => 'warning', 'text' => 'Completar');
        } elseif ($action == "resell") { /* Resell */
            $data['modal_title'] = "Revender Anúncio";

            $data['text'] = 'Você tem certeza que deseja revender o anúncio "<strong></strong>"?';

            $data['button'] = array('type' => 'secondary', 'text' => 'Revender');
        }

        $data['modal_size'] = "small";

        $this->template->load('modal', 'alert', $data);

    }


    public function reorder_img() {
        $ordem = $this->input->post('i');
        $this->ads_model->imagesSetOrd($ordem);
    }


    public function insert()
    {

        $hash = $this->input->post("hash");

        $category = (int) $this->input->post("category");

        $title = $this->input->post("title");
        $desc = $this->input->post("desc");
        $price = db_money($this->input->post("price"));
        $p2 = NULL;

        if (isset($_POST['condicao']) && !empty($_POST['condicao']))
            $condicao = $this->input->post('condicao');
        else
            $condicao = null;
        
        $tipo = "varejo";
    

        $video = $this->input->post("video");
        $cep = $this->input->post("cep");
        $state = $this->input->post("state");
        $region = $this->input->post("region");
        $city = $this->input->post("city");
        $neighborhood = $this->input->post("neighborhood");
        $address = validate_name($this->input->post("address"));
        $custom = $this->input->post("custom[]");
        $custom_checkbox = $this->input->post("custom_checkbox[]");
        $altura = $this->input->post("altura");
        $largura = $this->input->post("largura");
        $diametro = $this->input->post("diametro");
        $peso = db_money($this->input->post("peso"));
        $comprimento = $this->input->post("comprimento");
        $qtdmax = $this->input->post("qtdmax");
        $freteCombinado = $this->input->post("defaultExampleRadios");


        if ($altura < "2" || $altura > "105") {
            $this->session->set_flashdata('danger', 'A altura não pode ser menor que 2 ou maior que 105');
            redirect($this->agent->referrer());
            die();
        }

        if ($comprimento < "16" || $comprimento > "105") {
            $this->session->set_flashdata('danger', 'O comprimento não pode ser menor que 16 ou maior que 105');
            redirect($this->agent->referrer());
            die();
        }

        if ($largura < "11" || $largura > "105") {
            $this->session->set_flashdata('danger', 'A largura não pode ser menor que 11 ou maior que 105');
            redirect($this->agent->referrer());
            die();
        }

        $somaDimensoes = $altura + $largura + $comprimento; //Soma máxima das dimensões não pode passar de 200cm, definido pelo correios

        if($somaDimensoes > "200"){
            $this->session->set_flashdata('danger', 'A soma máxima das dimensões não pode ultrapassar 200cm.');
            redirect($this->agent->referrer());
            die();
        }

        $slug = cleanString(
            url_title(
                strtolower(
                    substr($title, 0, 28) . "-" . rand(1, 999)
                )
            )
        );

        $data = array(
            'ads_cat_id' => $category,
            'ad_qtdmax' => $qtdmax,
            'ad_condicao' => (($condicao) ? $condicao : null),
            'use_id' => $this->session->userdata('login'),
            'ad_name' => $title,
            'ad_desc' => $desc,
            'ad_price' => db_money($price),
            'ad_video' => $video,
            'ad_cep' => (($cep) ? $cep : ""),
            'ad_state' => (($state) ? $state : ""),
            'ad_region' => (($region) ? $region : ""),
            'ad_city' => (($city) ? $city : ""),
            'ad_neighborhood' => (($neighborhood) ? $neighborhood : ""),
            'ad_address' => (($address) ? $address : ""),
            'ad_use_info' => 1,
            'ad_slug' => $slug,
            'ad_status' => 1,
            'ad_altura' => $altura,
            'ad_largura' => $largura,
            'ad_diametro' => $diametro,
            'ad_peso' => $peso,
            'ad_comprimento' => $comprimento,
            'ad_tipo' => $tipo,
            'ad_frete' => $freteCombinado
        );

        $ad = $this->ads_model->insert($data);

        /* $fretes = $this->input->post('fretes');
         foreach ($fretes as $key => $value) {

            foreach ($value as $tipo => $item) {
                $freteCombinar = [
                    "frete_id_produto" => $ad,
                    "frete_valor" => $item,
                    "frete_local" => $value,
                ];
            }
        }*/

        //print_r($freteCombinar);
        //die();

        if ($ad) {
            /* Set Images */
            $this->ads_model->imagesSetAds($hash, $ad, $order);

            $this->ads_model->cleanImagesHashs($ad);

            /* grava os opcionais do anuncio caso houver */
            $variacoes = $this->input->post('variacoes');
            if (!empty($variacoes) && isset($_FILES['variacoes']['name']) && count($_FILES['variacoes']['name']) > 0) {

                $upload = $this->main_model->multipleUpload();

                foreach ($variacoes as $key => $value) {

                    foreach ($value as $tipo => $item) {
                        $ads_options = [
                            "ad_id" => $ad,
                            "ad_option_id" => $key,
                            "ad_option_val" => $item,
                            "ad_option_img" => $upload[$tipo]['file_name']
                        ];
                    }
                }
            }



            /* Variables */
            $user = $this->user_model->info();
            $user_name = $user->use_name;
            $user_email = $user->use_email;
            $ad_name = $title;
            $datetime = date('d/m/Y H:i:s');

            /* Email Details */
            $email_details = $this->main_model->emailsDetails(8);

            /* Message */
            $content = $email_details->email_content;

            if (preg_match_all('/({\$+\w+})/', $content, $matches)) {

                foreach ($matches[0] as $key => $value) {
                    $variable = str_replace('{', '', $value);
                    $variable = str_replace('}', '', $variable);
                    $string = eval('return ' . $variable . ';');

                    $content = str_replace($value, $string, $content);
                }
            }

            $email_message = $content;

            /* Subject */
            $email_subject = $email_details->email_subject;


            /* To */
            $email_to = $user_email;

            /* Send Email */
            $this->main_model->email($email_to, $email_subject, $email_message);

            $this->session->set_flashdata('return', 'announce_insert_success');
        }

        redirect("cliente/painel");
    }

    public function categories() {
        $parent = $this->input->post("category");

        $categories = $this->ads_model->categories($parent);

        if ($categories) {
            echo '<ul>';

            foreach ($categories as $key => $cat) {
                echo '<li data-id="' . $cat->ads_cat_id . '">' . $cat->ads_cat_name . '</li>';
            }

            echo '</ul>';
        } else {
            echo '<small>Não existe subcategorias para essa categoria.</small>';
        }
    }

    private function isTamanho($campo) {
        if (strlen($_FILES[$campo]['name']) > 0) {
            $logo = $_FILES[$campo]['tmp_name'];
            list($largura, $altura) = getimagesize($logo);
            if ($largura < 350 and $altura < 260) {
                return FALSE;
            }
        }
        return TRUE;
    }

    public function images_upload($hash) {
        if ($this->isTamanho('file')) {
            $upload = $this->main_model->uploadImage('file', 'ads');
            if (strlen($upload['file_name'])) {
                $data = array(
                    'ad_hash' => $hash,
                    'ads_img_path' => $upload['full_path'],
                    'ads_img_file' => $upload['file_name']
                );

                $image = $this->ads_model->imagesInsert($data);

                echo $image;
            }
        } else {
            echo 'error';
        }
    }

    public function images_remove() {
        $image_id = $this->input->post('image');

        $image = $this->ads_model->imagesDetails($image_id);

        $this->ads_model->imagesDelete($image_id);

        unlink($image->ads_img_path);
    }

    public function preview_images() {
        $hash = $this->input->post('hash');

        $images = $this->ads_model->images($hash, true);

        if ($images) {
            echo '
			<div class="ap-images">
				<div class="ap-i-master">
			';

            foreach ($images as $key => $image) {
                $w = 740;
                $h = 400;

                echo '<img data-image="' . $image->ads_img_id . '" ' . (($key == 0) ? 'class="active"' : '') . ' src="' . thumbnail(@$image->ads_img_file, "ads", $w, $h, 2) . '">';
            }

            echo '
				</div>
			';

            if (count($images) > 1) {
                echo '
					<div class="ap-i-controls">
						<div class="ap-ic-btn"><span id="ap-ic-prev"><i class="fa fa-chevron-left"></i></span></div>
						<div class="ap-ic-btn"><span id="ap-ic-next"><i class="fa fa-chevron-right"></i></span></div>
					</div>
					<div class="ap-i-slider" id="ap-i-slider">
				';

                foreach ($images as $key => $image) {
                    echo '
						<div data-image="' . $image->ads_img_id . '" class="item">
							<img src="' . thumbnail(@$image->ads_img_file, "ads", 200, 150, 2) . '">
						</div>
					';
                }

                echo '
					</div>
				';
            }

            echo '</div>';
        } else {
            echo '
				<div class="ap-images">
					<div class="ap-i-master">
						<img class="active" src="' . thumbnail(false, false, 740, 400) . '">
					</div>
				</div>
			';
        }
    }

}
