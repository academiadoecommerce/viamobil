<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ads extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index($page = false) {

        if (isset($_GET['search']) && !empty($_GET['search'])) {
            if (isset($_GET['category_name']) && $_GET['category_name'] == 'lojas') {
                header('Location: '.base_url('lojas?search='.$_GET['search']));
                exit;
            } else if(isset($_GET['category_name']) && $_GET['category_name'] == 'anuncios'){
                header('Location: '.base_url('anuncios?search='.$_GET['search']));
                exit;
            }
        }

        $preco = $this->filtroPreco($this->input->get('preco'));
        paginacao()->config(50);

        $catparent = $this->ads_model->categoriesDetails($this->input->get('categoria'));

        $ads = $this->ads_model->listing(false, paginacao()->getQtd(), paginacao()->getInicio(), array(
            'ads.ad_name' => $this->input->get('search')
                ), array(
            'ads.ad_condicao' => $this->input->get('condicao'),
            'ads.ad_price >=' => $preco[0],
            'ads.ad_price <=' => $preco[1],
            'ads.ad_state' => $this->input->get('estado'),
            'ads.ad_trade' => ($this->input->get('tipo') == 'trade') ? '1' : '',
            'ads.ad_service' => ($this->input->get('tipo') == 'service') ? '1' : '',
            'ads.ad_region' => $this->input->get('regiao'),
            'ads.ad_city' => $this->input->get('cidade')), array(
            'ads.ads_cat_id' => $this->input->get('categoria'),
            'ads.ads_cat_parent' => $this->input->get('categoria')
        ));
        $total_rows = $this->ads_model->contallAds(array(
            'ads.ad_name' => $this->input->get('search')
                ), array(
            'ads.ad_condicao' => $this->input->get('condicao'),
            'ads.ad_price >=' => $preco[0],
            'ads.ad_price <=' => $preco[1],
            'ads.ad_state' => $this->input->get('estado'),
            'ads.ad_trade' => ($this->input->get('tipo') == 'trade') ? '1' : '',
            'ads.ad_service' => ($this->input->get('tipo') == 'service') ? '1' : '',
            'ads.ad_region' => $this->input->get('regiao'),
            'ads.ad_city' => $this->input->get('cidade')
                ), array(
            'ads.ads_cat_id' => $this->input->get('categoria')
        ));

        if (isset($_GET['categoria']) && !empty($_GET['categoria'])) {
        $ads['filtro_categ'] = $this->input->get('categoria');
        }

        $this->listing($ads, FALSE, $total_rows, false);
    }

    public function get_anuncios_populares(){
        return $this->ads_model->get_anuncios_populares();
    }

    public function filtroPreco($preco) {
        $p = explode('_', $preco);
        return $p;
    }

    public function category($code) {
        $this->session->set_flashdata('ads_filter_category', $code);

        redirect('anuncios');
    }

    public function state($slug) {

    }

    public function get_state_by_id($id) {

    }

    public function listing($ads, $data = false, $total_rows, $ajax = FALSE) {

        //anuncios patrocinados
        $data['patrocinados'] = $this->db->order_by('adv_id', 'desc')->where("adv_tipo", "patrocinado")->get("advertising_p")->result();
        /* Conta total das Categorias pai */
        $data['count_categories'] = $this->ads_model->adscategories(0);  // Select das categorias Pai.

        $data['ads'] = $ads;
        $data['total'] = $total_rows;
        $data['categories'] = $this->ads_model->categories(0);


        $data['breadcrumbs'][] = array('name' => 'Anúncios', 'link' => base_url('anuncios'));

        if ($ads['filtro_categ']) {

            if ($this->ads_model->categoriesDetails($ads['filtro_categ'])->ads_cat_parent > 0) {
                $data['breadcrumbs'][] = array(
                    'name' => $this->ads_model->categoriesDetails($this->ads_model->categoriesDetails($ads['filtro_categ'])->ads_cat_parent)->ads_cat_name,
                    'link' => base_url('anuncios/?categoria='.$this->ads_model->categoriesDetails($this->ads_model->categoriesDetails($ads['filtro_categ'])->ads_cat_parent)->ads_cat_id));
                // $data['breadcrumbs'][] = array('name' => $this->ads_model->categoriesDetails($this->ads_model->categoriesDetails($ads['filtro_categ'])->ads_cat_parent)->ads_cat_name, 'link' => base_url('anuncios'));
                $data['breadcrumbs'][] = array('name' => $this->ads_model->categoriesDetails($ads['filtro_categ'])->ads_cat_name);

            } else

            $data['breadcrumbs'][] = array('name' => $this->ads_model->categoriesDetails($ads['filtro_categ'])->ads_cat_name);

            if ($this->ads_model->categoriesDetails($ads['filtro_categ'])->ads_cat_parent == 0)
                $data['categories'] = $this->ads_model->categories($ads['filtro_categ']);

            else if($this->ads_model->categoriesDetails($ads['filtro_categ'])->ads_cat_parent > 0)
                $data['categories'] = $this->ads_model->categories($this->ads_model->categoriesDetails($ads['filtro_categ'])->ads_cat_parent);
        }


        $data['seo_title'] = "Anúncios";

        $data['estado'] = $this->ads_model->listaFiltro('states', 'sta_name', 'ASC', array('sta_id', 'sta_name'));
        if (strlen($this->input->get('estado')) > 0) {
            $data['regiao'] =  $this->address_model->getRegions($this->input->get('estado'));
        }
        if (strlen($this->input->get('regiao')) > 0) {
            $data['cidade'] = $this->ads_model->listaFiltro('cities', 'cit_name', 'ASC', array('cit_id', 'cit_name'), array(
                'regiao_id' => $this->input->get('regiao')
            ));
        }


        $this->load->view('app');
        $this->load->view('header');
        $this->load->view('ads', $data);
        $this->load->view('footer');
    }

    public function loadMorePerguntas($code, $perPage){
        $page = $this->input->get('page');
        $start = $page * $perPage;
        $result = $this->duvida_model->getByProdutoPage($code, $perPage, $start);

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function details($code) {

        $this->load->model("duvida_model");
        //busca banner de publicidade da pagina de detalhe de anuncio
        $data['img_publicidade'] = $this->db->where('adv_position', 'anuncio_detalhe')->get("advertising_p")->row();

        /* busca detalhes do anuncio pelo id */
        $data['ad'] = $this->ads_model->details($code);

        if ($data['ad']) {
            if ($data['ad']->ad_use_info) {
                $state = $this->main_model->statesDetails($data['ad']->use_state);
                $regions = $this->main_model->regionsDetails($data['ad']->use_region);
                $city = $this->main_model->citiesDetails($data['ad']->use_city);

                $city = @$city->cit_name;
                $regions = @$regions->regiao_nome;
                $state = @$state->sta_name;

                $data['state'] = $state;
                $data['region'] = $regions;
                $data['city'] = $city;
                $data['neighborhood'] = $data['ad']->use_neighborhood;
                $data['address'] = $data['ad']->use_address;
                $data['cep'] = $data['ad']->use_cep;
            } else {
                $state = $this->main_model->statesDetails($data['ad']->ad_state);
                $regions = $this->main_model->regionsDetails($data['ad']->ad_region);
                $city = $this->main_model->citiesDetails($data['ad']->ad_city);

                $city = @$city->cit_name;
                $regions = @$regions->regiao_nome;
                $state = @$state->sta_name;

                $data['state'] = $state;
                $data['region'] = $regions;
                $data['city'] = $city;
                $data['neighborhood'] = $data['ad']->ad_neighborhood;
                $data['address'] = $data['ad']->ad_address;
                $data['cep'] = $data['ad']->ad_cep;
            }

            /* busca imagens do anuncio */
            $data['images'] = $this->ads_model->images($code);

            /* busca opcionais do anuncio */
            $data['options'] = $this->ads_model->ads_options($code);

            /* busca categorias e subcateg. do anuncio */
            $data['category_parent'] = $this->ads_model->categoriesDetails($data['ad']->ads_cat_parent);
            $data['cat1'] = $this->ads_model->categoriesDetails($data['category_parent']->ads_cat_parent);

            /* Verify Shop */
            $data['shop'] = $this->shops_model->slug($data['ad']->use_id);
            if ($data['shop']) {
                $data['ads_shop'] = count($this->shops_model->ads($data['ad']->use_id));
            } else {
                $data['ads_shop'] = false;
            }

            /* busca anuncios relacionados */
            $data['related'] = $this->ads_model->related(4, $data['ad']->use_id, $code);

            /* Ad Link */
            $data['link'] = base_url('anuncio/' . $data['ad']->ad_slug);

            /* Breadcrumbs */
            $data['breadcrumbs'][] = array('name' => 'Anúncios', 'link' => base_url('anuncios'));
            $data['breadcrumbs'][] = array('name' => $data['cat1']->ads_cat_name);
            $data['breadcrumbs'][] = array('name' => $data['category_parent']->ads_cat_name);

            $data['breadcrumbs'][] = array('name' => $data['ad']->ads_cat_name);
            $data['breadcrumbs'][] = array('name' => $data['ad']->ad_name);

            /* Incrementa ao contador de visualizacoes do anuncio */
            $visits = (int) $data['ad']->ad_visits;
            $new_visit = $visits + 1;
            $this->ads_model->recordVisit($data['ad']->ad_id, $new_visit);

            /* SEO */
            $data['seo_title'] = $data['ad']->ad_name . " - Anúncios";

            if($data['ad']->ad_frete == "sim"){
                $this->load->model("ads_model");
                $data['frete'] = $this->ads_model->getFretes($data['ad']->ad_id);
            }

            $perPage = 5;
            //ALTERAR DEPOIS PARA NUM_ROWS
            //$count = $this->duvida_model->getByProduto($code);
            //$data['questions'] = $this->duvida_model->getByProduto($code);

            if(!empty($this->input->get("page"))){

                if($this->input->get('method') == 'json'){
                    return $this->loadMorePerguntas($code, $perPage);
                }
//              $start = ceil($this->input->get("page") * $perPage);
//              $data['questions'] = $this->duvida_model->getByProduto($code, $start, $page);
//
//              echo json_encode($result);
            }else{
              //$count = $this->duvida_model->getByProdutoCount($code);

              /*if($count <= 2){
                $data['questions'] = $this->duvida_model->getByProduto($code, 0, $perPage);
              }else{*/
                $data['questions'] = $this->duvida_model->getByProduto($code, $perPage, 0);
              //}


            }
            $data['variacoesCount'] = $this->ads_model->validVariacoes($code);
            
            //$this->output->enable_profiler(true);

            /* View */
            $this->load->view('app');
            $this->load->view('header');
            $this->load->view('ads_details', $data);
            $this->load->view('footer');
        } else {
            /* SEO */
            $data['seo_title'] = "Anúncio não Disponível - Anúncios";

            /* View */
            $this->load->view('app');  // Carrega a view app.
            $this->load->view('header');  // carrega o template 'app' e a view 'main' passando os dados para esta.
            $this->load->view('ads_details_denied', $data);  // carrega a view 'main' passando os dados para esta.
            $this->load->view('footer');  // carrega o template 'app' e a view 'main' passando os dados para esta.

        }
    }

    public function route($slug) {
        $ad = $this->ads_model->getBySlug($slug);

        $this->details($ad->ad_id);
    }

    public function send_message() {
        /* Post */
        $code = $this->input->post('code');
        $name = validate_name($this->input->post('name'));
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $msg = $this->input->post('msg');

        /* Ad Details */
        $ad = $this->ads_model->details($code);
        $ad_name = $ad->ad_name;
        $ad_link = base_url('anuncio/' . $ad->ad_slug);

        /* Email Details */
        $email_details = $this->main_model->emailsDetails(1);

        /* Message */
        $content = $email_details->email_content;

        if (preg_match_all('/({\$+\w+})/', $content, $matches)) {

            foreach ($matches[0] as $key => $value) {
                $variable = str_replace('{', '', $value);
                $variable = str_replace('}', '', $variable);
                $string = eval('return ' . $variable . ';');

                $content = str_replace($value, $string, $content);
            }
        }

        $message = $content;

        /* Subject */
        $subject = $email_details->email_subject;

        /* To */
        $to = $this->input->post('user_email');

        /* Sender Copy */
        $sender_copy = $this->input->post('sender-copy');

        if ($sender_copy) {
            $bcc = $email;
        } else {
            $bcc = false;
        }

        /* Send Email */
        $this->main_model->email($to, $subject, $message, $bcc, $email, $name);

        /* Return */
        $this->session->set_flashdata("return", "send_message_success");

        /* Redirect */
        redirect("anuncio/" . $ad->ad_slug);
    }

    public function add_favorite($code) {
        /* Verify Session */
        if ($this->session->userdata('login')) {

            /* Add Favorite */
            $this->user_model->favoritesInsert($code);

            /* Return */
            $this->session->set_flashdata("return", "ads_add_favorite");

            /* Redirect */
            redirect("ads/details/" . $code);
        } else {
            /* Return */
            $this->session->set_flashdata("return", "login_required");

            /* Redirect */
            redirect("ads/details/" . $code);
        }
    }

    public function remove_favorite($code) {
        /* Remove Favorite */
        $this->user_model->favoritesDelete($code);

        /* Return */
        $this->session->set_flashdata("return", "ads_remove_favorite");

        /* Redirect */
        redirect("ads/details/" . $code);
    }

    public function report($code, $view = "modal") {
        if ($view == "modal") {
            $data['code'] = $code;
            $data['modal_title'] = "Denunciar esse anúncio";
            $data['modal_size'] = "medium";

            $this->template->load('modal', 'ads_report', $data);
        } elseif ($view == "send") {
            /* POST */
            $name = validate_name($this->input->post('name'));
            $reason = $this->input->post('reason');
            $text = $this->input->post('text');

            /* ad details */
            $ad = $this->ads_model->details($code);
            $ad_name = $ad->ad_name;
            $ad_link = base_url('anuncio/' . $ad->ad_slug);

            /* Email Details */
            $email_details = $this->main_model->emailsDetails(4);

            /* Message */
            $content = $email_details->email_content;

            if (preg_match_all('/({\$+\w+})/', $content, $matches)) {

                foreach ($matches[0] as $key => $value) {
                    $variable = str_replace('{', '', $value);
                    $variable = str_replace('}', '', $variable);
                    $string = eval('return ' . $variable . ';');

                    $content = str_replace($value, $string, $content);
                }
            }

            $message = $content;

            /* Subject */
            $subject = $email_details->email_subject;

            /* to */
            $to = $this->main_model->config('cfg_contact_email');

            /* send email */
            $this->main_model->email($to, $subject, $message);

            /* return */
            $this->session->set_flashdata("return", "ads_report_success");

            /* redirect */
            redirect("ads/details/" . $code);
        }
    }

    public function email_share($code, $view = "modal") {
        if ($view == "modal") {
            $data['code'] = $code;
            $data['modal_title'] = "Compartilhar via e-mail";
            $data['modal_size'] = "medium";

            $this->template->load('modal', 'ads_email_share', $data);
        } elseif ($view == "send") {
            /* POST */
            $name = validate_name($this->input->post('name'));
            $email = $this->input->post('email');
            $text = $this->input->post('text');

            /* ad details */
            $ad = $this->ads_model->details($code);
            $ad_name = $ad->ad_name;
            $ad_link = base_url('anuncio/' . $ad->ad_slug);

            /* Email Details */
            $email_details = $this->main_model->emailsDetails(5);

            /* Message */
            $content = $email_details->email_content;

            if (preg_match_all('/({\$+\w+})/', $content, $matches)) {

                foreach ($matches[0] as $key => $value) {
                    $variable = str_replace('{', '', $value);
                    $variable = str_replace('}', '', $variable);
                    $string = eval('return ' . $variable . ';');

                    $content = str_replace($value, $string, $content);
                }
            }

            $message = $content;

            /* Subject */
            $subject = $email_details->email_subject;

            /* to */
            $to = $email;

            /* send email */
            $this->main_model->email($to, $subject, $message, false);

            /* return */
            $this->session->set_flashdata("return", "ads_email_share_send");

            /* redirect */
            redirect($ad_link);
        }
    }

    public function get_geoip(){
        $api_key= '37de8fedab7846a0b37262779a9528a7';
        $ip_addr = $_SERVER['REMOTE_ADDR'];
        $geoplugin = unserialize( file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip_addr) );

        if ( is_numeric($geoplugin['geoplugin_latitude']) && is_numeric($geoplugin['geoplugin_longitude']) ) {
            $lat = $geoplugin['geoplugin_latitude'];
            $long = $geoplugin['geoplugin_longitude'];
        }

        // $tags=json_decode(file_get_contents('http://gd.geobytes.com/GetCityDetails?fqcn='. $ip_addr), true);
        $tags=json_decode(file_get_contents("https://api.opencagedata.com/geocode/v1/json?q={$lat},{$long}&key={$api_key}&language=pt&pretty=1"));


        // $result = $tags['results'][0]['components'];
        // $result = $tags;

        // echo '<pre>';
        // print_r($tags->results[0]->components);
        // echo "</pre>";
        return $tags->results[0]->components;

    }

    public function auto_frete($dados, $auto = ''){

        // echo '<pre>';
        // print_r($dados);
        // die('die');
        // error_reporting(E_ALL);
        // ini_set('display_errors', 'On');
        $cep_destino = $auto;
        $comprimento = $dados['ad_comprimento'];
        $largura = $dados['ad_largura'];
        $altura = $dados['ad_altura'];
        $diametro = $dados['ad_diametro'];
        $cep_origem = $dados['use_cep'];
        $peso = $dados['ad_peso'];

        $url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?';
        $url .= 'nCdEmpresa=0&';
        $url .= 'sDsSenha=0&';
        $url .= 'nCdServico=41106&';
        $url .= 'sCepOrigem='.$cep_origem.'&';
        $url .= 'sCepDestino='.$cep_destino.'&';
        $url .= 'nVlPeso='.$peso.'&';
        $url .= 'nCdFormato=1&';
        $url .= 'nVlComprimento='.$comprimento.'&';
        $url .= 'nVlAltura='.$altura.'&';
        $url .= 'nVlLargura='.$largura.'&';
        $url .= 'nVlDiametro='.$diametro.'&';
        $url .= 'sCdMaoPropria=S&';
        $url .= 'nVlValorDeclarado=0&';
        $url .= 'sCdAavisoRecebimento=N&';
        $url .= 'StrRetorno=xml&';
        $url .= 'nIndicaCalculo=3';

        $xml = simplexml_load_file($url);
        $frete = $xml->cServico;
        $valor = $frete->Valor;

        return $frete;
        //echo "teste";

    }

}

/* End of file Ads.php */
/* Location: ./application/controllers/Ads.php */
