<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
/**
 * Service Controller
 */
class Categorias extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }
 
    public function index()
    {        

	/* Categories */
	$data['ads_categories'] = $this->ads_model->categories(0);  // Select das categorias Pai.

	$this->load->view('app');
	$this->load->view('header');
	$this->load->view('categorias', $data);
	$this->load->view('footer');

    }
}