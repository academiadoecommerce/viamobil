<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require 'vendor/autoload.php';

use Moip\Moip;
use Moip\Auth\OAuth;
use Moip\Auth\BasicAuth;
use Moip\Exceptions\ValidationException;
use Moip\Helper\Pagination;

class Checkout extends CI_Controller {

	protected $moip;
	public function __construct() {

        parent::__construct();
    	$this->moip = new Moip(new OAuth(accessToken), Moip::ENDPOINT_PRODUCTION);
    	// $this->moip = new Moip(new OAuth(token, key), Moip::ENDPOINT_SANDBOX);
	}

	public function index() {

	}

	public function view() {
		$data['produtos'] = $this->cart->contents();

		$this->load->view('app');
		$this->load->view('header');
		$this->load->view('checkout_form',$data);
		$this->load->view('footer'); 
	}


	public function cadastrar_vendedor_moip($id){
		$id_vendedor = $id;
		$this->load->model('vendedor_model');
		$vendedores = $this->vendedor_model->getVendedor($id_vendedor);

		foreach ($vendedores as $vend) {
			$nome_vendedor = $vend['use_name'];
			$cpf_vendedor = $vend['use_cpf'];
			$email_vendedor = $vend['use_email'];
			$cep_vendedor = $vend['use_cep'];
			$estado_vendedor = $vend['sta_initials'];
			$cidade_vendedor = $vend['cit_name'];
			$endereco_vendedor = $vend['use_address'];
			$endereco_number_vendedor = $vend['use_address_number'];
			$bairro_vendedor = $vend['use_neighborhood'];
			$data_nascimento = $vend['use_nascimento'];

			if (!empty($vend['use_phone'])) {
				$fone_vendedor = str_replace(['-', ' ', '(', ')'], '', $vend['use_phone']);
				$fone_vendedor = substr($fone_vendedor,2,9);
				$ddd_vendedor = substr($fone_vendedor, 0, 2);
			}

			if (!empty($vend['use_celular'])) {
				$celular_vendedor = $vend['use_celular'];
			}
		}

		try {
			$account = $this->moip->accounts()
				->setName($nome_vendedor)
				->setLastName('De Tal')
				->setEmail($email_vendedor)
				->setBirthDate($data_nascimento)
				->setTaxDocument($cpf_vendedor)
				->setType('MERCHANT')
				->setPhone($ddd_vendedor, $fone_vendedor, "55")
				->addAddress($endereco_vendedor, $endereco_number_vendedor, "teste", $cidade_vendedor, $estado_vendedor, $cep_vendedor, $bairro_vendedor)
				->setTransparentAccount(false)
				->create();
				return $this->vendedor_model->setMoipId($id_vendedor, $account->getId());
		} catch (Exception $e) {
			$this->session->set_flashdata('msg', "Não foi possível efetivar sua solicitação, informe o admnistrador do site. erro x887.");
			redirect($_SERVER['HTTP_REFERER']);
			exit;
		}
		return false;
	}


	public function cadastrar_cliente_moip(){
		$moip = $this->moip;
		$this->load->model('cliente_model');
		$clientes = $this->cliente_model->getCliente($this->session->userdata('login'));

			foreach ($clientes as $cli) {
				$nome_cliente = $cli['use_name'];
				$cpf_cliente = $cli['use_cpf'];
				$email_cliente = $cli['use_email'];
				$cep_cliente = $cli['use_cep'];
				$estado_cliente = $cli['sta_name'];
				$cidade_cliente = $cli['cit_name'];
				$endereco_cliente = $cli['use_address'];
				$endereco_numero = $cli['use_address_number'];
				$bairro_cliente = $cli['use_neighborhood'];
				$data_nascimento = $cli['use_nascimento'];

				if(!empty($cli['use_phone'])){
					$fone_cliente = $cli['use_phone'];
					$ddd_cliente = substr($fone_cliente, 0,2);
				}

				if(!empty($cli['use_celular'])){
					$celular_cliente = $cli['use_celular'];
					
				}
			}

			try {
			  $customer = $moip->customers()->setOwnId(uniqid())
			    ->setFullname($nome_cliente)
			    ->setEmail($email_cliente)
			    ->setBirthDate($data_nascimento)
			    ->setTaxDocument($cpf_cliente)
			    ->setPhone((int)$ddd_cliente, (int)$fone_cliente)
			    ->addAddress('SHIPPING',
			      $endereco_cliente, $endereco_numero,
			      $bairro_cliente, $cidade_cliente, $estado_cliente,
			      $cep_cliente, 10)
			    ->create();
			} catch (Exception $e) {
			//   printf($e->__toString());
				$this->session->set_flashdata('msg', "Não foi possível efetivar sua solicitação, informe o admnistrador do site. erro x889.");
				redirect($_SERVER['HTTP_REFERER']);
				exit;
			}

			$link = $customer->getLinks()->getLink('self');//retorna o link contendo o ID MOIP GERADO
			$posicao = strpos($link, "CUS"); //RETORNA A POSICAO ONDE INICIA O ID MOIP 
			$moipId = substr($link, $posicao);
			$this->cliente_model->setMoipId($this->session->userdata('login'), $moipId);
			return true;
	}	


	public function consultar_cliente_moip(){
		$this->load->model('cliente_model');
		$customer_id = $this->cliente_model->getMoip($this->session->userdata('login'));
		
		if($customer_id == null){
			return false;
		}

		try {
		    $customer = $this->moip->customers()->get($customer_id);
			return $customer_id;
		} catch (Exception $e) {
			$this->session->set_flashdata('msg', "Não foi possível efetivar sua solicitação, informe o admnistrador do site. erro x876.");
			redirect($_SERVER['HTTP_REFERER']);
			exit;
		}
	}


	public function consulta_vendedor_moip($id){
		$this->load->model('vendedor_model');

		if($id == null){
			$account_id = $this->vendedor_model->getMoip($this->session->userdata('primeiro-vendedor'));
		}else{
			$account_id = $this->vendedor_model->getMoip($id);
		}

		if($account_id == null){
			return false;
		}

		try {
			$account = $this->moip->accounts()->get($account_id);
			return $account_id;//trocar por $account
		} catch (Exception $e) {
			// printf($e->__toString());
			$this->session->set_flashdata('msg', "Não foi possível efetivar sua solicitação, informe o admnistrador do site. erro x872.");
			redirect($_SERVER['HTTP_REFERER']);
			exit;
		}
	}

		
	public function sucessoPedido(){
		if($this->input->get('code')) {
			redirect( base_url("cliente/moip-permissao/{$this->input->get('code')}") );
			exit;
		}
		$order = $this->session->userdata('order');
		$this->session->unset_userdata('order');
		redirect( base_url("profile/detalhes/{$order}")); 
	}


	// criar pedido
	public function criar_pedido(){

		foreach ($this->cart->contents() as $produto) {
			if($produto["tipo"] == "atacado" && $produto["qty"] > $produto["qtd_min"]){
				$this->session->set_flashdata('danger', 'O produto ' .$produto['name'].' está com sua quantidade acima da quantidade permitida, para compras com esta quantidade favor comprar o mesmo em atacado.');
				echo "/cart";
				die();
			}
		}

		$frete = $this->input->post("frete");
		if(!$_POST['frete'] || empty($_POST['frete'])){
			$this->session->set_flashdata('danger', 'O valor de frete não foi definido');
			echo "/cart";
			die();
		}
		
		$this->load->model('pedido_model');
		$this->load->model('user_model');
		
		//COMISSAO
		$this->load->model('checkout_model');
		$comissao = $this->checkout_model->getComissao();
		$venda = 100.00; //Venda recebe 100%
		$venda -= $comissao;//retira-se do venda a comissao do gerente
		//COMISSAO

		$idMoipGer = $this->checkout_model->getIdMoipGerente();

		$customer = $this->consultar_cliente_moip();
		if(!$customer){
			$this->cadastrar_cliente_moip();	
			$customer = $this->consultar_cliente_moip();
		}

		$tipo = $this->user_model->getTipo($this->session->userdata('login'));
		
		if($tipo = "lojista"){
		//condicao criada para que cliente não tenha id moip de vendedor
			$vendedor = $this->consulta_vendedor_moip($this->session->userdata('primeiro-vendedor'));
			
			/*if(!$vendedor){
				$this->cadastrar_vendedor_moip();	
				$vendedor = $this->consulta_vendedor_moip();
			}*/
		}

		$valorBruto = 0;

		$moip = $this->moip;

		$order = $moip->orders()->setOwnId(uniqid());	
		
			foreach ($this->cart->contents() as $produto):
		        $order = $order->addItem($produto['name'],
		            (int)$produto['qty'],
		            $produto['name'],
					(int)str_replace(',', '',$produto['price'] * 100)
				);
		        $valorBruto += $produto['price'] * $produto['qty'];

			endforeach;


		$order->setShippingAmount($frete * 100)
			->setCustomerId($customer)
			->addReceiver($vendedor, 'SECONDARY', null, $venda, true)
			->addReceiver($idMoipGer, 'PRIMARY', null, $comissao, false)
			->setUrlSuccess("https://viamobil.com.br/sucesso/");
		$order->create();


		$valor_comissao = ($comissao / 100) * $valorBruto;

		$pedido = array(
			"order_num" => $order->getId(),
			"order_cliente" => $this->session->userdata('login'),
			"order_vendedor" => $this->session->userdata('primeiro-vendedor'),
			"order_valor" => $valorBruto,
			"order_situacao" => "AGUARDANDO",
			"order_frete" => $frete,
			"order_data" => date("Y-m-d H:i:s"),
			"order_reclamacao" => 0,
			"order_valor_comissao" => $valor_comissao,
			"order_tipo" => "Varejo"
			//"order_taxa_moip" => $order->getId()->getAmountFees()
		);

		$this->pedido_model->addOrder($pedido);
		
		foreach ($this->cart->contents() as $items):

			$pedidos_details = array(
				"orders_produto" => $items['id'],
				"orders_valor_unit" => $items['price'],
				"orders_qtd" => $items['qty'],
				"orders_num" => $order->getId(),
                "cor" => $items['cor'],
                "tamanho" => $items['tamanho']

			);
       	 	$this->pedido_model->addOrderDetais($pedidos_details);
       	 	
       	endforeach;

       	$this->session->set_userdata('order', $order->getId());

        $link = $order->getLinks()->getCheckout('payCheckout');
        $link_boleto = $order->getLinks()->getCheckout('payBoleto');
        $link_cartao = $order->getLinks()->getCheckout('payCreditCard');

		$email = $this->user_model->getMail($this->session->userdata('login'));//Retorna email do cliente
		$email_vendedor = $this->user_model->getMail($this->session->userdata('primeiro-vendedor'));
		$link_vendedor = "";
        $this->email_pedido($email, $link, "19", $order->getId());
        $this->email_pedido($email_vendedor, $link_vendedor, "21", $order->getId());

		echo $link;
	}

	public function pedidoDireto(){
        $id = $this->input->post("id");
        $qtd = $this->input->post("quantidade");
 		$frete = $this->input->post("frete");
 		$vendedor = $this->input->post("vendedor");
 		$cor = $this->input->post("cor");
 		$tamanho = $this->input->post("tamanho");
 		
 		$this->load->model('ads_model');
        $prod = $this->ads_model->getProduto($id);

        /*if($prod->ad_tipo == "atacado" && $qtd < $prod->ad_qtdmin ){
    		$this->session->set_flashdata("danger", "A quantidade minima para este produto é {$prod->ad_qtdmin}");
	        echo $nome;
	        die();

        }*/

    	//if($prod->ad_tipo == "varejo"){
			
		if($frete <= "0"){
			$this->session->set_flashdata('danger', 'O valor de frete não foi definido');
			redirect($_SERVER['HTTP_REFERER']);
			exit;
    	}
			
    	//}
		
        if($qtd == "") $qtd = 1;
		
        $this->load->model('pedido_model');
        $this->load->model('user_model');
		
        //COMISSAO
        //COMISSAO
		$this->load->model('checkout_model');
		$comissao = $this->checkout_model->getComissao();
		$venda = 100.00; //Venda recebe 100%
		$venda -= $comissao;//retira-se do venda a comissao do gerente
		//COMISSAO

		$idMoipGer = $this->checkout_model->getIdMoipGerente();

		$customer = $this->consultar_cliente_moip();
		if(!$customer){
			$this->cadastrar_cliente_moip();	
			$customer = $this->consultar_cliente_moip();
		}

		$tipo = $this->user_model->getTipo($this->session->userdata('login'));
		
		if($tipo = "lojista"){

			$vendedorMoip = $this->consulta_vendedor_moip($vendedor);
			
		};

		$valorBruto = 0;

        $moip = $this->moip;
 
        $order = $moip->orders()->setOwnId(uniqid());
         

        $order = $order->addItem($prod->ad_name,(int)$qtd,$prod->ad_name,(int)str_replace(',', '',$prod->ad_price * 100));

        $valorBruto += $prod->ad_price * $qtd;

 
       $order->setShippingAmount($frete * 100)
			->setCustomerId($customer)
			->addReceiver($vendedorMoip, 'SECONDARY', null, $venda, true)//ALTERAR PARA VARIAVEL
			->addReceiver($idMoipGer, 'PRIMARY', null, $comissao, false)
			->setUrlSuccess("https://viamobil.com.br/sucesso/");
		$order->create();


		$valor_comissao = ($comissao / 100) * $valorBruto;

		$pedido = array(
			"order_num" => $order->getId(),
			"order_cliente" => $this->session->userdata('login'),
			"order_vendedor" => $prod->use_id,
			"order_valor" => $prod->ad_price * $qtd,
			"order_situacao" => "AGUARDANDO",
			"order_frete" => $frete,
			"order_data" => date("Y-m-d H:i:s"),
			"order_reclamacao" => 0,
			"order_tipo" => "Atacado",
			"order_valor_comissao" => $valor_comissao 
		);

		$this->pedido_model->addOrder($pedido);
		
		$pedidos_details = array(
			"orders_produto" => $id,
			"orders_valor_unit" => $prod->ad_price,
			"orders_qtd" => $qtd,
			"orders_num" => $order->getId(),
			"cor" => $cor,
            "tamanho" => $tamanho

		);
       	
       	$this->pedido_model->addOrderDetais($pedidos_details);

       	$this->session->set_userdata('order', $order->getId());
        $link = $order->getLinks()->getCheckout('payCheckout');
        $link_boleto = $order->getLinks()->getCheckout('payBoleto');
        $link_cartao = $order->getLinks()->getCheckout('payCreditCard');
 
        $email = $this->user_model->getMail($this->session->userdata('login'));//Retorna email do cliente
        $email_vendedor = $this->user_model->getMail($this->session->userdata('primeiro-vendedor'));
        $link_vendedor = "";
        
        /*if($prod->ad_tipo == "atacado"){
        	$this->pedido_model->addBoleto($link_boleto,$order->getId());
            echo $link_boleto;
        }else{*/
            echo $link;
		//}

 
    }


	public function successOrder(){
		$this->load->model('user_model');
		$email = $this->user_model->getMail($this->session->userdata('login'));//Retorna email do cliente
		$pedido = "";
		$valor = $this->cart->total();
		$this->email_pedido($email, $valor, "20", $pedido);
		$this->load->view('app');
		$this->load->view('header');
		$this->load->view('success-order');
		$this->load->view('footer'); 
	}

	public function email_pedido($email, $conteudo, $id, $numero_pedido){
		/* Email Details */
		$email_details = $this->main_model->emailsDetails($id);


		$pedido_venda = $numero_pedido; //USADA NO PAGAMENTO APROVADO ID 20
		$pedido = $numero_pedido; // USADA NO PEDIDO REALIZADO COM SUCESSO ID 19
		$link = $conteudo;//ID 19
		$valor = dinheiro($conteudo);//ID 20
		/* Message */
		$content = $email_details->email_content;

		if (preg_match_all('/({\$+\w+})/', $content, $matches)){

			foreach ($matches[0] as $key => $value) {
				$variable = str_replace('{', '', $value);
				$variable = str_replace('}', '', $variable);
				$string = eval('return '. $variable . ';');

				$content = str_replace($value, $string, $content);
			}

		}
		
		$message = $content;

		/* Subject */
		$email_subject = $email_details->email_subject;

		/* To - Destinatário */
		$to = $email;

		/* Send Email */
		$this->main_model->email($to, $email_subject, $message);
	}

}