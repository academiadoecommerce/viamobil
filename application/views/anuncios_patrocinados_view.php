<section class="p-0 mt-5">

	<div class="container px-mobile">
		<div class="row m-0">
			<header class="section-heading heading-line">
				<h4 class="title-section bg pl-0"> ANÚNCIOS PATROCINADOS </h4>
			</header>
		</div>

		<div class="row">
			<div class="col-12 p-0 px-2">
				<div class="card border-0 bgnone">
					<?php
				    $patroc =  $this->db->order_by('adv_id', 'desc')
					    ->where("adv_tipo", "patrocinado")
					    ->get("advertising_p")
					    ->result();

				    if ($patroc) {
				        echo '<div class="col-md-12 p-0">';
				        echo '<ul class="row no-gutters toslick" style="display:none">';
				        foreach ($patroc as $key => $ad) { ?>
						<li class='col-sm-6 col-md-3 col-lg-2 bgwhite-child p-0'>
							<div class="al-item" style="padding-bottom: 15px;">

								<a class="itembox hover-grey" target="_blank" href="<?=$ad->adv_url?>"
									title="<?= $ad->adv_name ?>">

									<div class="card-body border-grey rounded m-2 p-0 hover-shadow"
										style="text-align: left; height: 253px;">
										<div style="
												width: 100%;
												height: 60%;
												background-image: url(<?=base_url("uploads/publicidade/{$ad->adv_img}")?>);
												background-size: contain;
												background-repeat: no-repeat;
												background-position: center;">
										</div>

										<hr class="mt-0 mb-1">

										<div class="row mx-2">
											<font size="5" class="ad-price" 
												style="height: 70px; font-size: 1.25em">
												<?=$ad->adv_name?>
											</font>
										</div>

										<div class="row mx-2">
											<div class="text-center p-0 mt-3 justity-content-center w-100">
												<a href="<?=$ad->adv_url?>" 
													class="btn btn-primary p-1 w-100">
													<i class="fa fa-cart-plus"></i>
													Ver Detalhes
												</a>
											</div>
										</div>
									</div>
								</a>
							</div>
						</li>
						<?php } ?>
						</ul>
					</div>
					<?php } else { ?>

					<?php echo $this->main_model->advertisingBox('top', '100%', '90px'); ?>

					<div class="ads-listing" id="ads-listing">
						<div align="center"><strong>Opss!<br>Nenhum anúncio encontrado!
							<br>
							Tente buscar uma palavra diferente, ou use os filtros!</strong>
						</div>
					</div>

					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
