<?php if ($this->session->userdata('login')) { ?>
    <h3>Suas Perguntas</h3>
<div class="col-md-12" id="post-data-login">

    <?php foreach ($questions as $duv): ?>

        <?php if ($duv["duvida_cliente"] == $this->session->userdata('login')) { ?>

            <?php if ($duv["duvidas_content_tipo"] == "cliente") { ?>
                <br>
                <i class="fas fa-comment-alt"></i>&nbsp;&nbsp;<span><?php echo $duv["duvidas_content_message"] ?></span>
                <br>
            <?php } else { ?>
                <i class="far fa-comment-alt"></i>&nbsp;&nbsp;
                <span style="color: #999;">
					<?php echo $duv["duvidas_content_message"]; ?>
                    <?php echo date("d/m/y", strtotime($duv["duvida_data_fim"])); ?>
                    <?php echo date("H:i", strtotime($duv["duvidas_content_hora"])); ?>
				</span>
                <br>
            <?php } ?>

        <?php } ?>

    <?php endforeach ?> 
</div>
<?php } ?>

<br><br>

<h3>Últimas Perguntas</h3>

<div class="col-md-12" id="post-data">
	
    <?php foreach ($questions as $duv): ?>

        <?php if ($duv["duvida_cliente"] != $this->session->userdata('login')) { ?>

            <?php if ($duv["duvidas_content_tipo"] == "cliente") { ?>
                <br>
                <i class="fas fa-comment-alt"></i>&nbsp;&nbsp;<span><?php echo $duv["duvidas_content_message"] ?></span>
                <br>
            <?php } else { ?>
                <i class="far fa-comment-alt"></i>&nbsp;&nbsp;
                <span style="color: #999;">
					<?php echo $duv["duvidas_content_message"]; ?>
                    <?php echo date("d/m/y", strtotime($duv["duvida_data_fim"])); ?>
                    <?php echo date("H:i", strtotime($duv["duvidas_content_hora"])); ?>
				</span>
                <br>
            <?php } ?>

        <?php } ?>
    <?php endforeach ?> 

</div>

<br>

<a id="btn-pergunta-vermais" style="color: #1259c3;">Ver mais</a>

<div class="ajax-load text-center" style="display:none">
    <div class="pace p-2" style="display: inline-flex;">
        <div class="pace-activity" style="position: unset;right: unset;margin: auto;"></div>
    </div>
    Carregando mais perguntas
</div>


<script type="text/javascript">

	<?php if ($this->session->userdata('login')) { ?>
		var id = <?php echo $this->session->userdata('login') ?>;

	<?php }else{?>

		var id = 0;

	<?php }?>

    var page = 0;


	$('#btn-pergunta-vermais').on('click', function () {
		page++;
    	loadMoreData(page);
	});




    function loadMoreData(page) {
        $.ajax({
            url: '',
            type: "get",
            data: {page: page, method: 'json'},
            beforeSend: function () {
                $('.ajax-load').show();
            }
        }).done(function (data) {
            if (data == "") {
                $('.ajax-load').html("Não encontramos mais perguntas.");
                return;
            }
            $.each(data, function(key, value){

                if(value.duvida_cliente == id){
                	if(value.duvidas_content_tipo == "lojista"){
                    	$("#post-data-login").append(`
                        	<i class="far fa-comment-alt"></i>&nbsp;&nbsp;
                        	<span style="color: #999;">${value.duvidas_content_message}</span>`);
                	} else {
                    	$("#post-data-login").append(`
                         <div class="mt-3">
                            <i class="fas fa-comment-alt"></i>&nbsp;&nbsp;
                            <span>${value.duvidas_content_message}</span>
                         </div>`);
                	}
                }else{
                	if(value.duvidas_content_tipo == "lojista"){
                    	$("#post-data").append(`
                        	<i class="far fa-comment-alt"></i>&nbsp;&nbsp;
                        	<span style="color: #999;">${value.duvidas_content_message}</span>`);
                	} else {
                    	$("#post-data").append(`
                         <div class="mt-3">
                            <i class="fas fa-comment-alt"></i>&nbsp;&nbsp;
                            <span>${value.duvidas_content_message}</span>
                         </div>`);
                	}
                }


            });
            $('.ajax-load').hide();

        })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('Falha na resposta do servidor...');
            });
    }
</script>
