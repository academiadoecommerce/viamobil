<!--Modal Deletar-->
<div id="modalDelete" class="modal fade">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Apagar registro</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <p>Tem certeza que deseja apagar esse registro?</p>
      
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
        <button type="button" id="Yes" class="btn btn-danger" onclick="$('#Yes').submit()">Sim</button>
      </div>
    </div>
  </div>
</div>
<!-- Fim modal delete -->