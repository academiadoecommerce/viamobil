<!-- anuncios populares slider section -->
<section class="p-0 mt-5">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <div class="container px-mobile">
        <div class="row m-0">
            <header class="section-heading heading-line">
                <h4 class="title-section bg pl-0"> ANÚNCIOS POPULARES </h4>
            </header>
        </div>
        <div class="row">
            <div class="col-12 p-0 px-2">
                <div class="card border-0 bgnone">
                    <?php $ads = $this->ads_model->get_anuncios_populares(); 
                    if ($ads) { ?>
                    <div class="col-md-12 p-0">
                        <ul class="row no-gutters toslick" style="display:none">
                            <?php foreach ($ads as $key => $ad) { ?>
                            <li class='col-sm-6 col-md-3 col-lg-2 bgwhite-child p-0'>

                                <div class="al-item" style="padding-bottom: 15px;">

                                    <a class="itembox hover-grey" target="_blank"
                                        href="<?= base_url("anuncio/{$ad->ad_slug}") ?>" title="<?= $ad->ad_name ?>">

                                        <div class="card-body border-grey rounded m-2 p-0 hover-shadow"
                                            style="text-align: left; height: 253px;">
                                            <div 
                                                style=" width: 100%; height: 220px;
                                                background-image: url(<?= thumbnail(@$ad->ads_img_file, "ads", 300, auto, 2) ?>);
                                                background-size: contain; background-repeat: no-repeat; background-position: center;">
                                            </div>
                                            <hr class="mt-0 mb-1">

                                            <div class="row mx-2 mt-2 h5">
                                                <span class="ad-price">
                                                <?php
                                                    /*if(isset($ad->ad_price2) && $ad->ad_price2 > 0) 
                                                        echo dinheiro($ad->ad_price2, 2 , true) 
                                                        ." - ". 
                                                        dinheiro($ad->ad_price, 2 , false);
                                                    else*/
                                                        echo dinheiro($ad->ad_price, 2 , true);
                                                    ?>
                                                </span>
                                            </div>

                                            <div class="row mx-2">
                                                <font size="3" style="height: 40px; font-size: 15px">
                                                    <?=truncate($ad->ad_name, 60)?>
                                                </font>
                                            </div>
                                    </a>

                                        <div class="row mx-2 mt-3">
                                            <div class="col-12 p-0">
                                                <div class="text-center">
                                                    <a href="<?=base_url("anuncio/{$ad->ad_slug}")?>" class="btn btn-primary p-1 w-100">
                                                            <i class="fa fa-cart-plus"></i>
                                                            Ver Detalhes
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                <?php } else { ?>
                <?php echo $this->main_model->advertisingBox('top', '100%', '90px'); ?>
                <div class="ads-listing" id="ads-listing">
                    <div align="center"><strong>Opss!<br>Nenhum anúncio encontrado!
                        <br>
                        Tente buscar uma palavra diferente, ou use os filtros!</strong>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    </div>
</section>