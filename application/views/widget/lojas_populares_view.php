<section class="p-0 mt-4">
	<div class="container px-mobile">
		<div class="row m-0">
			<header class="section-heading heading-line">
				<h4 class="title-section bg pl-0"> LOJAS POPULARES</h4>
			</header>
		</div>
		<div class="row">
			<div class="col-12 p-0 px-2">
				<div class="card border-0 bgnone">
					<div class="ads-shops">
						<?php
						if ($shops) { ?>
							<ul style="display:none" class="row no-gutters lojatoslick <? if(count($shops) > 4 || $this->agent->is_mobile()) echo 'toslick';?> ">
								<?php foreach ($shops as $key => $shop) {
									$image = thumbnail(@$shop->shop_img_file, "shops", 200, 200, 2);
									$ads   = count($this->shops_model->ads($shop->use_id));
									$shop->ads_cat_name = $this->ads_model->categoriesDetails($shop->ads_cat_id)->ads_cat_name;
									echo '<li style="flex: 0 0 20%;
									max-width: 20%;" class="col-sm-6 col-md-2 border-0 bgwhite-child">
									<div class="itembox">
									<div class="card-body border-grey rounded m-2 p-0 hover-shadow" style="text-align: left; height: 253px;">
									<a class="d-block hover-grey" target="_blank" title="' . $shop->shop_name . '" href="' . base_url('loja/' . $shop->shop_slug) . '">
									<img class="img-sm" alt="' . $shop->shop_name . '" src="' . $image . '">
									<hr class="mt-0 mb-2">
									<div class="mx-2">
									<h5 class="mt-2 ad-price">' . $shop->shop_name . '</h5>
									<div class="desc mt-1">
									<p class="mb-1" style="font-size: 17px;"><!--<i class="fa fa-fw fa-folder"></i>--> Categoria: ' . truncate($shop->ads_cat_name, 45) . '</p>
									</div>
									<span class="mt-2 h6"><strong class="text-success"> ' . $ads . ' anúncios</strong></span>
									<a class="btn btn-primary p-1 w-100 mt-4"><i class="fa fa-shopping-bag"></i> Ver Loja</a>
									</div>
									</a>
									</div>
									</div>
									</li>';
								}
								echo '</ul>';
							} else {
								echo '<div align="center"><strong>Opss!<br>Nenhuma loja cadastrada!
								<br>';
							} ?>
						</div>
					</div>
				</div>
			</div>
		</section>
