<?php if ($related ?? false) : ?>
<section class="p-0 mt-5">
    <hr>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <div class="container px-mobile">
        <div class="row m-0">
            <header class="section-heading ">
                <h4 class="title-section pl-0"> ANÚNCIOS RELACIONADOS </h4>
            </header>
        </div>
        <div class="row">
            <div class="col-12 p-0 px-2">
                <div class="card border-0 bgnone">
                    <div class="col-md-12 p-0">
                        <ul class="row no-gutters toslick" style="display:none">
                            <?php foreach ($related as $key => $relacionado): ?>
                            <li class='col-sm-6 col-md-4 col-lg-4 bgwhite-child p-0'>
                                <div class="al-item" style="padding-bottom: 15px;">
                                    <a class="itembox hover-grey" target="_blank"
                                        href="<?= base_url("anuncio/{$relacionado->ad_slug}") ?>"
                                        title="<?= $relacionado->ad_name ?>">
                                        <div class="card-body border-grey rounded m-2 p-0 hover-shadow"
                                            style="text-align: left; height: 253px;">
                                            <div style=" width: 100%; height: 220px;
                                                background-image: url(<?= thumbnail(@$relacionado->ads_img_file, "ads", 300, 'auto', 2) ?>);
                                                background-size: contain; background-repeat: no-repeat; background-position: center;">
                                            </div>
                                            <hr class="mt-0 mb-1">
                                            <div class="row mx-2 mt-2 h5">
                                                <span class="ad-price">
                                                    <?php echo dinheiro($relacionado->ad_price, 2 , true); ?>
                                                   
                                                </span>
                                            </div>
                                            <div class="row mx-2">
                                                <font size="3" style="height: 40px; font-size: 15px">
                                                    <?=$relacionado->ad_name?>
                                                </font>
                                            </div>
                                    </a>
                                    <div class="row mx-2 mt-3">
                                        <div class="col-12 p-0">
                                            <div class="text-center">
                                                <a href="<?=base_url("anuncio/{$relacionado->ad_slug}")?>" class="btn btn-primary p-1 w-100"><i
                                                    class="fa fa-cart-plus"></i> Ver Detalhes
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    </div>
</section>
<?php endif; ?>