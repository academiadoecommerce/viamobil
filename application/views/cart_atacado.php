<link rel="stylesheet" href="<?php echo base_url( "assets/css/cart.css" ); ?>">

<br> <br> <br> <br> <br> <br>
<?php if( $this->session->flashdata( 'error' ) ) { ?>
<div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata( 'error' ); ?>
</div>
<?php } ?>
<form method="POST" action="<?php echo base_url('index.php/Cart/atualizar')?>">
    <div class="container" style="min-height: 372px;">
        <div class="card shopping-cart">
            <div class="card-header ">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                Produtos no seu Carrinho de Compras
                <div class="clearfix"></div>
            </div>
            <div class="card-body py-3">
                <?php $i = 1; ?>
                <?php foreach ($produtos as $items): ?>
                <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
                <!-- PRODUCT -->
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-2 text-center">
                        <img class="img-responsive" src="<?= thumbnail(@$items['img'], "ads", 120, 80, 2) ?>"
                            alt="prewiew" width="120" height="80">
                    </div>
                    <div class="col-12 text-sm-center col-sm-12 text-md-left col-md-6">
                        <h5 class="product-name">
                            <strong><?php echo $items['name']; ?></strong>
                        </h5>
                        <h5>
                            <small>Vendedor: <b><?=$items['vendedor']?></b></small>
                        </h5>
                        <?php if(isset($items['tamanho']) || isset($items['cor'])): ?>
                        <h5>
                            <small>
                               Variações:
                                <?=$items['cor'] ? "Cor: {$items['cor']}" : ''?>
                                &nbsp;&nbsp;&nbsp;
                                <?=$items['tamanho'] ? "Tamanho: {$items['tamanho']}" : ''?>
                            </small>
                        </h5>
                        <?php endif; ?>
                    </div>
                    <div class="pt-3 col-12 col-sm-12 text-sm-center col-md-4 text-md-right row">
                        <div class="pt-3 col-3 col-sm-3 col-md-6 text-md-right" style="padding-top: 5px">
                            <h6><strong>R$ <?php echo $items['price']; ?> <span class="text-muted">x</span></strong>
                            </h6>
                        </div>
                        <div class="pt-2 col-4 col-sm-4 col-md-4">
                            <div class="">
                                <input type="number" min="1"
                                    max="<?=( ($items['qtd_min'] > 0) ? $items['qtd_min']-1 : '' )?>"
                                    name="<?=$i?>[quantidade_item]" step="1" value="<?=$items['qty']?>"
                                    class="<?=$i?>qty w-50" size="5" id="quantidade-item"
                                    data-tipo="<?php echo $items['tipo']?> "
                                    data-minimo="<?php echo $items['qtd_min']?>">

                                <input type="hidden" name="<?=$i?>[id-item]" step="1" value="<?=$items['rowid']?>"
                                    class="qty w-50" size="3" id="id-item">

                                <input type="hidden" name="maximo" step="1" value="<?=$items['max']?>"
                                    class="qty w-50" size="3" id="maximo">
                            </div>
                        </div>
                        <div class="pt-3 col-2 col-sm-2 col-md-2 text-right" style="/*line-height: 80px*/">
                            <a href="<?=base_url("cart/remove/{$items['rowid']}")?>" class="btn btn-danger btn-sm">
                                <i class="fa fa-trash m-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <hr>
                <?php $i++; ?>
                <?php endforeach; ?>
                <!-- END PRODUCT -->
</form>
<div class="row px-4 py-2" style="justify-content:space-between">
    <div style="line-height:15px">
        <div class="float-left" style="margin: 5px">
            Preço total: <b id="valor_total">R$ <?php echo $this->cart->total(); ?></b>
        </div>
        <br>
        <div class="float-left" style="margin: 5px">
            Total de Itens: <b id="total_item_r"><?= $this->cart->total_items() ?></b>
        </div>
        <br>
    </div>
    <div style="margin: 5px">
        Frete:<span id="retorno-frete">
        <label for="input-cep">Calcular</label>
            <!-- <a data-toggle="modal" data-target="#modal-frete"> Calcular</a> -->
            </span> 
            <div class="input-group">
                <input class="form-control" type="text" required name="cep" id="input-cep"
                    placeholder="00000-000" data-mask="99999-999" 
                    value="<?php echo ( ( $cep_cliente ) ? $cep_cliente : '' ); ?>"
                />
                <div class="input-group-append">
                <button class=" btn btn-default" id="btn_calcular" style="width:50px">Ok</button>
                </div>
            </div>
        <input type="hidden" name="valor_frete" id="valor_frete">
    </div>
</div>
</div>
<div class="card-footer p-3">
    <a href="<?=base_url('anuncios')?>" class="btn btn-info float-left">
        <i class="fas fa-chevron-left"></i> Continuar comprando</a>
    <?php if($this->session->userdata('login')){?>
    <a onclick="pedido();" style="width: unset;" class="btn btn-success float-right">Finalizar compra <i
            class="fa fa-shopping-cart m-1"></i></a>
    <?php }else{ ?>
     <a data-toggle="modal" data-target="#modal-required" style="width: unset;"
        class="btn btn-success float-right btn-checkout" data-to>Finalizar compra <i
            class="fa fa-shopping-cart m-1"></i></a>
    <?php }?>
</div>
</div>
</div>




<!-- modal frete -->
<div class="modal fade" id="modal-frete" tabindex="-1" role="dialog">
    <div class="modal-dialog login1" style="margin-top: 130px;">
        <div class="modal-content">
            <div class="user-box">
                <div class="page-header" style="margin: 0 0 20px;">
                    <h3>Formas de entrega</h3>
                </div>
                <h5>Você poderá ver custos e prazos de entrega precisos.</h5>

                <div class="modal-frete-form">
                    <div class="form-group row">
                        <div class="col-12">
                            <!-- <input class="form-control" type="text" required name="cep" id="input-cep"
                                placeholder="00000-000" data-mask="99999-999" 
                                value="<?php echo ( ( $cep_cliente ) ? $cep_cliente : '' ); ?>"> -->
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <!-- <button class="form-control" id="btn_calcular">Calcular</button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var base_url = '<?php echo base_url() ?>';
</script>
<script src="<?= base_url('assets/js/custom/cart.js') ?>"></script>