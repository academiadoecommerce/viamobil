<style type="text/css">
    li { list-style-type: none; }
    ul#submenu { float: left; width: 638px; }
    .lists { float: left; width: 33%; }
    #idou { text-align: center; width: 100%; padding-top: 10px; }
</style>

<!-- ========================= RETURN ALERTS ========================= -->
<?php
$return = $this->session->flashdata('return');
$ret = $this->main_model->returnDetails($return);

if($ret){
	echo '
	<div id="msg-box" style="padding: 10px;">
		<div style="padding-left: 20px;" class="row alert alert-'.$ret->ret_type.'"><i class="fa fa-fw '.$ret->ret_icon.'"></i>'.$ret->ret_text.'</div>
	</div>';
} ?>

<!-- ========================= SECTION MAIN ========================= -->

<div class="row d-block m-0">
    <div class="col-md-12 p-0">
        <div class="owl-init slider-main owl-carousel d-flex">
            <?
							foreach ($imgbanner as $key) { ?>
            <div class="item-slide">
                <a target="_blank" href="<?=$key->banner_slug?>">
                    <img alt="<?=$key->banner_name?>" src="<?= base_url('assets/img/banners/'.$key->banner_img)?>">
                </a>
            </div>
            <? } ?>
        </div>
        <style type="text/css">
            .owl-dots { display: none; justify-content: center; }
            button.owl-dot { border: 1px solid #b5b5b5 !important; height: 14px; margin: 8px; border-radius: 10px; background: transparent; }
            button.owl-dot.active { border: 1px solid #2ebf77 !important; }
        </style>
    </div>

    <!--card sub banner float-->
    <div class="row m-0 px-4 bannercard hide">
        <div class="col-sm-6 col-xs-12">
            <div class="panel panel-default bgwhite p-2">
                <div class="panel-body">
                    <p class="lead text-center font-weight-bold">Melhores lojas</p>

                    <p>
                        <?php
                            $c = 0;
                            foreach ($bestseller as $key => $value) {
                                $c++;
                                if($c == 7) break;
                        ?>
                        <div class="ml-3 p-2" style="width:30%; height:100px;float: left;">
                            <div style="width: 100%; height: 100%;
                                background-image: url(<?= thumbnail(@$value->ads_img_file, "ads", 300, auto, 2) ?>);
                                background-size: contain;background-repeat: no-repeat;background-position: center;">
                            </div>
                        </div>
                        <?php } ?>
                    </p>
                    <div class="mt-2"> <br>
                        <p class="text-center" style="position: relative; top: 15px; margin-bottom: 33px;">
                            <span>Encontre os vendedores mais qualificados! </span>
                            <a href="<?= base_url( 'anuncios' ); ?>" class="btn btn-outline-primary text-dark"
                                style=" margin-left: 23px; height: 25px; padding: 1px 18px; ">Ver mais</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xs-12">
            <div class="panel panel-default bgwhite p-2">
                <div class="panel-body">
                    <p class="lead text-center font-weight-bold">Mais vendidos</p>
                    <p>
                        <?php $c = 0;
                        foreach ($bestseller as $key => $value) {
                            $c++;
                            if($c == 7) break;
                        ?>
                        <div class="ml-3 p-2" style="width:30%; height:100px;float: left;">
                            <a href="<?= base_url("anuncio/{$value->ad_slug}") ?>" title="<?= $value->ad_name ?>">
                                <div style="width: 100%; height: 100%;
                                    background-image: url(<?= thumbnail(@$value->ads_img_file, "ads", 300, auto, 2) ?>);
                                    background-size: contain;background-repeat: no-repeat;background-position: center;">
                                </div>
                            </a>
                        </div>
                        <?php } ?>
                    </p>
                    <div class="mt-2"> <br>
                        <p class="text-center" style="position: relative; top: 15px; margin-bottom: 33px;">
                            <span>Confira a lista dos produtos mais vendidos na semana! </span>
                            <a href="<?= base_url("anuncio/{$value->ad_slug}") ?>" class="btn btn-outline-primary text-dark"
                                style=" margin-left: 23px; height: 25px; padding: 1px 18px; ">Ver mais</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<!-- ========================= SECTION ANUNCIOS POPULARES 	// ========================= -->
<?php require 'widget/subbanner_info_widget.php'; ?>

<!-- ========================= SECTION ANUNCIOS POPULARES 	// ========================= -->
<?php require 'widget/carrousel_ads.php'; ?>

<!-- ========================= SECTION ANUNCIOS POPULARES 	// ========================= -->
<?php //require 'widget/anuncios_populares_view.php'; ?>

<!-- ========================= SECTION LOJAS POPULARES 		// ========================= -->
<?php require 'widget/lojas_populares_view.php'; ?>

<!-- ========================= SECTION ANUNCIOS PATROCINADOS 		// ========================= -->
<?php require 'widget/anuncios_patrocinados_view.php'; ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('.toslick').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,

            prevArrow: '<a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">\n' +
                '                    <i class="fa fa-angle-left"></i>\n' +
                '                </a>',

            nextArrow: '<a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">\n' +
                '                    <i class="fa fa-angle-right"></i>\n' +
                '                </a>',

            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        $(".toslick").fadeIn(700)
        $(".lojatoslick").fadeIn(700)
    })
</script>

<style type="text/css">
    .slick-arrow {
        cursor: pointer;
        position: absolute;
        top: 40% !important;
        z-index: 9;
        width: 60px;
        height: 60px;
        border-radius: 50%;
        text-align: center;
        padding-top: 18px;
        opacity: 0;
        transition: .2s;
    }
    .sl-next { right: -20px; }
    .sl-prev { left: -20px; }
    .toslick:hover .slick-arrow { color: #1e2648; background: inherit; opacity: 1; }
</style>
