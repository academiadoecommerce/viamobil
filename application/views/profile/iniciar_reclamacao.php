<div class="col-12">
	<h4 class=" w-100 font-weight-bold ml-1">Reclame aqui</h4>
</div>
<div class="d-flex">
	<div class="col-md-6">
		<div class="md-form mb-2">
			<label data-error="wrong" data-success="right" for="order">ORDER</label>
			<input type="text" id="order" value="<?=(($this->input->get('order')) ? $this->input->get('order') : '')?>" class="form-control validate" disabled="">
		</div>
		<div class="md-form mb-2">
			<label data-error="wrong" data-success="right" for="assunto">Assunto</label>
			<input type="text" id="assunto" class="form-control validate">
		</div>
		<div class="md-form">
			<label data-error="wrong" data-success="right" for="descricao">Descreva sua reclamação</label>
			<textarea type="text" id="descricao" class="md-textarea form-control" rows="4" placeholder="Descreva com detalhes a sua reclamação"></textarea>
		</div>
		<div class="d-flex justify-content-start">
			<button type="button" class="btn btn-primary" onclick="reclamacao()">Iniciar</button>
		</div>
	</div>
	<div class="col-md-6 p-2">
		<div class="alert mt-4" style="position:unset; width:unset">
			<strong>Saiba como funciona nossa política de reclamação.</strong> <br> <br>
			<p>O objetivo desta política é proporcionar um mecanismo para lidar com reclamações, de forma a
melhorar a qualidade do nosso trabalho, ampliar nossa credibilidade, aumentar confiança no que fazemos e
evidenciar áreas que precisam ser aprimoradas <br> <a href="#">Saiba mais</a></p>
		</div>
	</div>
</div>


<script type="text/javascript">
var base_url  = '<?php echo base_url() ?>'
   function reclamacao(){
       var id_pedido = document.getElementById('order').value
       var assunto   = document.getElementById('assunto').value
       var descricao = document.getElementById('descricao').value

       $.post(base_url+'/Reclamacao/inicia', { id_pedido, assunto, descricao } )
       .done( function( data ){
          data = $.parseJSON(data);
          if (data.status == 'success') {
            console.log(data);
            window.location.replace(data.redirect);
          }
       });
   }
</script>