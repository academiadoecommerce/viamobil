<script src="<?=base_url('assets/js/custom/cep.js')?>"></script>
<script src="<?=base_url('assets/js/jquery.maskMoney.js')?>"></script>
<style type="text/css">
	.page-add-ads div.col-add {height: 250px;}
	.simple-page ul {list-style-position: unset;}
</style>

<div class="simple-page announce-insert page-add-ads">
	<h1>Editar Anúncio</h1>

	<form enctype="multipart/form-data" method="POST" accept-charset="utf-8" action="<?=base_url('profile/ad_edit/save/'.$ad->ad_id)?>" id="ai-form"  class="form form-simple ai-form">
		<input type="hidden" name="hash" id="ai-hash" value="<?=$hash?>">
		<input type="hidden" id="ai-ads-code" value="<?=$ad->ad_id?>">

		<div class="row" id="ai-f-category-required">
			<div class="medium-12 columns">
				<div class="alert alert-danger desbug-alert"><strong>Atenção!</strong> Selecione uma categoria para continuar...</div>
			</div>
		</div>

		<div class="row ads-step-start">
	
			<div class="col-12 col-md-4">
                <div class="form-group border rounded p-2 col-add shadow-sm bgwhite">
                    <ul id="categ">
                        <? foreach ($categories as $cat) { ?>
                            <li class="<?=(($cat1->ads_cat_id == $cat->ads_cat_id) ? 'selected' : '')?>"  id="<?=$cat->ads_cat_id?>" ><?=$cat->ads_cat_name?></li>
                        <? } ?>
                        
                        <input type="text" name="categ" hidden value="<?=$ad->ads_cat_id?>">
                    </ul>
                  </div>                
            </div>

			<div class="col-12 col-md-4 <?=(($cat2->ads_cat_id) ? '' : 'd-none')?> colsub">
                <div class="form-group border rounded p-2 col-add shadow-sm bgwhite">
                    <ul id="subcateg">
                        <? foreach($categoriafilho as $filho){ ?>
                            <li 
                            class="
                            <?=(($filho->ads_cat_parent == $cat1->ads_cat_id) ? '' : 'd-none ')?>
                            <?=(($cat2->ads_cat_id == $filho->ads_cat_id) ? 'selected' : '')?> " data-parent="<?=$filho->ads_cat_parent?>"  id="<?=$filho->ads_cat_id?>" ><?=$filho->ads_cat_name?></li>
                        <? } ?>
                    </ul>
                  </div>                
            </div>

            <!-- categorias terciarias -->
            <div class="col-12 col-md-4 tertiary <?=(($cat3->ads_cat_id) ? '' : 'd-none')?>">
                <div class="form-group border rounded p-2 col-add shadow-sm tertiary bgwhite">
                    <ul id="tertiary">
                        <? foreach($categ_tertiary as $tertiary){ ?>
                            <li class="
                            <?=(($cat3->ads_cat_parent == $tertiary->ads_cat_parent) ? '' : 'd-none')?> 
                            <?=(($cat3->ads_cat_id == $tertiary->ads_cat_id) ? 'selected' : '')?>" data-parent="<?=$tertiary->ads_cat_parent?>"  id="<?=$tertiary->ads_cat_id?>" ><?=$tertiary->ads_cat_name?></li>
                        <? } ?>
                    </ul>
                  </div>                
            </div>
		</div>

		<div class="row">

            <div class="hide-for-small-only medium-2 columns">
                <label class="text-right middle">Título:<span class="required">*</span></label>
            </div>
            
            <div class="small-12 medium-10 large-10 end columns">
                <label class="show-for-small-only">Título:<span class="required">*</span></label>
                
                  <div id="msg_titulo" class="alert alert-subtitle desbug-alert" id="msgImg" style="display: none;">
                    <ul><li><span style="font-size: 15px;"><i class="fa fa-info-circle"></i> Atenção:</span> <span style="color: orange; font-size: 14px;">	 	<strong>Não aceitamos venda de animais! Não compre, adote <i class="fa fa-heart"></i></strong></span></li></ul>
                </div>
                

                <input type="text" value="<?=$ad->ad_name?>" required name="title" id="ai-title" placeholder="Título do Anúncio" maxlength="70">
            </div>
        </div>

		<div class="row">
			<div class="hide-for-small-only medium-2 columns">
				<label class="text-right middle">Descrição:<span class="required">*</span></label>
			</div>
			<div class="small-12 medium-10 large-10 end columns">
				<label class="show-for-small-only">Descrição:<span class="required">*</span></label>
				<textarea required name="desc" rows="6" placeholder="Descrição do Anúncio"><?=$ad->ad_desc?></textarea>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="hide-for-small-only medium-2 columns">
				<label class="text-right middle">Descrição Atacado:<span class="required">*</span></label>
			</div>
			<div class="small-12 medium-10 large-10 end columns">
				<label class="show-for-small-only">Descrição Atacado:<span class="required">*</span></label>
				<textarea required name="desc_atacado" id="desc_atacado" rows="6" placeholder="Descrição do Anúncio"><?=$ad->ad_descricao_atacado?></textarea>
			</div>
		</div>

        <div id="desativa" <?php echo ($ad->ads_cat_id == 114) ? 'style="display:none;"':'';?> class="row mt-4">
			<div id="not_desktop_preco" <?php echo ($ad->adote) ? 'style="display:none;"':'';?> class="hide-for-small-only medium-2 columns">
				<label class="text-right middle">Preço:<span class="required">*</span></label>
			</div>
			
			<div id="not_preco" <?php echo ($ad->adote) ? 'style="display:none;"':'';?> class="small-12 medium-4 large-4 end columns">
				<label class="show-for-small-only">Preço:<span class="required">*</span></label>


				<div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">R$</span>
                  </div>
                  <input type="text" required class="form-control input-money" name="price" id="price1" placeholder="Preço do Anúncio" value="<?=$ad->ad_price?>">
                </div>
			</div>
		</div>

		<div id="desativa" <?php echo ($ad->ads_cat_id == 114) ? 'style="display:none;"':'';?> class="row mt-4">
			<div id="not_desktop_preco" <?php echo ($ad->adote) ? 'style="display:none;"':'';?> class="hide-for-small-only medium-2 columns">
				<label class="text-right middle">Preço:<span class="required">*</span></label>
			</div>
			
			<div id="not_preco" <?php echo ($ad->adote) ? 'style="display:none;"':'';?> class="small-12 medium-4 large-4 end columns">
				<label class="show-for-small-only">Preço Atacado:<span class="required">*</span></label>

				<div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">R$</span>
                  </div>
                  <input type="text" required class="form-control input-money" name="price2" id="price2" placeholder="Preço do Anúncio" value="<?=$ad->ad_price2?>">
                </div>
			</div>
		</div>


		<div id="desativa" class="row mt-4">
			<div id="not_desktop_preco" class="hide-for-small-only medium-2 columns">
				<label class="text-left middle">Altura (cm):<span class="required">*</span></label>
				
			</div>
			<div id="not_preco" class="small-12 medium-4 large-4 end columns">
				<label class="show-for-small-only">Altura (cm):<span class="required">*</span></label>
				<div class="input-group mb-3">
					<input type="number" required class="form-control" aria-label="Amount (to the nearest dollar)" name="altura" id="altura" placeholder="Altura" value="<?=$ad->ad_altura?>">
				</div>
			</div>
		</div>

		<div id="desativa" class="row mt-4">
			<div id="not_desktop_preco" class="hide-for-small-only medium-2 columns">
				<label class="text-left middle">Largura (cm):<span class="required">*</span></label>
				
			</div>
			<div id="not_preco" class="small-12 medium-4 large-4 end columns">
				<label class="show-for-small-only">Largura (cm):<span class="required">*</span></label>
				<div class="input-group mb-3">
					<input type="number" required class="form-control" aria-label="Amount (to the nearest dollar)" name="largura" id="largura" placeholder="Largura" value="<?=$ad->ad_largura?>">
				</div>
			</div>
		</div>		
		
		<div id="desativa" class="row mt-4">
			<div id="not_desktop_preco" class="hide-for-small-only medium-2 columns">
				<label class="text-left middle">Comprimento (cm):<span class="required">*</span></label>
				
			</div>
			<div id="not_preco" class="small-12 medium-4 large-4 end columns">
				<label class="show-for-small-only">Comprimento (cm):<span class="required">*</span></label>
				<div class="input-group mb-3">
					<input type="number" required class="form-control" aria-label="Amount (to the nearest dollar)" name="comprimento" id="comprimento" placeholder="Comprimento" value="<?=$ad->ad_comprimento?>">
				</div>
			</div>
		</div>

		<div id="desativa" class="row mt-4">
			<div id="not_desktop_preco" class="hide-for-small-only medium-2 columns">
				<label class="text-left middle">Diametro:</label>
				
			</div>
			<div id="not_preco" class="small-12 medium-4 large-4 end columns">
				<label class="show-for-small-only">Diametro:</label>
				<div class="input-group mb-3">
					<input type="number" class="form-control" aria-label="Amount (to the nearest dollar)" name="diametro" id="diametro" placeholder="Diametro" value="<?=$ad->ad_diametro?>">
				</div>
			</div>
		</div>

		<div id="desativa" class="row mt-4">
			<div id="not_desktop_preco" class="hide-for-small-only medium-2 columns">
				<label class="text-left middle">Peso (kg):<span class="required">*</span></label>
			</div>
			<div id="not_preco" class="small-12 medium-4 large-4 end columns">
				<label class="show-for-small-only">Peso (kg):<span class="required">*</span></label>
				<div class="input-group mb-3">
					<input type="text" required class="form-control" aria-label="Amount (to the nearest dollar)" name="peso" id="peso" data-mask="00,00" placeholder="Peso" value="<?=$ad->ad_peso?>">
				</div>
			</div>
		</div>

		<div id="desativa" class="row mt-4">
			<div id="not_desktop_preco" class="hide-for-small-only medium-2 columns">
				<label class="text-left middle">Quantidade Min:<span class="required">*</span></label>
				
			</div>
			<div id="not_preco" class="small-12 medium-4 large-4 end columns">
				<label class="show-for-small-only">Quantidade Min:<span class="required">*</span></label>
				<div class="input-group mb-3">
					<input type="number" required class="form-control" aria-label="Amount (to the nearest dollar)" name="qtdmin" id="qtdmin" placeholder="1" value="<?=$ad->ad_qtdmin?>">
				</div>
			</div>
		</div>


		<div id="desativa" class="row mt-4">
			<div id="not_desktop_preco" class="hide-for-small-only medium-2 columns">
				<label class="text-left middle">Quantidade Max:<span class="required">*</span></label>
				
			</div>
			<div id="not_preco" class="small-12 medium-4 large-4 end columns">
				<label class="show-for-small-only">Quantidade Max:<span class="required">*</span></label>
				<div class="input-group mb-3">
					<input type="number" required class="form-control" aria-label="Amount (to the nearest dollar)" name="qtdmax" id="qtdmax" placeholder="1" value="<?=$ad->ad_qtdmax?>">
				</div>
			</div>
		</div>

		<!-- custom fields - begin -->
		<div id="ai-custom-fields"></div>

		<!-- custom fields - end -->
		<div class="row">
			<div class="hide-for-small-only medium-2 columns">
				<label class="text-right middle">Imagens:<span class="required">*</span></label>
			</div>
			<div class="small-12 medium-10 large-10 end columns">
				<label class="show-for-small-only">Imagens:<span class="required">*</span></label>
	
				<div class="dropzone" id="images-upload">
					<?php
						if($images){
							foreach ($images as $key => $img) {
								echo '
									<div class="dz-preview dz-file-preview dz-image-db" id="i-'.$img->ads_img_id.'">
										<div class="dz-remove-file"><span title="Apagar Imagem" data-image="'.$img->ads_img_id.'"><i class="fa fa-fw fa-trash"></i></span></div>
										<div class="dz-image">
											<img src="'.thumbnail($img->ads_img_file, 'ads', 120, 120).'">
										</div>
									</div>
								';
							}
						}
					?>
				</div>

				<div id="image-preview-template" style="display: none">
					<div class="dz-preview dz-file-preview">
						<div class="dz-remove-file" style="display: none"><span title="Apagar Imagem" data-dz-remove><i class="fa fa-fw fa-trash"></i></span></div>
						<div class="dz-image">
							<img data-dz-thumbnail />
						</div>
						<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
						<div class="dz-success-mark"><span><i class="fa fa-fw fa-check"></i></span></div>
						<div class="dz-error-mark"><span><i class="fa fa-fw fa-close"></i></span></div>
						<div class="dz-error-message"><span data-dz-errormessage></span></div>
					</div>
				</div>					
				<button type="button" class="btn btn-m-bottom-medium" id="images-upload-button">
					<i class="fa fa-picture-o"></i> Adicionar<br/>fotos
				</button>
				<div style="clear: both;"></div>
				<div class="alert alert-subtitle hide-for-small-only desbug-alert" id="msgImg" style="display: none;">
                                    <ul><li><span style="font-size: 15px;">Atenção:</span> <span style="color: red; font-size: 14px;"><strong>Altura e largura mínima:</strong> 350x260px</span></li></ul>
                                </div>
				<div class="alert alert-subtitle desbug-alert">
					<ul>
						<li><strong>Tamanho máximo:</strong> 8MB</li>
						<li><strong>Altura e largura mínima:</strong> 400px</li>
						<li><strong>Máximo de imagens permitidas:</strong> 10</li>
						<li><strong>Tipos arquivo permitido:</strong> JPG, JPEG e PNG</li>
						<li><strong>A primeira imagem será usada como a principal</strong></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="hide-for-small-only medium-2 columns">
				<label class="text-right middle">Vídeo:</label>
			</div>
			<div class="small-12 medium-6 large-6 end columns">
				<label class="show-for-small-only">Vídeo:</label>
				<input type="text" name="video" placeholder="Link Vídeo do Produto" value="<?=$ad->ad_video?>" data-original-title="Só aceitamos videos do Youtube, caso coloque de outro site, não garantimos o funcionamento." data-toggle="tooltip" data-placement="top" title="">
				
			</div>
		</div>

		<div class="row">
			<div class="small-12 columns" align="center">
				<div class="ai-f-actions">
					<button type="button" onclick="window.location.href = '<?=base_url('cliente/painel')?>'" class="btn btn-default">Cancelar</button>
                    <button type="submit" id="publish-button"  class="btn btn-primary"><i class="fa fa-floppy-o"></i>Salvar</button>
				</div>
			</div>
		</div>
	</form>
</div>
<script>
    $(function() {
        $("#ai-adote").click(function() {
            if ($('#ai-yes-adote').is(':checked')) {
                $('#ai-price').prop('disabled', false);
                $('#ai-yes-adote').prop('checked', false);
                $('#not_preco').show();
                $('#not_desktop_preco').show();
                $('#service').show();
                $('#trade').show();
                $('#bloco').addClass('small-12 medium-6 large-6 columns');
                $('#bloco').removeAttr('style');
            } else {
                $('#bloco').removeClass('small-12 medium-6 large-6 columns');
                $('#bloco').css({"margin-left":"150px", "padding-bottom":"60px"});
                $('#service').hide();
                $('#trade').hide();
                $('#not_preco').hide();
                $('#not_desktop_preco').hide();
                $('#ai-price').prop('disabled', true);
                $('#ai-yes-adote').prop('checked', true);
            }
        });
    });

    $("#ai-f-c-parent").on('click', 'li', function(event) {
        var btn = $(this);
        var cat = btn.attr('data-id');
        event.preventDefault();
        if (cat == 1) {
            $('#msg_titulo').show();
            $('#adote').show();
        } else {
            $('#msg_titulo').hide();
            $('#adote').hide();
        }
        if (cat == 114) {
            $('#ai-price').prop('disabled', true);
            $('#desativa').hide();
        } else {
            $('#ai-price').prop('disabled', false);
            $('#desativa').show();
        }
    });
</script>
<script>
    var total_img = $('.dz-image').length;
    if(total_img == 1){
        $('#publish-button').prop('disabled', true);
    }else{
        $('#publish-button').prop('disabled', false);
    }
</script>
<script src="<?=base_url('assets/js/vendor/dropzone.js')?>"></script>
<script src="<?=base_url('assets/js/custom/announce.js')?>"></script>
<script src="<?=base_url('assets/js/custom/profile_ad_edit.js')?>"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#ai-f-c-parent ul').prepend($('#ai-f-c-parent ul li.active'));
		$('#ai-f-c-sub ul').prepend($('#ai-f-c-sub ul li.active'));
	});
</script>




<script type="text/javascript">
    var hash = $("#ai-hash").val();
    var previewTemplate = document.getElementById("image-preview-template").innerHTML;

    var myDropzone = new Dropzone("div#images-upload", {
        url: base_url + "/announce/images_upload/" + hash,
        method: 'POST',
        maxFiles: 10,
        maxFilesize: 10,
        acceptedFiles: "image/jpeg,image/png,image",
        clickable: "#images-upload-button",
        previewTemplate: previewTemplate
    });

    console.log(myDropzone);

    myDropzone.on('addedfile', function(event) {
        $(".dz-message").remove();
    });

    myDropzone.on("success", function(file) {
        var box = file.previewElement;
        var response = file.xhr.response;
        $('#msgImg').hide();
        if (response == 'error') {
            myDropzone.removeFile(file);
            $('#msgImg').show();
            setTimeout(function() {
               $('#msgImg').hide();
            }, 5000);
        } else {
            $('#publish-button').prop('disabled', false);
            box.id = "i" + response;
            setTimeout(function() {
                $("#i" + response).children('.dz-remove-file').show();
            }, 500);
        }
        
        //console.log(response);


    });

    myDropzone.on("complete", function(file) {
        var status = file.status;

        if (status == "error") {
            var timer = (Math.floor((Math.random() * 10) + 1)) * 500;

            setTimeout(function() {
                myDropzone.removeFile(file);
            }, timer);
        }
    });

    myDropzone.on('removedfile', function(file) {
       
        if (file.status == "success") {
            var response = file.xhr.response;
            
            $.ajax({
                url: base_url + '/announce/images_remove',
                type: 'POST',
                data: {image: response},
            }).done(function() {
                        if($('.dz-image').length == 1){
                            $('#publish-button').prop('disabled', true); 
                        }
                        console.clear();
                    }).fail(function() {
                        console.log("image delete: error");
                    });
             
        }
    });
</script>





<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	  <script>
	    $(document).ready(function(){

	          $(document).on('mousedown', '.dz-preview', function(){
	                id = $(this).attr('id')
	                id = id.replace('-', '')
	                id = id.replace('i', '')
	                $(this).attr('id', 'i-'+id)
	          })

	          $('#images-upload').on('mouseover', function() {
	            $('.dz-started, #images-upload').sortable({
	            update: function (event, ui) {
	                var data = $(this).sortable('serialize')

	                // POST to server using $.post or $.ajax
	                $.ajax({
	                    data: data,
	                    type: 'POST',
	                    url: '<?=base_url('announce/reorder_img')?>'
	                })
	                }
	            })
	        })
	      })
	  </script>


<script type="text/javascript">

    $(document).ready(function(){

    	$('.col-add').each(function(){
			$(this).find('ul').prepend($(this).find('li.selected'))
		})
        
        $('ul#categ li').click(function(){

           var categ_id = $(this).attr('id');

            $('#categ li').removeClass('selected');
            $(this).addClass('selected');

            $("ul#subcateg li").addClass('d-none') ;
            $("div.tertiary").addClass('d-none') ;
            var cat = $("ul#subcateg").find("[data-parent='"+categ_id+"']").fadeIn(800).removeClass('d-none') ;
            if(cat.length > 0) {
                $('div.colpronto').addClass('d-none');
                $('div.colsub').fadeIn(800).removeClass('d-none');
            }else{
                $('div.colsub').fadeIn(800).addClass('d-none');
                $('div.colpronto').fadeIn(800).removeClass('d-none');
            }

           // adicionar este id em imput hidden
            $('ul#categ input').attr('value', categ_id);
        });
        

        $('#subcateg li').click(function(){
            var subcateg_id = $(this).attr('id');
            $('#subcateg li').removeClass('selected');
            $(this).addClass('selected');
            $("ul#tertiary li").addClass('d-none');

            // verifica se esta sub possui subs
            var tertiary = $("ul#tertiary").find("[data-parent='"+subcateg_id+"']").fadeIn(800).removeClass('d-none') ;

            if (tertiary.length > 0) {
                $('div.colpronto').addClass('d-none');
                $('div.tertiary').fadeIn(800).removeClass('d-none');
            }else{
                $('div.tertiary').fadeOut(800).addClass('d-none');
                $('div.colpronto').fadeIn(800).removeClass('d-none');
            }

           // adicionar este id em imput hidden
            $('ul#categ input').attr('value', subcateg_id);
        });

        $('ul#tertiary li').click(function(){

           var categ_id = $(this).attr('id');

            $('#tertiary li').removeClass('selected');
            $(this).addClass('selected');
           
            $('div.colpronto').fadeIn(800).removeClass('d-none');

            $('.ads-step-start').animate({scrollLeft:'+=500'},500);

           // adicionar este id em imput hidden
            $('ul#categ input').attr('value', categ_id);
        });

        $('[name = ads_cat_name]').keyup(function(){
            if(this.value.length > 5) {
                $('[alt = pronto]').fadeIn(800);
                $('.colpronto h3').fadeIn(800);
                $('#continuar').removeAttr('disabled');

            } else if(this.value.length < 6) {
                $('[alt = pronto]').fadeOut(800);
                $('.colpronto h3').fadeOut(800);
                $('#continuar').attr('disabled', 'true');
            }
        })

    });

</script>