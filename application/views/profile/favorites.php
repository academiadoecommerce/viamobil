<div class="profile-favorites">
	<h1>Favoritos</h1>

	<?php
		if($favorites) { ?>
	<div class="">
		<div class="row">

			<?php foreach ($favorites as $key => $favorite) { ?>
			<div class="col-4">
				<div class="pf-timestamp text-center">Adicionado em:
					<?php echo string_date_time($favorite->use_fav_timestamp); ?></div>

				<?php echo $this->ads_model->ads_item($favorite); ?>

			</div>
			<?php } ?>

		</div>
	</div>
	<?php } else { ?>
	<?php $anunci = base_url('anuncios'); ?>
	<div class="alert text-center" style="position: unset;width: unset;opacity: unset;">
		Você não possui nenhum favorito no momento.<p>Que tal buscar algo para comprar?<p>
	</div>
	<div class="text-center">
		<a href="<?php echo $anunci?>" class="btn btn-success">
			<i class="fa fa-credit-card-alt" aria-hidden="true"></i>
			Comprar algo
		</a>
	</div>
	<?php } ?>
</div>