<style type="text/css">
   .media .avatar {
       width: 50px;
   }
   
   .shadow-textarea textarea.form-control::placeholder {
       font-weight: 300;
   }

</style>
<!-- Include the above in your HEAD tag ---------->
<form action="<?= base_url('Duvidas/message')?>" method="POST" style="box-shadow: 0px 0px 5px 0px #80808033;">

   <?php foreach($conteudo as $content):?>
      <div class="media">
        <img class="d-flex rounded-circle avatar z-depth-1-half mr-3" src="https://www.viamobil.com.br/assets/img/icon-user.png"
          alt="Avatar">
        <div class="media-body">
          <h5 class="mt-0 font-weight-bold blue-text"><?php echo $content["cliente"]?></h5>
            <span>
               <small>
                  <i>Enviada em <?php echo date("d-m-Y", strtotime($content["duvida_data_inicio"])) ?> as <?php echo $content["duvidas_content_hora"]?></i>
               </small>
            </span>
            <br>

            <?php echo $content["duvidas_content_message"]?>

          <div class="media mt-3 shadow-textarea">
            <div class="media-body">
              <div class="form-group basic-textarea rounded-corners">
                <textarea class="form-control z-depth-1" id="exampleFormControlTextarea345" rows="3" placeholder="Digite sua resposta" id="message" name="message"></textarea>
                  <br>
                  <button class="btn btn-primary" id="btn-chat" type="submit">Enviar</button>
                  <input type="hidden" name="duvida_id" value="<?php echo $content['duvida_id'] ?>">
              </div>
            </div>
          </div>
        </div>
      </div>

   <?php endforeach;?>

</form>