    <?php
    if(isset($orders) && count($orders) > 0):
    foreach ($orders as $order) : ?>
    <!-- blocos de pedido -->
      <article class="card card-product p-3">
      	<div class="card-body">
      	<div class="row">
      		<article class="col-sm-5">
                  <!-- titulo bloco pedido -->
                  <?php $situacao = $order[ "order_situacao" ]; ?>
               <strong class="f18 text-<?php if($situacao == 'PAGO')
               { echo 'success'; }
               else if($situacao == 'AGUARDANDO')
               { echo 'warning'; } ?>">
                  Pedido <?php echo strtolower( ucfirst( $situacao ) ) ?> 
                </strong>
                    <br><font size ="2" class="grey" style="position: relative; top: -5px"><?php echo $order[ "order_num" ] ?></font>
                <br>
                <img src="<?= thumbnail( @$order['img'], "ads", 120, 120, 2 ) ?>" width="120" class="img-thumbnail mb-2 p-0" alt="produto" style="float:left">
                <span style="float:left" class="ml-2"> <?php echo $order["ad_name"] ?> 
                <br><span class="price c3 f18"> <?php echo dinheiro($order["soma"],2, true) ?> </span>
                <br>
                 <span>
                      <b>Cor:</b>
                        <?php echo $order["cor"]; ?>
                    </span>

                    <br>

                    <span>
                      <b>Tamanho:</b>
                        <?php echo $order["tamanho"]; ?>
                    </span>

                  <br>

                  <span>
                    <b>Quantidade:</b>
                          <?php echo $order["quantidade"] ?> Und.
                  </span>
           
      		</article>

            <aside class="col-sm-4 border-left"> <!--border-left -->
                <p class="mt-5"><?php echo $order[ "nome_cliente" ]; ?></p>
                <p class="">
                <pre>
                  </pre>
                <p class="">
                  <form method="POST" id="mobile-message" action="<?php echo base_url( "chat/novo_chat" ); ?>">
                     <input type="hidden" name="sender_id" value="<?php echo $order['id_vendedor'] ?>">
                     <input type="hidden" name="recipient_id" value="<?php echo $order['id_cliente'] ?>">
                     <input type="hidden" name="ad_id" value="<?php echo $order['id_produto'] ?>">
                     <input type="hidden" name="use_email" value="<?php echo $order['email_cliente'] ?>">
                     <input type="hidden" name="use_name" value="<?php echo $order['nome_cliente'] ?>">
                     <input type="hidden" name="ad_name" value="<?php echo $order['ad_name'] ?>">
                     <button type="submit" style="font-size:12px">
                        <i class="fa fa-comments"></i> Abrir chat
                     </button>
                  </form>
               </p>
                </p>
              
            </aside>
      		<aside class="col-sm-3"> <!--border-left -->
      			<div class="action-wrap m-0 p-0">
      				<p>
                <?php if($order["order_situacao"] == "PAGO" && $order["tipo"] != "Atacado"){?>
                  <a target="_blank" onclick="geraEtiqueta('<?php echo $order['order_num']?>');" style="width: unset;" class="btn btn-info mt-5">Imprimir Etiqueta</a>
                  <!--<a onclick="geraPLP('<?php echo $order['order_num']?>');" style="width: unset;" class="btn btn-info mt-5">Imprimir PLP</a>-->
                <?php } ?>

                <?php if($order["tipo"] != "Atacado" && $order["order_situacao"] != "PAGO"){?>
                  <button disabled style="width: unset;" class="btn btn-info mt-5">Etiqueta</button>
                <?php } ?>
      				</p>
                    <div class="mt-5">
      				    <a href="<?php echo base_url("/Profile/detalhes/{$order['order_num']}") ?>" class="f14 mt-5 a-link"> Ver detalhes do pedido</a>
                    </div>
      			</div>
      		</aside>
      	</div>
      	</div>
      </article>
    <?php endforeach;
    else: ?>
    <h1>Minhas vendas</h1>
    <div class="alert text-center" style="position: unset;width: unset;opacity: unset;">Você ainda não fez nenhuma venda.</div><div class="text-center"></div>	
		<? endif; ?>


<script type="text/javascript">
  function geraEtiqueta(order){
   window.location.href = base_url+'index.php/Correios/gerar_etiqueta/'+order;
   //$.post(base_url+'/index.php/Correios/gerar_etiqueta',{order});
  }

  function geraPLP(order){
   window.location.href = base_url+'index.php/Correios/gerar_plp/'+order;
   //$.post(base_url+'/index.php/Correios/gerar_etiqueta',{order});
  }
</script>
