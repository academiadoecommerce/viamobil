	
<script src="<?= base_url('assets/js/vendor/jquery.js'); ?>"></script>	

<div class="profile-details">
	<form method="POST" action="<?= base_url('profile/details/save'); ?>" id="pd-form" class="form form-simple pd-form">
		<h1> <?= first_name($user->use_name); ?> Esses são seus dados | Seu ID é <?= $user->use_id; ?></h1>

		<div class="form-group row">
			<label class="col-lg-2 text-right">Nome:<span class="required">*</span></label>
			<div class="col-lg-10">
				<input class="form-control" type="text" required name="name" placeholder="Nome Completo" value="<?= $user->use_name; ?>">
			</div>
		</div>

		<div class="form-group row">
			<label class="col-lg-2 text-right">E-mail:<span class="required">*</span></label>
			<div class="col-lg-10">
				<input class="form-control" type="email" disabled name="email" value="<?= $user->use_email; ?>">
			</div>
		</div>

		<?php if ($this->session->userdata('login_tipo') == 'lojista') { ?>
		<div class="form-group row">
			<label class="col-lg-2 text-right">CPF / CNPJ:<span class="required">*</span></label>
			<div class="col-lg-10">
				<input class="form-control" type="text" disabled name="cpf" 
				id="cpf" value="<?php echo $user->use_cpf; ?>"
				>
			</div>
		</div>
		<?php } ?>

		<div class="form-group row">
			<label class="col-lg-2 text-right">Senha:<span class="required">*</span></label>
			<div class="col-lg-3">
				<input class="form-control" type="password" disabled required name="password" id="pd-password" placeholder="Digite uma senha forte">
			</div>
			<div class="form-check col-lg-7">
				<label class="form-check-label" for="pd-pass-check">
				<input class="form-check-input" type="checkbox" id="pd-pass-check">Alterar Senha</label>
			</div>
		</div>

		<div class="form-group row">
			<label class="col-lg-2 text-right"></label>
			<div class="col-lg-3">
				<? if( isset($user->use_moip_id_vend) && !empty($user->use_moip_id_vend) ): ?>
				<a class="btn btn-danger" href="<?= base_url('profile/desconectar'); ?>" role="button">Desconectar Wirecard</a>
				<? else: ?>
				<button class="btn btn-success" id="connectWirecard">Conectar-se a Wirecard</button>
				<? endif ?>
			</div>
		</div>

		<br>

		<h1>Dados para contato</h1>
		<br>

		<div class="form-group row">
			<label class="col-lg-2 text-right">Telefone:</label>
			<div class="col-lg-3">
				<input class="form-control input-phone" type="text" name="phone" placeholder="(__) ____-____" value="<?php echo $user->use_phone; ?>">
			</div>
		</div>

		<div class="form-group row">
			<label class="col-lg-2 text-right">Celular:<span class="required">*</span></span></label>
			<div class="col-lg-3">
				<input class="form-control input-phone" type="text" name="celular" placeholder="(__) ____-____" value="<?= $user->use_celular; ?>">
			</div>
		</div>

		<div class="form-group row">
			<label class="col-lg-2 text-right">WhatsApp:</label>
			<div class="col-lg-3">
				<input class="form-control input-phone" type="text" name="whatsapp" placeholder="(__) ____-____" value="<?= $user->use_whatsapp; ?>">
			</div>
		</div>

		<div class="form-group row">
			<label class="col-lg-2 text-right">Site Pessoal:</label>
			<div class="col-lg-5">
				<input class="form-control" type="text" name="website" placeholder="Informe seu site pessoal" value="<?= $user->use_website; ?>">
			</div>
		</div>
		<br>

		<h1>Endereço</h1>
		
		<div class="row">
			<div class="col-lg-10 large-offset-2">
				<div class="ad_address">

					<div class="form-group row">  
				        <label class="col-lg-2 text-right">CEP:<span class="required">*</span></label>
				        <div class="col-lg-5">
				            <input class="form-control input-cep" type="text" required name="cep" id="ai-cep" maxlength="9" value="<?= $user->use_cep; ?>" placeholder="Digite o CEP" data-mask="99999-999">
				        </div>
				       <div class="col-lg-5">
				        	<a href="https://www.buscacep.correios.com.br/sistemas/buscacep/" class="cep_link" target="_blank">
					            <span class="btn btn-help hidden-xs">
	                                <i class="fa fa-question"></i>
	                            </span>
				                Não sei meu CEP
				            </a>
				        </div>
				    </div>

				    <!-- Estado -->
				    <div class="form-group row">				        
				        <label class="col-lg-2 text-right">Estado:<span class="required">*</span></label>				   
				        <div class="col-lg-8">
				                <select class="form-control" name="state" id="ai-state" required >
			                    <option>Selecione seu Estado</option>
			                    <?php foreach ($states as $key => $state) { ?>
									<option 
										<?php (($user->use_state == $state->sta_id) ? 'selected' : ''); ?> 
										value="<?php echo $state->sta_id; ?>">
										<?php echo $state->sta_name; ?> 
									</option>
								<?php } ?>
				            </select>
				        </div>
				    </div>

				    <!-- Região -->
				    <div class="form-group row">
				        <label class="col-lg-2 text-right">Região:<span class="required">*</span></label>
				        <div class="col-lg-8">
							<select class="form-control" name="region" id="ai-region">
								<option value="">Região da Cidade</option>
								<?php if($region) { ?>
								<option value="<?=$region->regiao_id?>" selected="selected"><?= $region->regiao_nome ?></option>
								<?php } ?>
							</select>
				        </div>
				    </div>
				    
				    <!-- Cidade -->
				    <div class="form-group row">
				        <label class="col-lg-2 text-right">Cidade:<span class="required">*</span></label>
				        <div class="col-lg-8">
				            <select class="form-control" name="city" id="ai-city" required>
				                <option value="">Selecione uma cidade</option>
				                <?php
								if($user->use_city){
									$cities = $this->main_model->cities($user->use_state);
									foreach ($cities as $key => $city) { ?>
										<option 
											<?php echo (($user->use_city == $city->cit_id) ? 'selected' : ''); ?> value="<?php echo $city->cit_id; ?>">
											<?php echo $city->cit_name; ?>
										</option>
									<?php } 
								} ?>
				            </select>
				        </div>
				    </div>

					<!-- Bairro -->
					<div class="form-group row">
			            <label class="col-lg-2 text-right">Bairro:<span class="required">*</span></label>
				        <div class="col-lg-8">
				            <select class="form-control" name="neighborhood" id="ai-neighborhood" required>
				                <option>Selecione um bairro</option>
								<option value="<?php echo $user->use_neighborhood; ?>" selected>
									<?php echo $user->use_neighborhood; ?> 
								</option>
				            </select>
				        </div>
				    </div>

				    <!-- Endereço -->
				    <div class="form-group row" id="box-address">
				        <label class="col-lg-2 text-right">Endereço:<span class="required">*</span></label>
            			<div class="col-lg-8">
				            <input type="text" placeholder="Nome da rua" name="address" id="ai-address" value="<?= (($user->use_address) ? $user->use_address : ''); ?>">
				        </div>
				    </div>

				    <div class="form-group row" id="box-address">
				        <label class="col-lg-2 text-right">Número:<span class="required">*</span></label>
            			<div class="col-lg-3">
							<input type="text" placeholder="ex: 101" 
								name="address_number" id="ai-address-number" 
								value="<?= (($user->use_address_number) ? $user->use_address_number : ''); ?>"
							>
				        </div>
				    </div>
				</div>
			</div>
		</div>

		<br>

		<div class="form-group row">
			<div class="col-lg-2">&nbsp;</div>
			<div class="col-lg-10">
				<button type="submit" class="btn btn-primary">
					<i class="fa fa-floppy-o"></i>
					Salvar
				</button>
			</div>
		</div>
	</form>
</div>

<script src="<?= base_url('assets/js/custom/profile_details.js'); ?>"> </script> <??>