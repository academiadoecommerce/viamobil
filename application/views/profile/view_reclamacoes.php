<?php 
if(isset($reclamacoes) && count($reclamacoes) > 0):
foreach ($reclamacoes as $recl) : ?>
<article class="card card-product p-3">
   <div class="card-body">
   <div class="row">
      <article class="col-sm-4 pt-2">
            <strong class=" c3 f18"> <?php echo $recl["reclama_pedido"] ?> </strong>
      </article>
      <article class="col-sm-3 pt-2">
            <span class="c6 f14 m-0"> <?php echo date("d-m-Y", strtotime($recl["reclama_data"])) ?> </span>
      </article>
      <aside class="col-sm-3"> <!--border-left -->
         <div class="action-wrap m-0 p-0">
            <div class="price-wrap h4">
               <span class="price c3 f18">
                  <?php if($recl["reclama_encerrado"] == "2"){?>
                    <span>Encerrado</span>
                  <?php }else{?>
                    Em andamento
                 <?php }?> </span>
            </div>
         </div>
      </aside>
      <aside class="col-sm-2">
         <a class="btn btn-danger" href="<?= base_url("Reclamacao/chat/{$recl['reclama_pedido']}")?>">Chat</a>
      </aside>

   </div>
   </div>
</article>

                  <?php endforeach;
                  else: ?>
                  <div class="profile-favorites">
	<h1>Minhas reclamações</h1>
	<div class="alert text-center" style="position: unset;width: unset;opacity: unset;">Você não possui nenhuma reclamação</p><p></p></div>
</div>
<? endif ?>
      </tbody>
    </form>
  </table>

<!--<div class="modal fade" id="reclame-aqui" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Reclame aqui</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">

        <div class="md-form mb-5">
          <label data-error="wrong" data-success="right" for="order">ORDER</label>
          <input type="text" id="order" class="form-control validate" disabled="">
        </div>

        <div class="md-form mb-5">
          <label data-error="wrong" data-success="right" for="assunto">Assunto</label>
          <input type="text" id="assunto" class="form-control validate">
        </div>

        <div class="md-form">
           <label data-error="wrong" data-success="right" for="descricao">Descreva sua reclamação</label>
          <textarea type="text" id="descricao" class="md-textarea form-control" rows="4" placeholder="Descreva com detalhes a sua reclamação"></textarea>
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="button" class="btn btn-primary"  onclick="reclamacao()">Enviar</button>
      </div>
    </div>
  </div>
</div>-->

<script type="text/javascript">

  function chatInicia(pedido){
    
   //window.location.href = base_url+'/index.php/Etiqueta/gerarEtiqueta';
  $.post(base_url+'/index.php/Reclamacao/chat',{pedido},function(data){
          window.location.href = "https://v2.soatacado.com/chat_pedido";
  });

  }

</script>
