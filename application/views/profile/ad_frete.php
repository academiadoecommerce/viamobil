
<a class="btn btn-primary btn-label" data-toggle="modal" data-target="#addFrete">
  <i class="fas fa-plus-circle"></i>
  Adicionar
</a>

<?php if( $this->session->flashdata( 'error' ) ) { ?>
<div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata( 'error' ); ?>
</div>
<?php } ?>


<?php if( $this->session->flashdata( 'sucesso' ) ) { ?>
<div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata( 'sucesso' ); ?>
</div>
<?php } ?>


<table class="table">
  <thead>
    <tr>
      <th><center>Descrição</center></th>
      <th><center>Valor</center></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($frete as $fretes){

    $produto_id = $fretes["frete_id_produto"];

    ?>
    <tr>
      <td><?php echo $fretes["frete_local"]?></td>
      <td><?php echo "R$ ". dinheiro($fretes["frete_valor"])?></td>

      <td> 
          <a class="btn btn-info btn-label modal-open" title="Editar" data-toggle="modal" data-target="#addFrete"
             data-whatever="<?= $fretes["frete_local"] ?>" 
             data-whatevervalor="<?= $fretes["frete_valor"]?>"
             data-whateverid = "<?= $fretes["frete_id"]?>">
            <i class="fas fa-edit"></i>Editar
        </a>
      </td>

       <td>
        <a class="btn btn-danger btn-label modal-open" onclick="deletar(<?= $fretes["frete_id"]?>)">
           <i class="fas fa-trash"></i>
            Excluir
        </a>
        </td>

    </tr>
    <?php }?>
  </tbody>
</table>


<!-- Modal -->
<div class="modal fade" id="addFrete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Frete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form role="form" method="post" class="formulario" action="<?= base_url("Profile/insertFrete") ?>" id="add_frete">

          <div class="form-group">
            <input type="hidden" class="form-control" id="produto" name="produto" value="<?php echo $id_produto?>">
          </div>

          <div class="form-group">
            <label for="descricao">Descrição</label>
            <input required type="text" class="form-control" id="descricao" name="descricao">
          </div>

          <div class="form-group">
            <label for="valor">Valor</label>
            <input required type="text" class="form-control" id="valor" name="valor">
          </div>

          <div class="form-group">
            <input type="hidden" class="form-control" id="freteid" name="freteid">
          </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" onclick="$('#add_frete').submit()">Salvar</button>
      </div>
      </form>
    </div>
  </div>
</div>



<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmar</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="modal-btn-si">Sim</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">Não</button>
      </div>
    </div>
  </div>
</div>

<div class="alert" role="alert" id="result"></div>

<?php $this->load->view('modal/modalDelete');?><!-- Carrega a view para exclusao -->

<script type="text/javascript">

   $('#addFrete').on('show.bs.modal', function (event) {
       var button = $(event.relatedTarget) 
       var local = button.data('whatever')
       var valor = button.data('whatevervalor')
       var id = button.data('whateverid') 
       var modal = $(this)
       modal.find('#descricao').val(local)
       modal.find('#valor').val(valor)
       modal.find('#freteid').val(id)

     })


    function deletar(id){
      var produto = <?php echo $produto_id ?>;

      if(confirm("Deseja realmente deletar")){
        window.location.href = base_url+'Profile/deleteFrete/' + id +'/' +produto;
      }

    }

</script>