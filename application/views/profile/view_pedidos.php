<?php
   require 'vendor/autoload.php';
   use Moip\Moip;
   use Moip\Auth\OAuth;
   //$moip = new Moip(new OAuth(accessToken), Moip::ENDPOINT_PRODUCTION);
   ?>
<!-- alert flashdata -->
<?php if($this->session->flashdata('success')){?>
<div class="alert alert-danger alert-dismissable">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <?php echo $this->session->flashdata('success');?>
</div>
<?php } ?>

<!-- blocos de pedido -->
<?php if (isset($orders) && count($orders) > 0): ?>
<?php foreach ($orders as $order) : ?>
<article class="card card-product p-3">
      	<div class="card-body">
      	<div class="row">
      		<article class="col-sm-5">
                  <!-- titulo bloco pedido -->
               <?php $situacao = $order[ "order_situacao" ]; ?>
               <strong class="f18 text-<?php if($situacao == 'PAGO')
               { echo 'success'; }
               else if($situacao == 'AGUARDANDO')
               { echo 'warning'; } ?>">
                   Pedido <?php echo strtolower( ucfirst( $situacao ) ) ?> 
               </strong>
                    <br><font size ="2" class="grey" style="position: relative; top: -5px"><?php echo $order[ "order_num" ] ?></font>
                <br>
                <img src="<?= thumbnail( @$order['img'], "ads", 120, 120, 2 ) ?>" width="120" class="img-thumbnail mb-2 p-0" alt="produto" style="float:left">
                <span style="float:left" class="ml-2"> <?php echo $order["ad_name"] ?> 
                  <br>
                    <span class="price c3 f18"> 
                      <?php echo dinheiro($order["soma"] + $order["frete"],2, true) ?> 
                    </span>
                    <br>

                    <span>
                      <b>Cor:</b>
                        <?php echo $order["cor"]; ?>
                    </span>

                    <br>

                    <span>
                      <b>Tamanho:</b>
                        <?php echo $order["tamanho"]; ?>
                    </span>

                  <br>

                  <span>
                    <b>Quantidade:</b>
                          <?php echo $order["quantidade"] ?> Und.
                  </span>
           
      		</article>

            <aside class="col-sm-4 border-left"> <!--border-left -->
                <p class="mt-5"><?php echo $order[ "nome_vendedor" ]; ?></p>

                <p class="">
                  <form method="POST" id="mobile-message" action="<?php echo base_url( "chat/novo_chat" ); ?>">
                     <input type="hidden" name="sender_id" value="<?php echo $order['id_cliente'] ?>">
                     <input type="hidden" name="recipient_id" value="<?php echo $order['id_vendedor'] ?>">
                     <input type="hidden" name="ad_id" value="<?php echo $order['id_produto'] ?>">
                     <input type="hidden" name="use_email" value="<?php echo $order['email_vendedor'] ?>">
                     <input type="hidden" name="use_name" value="<?php echo $order['nome_vendedor'] ?>">
                     <input type="hidden" name="ad_name" value="<?php echo $order['ad_name'] ?>">
                     <button type="submit" style="font-size:12px">
                        <i class="fa fa-comments"></i> Abrir chat
                     </button>
                  </form>
               </p>
               
            </aside>
      		<aside class="col-sm-3"> <!--border-left -->
      			<div class="action-wrap m-0 p-0" style="height:100%">
      				<p>
                     <?php if( $order['order_reclamacao'] == "0" || !$order['order_reclamacao'] ) { ?>
                     <a style="width: unset;" <?php echo 
                     (($order["order_situacao"] == 'CRIADO' || $order["order_situacao"] == 'ENCERRADO') ? 
                     'disabled class="btn btn-default mt-2 disabled"' : 
                     'href="'.base_url().'/profile/iniciar_reclamacoes?order='.$order["order_num"].'" class="btn btn-info mt-2"')?>>
                     Iniciar Reclamação</a>
                     <?php } ?>
                     <?php if($order['order_reclamacao'] == "1"){?>
                     <button style="width: unset;" class="btn btn-info" onclick="encerra('<?php echo $order["order_num"] ?>')" >
                     Encerrar Reclamação</button>
                     <?php }?>
      				</p>
                    <div class="mt-5" style="position:absolute; bottom: 10px">
      				    <a href="<?php echo base_url("/Profile/detalhes/{$order['order_num']}") ?>" class="f14 mt-5 a-link"> Ver detalhes do pedido</a>
                    </div>
      			</div>
      		</aside>
      	</div>
      	</div>
      </article>

<?php endforeach ?>

                  <? else: ?>
   <div class="profile-favorites">
	<h1>Meus Pedidos</h1>
	<div class="alert text-center" style="position: unset;width: unset;opacity: unset;">Você ainda não fez nenhum pedido.<p>Que tal buscar algo para comprar?</p><p></p></div><div class="text-center"><a href="https://shopbras.net/anuncios" class="btn btn-success"><i class="fa fa-credit-card-alt" aria-hidden="true"></i>Comprar algo</a></div>	
</div>
<? endif ?>

<script type="text/javascript">
var base_url  = '<?php echo base_url() ?>'

   function reclamacao(id){
       var id_pedido = document.getElementById('order').value
       var assunto   = document.getElementById('assunto').value
       var descricao = document.getElementById('descricao').value

       $.post(base_url+'index.php/Reclamacao/inicia', { id_pedido, assunto, descricao } )
       .done( function( data ){
          data = $.parseJSON(data);
          if (data.status == 'success') {
            console.log(data);
            window.location.replace(data.redirect);
          }
       });
   }

   function encerra(order){
     $.post(base_url+'/index.php/Reclamacao/encerra',{ order} );
   }
</script>