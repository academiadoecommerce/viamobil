<link rel="stylesheet" type="text/css" href="<? echo base_url();?>/assets/css/chat_pedido.css">
<!-- Include the above in your HEAD tag ---------->
<form action="<?= base_url('Duvidas/message')?>" method="POST" style="box-shadow: 0px 0px 5px 0px #80808033;">
   <div class="panel-body" style="height: 400px;">
      <ul class="chat_pedido">
         <?php foreach($conteudo as $content):?>
         <?php if($content["duvidas_content_tipo"] == "cliente"){?>
         <li class="left clearfix">
            <span class="chat-img pull-left">
            <img src="https://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
            </span>
            <div class="chat-body clearfix">
               <div class="header">
                  <strong class="primary-font"><?php echo $content["cliente"]?></strong> <small class="pull-right text-muted">
                  <span class="glyphicon glyphicon-time"></span><?php echo $content["duvidas_content_hora"]?></small>
               </div>
               <p>
                  <?php echo $content["duvidas_content_message"]?>
               </p>
            </div>
         </li>
         <?php }else{?>
            <li class="right clearfix">
               <div class="w-50">
                  <span class="chat-img pull-right">
                     <img src="https://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                  </span>
                  <div class="chat-body clearfix">
                     <div class="header">
                        <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?php echo $content["reclama_hora"]?></small>
                        <strong class="pull-right primary-font"><?php echo $content["vendedor"]?></strong>
                     </div>
                     <p>
                        <?php echo $content["duvidas_content_message"]?>
                     </p>
                  </div>
               </div>
            </li>
         <?php }?>
         <?php endforeach;?>
      </ul>
   </div>
   <div class="panel-footer p-2" style="background:#80808033">
      <div class="input-group">
         <input id="message" name="message" type="text" class="form-control input-sm" placeholder="Digite sua mensagem aqui..." />
         <span class="input-group-btn">
         <button class="btn btn-warning " id="btn-chat" type="submit">
         Enviar</button>
         <input type="hidden" name="duvida_id" value="<?php echo $content['duvida_id'] ?>">
         </span>
      </div>
   </div>
   </div>
</form>

<script type="text/javascript">
   $('.panel-body').animate({ scrollTop: 9999999 }, 100);
</script>