<!-- 
   VIEW RESPONSÁVEL APENAS POR RETORNAR AS DÚVIDAS CUJO STATUS SEJA DIFERENTE DE 2 coluna(duvida_status) 
   da tabela panamerico_duvidas
   CASO O VENDEDOR RESPONDA A DÚVIDA A MESMA NÃO PRECISA MAIS APARECER EM SUA DASHBOARD, AO RESPONDER É FEITO O UPDATE
   SETANDO O VALOR 2 NA COLUNA duvida_status SENDO ASSIM ENCERRANDO A DÚVIDA.
   -->
<?php 
   if(isset($duvidas) && count($duvidas) > 0):
   foreach ($duvidas as $duv) : ?>
<article class="card card-product p-3">
   <div class="card-body">
      <div class="row">
         <article class="col-sm-2 pt-2">
            <img src="<?= thumbnail( @$duv['img'], "ads", 100, 100, 2 ) ?>" width="70" class="img-thumbnail mb-2 p-0" alt="produto" style="float:left">
         </article>
         <article class="col-sm-4 pt-2">
            <strong class=" c3 f18"> <?php echo $duv["ad_name"] ?> </strong>
         </article>
         <article class="col-sm-2 pt-2">
            <span class="c6 f14 m-0"> <?php echo dinheiro($duv["ad_price"]) ?> </span>
         </article>
         <aside class="col-sm-2">
            <!--border-left -->
            <div class="action-wrap m-0 p-0">
               <div class="price-wrap h4">
               </div>
            </div>
         </aside>

         <aside class="col-sm-2">
            <!--border-left -->
            <div class="action-wrap m-0 p-0">
               <div class="price-wrap h4">
                  <a href="https://viamobil.com.br/anuncio/<?php echo $duv['ad_slug']?>" class="btn btn-primary">Comprar</a>
               </div>
            </div>
         </aside>
      </div>

      <?php foreach ($mensagem as $msg) : 

         if($msg["duvidas_content_fk"] == $duv["duvida_id"]) {

            if($msg["duvidas_content_tipo"] == "cliente" ){ ?>
               <div class="row">
                  <!--<i class="fas fa-comment" style="color: #79a9f5;">--></i><span><?php echo $msg["duvidas_content_message"]?></span>
               </div>
            <?php }else{ ?>
               <div class="row">
                  &nbsp;&nbsp;&nbsp;<!--<i class="fa fa-comment" style="color: #9cd1f3;">--></i><span><?php echo $msg["duvidas_content_message"]?></span>
               </div>
            <?php } ?>

         <?php }?>
      <?php endforeach; ?>

   </div>
</article>
<?php endforeach;
   else: ?>
<div class="profile-favorites">
   <h1>Minhas Dúvidas</h1>
   <div class="alert text-center" style="position: unset;width: unset;opacity: unset;">
      Você não possui nenhuma pergunta</p>
      <p></p>
   </div>
</div>
<? endif ?>
</tbody>
</form>
</table>
<script type="text/javascript">
   function chatInicia(pedido){
     
    //window.location.href = base_url+'/index.php/Etiqueta/gerarEtiqueta';
   $.post(base_url+'/index.php/Reclamacao/chat',{pedido},function(data){
           window.location.href = "https://viamobil.com.br/chat_pedido";
   });
   
   }
   
</script>