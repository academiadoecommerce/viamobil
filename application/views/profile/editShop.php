<link href="<?= base_url('assets/css/tabs.css') ?>" rel="stylesheet" />

<div class="simple-page simple-page-intern">
    <form method="POST" action="<?= base_url('profile/shop/update') ?>" id="sc-form" class="form form-simple"
        enctype="multipart/form-data">
        <input type="hidden" name="shop" id="sc-code" value="<?= $shop->shop_id ?>">
        <h1>Dados da Loja</h1>

        <div class="row">
            <div class="hide-for-small-only medium-2 columns">
                <label class="text-right middle">Nome da Loja:<span class="required">*</span></label>
            </div>
            <div class="small-12 medium-10 columns">
                <label class="show-for-small-only">Nome da Loja:<span class="required">*</span></label>
                <input type="text" required name="name" placeholder="Informe o nome da sua loja" maxlength="50"
                    value="<?= $shop->shop_name ?>">
            </div>
        </div>

        <div class="row">
            <div class="hide-for-small-only medium-2 columns">
                <label class="text-right middle">Descrição:<span class="required">*</span></label>
            </div>
            <div class="small-12 medium-10 columns">
                <label class="show-for-small-only">Descrição:<span class="required">*</span></label>
                <textarea name="desc" required placeholder="Descrição da loja" maxlength="500"
                    style="height: 150px;"><?= $shop->shop_desc ?></textarea>
            </div>
        </div>

        <div class="row">
            <div class="hide-for-small-only medium-2 columns">
                <label class="text-right middle">Categoria:<span class="required">*</span></label>
            </div>
            <div class="small-12 medium-5 end columns">
                <label class="show-for-small-only">Categoria:<span class="required">*</span></label>
                <select name="category">
                    <?php
                    foreach ($categories as $key => $category) {
                        echo '<option ' . (($shop->ads_cat_id == $category->ads_cat_id) ? 'selected' : '') . ' value="' . $category->ads_cat_id . '">' . $category->ads_cat_name . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>


        <div class="row">
            <div class="hide-for-small-only medium-2 columns">
                <label class="text-right middle">URL da Loja:<span class="required">*</span></label>
            </div>
            <div class="small-12 medium-8 end columns">
                <label class="show-for-small-only">URL da Loja:<span class="required">*</span></label>
                <div class="row ">
                    <div class="small-6 large-5 columns">
                        <span class="prefix hide-for-small-only">soatacados.com/loja/</span>
                    </div>

                    <div class="small-6 large-7 columns"
                        data-original-title="Você não poderá alterar essa informação futuramente." data-toggle="tooltip"
                        data-placement="top" title="">
                        <input type="text" name="slug" id="sc-slug" required placeholder="Seu perfil"
                            value="<?= $shop->shop_slug ?>">
                    </div>
                </div>

                <div class="alert alert-subtitle hide-for-small-only desbug-alert"
                    style="position: unset;width: unset;opacity: unset;"><strong><i class="fa fa-link"
                            aria-hidden="true"></i> Essa URL serve para você divulgar/acessar sua loja.</strong></div>
            </div>
        </div>

        <div class="row">
            <div class="hide-for-small-only medium-2 columns">
                <label class="text-right middle">Logo:<span class="required">*</span></label>
            </div>
            <div class="small-12 medium-7 columns">

                <input type="file" name="image" class="d-none" id="sc-f-images-input" accept="image/*" capture="camera">
                <label class="btn btn-secondary btn-m-bottom-medium" for="sc-f-images-input" id="sc-f-images-label">
                    <i class="fa fa-image"></i> Selecionar uma Imagem
                </label>

                <div class="alert alert-subtitle hide-for-small-only desbug-alert"
                    style="position: unset;width: unset;opacity: unset;">
                    <ul>
                        <li><strong>Tamanho máximo:</strong> 2MB</li>
                        <li><strong>Altura e largura mínima:</strong> 400px</li>
                        <li><strong>Tipos arquivo permitido:</strong> JPG, JPEG e PNG</li>
                    </ul>
                </div>
            </div>
            <div class="small-12 medium-3 columns">
                <div id="sc-f-images-box">
                    <?php
                    $image = thumbnail(@$shop->shop_img_file, "shops", 190, 190, 2);
                    echo '<img src="' . $image . '">';
                    ?>
                </div>
            </div>
        </div>

        <br>
        <h1>Dados de Contato</h1>
        <div class="row">
            <div class="hide-for-small-only medium-2 columns">
                <label class="text-right middle">Telefone:<span class="required">*</span></label>
            </div>
            <div class="small-12 medium-4 end columns">
                <label class="show-for-small-only">Telefone:<span class="required">*</span></label>
                <input type="text" class="input-phone" id="sc-phone" required <?= (($shop->shop_user_info)) ?>
                    name="phone" placeholder="(__) ____-____" value="<?= $shop->shop_phone ?>">
            </div>

        </div>

        <br>
        <div class="row">
            <div class="medium-10 medium-offset-2 columns">
                <button type="button" class="btn btn-primary" id="sc-submit">
                    <i class="fa fa-floppy-o"></i>
                    Salvar
                </button>
            </div>
        </div>
    </form>
</div>