<?php 
   //funcao para definir o local para a funcao strtotime
   setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
   
   ?>
<div class="mb-2 ml-1">
   <strong >Pedido <?php echo $orders[0]["order_num"]. " - " . strftime( "%d de %B de %Y", strtotime($orders[ 0 ] [ "order_data" ] ) ) ?></strong>
</div>
<article class="card card-product p-3 col-6 d-inline-flex">
   <div class="card-body">
      <div class="row">
         <article class="col-sm-12">
            <?php $situacao = $orders[0]["order_situacao"]; ?>
            <b class="text-<?php if($situacao == 'AGUARDANDO') {echo 'warning';} else if($situacao == 'PAGO') {echo 'success';} ?>">
            <?php echo $situacao ?>
            <br>

            <?php if($orders[0]['order_cliente'] == $this->session->userdata("login")){ ?>
               <?php if($situacao != "PAGO"){?>
                  <h6>
                     <small>
                        <p class="text-dark font-italic">
                           Seu pagamento pode demorar até 2 horas para constar como pago.
                        </p>
                     </small>
                  </h6>
               <?php }?>
            <?php }?>

            </b>
            <p>Você <?php echo (( $orders[0]['order_cliente'] == $this->session->userdata("login") ) ? "comprou" : "vendeu" ) ?>:</p>
            <?php foreach ($orders as $order) : ?>
            <dl class="dlist-align">
               <img width="50"
                  src="<?= thumbnail( @$order['ads_img_file'], "ads", 35, 35, 2 ) ?>" 
                  class="img-thumbnail mb-2 p-0 float-left" alt="produto"
                  >
               <dt class="c3 f14 w-50 ml-2"><?php echo $order["ad_name"] ?></dt>
               <br>
            </dl>
            <p class="pl-2 c6 f14 d-inline"> <?php echo $order[ "qtd" ] ?> unidade(s)</p>
            <br>
            <p class="pl-2 c6 f14 d-inline"> Valor unitário: <?php echo dinheiro($order["unitario"],2, true) ?> </p>
            <br> <br>
            <?php endforeach ?>
         </article>
      </div>
   </div>
</article>
<article class="card card-product p-3 col-5 d-inline-flex border-0 ml-3 bggrey" style="position: relative; top:5px">
   <div class="card-body">
      <div class="row">
         <article class="col-sm-12">
            <div class="price-wrap h4">
               <span class="price c3 f18">
               Total: <?php echo dinheiro($soma[0]["soma"],2, true) ?>
               </span>
            </div>

            <?php if($orders[0]["order_tipo"] != "Atacado"){?>
               <span class="price c3 f14">
               Frete: <?php echo dinheiro($orders[0]["order_frete"],2, true) ?>
               </span>
            <?php }?>

             <?php if($orders[0]['order_vendedor'] == $this->session->userdata("login") && $orders[0]["order_tipo"] == "Atacado"){?>
               <span class="price c3 f14">
                  <i class="fas fa-truck"> Combine o envio com o cliente</i>
               </span>
            <?php }?>
            
            <br>
            <?php if($orders[0]['order_vendedor'] == $this->session->userdata("login")){?>
            <span class="price c3 f14">
            Comissão paga: <?php echo dinheiro($orders[0]["comissao"],2, true) ?>
            </span>
            <?php }?>
            <br>
            <?php if($orders[0]['order_vendedor'] == $this->session->userdata("login")){?>
            <span class="price c3 f14">
            Valor a receber: <?php echo dinheiro(($soma[0]["soma"] + $orders[0]["order_frete"]) - $orders[0]["comissao"],2, true) ?>
            </span>
            
            <?php } ?>

            <p class="">
               <form method="POST" id="mobile-message" action="<?php echo base_url( "chat/novo_chat" ); ?>">
                  <input type="hidden" name="sender_id" value="<?php echo $orders[0]['order_cliente'] ?>">
                  <input type="hidden" name="recipient_id" value="<?php echo $orders[0]['order_vendedor'] ?>">
                  <input type="hidden" name="ad_id" value="<?php echo $orders[0]['ad_id'] ?>">
                  <input type="hidden" name="use_email" value="<?php echo $orders[0]['email_cliente'] ?>">
                  <input type="hidden" name="use_name" value="<?php echo $orders[0]['nome_cliente'] ?>">
                  <input type="hidden" name="ad_name" value="<?php echo $orders[0]['ad_name'] ?>">
                  <button type="submit" style="font-size:12px">
                     <i class="fa fa-comments"></i> Abrir chat
                  </button>
               </form>
            </p>
            <p>
               <?php if($orders[0]["order_tipo"] != "Atacado") { ?>
                  <button style="width: unset;" disabled class="btn btn-default mt-5">
                  Rastrear Pedido
                  </button>
               <?php } ?>

                <?php if($orders[0]['order_cliente'] == $this->session->userdata("login") && $orders[0]["order_situacao"] != "PAGO" && $orders[0]['order_tipo'] == "Atacado"){?>
                  <button style="width: unset;"  class="btn btn-info mt-5" onclick="location.href='<?php echo $orders[0]["order_boleto_link"]?>'" formtarget="_blank">
                    Imprimir Boleto
                  </button>
               <?php }?>
               <?php if($orders[0]['order_vendedor'] == $this->session->userdata("login")){?>
                  <?php if($orders[0]["order_situacao"] == "PAGO" && $orders[0]["tipo"] != "Atacado"){?>
                     <a target="_blank" onclick="geraEtiqueta('<?php echo $orders[0]['order_num']?>');" 
                        style="width: unset;" class="btn btn-info mt-5">Imprimir Etiqueta</a>
                  <?php }?>

               <?php }?>
            </p>
         </article>
      </div>
   </div>
</article>
<script type="text/javascript">
   function geraEtiqueta(order){
    window.location.href = base_url+'index.php/Correios/gerar_etiqueta/'+order;
    //$.post(base_url+'/index.php/Correios/gerar_etiqueta',{order});
   }
</script>