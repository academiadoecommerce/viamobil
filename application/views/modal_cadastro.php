<div class="modal  fade" id="form-registro" tabindex="-1" role="dialog">
  <div class="modal-dialog  login1"  style="margin-top: 30px;">
      <div class="modal-content">
          <div class="user-box">
              <div class="page-header" style="margin: 0 0 20px;">
                  <h3 class="text-center">Cadastre-se grátis</h3>
              </div>

              <nav>
                <div class="nav nav-tabs text-center" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active w-50" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                    Sou Lojista
                  </a>
                  
                  <a class="nav-item nav-link w-50 text-center" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                    Sou Cliente
                  </a>

                </div>
              </nav>

              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                  <form method="POST" action="<?= base_url('register/insert') ?>" id="form-register" class="form form-simple">
                    <input type="hidden" name="use_type" value="lojista">

                    <br>
          
                    <div class="form-group row my-1">
                      <div class="col-12">
                       <span id="msgNome" style="color: red; font-size: 12px; font-family: arial; display: none;">Por favor, preencha nome e sobrenome. Ex. Maria silva</span>
                         <input class="form-control my-1" type="text" required name="name" autocomplete="off" id="nome" placeholder="Nome Completo">
                      </div>
                    </div>
                    
                    <div class="form-group row my-1">

                      <div class="col-7">
                        <input class="form-control my-1" type="email" required name="email" id="input-email" placeholder="E-mail (exemplo@exemplo.com.br)">
                      </div>

                      <div class="col-5">
                        <input class="form-control my-1" type="telefone" required name="telefone" id="telefone" placeholder="Número de telefone ou celular">
                      </div>

                    </div>

                    <div class="form-group row my-1">
                      <div class="col-8">
                        <input class=" my-1 form-control" type="text" name="cpfCnpj" id="cpfCnpj" maxlength="14" onkeyup="somenteNumeros(this);" placeholder="Número do CPF/CNPJ">
                      </div>
                      <div class="col-4">
                        <input type="text" required class="my-1 form-control input-cep" name="cep" id="ai-cep" maxlength="9" placeholder="CEP">
                      </div>              
                    </div>    


                    <div class="form-group row my-1">    
                      <div class="col-6">
                        <select class="form-group form-control my-1" name="state" id="ai-state" required >
                            <option>Selecione seu Estado</option>
                        </select>                 
                      </div>

                      <div class="col-6">
                        <select class="form-group form-control my-1" name="city" id="ai-city" required>
                            <option value="">Selecione um município</option>
                        </select> 
                      </div>

                    </div>   



                    
                    <div class="form-group row my-1" id="box-address">

                      <div class="col-9">
                        <input class=" my-1 form-control" type="text" name="address" id="ai-address" placeholder="Nome da rua" value="">
                      </div>

                      <div class="col-3">
                        <input class=" my-1 form-control" type="text" name="number_address" id="ai-address-number" placeholder="número">
                      </div>

                    </div>
                    
                    <div class="form-group  row my-1" id="box-neighborhood" style="display:none;">
                      <div class="col-12">
                        <div id="label-neighborhood" class="d-none" style="margin-bottom: 10px;">...</div>
                        <input class=" my-1 form-control" type="text" name="neighborhood" id="ai-neighborhood" value="">
                      </div>
                    </div>  
                    
                    <!-- <div class="form-check">
                      <input style="margin-top: 7px;" type="checkbox" name="lojista" class="form-check-input" id="lojista-check">
                      <label class="form-check-label" for="lojista-check">Lojista</label>
                    </div> -->

                    <div class="form-group row my-1">
                      <div class="col-12 mb-1">
                        <input class="form-control my-1" required type="text" name="urlSite" id="urlSite" placeholder="URL do Site">
                        <!-- <small id="emailHelp " class="form-text text-muted"> Obrigatório para logistas.</small> -->
                      </div>
                    </div>  
                    
                    <div class="form-group row my-1">
                      <div class="col-12">
                      <input class="form-control my-1" type="password" required onkeyup="passwordStrength(this.value)" id="senha" name="password" placeholder="Senha (escolha um senha forte)">
                        <div class="passwordForce">
                          <div class="no-strength"></div>
                        </div>
                      </div>
                    </div>

                    <div id="register-return"></div>

                    <div class="form-group row my-1">
                      <div class="col-lg-12">
                        <div class="checkbox-custom">
                            <input type="checkbox" required="" id="ai-terms">
                            <label for="ai-terms">Concordo com os <a href="<?=base_url()?>/ajuda/termos-de-uso" target="_blank">Termos do <?php echo NOME_SITE ?></a>.</label>
                        </div>
                      </div>
                    </div> 

                    <br>

                    <div class="form-group row my-1">
                      <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary  btn-lg btn-block" id="cadastrar"> <i class="fa fa-user-plus" aria-hidden="true"></i> Cadastrar</button>
                      </div>
                    </div>            
                    
                  </form>
                </div>


                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                  <form method="POST" action="<?= base_url('register/insert') ?>" id="form-register" class="form form-simple">

                    <input type="hidden" name="use_type" value="cliente">

                    <br>
          
                    <div class="form-group row my-1">
                      <div class="col-12">
                       <span id="msgNome" style="color: red; font-size: 12px; font-family: arial; display: none;">Por favor, preencha nome e sobrenome. Ex. Maria silva</span>
                         <input class="form-control my-1" type="text" required name="name" autocomplete="off" id="nome" placeholder="Nome Completo">
                      </div>
                    </div>
                    
                    <div class="form-group row my-1">
                      <div class="col-12">
                        <input class="form-control my-1" type="email" required name="email" id="input-email" placeholder="E-mail (exemplo@exemplo.com.br)">
                      </div>
                    </div>


                    <div class="form-group row my-1">

                      <div class="col-8">
                        <input class=" my-1 form-control" type="text" name="telefone" id="telefone" maxlength="11" onkeyup="somenteNumeros(this);" placeholder="Número Telefone ou celular">
                      </div>

                      <div class="col-4">
                        <input type="text" required class="my-1 form-control input-cep" name="cep" id="c-cep" onload="$(this).mask('00000-000')" maxlength="9" placeholder="CEP">
                      </div>  

                    </div>    


                    <div class="form-group row my-1">    
                      <div class="col-6">
                        <select class="form-group form-control my-1" name="state" id="c-state" required >
                            <option>Selecione seu Estado</option>
                        </select>                 
                      </div>

                      <div class="col-6">
                        <select class="form-group form-control my-1" name="city" id="c-city" required>
                            <option value="">Selecione um município</option>
                        </select> 
                      </div>

                    </div>   



                    
                    <div class="form-group row my-1" id="box-address">

                      <div class="col-9">
                        <input class=" my-1 form-control" type="text" name="address" id="c-address" placeholder="Nome da rua" value="">
                      </div>

                      <div class="col-3">
                        <input class=" my-1 form-control" type="text" name="number_address" id="c-address-number" placeholder="número">
                      </div>

                    </div>
                    
                    <div class="form-group  row my-1" id="box-neighborhood" style="display:none;">
                      <div class="col-12">
                        <div id="label-neighborhood" class="d-none" style="margin-bottom: 10px;">...</div>
                        <input class=" my-1 form-control" type="text" name="neighborhood" id="c-neighborhood" value="">
                      </div>
                    </div>  
                    
                    <div class="form-group row my-1">
                      <div class="col-12">
                      <input class="form-control my-1" type="password" required onkeyup="passwordStrength(this.value)" id="senha" name="password" placeholder="Senha (escolha um senha forte)">
                        <div class="passwordForce">
                          <div class="no-strength"></div>
                        </div>
                      </div>
                    </div>

                    <div id="register-return"></div>

                    <div class="form-group row my-1">
                      <div class="col-lg-12">
                        <div class="checkbox-custom">
                                <input type="checkbox" required="" id="ai-terms">
                                <label for="ai-terms">Concordo com os <a href="<?=base_url()?>/ajuda/termos-de-uso" target="_blank">Termos do <?php echo NOME_SITE ?></a>.</label>
                            </div>
                      </div>
                    </div> 

                    <br>

                    <div class="form-group row my-1">
                      <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary  btn-lg btn-block" id="cadastrar"> <i class="fa fa-user-plus" aria-hidden="true"></i> Cadastrar</button>
                      </div>
                    </div>            
                    
                  </form>

                </div>

              </div>

            </div>
          </div>
        </div>
      </div>
      <script type="text/javascript">$(document).ready(function(){$("#c-cep").mask("99999-999");});</script>
      <script type="text/javascript">$(document).ready(function(){$("#ai-cep").mask("99999-999");});</script>
      <script src="<?= base_url("assets/js/custom/register.js") ?>"></script> 
      <script src="<?= base_url("assets/js/custom/modal_registro.js") ?>"></script>