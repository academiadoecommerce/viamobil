<script src="<?= base_url('assets/js/jquery-2.0.0.min.js')?>" type="text/javascript"></script>
	
<section class="section-main bg padding-y-sm">

	<div class="container-fluid" style="margin-top: 50px;">		
		<div class="col-lg-6 offset-lg-3" style="border: 1px solid #DCDCDC; padding: 20px; background-color: #ffffff;">
			<h2 class="color-36">Recuperação da Senha</h2>
			
			<div class="divider divider-m-top-none"></div>
			
			<form method="POST" action="<?=base_url('login/password/update')?>" id="password-form" class="form form-simple">
				<input type="hidden" name="token" value="<?=$token?>">
				
				<div class="row form-group">				
					<div class="col-lg-4">
						<label class="text-right middle">Nova Senha:<span class="required">*</span></label>
					</div>
					<div class="col-lg-8">						
						<input class="form-control" type="password" required name="new_pass" id="input-new-pass">
					</div>
				</div>

				<div class="row form-group">
					<div class="col-lg-4">
						<label class="text-right middle">Repita a Senha:<span class="required">*</span></label>
					</div>
					<div class="col-lg-8">						
						<input class="form-control" type="password" required name="repeat_pass" id="input-repeat-pass">
					</div>
				</div>

				<div id="password-return"></div>

				<div class="divider divider-m-top-none"></div>

				<div class="row">
					<div class="col-lg-12" style="text-align: right;">
						<button type="button" class="btn btn-link"> Sair </button>
						<button type="submit" class="btn btn-primary"> Atualizar </button>
					</div>
				</div>
			</form>
			<script src="<?=base_url('assets/js/custom/login_password_recover.js')?>"></script>
		</div>
	</div>
	
</section>





