<!-- modal report ads -->


<div class="modal fade" id="moda-report" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Denunciar Anúncio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form method="POST" action="<?=base_url('ads/report/'.$ad->ad_id.'/send')?>" class="form form-simple">
	<div class="row">
		<div class="hide-for-small-only medium-2 columns">
			<label class="text-right middle">Nome:<span class="required">*</span></label>
		</div>
		<div class="small-12 medium-9 columns">
			<label class="show-for-small-only">Nome:<span class="required">*</span></label>
			<input type="text" required name="name" placeholder="Seu nome" >
		</div>
	</div>
	<div class="row">
		<div class="hide-for-small-only medium-2 columns">
			<label class="text-right middle">Motivo:<span class="required">*</span></label>
		</div>
		<div class="small-12 medium-9 columns">
			<label class="show-for-small-only">Motivo:<span class="required">*</span></label>
			<input type="text" required name="reason" placeholder="Motivo da denúncia">
		</div>
	</div>
	<div class="row">
		<div class="hide-for-small-only medium-2 columns">
			<label class="text-right middle">Denúncia:<span class="required">*</span></label>
		</div>
		<div class="small-12 medium-9 columns">
			<label class="show-for-small-only">Denúncia:<span class="required">*</span></label>
			<textarea name="text" placeholder="Explique sua denúncia" style="height: 200px;" required></textarea>
		</div>
	</div>

	<div id="login-return"></div>

	<br>

	<div class="divider divider-m-top-none"></div>

	<div class="row">
		<div class="small-12 columns text-right">
			<button type="button" data-dismiss="modal" class="btn btn-link modal-close"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
			<button type="submit" class="btn btn-primary"><i class="fa fa-send"></i>Enviar</button>
		</div>
	</div>
</form>
      </div>
    </div>
  </div>
</div>