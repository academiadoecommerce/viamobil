<!-- modal email share -->

<div class="modal fade" id="modal-share-mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Compartilhar por email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form method="POST" action="<?=base_url('ads/email_share/'.$ad->ad_id.'/send')?>" class="form form-simple">
            
            <div class="row">
                <div class="hide-for-small-only medium-2 columns">
                    <label class="text-right middle">Nome:<span class="required">*</span></label>
                </div>
                
                <div class="small-12 medium-9 columns">
                    <input type="text" required name="name" placeholder="Seu nome" value="<?=(($this->session->userdata('login')) ? $this->user_model->info('use_name') : '')?>">
                </div>
            </div>

            <div class="row">
                <div class="hide-for-small-only medium-2 columns">
                    <label class="text-right middle">Email:<span class="required">*</span></label>
                </div>
                
                <div class="col-sm-12 col-md-9">
                    <input type="email" required name="email" placeholder="E-mail do seu amigo">
                </div>
            </div>

            <div class="row">
                <div class="hide-for-small-only medium-2 columns">
                    <label class="text-right middle">Mensagem:<span class="required">*</span></label>
                </div>
                <div class="small-12 medium-9 columns">
                    <textarea name="text" placeholder="Mensagem" style="height: 200px;"></textarea>
                </div>
            </div>

            <div id="login-return"></div>

            <br>

          <div class="modal-footer">
                <div class="row">
                    <div class="small-12 columns text-right">
                        <button type="button" class="btn btn-link modal-close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i>Enviar</button>
                    </div>
                </div>
            
          </div>
      
        </form>
      </div>
    </div>
  </div>
</div>