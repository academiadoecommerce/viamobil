<!-- MODAL CADASTRO ASK  -->
    <div class="modal fade" id="cadastro_ask_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog login1"  style="margin-top: 130px;">
            <div class="modal-content">
                <div class="user-box">
                    <div class="page-header" style="margin: 0 0 20px;">
                        <h3 class="text-center color-36">Como Deseja se registrar</h3>
                    </div>

					<div class="form-group row">
						<div class="col-lg-12 color-36">
							<p align="center">Crie uma conta gratis e comece vender com a gente!</p>
							<br><br>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">							
							<a href="<?= base_url('registrar?c=1') ?>" class="btn btn-primary btn-full myBtn" style="margin-bottom: 10px;">Comprador</a>
						</div>
						<div class="col-lg-6">							
							<a href="<?= base_url('registrar') ?>" class=" btn-full myBtn">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vendedor</a>
						</div>
					</div>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
	
<script>
    // Hide the Modal
    $(".myBtn").click(function(){
        $("#cadastro_ask_modal").modal("hide");
    });
</script>	


