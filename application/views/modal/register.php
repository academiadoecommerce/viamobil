<script>
    function isTipo(pVal)
    {
        var reTipo = /[A-z][ ][A-z]/;
        return reTipo.test(pVal);
    }

    $('#nome').keyup(function() {
        if (!isTipo($(this).val())) {
            $('#cadastrar').hide();
            $('#msgNome').show();
        } else {
            $('#cadastrar').show();
            $('#msgNome').hide();
        }
    });

    $('#nome').keypress(function(e) {
        if (e.which == 13) {
            e.preventDefault();
        }
    });
    
    $('#input-email').keypress(function(e) {
        if (e.which == 13) {
            e.preventDefault();
        }
    });

    $('#senha').keypress(function(e) {
        if (e.which == 13) {
            e.preventDefault();
        }
    });
</script>
<form method="POST" action="<?= base_url('register/insert') ?>" id="form-register" class="form form-simple">
    <div class="row">
        <div class="medium-12 columns">
            <span id="msgNome" style="color: red; font-size: 12px; font-family: arial; display: none;">Por favor, preencha nome e sobrenome. Ex. Maria silva</span>
            <input type="text" required name="name" autocomplete="off" id="nome" placeholder="Nome Completo">
        </div>
    </div>
	
    <div class="row">
        <div class="medium-12 columns">
            <input type="email" required name="email" id="input-email" placeholder="E-mail (exemplo@exemplo.com.br)">
        </div>
    </div>
	
    <div class="row">
        <div class="medium-12 columns">
            <input type="password" required onkeyup="passwordStrength(this.value)" id="senha" name="password" placeholder="Senha (escolha um senha forte)">
            <div class="passwordForce">
                <div class="no-strength"></div>
            </div>
        </div>
    </div>

    <div id="register-return"></div>

    <div class="row">
        <div class="medium-12 medium-12 columns">
            <button type="submit" class="btn btn-primary btn-full cadastrar" id="cadastrar"> <i class="fa fa-user-plus" aria-hidden="true"></i>Cadastrar</button>
        </div>
    </div>

    <!--<div class="row">
        <div class="medium-12 medium-12 columns">
            <a href="<?= $this->config->item('facebook_link_auth') ?>" target="_self" class="btn btn-facebook btn-full">
                <i class="fa fa-facebook"></i> Entrar com Facebook
            </a>
        </div>
    </div>-->

</form>

<script src="<?= base_url('assets/js/custom/register.js') ?>"></script>