<?php include_once('returns.php'); ?>
<style>
    input[type=number] {
        padding-left: 11px;
        border: 1px solid #c7c7c7;
        background: #fafafa;
        box-shadow: unset;
        width: 53px;
        height: 30px
    }

    input[type=number]::-webkit-inner-spin-button {
        position: relative;
        left: 8px;
        height: 36px;
        opacity: 1;
    }
</style>

<div class="container px-0-mobile">
    <!-- breadcrumbs -->
    <?php $this->load->view('breadcrumb.php'); ?>

    <!-- ensagens de erro -->
    <?php if ($this->session->flashdata('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif ?>

    <div class="medium-12 columns px-0-mobile">
        <div class="ads-page border-grey rounded-0">

            <!-- titulo anuncio -->
            <h1 class="ap-title h3 text-center  d-sm-none"><?= $ad->ad_name; ?></h1>

            <div class="row">
                <div class="medium-8 columns">

                    <!-- slider das imagens do anuncio -->
                    <?php if (isset($images) && !empty($images)): ?>
                        <?php $this->load->view('ads_detail_slider.php') ?>
                    <?php else: ?>
                        <div class="ap-images">
                            <div class="ap-i-master">
                                <img class="active" src="<?= thumbnail(false, false, 740, 400) ?>">
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="ap-desc"><br>
                        
                        <h3 id="show-descri-js">Informações </h3>
                        <!-- tab links das descricoes -->
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-varejo-tab" data-toggle="tab"
                                   href="#nav-varejo" role="tab" aria-controls="nav-varejo" aria-selected="true">Descrição</a>
                            </div>
                        </nav>

                        <!-- descricao do anuncio - conteudo tabs -->
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-varejo" role="tabpanel"
                                 aria-labelledby="nav-varejo-tab">
                                <p class="pt-2">
                                    <?= (($ad->ad_desc) ? nl2br($ad->ad_desc) : 'Esse anúncio não possui uma descrição para varejo.') ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <!-- perguntas sobre o produto-->
                    <?php $this->load->view('widget/pergunta_resposta.php'); ?>

                    <?php $this->load->view('widget/view_anuncios_duvidas.php'); ?>

                    <!-- anuncios relacionados -->
                    <?php $this->load->view('widget/anuncios_relacionados_view.php'); ?>


                </div>

                <!-- sidebar infos -->
                <div class="medium-4 columns">
                    <!-- tituto, preço e detalhes do anuncio -->
                    <div class="ap-salesman-info rounded-0 shadow-sm">
                        <div class="row col-12">
                        <span class="sb-color2">
                            <font size="2">
                                <?= $ad->ad_visits ?> Visualizações
                            </font>
                        </span>
                        </div>

                        <span style="float: right;">
                        <? $this->user_model->favorite_btn($ad->ad_id); ?>
                    </span>

                        <h1 class="ap-title">
                            <?= ($ad->ad_name); ?><br>
                            <small>Id: <span id="produto_id"><?= $ad->ad_id ?></span></small>
                            <?= (($ad->ad_verified) ? '<i class="fas fa-check-circle-o verify"></i>' : '') ?>
                        </h1>

                        <div class="row col-12 pr-0">
                            <span class="sb-color2">
                                <!-- preco varejo -->
                                <font size="6" class="text-dark my-3">
                                    <?= dinheiro($ad->ad_price, 2, true); ?>
                                </font>

                                <!-- formas de pagamento -->
                                <div class="my-3">
                                    <font class="text-dark">
                                        <i class="far fa-credit-card mb-2"></i> Formas de Pagemento:
                                    </font>
                                    <br>
                                    <div class="ml-4">
                                        <i class="card-icon visa-icon"></i>
                                        <i class="card-icon master-icon"></i>
                                        <i class="card-icon boleto-icon"></i>
                                    </div>
                                </div>

                                <!-- formas de envio -->
                                <div class="my-3">
                                    <font class="text-dark"><i class="fas fa-truck"></i>
                                        Envio para todo o país
                                    </font>
                                    <br>
                                    <font size="2" class="sb-color2 ml-4 info-frete">
                                        Saiba os prazos de entrega e as formas de envio.
                                    </font>
                                    <br>

                                    <?php if ($ad->ad_frete == "sim") { ?>
                                        <font size="2">
                                            <span id="meufrete" class="sb-color2 ml-4 info-frete"></span>
                                            <a href="javascript:void(0)" data-toggle="modal"
                                               data-target="#modal-frete-combinar"
                                               class="link text-info ml-4">
                                                Ver mais opções
                                            </a>
                                        </font>
                                    <?php } else { ?>
                                        <font size="2">
                                            <a href="javascript:void(0)" data-toggle="modal"
                                               data-target="#modal-frete"
                                               class="link text-info ml-4">
                                                Ver mais opções
                                            </a>
                                        </font>
                                    <?php } ?>

                                </div>
                            </span>
                        </div>

                        <div id="retorno"></div>
                        <input type="hidden" id="frete_val" value="">

                        <!-- variacao do anuncio -->

                        <div class="">
                            <?php
                                $cor = [];
                                $tamanho = [];
                                foreach ($options as $op) {
                                    if ($op->ad_option_id == 'cor')
                                        $cor[] = $op;
                                    if ($op->ad_option_id == 'tamanho')
                                        $tamanho[] = $op;
                                }
                            ?>
                            <div>
                            <?php if($variacoesCount > 0){ ?>
                                <label class="text-dark h6">Tamanho:</label><br>
                                <?php foreach ($tamanho as $key => $value): ?>
                                    <div class="col-xs-4 col-sm-3 col-md-2 nopad text-center d-inline"
                                         title="<?= $value->ad_option_val ?>">
                                        <label class="image-checkbox text-middle"><?= $value->ad_option_val ?>
                                            <input type="checkbox" name="tamanho[]" value="<?= $value->ad_option_val ?>"/>
                                            <i class="fa fa-check d-none"></i>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                                <br>

                            <div>
                                <label class="text-dark h6">Cores:</label><br>
                                <?php foreach ($cor as $key => $value): ?>
                                    <div class="col-xs-4 col-sm-3 col-md-2 nopad text-center d-inline"
                                         title="<?= $value->ad_option_val ?>">
                                        <label class="image-checkbox">
                                            <img class="img-responsive"
                                                 src="<?= base_url("admin/uploads/ads/{$value->ad_option_img}") ?>"/>
                                            <input type="checkbox" name="cor[]" value="<?= $value->ad_option_val ?>"/>
                                            <i class="fa fa-check d-none"></i>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            </div>
                        <?php }?>

                        <br>

                        <script>
                            // $(".image-checkbox").each(function () {
                            //     if ($(this).find('input[type="radio"]').first().attr("checked")) {
                            //         $(this).addClass('image-checkbox-checked');
                            //     } else {
                            //         $(this).removeClass('image-checkbox-checked');
                            //     }
                            // });

                            // sync the state to the input
                            $(".image-checkbox").on("click", function (e) {

                                $(this).parent().parent().find('.image-checkbox').removeClass('image-checkbox-checked');
                                $(this).parent().parent().find('.image-checkbox input[type="checkbox"]').prop('checked', false);

                                $(this).addClass('image-checkbox-checked');

                                var $checkbox = $(this).find('input[type="checkbox"]');

                                $checkbox.prop("checked", !$checkbox.prop("checked"));

                                e.preventDefault();
                            });
                        </script>

                        <style>
                            .text-middle {
                                line-height: 40px;
                            }

                            .nopad {
                                padding-left: 0 !important;
                                padding-right: 0 !important;
                            }

                            /*image gallery*/
                            .image-checkbox {
                                height: 45px;
                                width: 45px;
                                border-radius: 2px;
                                cursor: pointer;
                                box-sizing: border-box;
                                -moz-box-sizing: border-box;
                                -webkit-box-sizing: border-box;
                                border: 1px solid rgba(172, 172, 172, 0.47);
                                margin-bottom: 0;
                                outline: 0;
                            }

                            .image-checkbox input[type="checkbox"] {
                                display: none;
                            }

                            .image-checkbox-checked {
                                border-color: #ff7606;
                            }

                            .image-checkbox .fa {
                                position: absolute;
                                color: #ff7606;
                                /*background-color: #fff;*/
                                padding: 10px;
                                top: 0;
                                right: 0;
                            }

                            .image-checkbox-checked .fa {
                                display: block !important;
                            }
                        </style>

                        <center>
                            <font size="4">
                                <label for="quantidade"><font size="3">Quantidade:</font></label>
                                <input type="number" name="quantidade" step="1" value="1"
                                         min="1" max="<?php echo $ad->ad_qtdmax ?>"
                                       class=" mb-2 d-inline" size="5" id="quantidade">
                            </font>
                            Máximo <?php echo $ad->ad_qtdmax ?> unidade(s)
                            <br>

                            <input type="hidden" name="frete" id="fretePost">

                            <?php if($ad->ad_frete == "sim"){?>
                              <a style="width:80%" class="btn btn-primary mb-2 mt-2 comprar_atacado" id="comprar-atacado">
                                    Comprar
                                </a>
                            <?php }else{?>
                                  <!-- botao adicionar ao carrinho                    -->
                                <a data-add="<?= base_url("cart/add/{$ad->ad_id}") ?>" style="width: 80% !important"
                               class="btn-add-cart btn btn-primary mt-2">
                                <!-- <i class="fa fa-cart-plus"></i> -->
                                <i class="fas fa-shopping-bag"></i>
                                Adicionar ao carrinho</a>
                                <br>
                            <?php }?>

                        </center>

                        <hr>

                        <h4>Informações do Vendedor </h4>

                        <input type="hidden" id="vendedor" name="vendedor" value="<?php echo $ad->use_id ?>">
                        <ul class="p-0">
                            <li><i class="fa fa-fw fa-user"></i>
                                <?= $ad->use_name; ?>
                            </li>
                            <li class="px-1"><i class="fas fa-map-marker-alt"></i>
                                <?= $city;
                                echo " - " . $state; ?>
                            </li>
                        </ul>

                        <div class="row justify-content-center">
                            <div class="share-button m-3">
                                <font size="2"><a class="social-toggle"><i class="fas fa-share-alt"></i>
                                        Compartilhar</a></font>
                                <div class="social-networks">
                                    <ul>
                                        <li class="social-mail">
                                            <a href="javascript:void()" data-toggle="modal"
                                               data-target="#modal-share-mail"><i class="fa fa-envelope"></i></a>
                                        </li>

                                        <li class="social-facebook">
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?= current_url(); ?>"
                                               target="_blank"><i class="fab fa-facebook-f"></i></a>
                                        </li>

                                        <li class="social-whatsapp">
                                            <a href="https://web.whatsapp.com/send?text=<?= current_url(); ?>"
                                               data-action="share/whatsapp/share" target="_blank"><i
                                                        style="font-size: 22px;" class="fab fa-whatsapp"></i></a>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class="m-3">
                                <?php
                                if ($this->session->userdata('login')) { ?>

                                    <font size="2">
                                        <a href="javascript:void()" data-toggle="modal"
                                           data-target="#moda-report" class="ap-a-report modal-open">
                                            <i class="fa fa-bullhorn"></i> Denunciar
                                        </a>
                                    </font>

                                <?php } else { ?>

                                    <font size="2">
                                        <a href="javascript:void()" data-toggle="modal"
                                           data-target="#modal-required" class="ap-a-report modal-open">
                                            <i class="fa fa-bullhorn"></i> Denunciar
                                        </a>
                                    </font>

                                <?php } ?>
                            </div>
                        </div>

                    </div>

                    <!-- imagem publicidade -->
                    <div class="col-12 shadow-sm">
                        <a target="_blank" href="<?= $img_publicidade->adv_url ?>">
                            <img src="<?= base_url('uploads/publicidade/' . $img_publicidade->adv_img) ?>">
                        </a>
                    </div>
                    <?= $this->main_model->advertisingBox('top', '95%', '300px') ?>
                    <div class="hide-for-small-only"><?= $this->main_model->advertisingBox('top', '95%', '300px') ?></div>
                </div>
            </div>
            <div class="hide-for-small-only"><?= $this->main_model->advertisingBox('top', '100%', '90px') ?></div>
        </div>
    </div>
</div>


<script src="<?= base_url('assets/js/custom/ads_details.js') ?>"></script>

<script>
    function calcular() {
        $("#retorno").html("Calculando...");
        var base_url = '<?php echo base_url() ?>';
        var cep_destino = $("#cep_destino").val();

        var cep_origem = '<?php echo $ad->use_cep ?>';
        var comprimento = '<?php echo $ad->ad_comprimento ?>';
        var largura = '<?php echo $ad->ad_largura ?>';
        var altura = '<?php echo $ad->ad_altura ?>';
        var diametro = '<?php echo $ad->ad_diametro ?>';
        var peso = '<?php echo $ad->ad_peso ?>';

        $.post(base_url + 'index.php/Correios/frete', {
            cep_destino,
            comprimento,
            altura,
            largura,
            diametro,
            peso,
            cep_origem,
            qtd
        }, function (data) {
            $("#retorno").html(data);
        });
    }

</script>

<?php
require_once 'modal/ads_email_share.php';
require_once 'modal/ads_report.php';
require_once 'modal_frete.php';
require_once 'modal_frete_combinar.php';

//if ($ad->ad_tipo != "varejo")
    //require_once 'modal_atacado.php';
?>


<script type="text/javascript">
    $(document).ready(function () {
        <?php if( count($related) > 3 ): ?>
        $('.toslick').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,

            prevArrow: '<div class="sl-prev"><span class="fa fa-angle-left"></span><span class="sr-only">Prev</span></div>',

            nextArrow: '<div class="sl-next"><span class="fa fa-angle-right"></span><span class="sr-only">Next</span></div>',

            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        <?php endif; ?>
        $(".toslick").fadeIn(700)
    })
</script>
