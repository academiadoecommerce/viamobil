<div class="container px-0-mobile ">
	<link rel="stylesheet" href="<?php echo base_url( "assets/css/register_page.css" ); ?>">

	<div class="medium-12 columns px-0-mobile mt-4">
		<div class="ads-page border-grey rounded-0 px-5">
			<div class="row">

				<div class="col-12 col-md-6">
					<h2>Cadastre-se grátis</h2>
				</div>

				<!--<div class="col-12 col-md-6">
					<ul class="nav nav-tabs switch-btn-group nav-justified mt-2" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link nav-switch <?=((!$c) ? 'show active' : '')?>" id="nav-profile-tab" data-toggle="tab"
								href="#nav-profile" role="tab" aria-controls="nav-profile"
								aria-selected="<?=((!$c) ? 'true' : 'false')?>">Lojista</a>
						</li>

						<li class="nav-item">
							<a class="nav-link nav-switch <?=(($c) ? 'show active' : '')?>" id="nav-home-tab" data-toggle="tab"
								href="#nav-home" role="tab" aria-controls="nav-home"
								aria-selected="<?=(($c) ? 'true' : 'false')?>">Cliente</a>
						</li>
					</ul>
				</div>-->

			</div>
			<hr>

			<div class="tab-content" id="nav-tabContent">
				<div class="tab-pane fade <?=((!$c) ? 'show active' : '')?> " id="nav-profile" role="tabpanel"
					aria-labelledby="nav-profile-tab">

					<form method="POST" action="<?= base_url('register/insert') ?>" id="form-register" class="form form-simple">
						<div class="row">

							<div class="col-md-6">
								<h3 class="mt-0">Dados gerais</h3>
								<hr>
								<input type="hidden" name="use_type" value="lojista">

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="nome">Nome completo*</label>
										<input class="form-control my-1" type="text" required name="name" autocomplete="off" id="nome"
											placeholder="Nome Completo">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="input-email">E-mail*</label>
										<input class="form-control my-1" type="email" required name="email" id="input-email"
											placeholder="E-mail (exemplo@exemplo.com.br)" autocomplete="off">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="telefone">Telefone*</label>
										<input class="form-control my-1" type="telefone" 
											required name="telefone" id="telefone" 
											placeholder="Número de telefone ou celular"
											data-mask="(99)99999-9999">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="cpfCnpj">CPF*</label>
										<input class="cpfCnpj my-1 form-control" type="text" name="cpfCnpj" 
											id="cpfCnpj" maxlength="14"
											onkeyup="somenteNumeros(this);" placeholder="Número do CPF/CNPJ">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="cpfCnpj">Data Nascimento*</label>
										<input class=" my-1 form-control" type="date" name="data_nascimento" id="data_nascimento"
											maxlength="14">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12 mb-1">
										<label for="urlSite">Website</label>
										<input class="form-control my-1" type="text" name="urlSite" id="urlSite"
											placeholder="URL do Site">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="senha">Senha*</label>
										<input type="password" required onkeyup="passwordStrength(this.value)" autocomplete="off" id="senha"
											name="password" placeholder="Senha (escolha uma senha forte)" class="form-control my-1">
										<div class="passwordForce">
											<div class="no-strength"></div>
										</div>
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">

										<label>Você é:</label> <br>

										<div class="radio radio-info radio-inline">
											<input type="radio" id="inlineRadio1" value="revendedor" name="tip_vend" checked>
											<label for="inlineRadio1"> Revendedor </label>
										</div>

										<div class="radio radio-info radio-inline">
											<input type="radio" id="inlineRadio2" value="fabricante" name="tip_vend">
											<label for="inlineRadio2"> Fabricante </label>
										</div>

										<div class="radio radio-info radio-inline">
											<input type="radio" id="inlineRadio3" value="importador" name="tip_vend">
											<label for="inlineRadio3"> Importador </label>
										</div>

									</div>
								</div>

							</div>

							<div class="col-md-6">
								<h3 class="mt-0">Endereço da empresa</h3>
								<hr>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="ai-cep">CEP*</label>
										<input type="text" required class="my-1 form-control input-cep" name="cep" id="ai-cep" maxlength="9"
											placeholder="CEP">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12 col-md-5">
										<label for="ai-state">Estado*</label>
										<select class="form-group form-control my-1" name="state" id="ai-state" required>
											<option>Selecione seu Estado</option>
										</select>
									</div>
									<!-- <div class="form-group row my-1"> -->
									<div class="col-12 col-md-7">
										<label for="ai-city">Cidade*</label>
										<select class="form-group form-control my-1" name="city" id="ai-city" required>
											<option value="">Selecione um município</option>
										</select>
									</div>
								</div>

								<div class="form-group row my-1" id="box-address">
									<div class="col-12 col-md-9">
										<label for="ai-address">Endereço*</label>
										<input class=" my-1 form-control" type="text" name="address" id="ai-address"
											placeholder="Nome da rua" value="" required>
									</div>

									<div class="col-12 col-md-3">
										<label for="ai-address-number">Número*</label>
										<input class=" my-1 form-control" type="text" name="number_address" id="ai-address-number"
											placeholder="Número" required>
									</div>
								</div>

								<div class="form-group  row my-1" id="box-neighborhood">
									<div class="col-12">
										<label for="ai-neighborhood">Bairro*</label>
										<div id="label-neighborhood" class="d-none" style="margin-bottom: 10px;">...</div>
										<input class=" my-1 form-control" type="text" name="neighborhood" id="ai-neighborhood" value=""
											required placeholder="Nome do bairro">
									</div>
								</div>

								<div class="form-group  row my-1" id="box-neighborhood">
									<div class="col-12">
										<label for="ai-sobre">Conte mais sobre seu negócio*</label>
										<!-- modal informação sobre lojista 
                                            <span class="float-right">
										<a class="nav-link" href="#"  data-toggle="modal" data-target="#texto-exemplo">
											<small>Exemplo</small>
										</a>
									</span> 
										<div class="modal fade" id="texto-exemplo" tabindex="-1" role="dialog">
											<div class="modal-dialog login1" style="margin-top: 130px;">
												<div class="modal-content">
													Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
													labore et dolore magna aliqua.
												</div>
											</div>
										</div> -->
										<div id="label-neighborhood" class="d-none" style="margin-bottom: 10px;">...</div>
										<textarea class=" my-1 form-control" name="sobre" id="ai-sobre" required style="height: 121px;"
											placeholder="Descreve mais detalhes sobre seu negócio, para que possamos avaliar se você se enquadra como um de nossos parceiros."></textarea>
									</div>
                                </div>
                                
                                <!--<div class="form-group  row my-1" id="box-neighborhood">
									<div class="col-12">
                                        <label for="ai-sobre">Já possui conta no Wirecard?</label>
                                        <div class="radio radio-info radio-inline">
											<input type="checkbox" id="wirecard" value="possui_conta_wirecard" name="tip_vend" onchange="$('input.wirecard_id').toggle()" >
                                            <label for="wirecard"> Sim </label> <br>
                                        </div>
                                        <span style="padding: 0 6px; margin-top: 0"
                                            class="btn btn-help hidden-xs">
                                            <i style="font-size: 12px" class="fa fa-question" data-toggle="modal" data-target="#modalExemplo"></i>
                                        </span>
                                        <br>
                                        <span style="color: black; font-style: italic; font-size: 12px;"> Se não possuir uma conta, será criado automaticamente com os dados fornecidos</span>
                                        <input class=" my-1 form-control wirecard_id" type="text" name="wirecard_id" id="wirecard_id" value=""
											 placeholder="Informe seu id Wirecard" style="display:none">
                                    </div>
                                </div>-->
							</div>

							<div class="col-md-12">
								<hr>
								<div class="form-group row my-1">
									<div class="col-12 col-md-6">
										<div class="checkbox-custom">
											<input type="checkbox" required="" id="ai-terms">
											<label for="ai-terms">Concordo com os <a href="<?=base_url()?>/ajuda/termos-de-uso"
													target="_blank">Termos do <?php echo NOME_SITE ?></a>.</label>
										</div>
									</div>

									<div class="col-12 col-md-6">
										<button type="submit" class="btn btn-primary float-right" id="cadastrar"> <i class="fa fa-user-plus"
												aria-hidden="true"></i> Cadastrar</button>
									</div>

								</div>
							</div>
						</div>
					</form>
				</div>


				<!-- cadastro cliente -->
				<div class="tab-pane fade <?=(($c) ? 'show active' : '')?>" id="nav-home" role="tabpanel"
					aria-labelledby="nav-home-tab">
					<form method="POST" action="<?= base_url('register/insert') ?>" id="form-register" class="form form-simple">
						<div class="row">

							<div class="col-md-6">
								<h3 class="mt-0">Dados gerais</h3>
								<hr>
								<input type="hidden" name="use_type" value="cliente">

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="nome">Nome completo*</label>
										<input class="form-control my-1" type="text" required name="name" autocomplete="off" id="nome"
											placeholder="Nome Completo">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="input-email">E-mail*</label>
										<input class="form-control my-1" type="email" required name="email" id="input-email"
											placeholder="E-mail (exemplo@exemplo.com.br)">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="telefone">Telefone*</label>
										<input class="form-control my-1" type="telefone" required name="telefone" id="telefone"
											placeholder="Número de telefone ou celular">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="cpfCnpj">CPF*</label>
										<input class=" my-1 form-control" type="text" name="cpfCnpj" id="cpfCnpj" class="cpfCnpj" maxlength="14"
											onkeyup="somenteNumeros(this);" placeholder="Número do CPF">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="cpfCnpj">Data Nascimento*</label>
										<input class=" my-1 form-control" type="date" name="data_nascimento" id="data_nascimento"
											maxlength="14">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="senha">Senha*</label>
										<input class="form-control my-1" type="password" required onkeyup="passwordStrength(this.value)"
											id="senha" name="password" placeholder="Senha (escolha uma senha forte)">
										<div class="passwordForce">
											<div class="no-strength"></div>
										</div>
									</div>
								</div>

							</div>

							<div class="col-md-6">
								<h3 class="mt-0">Endereço</h3>
								<hr>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="c-cep">CEP*</label>
										<input type="text" required class="my-1 form-control input-cep" name="cep" id="c-cep" maxlength="9"
											placeholder="CEP">
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="c-state">Estado*</label>
										<select class="form-group form-control my-1" name="state" id="c-state" required>
											<option>Selecione seu Estado</option>
										</select>
									</div>
								</div>

								<div class="form-group row my-1">
									<div class="col-12">
										<label for="c-city">Cidade*</label>
										<select class="form-group form-control my-1" name="city" id="c-city" required>
											<option value="">Selecione um município</option>
										</select>
									</div>
								</div>

								<!-- <div class="form-group row my-1">
									<div class="col-12">
										<label for="ai-city">Cidade*</label>
										<input type="text" required class="my-1 form-control input-cidade" name="city" id="c-city" maxlength="9"
											placeholder="Cidade">
									</div>
								</div> -->

								<div class="form-group  row my-1" id="box-neighborhood">
									<div class="col-12">
										<label for="c-neighborhood">Bairro*</label>
										<div id="label-neighborhood" class="d-none" style="margin-bottom: 10px;">...</div>
										<input class=" my-1 form-control" type="text" name="neighborhood" id="c-neighborhood" value=""
											placeholder="Nome do bairro">
									</div>
								</div>

								<div class="form-group row my-1" id="box-address">
									<div class="col-12 col-md-9">
										<label for="c-address">Endereço*</label>
										<input class=" my-1 form-control" type="text" name="address" 
											id="c-address" placeholder="Nome da rua" value="">
									</div>

									<div class="col-12 col-md-3">
										<label for="c-address-number">Número*</label>
										<input class=" my-1 form-control" type="text" name="number_address" 
											id="c-address-number" placeholder="número">
									</div>
								</div>

							</div>

							<div class="col-md-12">
								<hr>
								<div class="form-group row my-1">
									<div class="col-12 col-md-6">
										<div class="checkbox-custom">
											<input type="checkbox" required="" id="ai-terms">
											<label for="ai-terms">Concordo com os 
												<a href="<?=base_url()?>/ajuda/termos-de-uso"
													target="_blank">Termos do <?php echo NOME_SITE ?></a>.
											</label>
										</div>
									</div>

									<div class="col-12 col-md-6">
										<button type="submit" class="btn btn-primary float-right" 
												id="">
												<!-- cadastrar -->
											<i class="fa fa-user-plus"
											aria-hidden="true"></i> 
											Cadastrar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Wirecard Help-->
<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Precisa de ajuda?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
     	<p>Se não souber como gerar seu <span> <b>ID MOIP</b> </span> entre em contato com o suporte do Wirecard.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <a href="https://suporte.wirecard.com.br/hc/pt-br">
        	<button type="button" class="btn btn-primary">Ir para Wirecard</button>
    	</a>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	$(document).ready(function () {
		$("#c-cep").mask("99999-999");
	});
</script>

<script type="text/javascript">
	$(document).ready(function () {
		$("#ai-cep").mask("99999-999");
	});
</script>

<!-- <script src="<?= base_url("assets/js/custom/register.js") ?>"></script> -->
<!-- <script src="<?= base_url("assets/js/custom/modal_registro.js") ?>"></script> -->


<?php if($this->session->flashdata('msg')){ ?>
 <div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata('msg');?>
 </div>
 <?php } ?>