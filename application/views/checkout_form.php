
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>


/** {
  box-sizing: border-box;
}*/

/*.row {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin: 0 -16px;
}*/

.col-25 {
  -ms-flex: 25%; /* IE10 */
  flex: 25%;
}

.col-50 {
  -ms-flex: 50%; /* IE10 */
  flex: 50%;
}

.col-75 {
  -ms-flex: 75%; /* IE10 */
  flex: 75%;
}

.col-25,
.col-50,
.col-75 {
  padding: 0 16px;
}

/*.container {
  background-color: #f2f2f2;
  padding: 5px 20px 15px 20px;
  border: 1px solid lightgrey;
  border-radius: 3px;
}

input[type=text] {
  width: 100%;
  margin-bottom: 20px;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 3px;
}*/

label {
  margin-bottom: 10px;
  display: block;
}

.icon-container {
  margin-bottom: 20px;
  padding: 7px 0;
  font-size: 24px;
}

/*.btn {
  background-color: #4CAF50;
  color: white;
  padding: 12px;
  margin: 10px 0;
  border: none;
  width: 100%;
  border-radius: 3px;
  cursor: pointer;
  font-size: 17px;
}

.btn:hover {
  background-color: #45a049;
}

a {
  color: #2196F3;
}*/

hr {
  border: 1px solid lightgrey;
}

span.price {
  float: right;
  color: grey;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (also change the direction - make the "cart" column go on top) */
@media (max-width: 800px) {
 /* .row {
    flex-direction: column-reverse;
  }*/
  .col-25 {
    margin-bottom: 20px;
  }
}
</style>
<section class="section-main bg padding-y-sm">
  <div class="container">
    <div class="card-body bg-white border p-4">
      <div class="row">
        <aside class="col-md-12">
<h2>Finalizar Compra</h2>
<p>É hora de escolher a forma de pagamento e o endereço de entrega!</p>
<div class="row">
  <div class="col-75">
    <div class="container">
      <form action="<?=base_url('checkout/send')?>">
      
        <div class="row">
          <div class="col-50">
            <h3>Informações de entrega</h3>
            <label for="fname"><i class="fa fa-user"></i> Nome Completo</label>
            <input type="text" id="name" name="name" placeholder="John A. Sauro">
            <label for="email"><i class="fa fa-envelope"></i> Email</label>
            <input type="text" id="email" name="email" placeholder="joao@exemplo.com">
            <label for="adr"><i class="fa fa-address-card-o"></i> Endereço</label>
            <input type="text" id="adr" name="address" placeholder="Rua das Grevilhas">
            <label for="city"><i class="fa fa-institution"></i> Cidade</label>
            <input type="text" id="city" name="city" placeholder="São Paulo">

            <div class="row">
              <div class="col-50">
                <label for="state">State</label>
                <input type="text" id="state" name="state" placeholder="SP">
              </div>
              <div class="col-50">
                <label for="zip">Zip</label>
                <input type="text" id="cep" name="cep" data-mask="99999-999" placeholder="00000-000">
              </div>
            </div>
          </div>

          <div class="col-50">
            <h3>Informações de Pagamento</h3>
            <label for="fname">Metodos disponíveis</label>
            <div class="icon-container">
              <i class="fa fa-cc-visa" style="color:navy;"></i>
              <i class="fa fa-cc-amex" style="color:blue;"></i>
              <i class="fa fa-cc-mastercard" style="color:red;"></i>
              <i class="fa fa-cc-discover" style="color:orange;"></i>
            </div>
            <label for="cname">Nome impresso no Cartão</label>
            <input type="text" id="cardname" name="cardname" placeholder="JOÃO A S SAURO">
            <label for="ccnum">Número do Cartão</label>
            <input type="text" id="ccnum" name="cardnumber" data-mask="9999-9999-9999-9999" placeholder="1111-2222-3333-4444">
            <label for="expmonth">Mês de validade</label>
            <input type="text" id="expmonth" name="expmonth" placeholder="Agosto">
            <div class="row">
              <div class="col-50">
                <label for="expyear">Ano de validade</label>
                <input type="text" id="expyear" name="expyear" placeholder="2018">
              </div>
              <div class="col-50">
                <label for="cvv">CCV</label>
                <input type="text" id="cvv" name="cvv" placeholder="352">
              </div>
            </div>
          </div>
          
        </div>
        <label>
          <input type="checkbox" checked="checked" name="sameadr"> Enviar para o mesmo endereço do Cadastro
        </label>
        <input type="submit" value="Continuar" class="btn btn-secondary">
      </form>
    </div>
  </div>
  <div class="col-25">
    <div class="container">
      <h4>Produtos <span class="price" style="color:black"><i class="fa fa-shopping-cart"></i> <b><?= $this->cart->total_items() ?></b></span></h4>
      <?php foreach ($produtos as $items): ?>
      <p><a href="#"><?php echo $items['name']?> </a> <span class="price">R$ <?php echo dinheiro($items['price'])?></span></p>
      <?php endforeach; ?>
      <hr>
      <p>Total <span class="price" style="color:black"><b>R$ <?php echo dinheiro($this->cart->total()) ?></b></span></p>
    </div>
  </div>
</div>

</aside>
</div>
</div>
</div>
</section>