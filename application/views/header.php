<?php
if ($this->session->userdata('login')) {
    $user = $this->user_model->info();
}
?>

<style>
    @media (max-width: 640px) {
        #col-search {
            display: none;
        }

        #logo {
            text-align: center;
        }

        #navbarTop {
            text-align: center;
            font-size: 18px;
        }

        .navbar-toggler {
            background-color: #37373A;
        }

        div.navbar-nav:hover {
            background-color: #CCCCCC;
        }

        div.navbar-nav {
            border-bottom: 1px solid #000000;
        }

        div#navbar-nav-top {
            border-top: 1px solid #000000;
        }
        .navbar_mob_desk{
            bottom: -7px !important;
        }
    }
</style>

<script>
    function somenteNumeros(num) {
        var er = /[^0-9.]/;
        er.lastIndex = 0;
        var campo = num;
        if (er.test(campo.value)) {
            campo.value = "";
        }
    }
</script>
<div class="fluid-container">
    <header class="section-header shadow-sm" style="padding-bottom: 0; position: relative; z-index: 99;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-2">
                    <a href="<?= base_url() ?>">
                        <img class="logo my-3" id="logo"
                             src="<?= base_url("assets/img/logo_via_mobil_site.png") ?>"
                             title="ViaMóbil">
                    </a>
                </div>
                <div class="col-12 d-lg-none" id="">
                    <form method="get" action="<?= base_url('anuncios') ?>">
                        <div class="input-group w-100 rounded" style="border: 1px solid #F2700F">
                            <select class="rounded bo border-0" name="category_name" style="width: 90px;">
                                <option value="anuncios">Anúncios</option>
                                <option value="lojas">Lojas</option>
                            </select>
                            <input style="border-left: 1px solid #F2700F !important;" name="search" type="text"
                                   class="form-control border-0 rounded-0" placeholder="Pesquisar por palavra" value="">
                            <div class="input-group-append">
                                <button class="btn btn-primary rounded-0 border-0" type="submit">
                                    <i class="fa fa-search" style=" margin: 6px; "></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-10 d-none d-lg-block">
                    <div class="row my-2 d-flex justify-content-between">
                        <div class="col-md-2 navbar-nav"></div>
                    </div>
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-8 pl-0 pr-2" id="col-search">
                            <form method="get" action="<?= base_url('anuncios/'); ?>">
                                <div class="input-group w-100 rounded" style="border: 1px solid #F2700F">
                                    <select class="rounded bo border-0" name="category_name" style="width: 130px;">
                                        <option value="anuncios" <? if ($this->uri->segment(1) == 'anuncios') echo
                                        'selected'; ?> >Anúncios
                                        </option>
                                        <option value="lojas"
                                            <?php if ($this->uri->segment(1) == 'lojas') echo 'selected'; ?> >
                                            Lojas
                                        </option>
                                    </select>
                                    <input style="border-left: 1px solid #dbdde6 !important;" name="search" type="text"
                                           class="form-control border-0 rounded-0" placeholder="Pesquisar por palavra"
                                           value="<?= @$_GET['search']; ?>">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary rounded-0 border-0" type="submit">
                                            <i class="fa fa-search"></i> Pesquisar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-lg-4 navbar-nav " id="navbar-nav-top">
                            <div class="widgets-wrap d-flex justify-content-end">
                                <?php if ($this->session->userdata('login')) { ?>
                                    <div class="widget-header p-1 pr-2 icontext">
                                        <div class="icon-wrap icon-xs bg2 round text-secondary">
                                            <a href="<?= base_url('cliente/painel') ?>">
                                                <i class="fa fa-user"></i>
                                            </a>
                                        </div>
                                        <div class="text-wrap">
                                            <small>Olá, <b><?= first_name($user->use_name) ?></b></small>
                                            <span>
											<a style="font-size: 13px;" href="<?= base_url('cliente/painel') ?>">Minha
												Conta</a> |
											<a style="font-size: 13px;" class="" href="<?= base_url('cliente/sair') ?>">
												Sair
												<i class="fas fa-sign-out-alt"></i>
											</a>
										</span>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="widget-header dropdown pt-1 pr-2">
                                        <a href="#" class="ml-3 icontext" data-toggle="dropdown" data-offset="20,10"
                                           aria-expanded="false">
                                            <div class="icon-wrap icon-xs bg2 round text-secondary"><i
                                                        class="fa fa-user"></i></div>
                                            <div class="text-wrap">
                                                <small>Minha conta</small>
                                                <span>Entrar | Cadastrar<i class="fa fa-caret-down ml-2"></i></span>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"
                                             style="position: absolute; transform: translate3d(-155px, 42px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <form class="px-4 py-3" method="POST" action="<?= base_url('login/in') ?>"
                                                  id="form-login">
                                                <div class="form-group">
                                                    <label>E-mail</label>
                                                    <input required name="email" id="input-email" type="email"
                                                           class="form-control" placeholder="email@exemplo.com">
                                                </div>
                                                <div class="form-group">
                                                    <label>Senha</label>
                                                    <input required name="password" id="input-password" type="password"
                                                           class="form-control" placeholder="Senha">
                                                </div>
                                                <button type="button" id="logar"
                                                        class="btn btn-primary w-25">Entrar
                                                </button>
                                            </form>
                                            <hr class="dropdown-divider">
                                            <a class="dropdown-item" href="#" data-toggle="modal"
                                               data-target="#cadastro_ask_modal">Cadastre-se</a>
                                            <a data-toggle="modal" data-target="#modal-password"
                                               class="modal-open dropdown-item" href="#">Esqueceu a senha?</a>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="widget-header p-1">
                                    <a href="<?= base_url('cart') ?>" class="icontext">
                                        <div class="icon-wrap icon-xs bg2 round text-secondary"
                                             style="padding-right: 2px;"><i class="fa fa-shopping-cart"></i></div>
                                        <div class="text-wrap">
                                            <small>Carrinho</small>
                                            <span><?= $this->cart->total_items() ?> itens</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light py-0 navbar_mob_desk" style="position: relative;bottom: 9px;">
            <div class="container">
                <button class="navbar-toggler px-3 py-2" type="button" data-toggle="collapse" data-target="#navbarTop"
                        aria-controls="navbarTop" aria-expanded="false" aria-label="Toggle navigation" style="position: absolute; right: 16px; top: -90px; color: #ff7606 !important;">
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse px-4" id="navbarTop">
                    <div class="category-wrap dropdown ml-3">
                        <button type="button" class="btn btn-primary border-primary dropdown-toggle"
                                data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bars"></i> Categorias
                        </button>
                        <div class="dropdown-menu" x-placement="top-start"
                             style="position: absolute; transform: translate3d(118px, -3px, 0px); top: 0px; left: 0px; will-change: transform; columns: 3; width: 700px;">

                            <?php foreach ($this->ads_model->categories(0) as $key => $value): ?>
                                <a class="dropdown-item"
                                   href="<?= base_url("anuncios/?categoria={$value->ads_cat_id}") ?>">
                                    <?= $value->ads_cat_name ?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a href="<?= base_url('ajuda/seja-lojista') ?>" class="nav-link"> Venda no ViaMóbil </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('anuncios') ?>" class="nav-link"> Anúncios </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('lojas') ?>" class="nav-link"> Lojas </a>
                        </li>
                        <?php if ($this->session->userdata('login_tipo') == 'lojista') { ?>
                            <li class="nav-item">
                                <a href="<?= base_url('announce') ?>" class="nav-link"> Inserir Anúncio </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="<?= base_url('contato') ?>" class="nav-link"> Fale conosco </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('quem-somos') ?>" class="nav-link"> Quem somos </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div class="menu-overlay"></div>
    <div class="side-menu-wrapper" style="display: none;">
        <a href="#" class="menu-close ">×</a>
        <ul>
            <li class="side-menu-wrapper-header"><i style="font-size: 30px" class="fas fa-user"></i> <br>
                <?php if ($this->session->userdata('login')) { ?>
                <div><span><a href="<?= base_url('cliente/painel') ?>">Olá,
							<b><?= first_name($user->use_name) ?></b></a></span> | <span><a
                                href="<?= base_url('cliente/sair') ?>">Sair</a></span></div>
            </li>
            <?php } else { ?>
                <div><span><a href="#" data-toggle="modal" data-target="#modal-login">Entrar</a></span> | <span><a
                                href="<?= base_url('/registrar') ?>">Cadastrar-se</a></span></div>
                </li>
            <?php } ?>
            <li><a href="<?= base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="<?= base_url('categorias') ?>"> <i class="fas fa-list-ul"></i> Categorias</a></li>
            <li><a href="<?= base_url('anuncios') ?>"><i class="fa fa-tag"> </i> Anúncios</a></li>
            <li><a href="<?= base_url('lojas') ?>"><i class="fa fa-shopping-cart"></i> Lojas</a></li>

            <?php if ($this->session->userdata('login')) { ?>
                <li><a href="<?= base_url('anunciar') ?>"><i class="fas fa-clipboard-check"></i> Inserir Anúncio</a>
                </li>
                <li><a href="<?= base_url('cliente/painel') ?>"><i class="fas fa-bars"></i> Minha conta</a>
                    <nav class="menu-dashboard">
                        <ul class="ml-0">
                            <?php if ($this->session->userdata('login_tipo') == 'lojista') { ?>
                                <li>
                                    <a href="<?= base_url('cliente/painel') ?>"
                                        <?= (($page == "dashboard") ? 'class="active"' : '') ?> target="_self">
                                        <span><i class="fa fa-fw fa-tags"></i></span>
                                        <span>Meus Anúncios</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= base_url('cliente/loja') ?>"
                                        <?= (($page == "shop" || $page == "shop_denied" || $page == "shop_create") ? 'class="active"' : '') ?>
                                       target="_self">
                                        <span><i class="fa fa-fw fa-shopping-cart"></i></span>
                                        <span>Minha Loja</span>
                                    </a>
                                </li>
                            <?php } ?>
                            <li>
                                <a href="<?= base_url('cliente/detalhes') ?>"
                                    <?= (($page == "details") ? 'class="active"' : '') ?> target="_self">
                                    <span><i class="fa fa-fw fa-cogs"></i></span>
                                    <span>Meu Cadastro</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('pedidos') ?>" <?= (($page == "view_pedidos") ? 'class="active"' : '') ?>
                                   target="_self">
                                    <span><i class="fas fa-box"></i></span>
                                    <span>Meus Pedidos</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('cart') ?>" <?= (($page == "view_pedidos") ? 'class="active"' : '') ?>
                                   target="_self">
                                    <span><i class="fa fa-fw fa-shopping-cart"></i></span>
                                    <span>Meu Carrinho</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= base_url('minhas_reclamacoes') ?>"
                                    <?= (($page == "view_reclamacoes") ? 'class="active"' : '') ?> target="_self">
                                    <span><i class="fa fa-comment"></i></span>
                                    <span>Minhas Reclamações</span>
                                </a>
                            </li>
                            <li>


                                <a href="<?= base_url('cliente/favoritos') ?>"
                                    <?= (($page == "favorites") ? 'class="active"' : '') ?> target="_self">
                                    <span><i class="fa fa-fw fa-heart"></i></span>
                                    <span>Favoritos</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('cliente/sair') ?>" target="_self">
                                    <span><i class="fa fa-fw fa-times"></i></span>
                                    <span>Sair</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </li>
            <?php } else { ?>
                <li>
                    <a href="#" data-toggle="modal" data-target="#modal-required"><i class="fas fa-clipboard-check"></i>
                        Inserir Anúncio
                    </a>
                </li>
                <li>
                    <a href="#" data-toggle="modal" data-target="#modal-required"><i class="fas fa-bars"></i>
                        Minha conta
                    </a>
                </li>
            <?php } ?>
            <br><br><br>
        </ul>
    </div>
