<div class="container">
    <section class="breadcrumbs">
        <?php
        include_once('returns.php');

        if (@$breadcrumbs) {
            echo '
            <div class="row hide-for-small-only">
                <div class="medium-12 columns">
                    <div class="breadcrumbs">
                       <ul>
                          <li><a href="' . base_url() . '">Início</a></li>';

                         foreach ($breadcrumbs as $key => $bc) {
                           echo '<li>' . ((@$bc['link']) ? '<a href="' . $bc['link'] . '" target="_self">' . $bc['name'] . '</a>' : $bc['name']) . '</li>';
                        }
                  echo '
                       </ul>
                    </div>
                </div>
            </div>'; }
                ?>
    </section>
<div class="medium-12 columns">
    <div class="ads-page border-grey rounded-0">
        <div class="row">
			<div class="col-3 p-0">
				<nav class="menu-dashboard"> 
					<ul class="m-0">
						<?php
							$pages = $this->main_model->pages();

							foreach ($pages as $key => $pag) {
								echo '
								<li>
									<a href="'.base_url('ajuda/'.$pag->page_slug).'" '.((@$p && $pag->page_id == $p->page_id) ? 'class="active"' : '').' target="_self">
										<span><i class="fa fa-fw '.$pag->page_icon.'"></i></span>
										<span>'.$pag->page_name.'</span>
									</a>
								</li>
								';
							}
						?>
						<li>
							<a href="<?=base_url('contato')?>" <?=(($page == "contact") ? 'class="active"' : '')?> target="_self">
								<span><i class="fa fa-fw fa-comments"></i></span>
								<span>Fale Conosco</span>
							</a>
						</li>
					</ul>
				</nav>
				<!--
				<div class="hide-for-small-only">
					<?=$this->main_model->advertisingBox('side', '266px', '600px')?>
				</div>
			-->
			</div>
			<div class="col-lg-9 columns">
				

				<div class="simple-page">
				<?php
					include_once("pages/".$page.".php");
				?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>