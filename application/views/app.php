<!DOCTYPE HTML>
<html lang="en">
	<head>
		
	<?php
	/* Config */
	$config = $this->main_model->config();

	/* SEO */
	$site_name = ((@$seo_title) ? $seo_title . " - " . $config->cfg_seo_title : $config->cfg_seo_title);
	$current_url = base_url($_SERVER['REQUEST_URI']);

	/* Maintenance */
	$maintenance = $config->cfg_maintenance;
	if ($maintenance && ( $_SERVER['REMOTE_ADDR'] != $maintenance ) && (CURRENT_PAGE != "manutencao")) {
	    header("location:" . base_url('manutencao'));
	}
	?>

<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title><?= $site_name ?></title>

<!-- meta tags -->
<meta name="description" content="<?= $config->cfg_seo_desc ?>">
<meta name="keywords" content="<?= $config->cfg_seo_keywords ?>">
<meta name="author" content="<?= $config->cfg_seo_title ?>">
<meta name="google-site-verification" content="oOELG0VwXLw9xuL-ZIzFE3P1CJV8ztFfTOVFb4yqwMU" />

<!-- og tags -->
<meta property="og:title" content="<?= $site_name ?>">
<meta property="og:url" content="<?= $current_url ?>">
<meta property="og:site_name" content="<?= $config->cfg_seo_title ?>">
<meta property="og:description" content="<?= $config->cfg_seo_desc ?>">
<meta property="fb:app_id" content="317733798566075">
<meta property="og:type" content="website">
<link rel="shortcut icon" type="image/png" href="<?= base_url('assets/img/favicon.png') ?>" />

<!--  NOVOS ESTILOS CSS -->
	<link href="<?= base_url('assets/fonts/fontawesome/css/fontawesome-all.min.css')?>" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url('assets/css/foundation.css') ?>"> 
	<!-- <link rel="stylesheet" href="<?= base_url('assets/font-awesome/css/font-awesome.min.css') ?>"> -->
	<!-- <link href="<?= base_url('assets/plugins/fancybox/fancybox.min.css')?>" type="text/css" rel="stylesheet"> -->
	<!-- <link rel="stylesheet" href="<?= base_url('assets/tooltipster/dist/css/tooltipster.bundle.min.css') ?>">   -->
	<link href="<?= base_url('assets/plugins/owlcarousel/assets/owl.carousel.min.css')?>" rel="stylesheet">
	<link href="<?= base_url('assets/plugins/owlcarousel/assets/owl.theme.default.css')?>" rel="stylesheet">	
    <link href="<?= base_url()?>assets/css/shortcodes.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/style.css" rel="stylesheet">		
	<link href="<?= base_url()?>assets/css/color.css" rel="stylesheet">	
	<link rel="stylesheet" href="<?= base_url('assets/css/chat.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/vue.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/chat_box.css') ?>">	
	<link href="<?= base_url('assets/css/bootstrap-custom.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/css/uikit.css" rel="stylesheet" type="text/css')?>"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/custom.css')?>">
	<link href="<?= base_url('assets/css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)')?>" />
<!-- FINAL DOS NOVOS ESTILOS -->

      <script src="https://code.jquery.com/jquery-3.3.1.min.js"integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>

	  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,500,600,700" rel="stylesheet">
	  
	  <script>const base_url = "<?=base_url()?>";</script>


  
	<style>
	html,body,h1,h2,h3,h4,h5,h6,a,li,div,ul{font-family: 'Open Sans', sans-serif !important;}
		img{
			max-width:100%;
			height:auto;
		}
		
		a:link{
		text-decoration:none;
		color: #495057;
		}

		a:visited{
		text-decoration:none;
		color: #495057;
		}

		a:hover{
		text-decoration:none;
		color: #FF6A00;
		}
	
		#lnk-ativo{
		text-decoration:none;
		color: #FFFFFF;
		font-size: 16px;
		text-align: left;	
		}

		h5.title{
			text-align: center;
		}

        ul#as-b-categories li:hover {
            background-color: #F5F5F5;
        }
        ul#as-b-categories a:hover {
            color: #000;
        }
        #as-b-categories .fa{ font-size: 30px;}
        .h-main-menu .fa{ font-size: 20px;}			
	</style>

		<script type="text/javascript">
		    function mudaPagina(url) {
		        window.location = '<?php echo base_url(); ?>' + url;
		    }

		    function loadingPaginacao(pagina, janela) {
		        $("#carregandoJanela_" + janela).show();
		        setTimeout(function() {
		            $("#janela_" + janela).load(pagina);
		            $("#carregandoJanela_" + janela).hide();

		            $('html, body').scrollTop(0);
		        }, 1000);
		    }


		    function filtroAnuncio(url) {
		        window.location = url;
		    }

		    function filtrarPor(pagina, janela) {
		        $("#carregandoJanela_" + janela).show();

		        setTimeout(function() {
		            $("#janela_" + janela).load(pagina);
		            $("#carregandoJanela_" + janela).hide();

		            $('html, body').scrollTop(0);
		        }, 1000);
		    }
		</script>

       <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '317733798566075',
                    xfbml: true,
                    version: 'v2.6'
                });
            };

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/pt_BR/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <div id="black-wall"></div>		
	
	</head>
<body>
	
<?php if($this->session->flashdata('danger')){?>
    <div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<?php echo $this->session->flashdata('danger');?>
    </div>
<?php }?>