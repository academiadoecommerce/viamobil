<div class="container ">
    
    
    <?php
    include_once('returns.php');

    if (@$breadcrumbs) {
        echo '
        <div class="row hide-for-small-only d-block">
            <div class="medium-12 columns my-3">
                <div class="breadcrumbs">
                   <ul>
                      <li><a href="' . base_url() . '">Início</a></li>';

                     foreach ($breadcrumbs as $key => $bc) {
                       echo '<li>' . ((@$bc['link']) ? '<a href="' . $bc['link'] . '" target="_self">' . $bc['name'] . '</a>' : $bc['name']) . '</li>';
                    }
              echo '
                   </ul>
                </div>
            </div>
        </div>'; 
    } ?>
</div>


<div class="container d-flex  rounded border-0 p-0 pt-2">
    <div class="row m-0 d-flex w-100">

	    <div class="col-md-2 p-0">
	        <?php include_once('shops_sidebar.php'); ?>

	        <div class="hide-for-small-only">
			<?=$this->main_model->advertisingBox('side', '266px', '600px')?>
			</div>
	    </div>
		
	    <div class="col-md-10">  
			<?=$this->main_model->advertisingBox('top', '100%', '90px')?>
			
			<?php
				echo $this->shops_model->shop_page($shop);

				$ads = $this->shops_model->ads($shop->use_id, paginacao()->getQtd(), paginacao()->getInicio());

				if ($ads) {
                        echo '<div class="col-md-12">';
                        echo '<ul class="row no-gutters">';
                        foreach ($ads as $key => $ad) { 
                        	$images = $this->ads_model->images($ad->ad_id);
        					$image = thumbnail(@$images[0]->ads_img_file, "ads", 235, 150, 2);
                        	?>
                           <li class="col-sm-6 col-md-4 col-lg-3 bgwhite-child">
                            
                                <div class="al-item" style="padding-bottom: 15px;">

                                    <a class="itembox hover-grey" target="_blank" href="<?= base_url("anuncio/{$ad->ad_slug}") ?>" title="<?= $ad->ad_name ?>">
                                        
                                        <div class="card-body border-grey rounded m-2 p-0 hover-shadow" style="text-align: left; height: 253px;">  
                                            <img class="" alt="<?= $ad->ad_name ?> " src="<?= $image ?>">

                                            <hr class="mt-0 mb-1">

                                            <div class="row mx-2"> 
                                                <span class="ad-price h5"> 
                                                    <?=dinheiro($ad->ad_price, 2, true)?>
                                                </span> 
                                            </div>

                                            <div class="row mx-2"> 
                                                <font size="4" style="height: 40px;">
                                                    <?=$ad->ad_name?>
                                                </font>
                                            </div>

                                            </a>

                                        <br>

                                        <div class="row mx-2" style="position: absolute; bottom: 35px;width: 87%;"> 
                                            <a href="<?= base_url("anuncio/{$ad->ad_slug}") ?>" class="btn btn-primary w-100"> 
                                                <i class="fa fa-cart-plus"></i> 
                                                Ver Detalhes 
                                            </a> 
                                        </div>
                                    </div>  
                                </div>      
                                    
                            </li>

                        <?php }
                        echo '</ul>';                       
                        echo '<div class="pagination-box"></div>';
                        echo '</div>';
                    }
			?>
		</div>
		</div>
	</div>
</div>
</div>
</div>

<script>
    $(".shophover").hover(function() {
    $(this).find('h3').css("text-decoration", "underline");

}, function() {
    $(this).find('h3').css("text-decoration", "none");
});

</script>
<script type="text/javascript">
	function pagination(){
		var perPage = 10;
		var paginationBox = $('.pagination-box');
		var totalRecords = $('#ads-listing').children('.al-page').length;

		paginationBox.paginate({
			count: totalRecords,
			start: 1,
			display: 5,
			onChange: function(page){
				$("#ads-listing .al-page").removeClass('active');
				$("#ads-listing .al-page[data-page='"+page+"']").addClass('active');
			}
		});
	}

	$(window).load(function() {
		//pagination();
	});
</script>

<?php require_once 'modal/ads_email_share.php';  ?>