<!-- MODEL DO LOGIN  -->
    <div class="modal fade " id="modal-frete-combinar" tabindex="-1" role="dialog">
        <div class="modal-dialog login1"  style="margin-top: 130px;">
            <div class="modal-content" style="padding: 15px;">
                <div class="user-box">
                    <div class="page-header" style="margin: 0 0 20px;">
                        <center><h3>Formas de entrega</h3></center>
                        <br>
                         <h6>Entrega a combinar com o vendedor</h6>
                    </div>
					      <div class="modal-body">

					      		<?php foreach($frete as $fretes){ ?>
					      		<article class="box getfretevalue" data-dismiss="modal" style="cursor: pointer;">
									<figure class="itemside" style="border: none;">
										<figcaption class="text-wrap">
										<h5 class="title"><i class="fas fa-plus"></i>&nbsp&nbsp&nbsp<?php echo $fretes["frete_local"]?></h5>
										<div id="item">
										<input type="hidden" readonly="" name="valor" value="<?php echo $fretes["frete_valor"]?>">
										<span class="float-right pull-right frete-preco" style="position: absolute;right: 7px;top: 50%;transform: translateY(-50%);font-size: 20px;"><?php echo dinheiro($fretes["frete_valor"])?></span>
										</div>
										</figcaption>
									</figure>
								</article>
							<?php }?>
					      </div>
					<div class="modal-frete-result d-none">

					</div>
                </div>
            </div>
        </div>
    </div>



<script>

$(document).ready(function(){
	// code to read selected table row cell data (values).
	$(".getfretevalue").on('click',function(){
		let valorPost = $(this).find('input').val();
		let valorView = $(this).find('span').text();
		var local = $(this).find('h5').text();

		$("#meufrete").text(local+" "+valorView);
		$("#fretePost").val(valorPost);
	});
});
</script>

<style type="text/css">

.box {
    padding: 5px 0.2rem;
    display: block;
    background: #fff;
    border-radius: 3px;
    box-shadow: 0 1px 3px rgba(51,51,51,0.1);
    margin-top: 20px;
}

.modal-body {
    position: relative;
    flex: 1 1 auto;
    padding: 0rem; 
}

</style>