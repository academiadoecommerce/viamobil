<!-- modal compra atacado -->
    <div class="modal fade" id="modal-wirecard" tabindex="-1" role="dialog">
        <div class="modal-dialog login1"  style="margin-top: 130px;">
            <div class="modal-content">
                <div class="user-box text-center">
                    <div class="page-header" style="margin: 0 0 20px;">
                        <h3>Compra em atacado</h3> 
						<p><i class="fas fa-truck green"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O frete deverá ser negociado com o vendedor após a compra.</p>
						<p><i class="card-icon boleto-icon" style=" height: 15px; "></i>Pagamentos para atacado apenas via boletos.</p>
                    </div>
                    <!-- <h5>Compre acima de {unidade} </h5> -->
					<div class="modal-frete-form">
						<div class="form-group row">
						  <div class="col-12">
                            <font size="4">
                                <label for="quantidade"><font size="3">Quantidade:</font></label>
                              
                            </font>
							<!-- <input class="form-control"> -->
						  </div>
						</div>
						<div class="form-group row">
						  <div class="col-12">
						  	<button class="form-control" id="help-wirecard">
                              teste
                            </button>
						  </div>
						</div>
					</div>
					<div class="modal-frete-result d-none">

						<div class="row">
							<div class="col-md-12">
								<article class="box" data-dismiss="modal" id="getfretevalue" style="cursor: pointer;">
									<figure class="itemside" style="border: none;">
										<div class="aside align-self-center">
											<span class="icon-wrap icon-md round bg-success">
												<i class="fa fa-truck white"></i>
											</span>
										</div>
									</figure>
								</article>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>