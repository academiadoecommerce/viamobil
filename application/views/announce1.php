
<div class="container page-add-ads">

    <h1 style="position: relative;top: 25px" class="text-center">Criar anuncio</h1>

    <form action="Announce/anunciar_prox" method="post">
        <div class="row pt-5 ads-step-start">

            <!-- categorias primarias -->
            <div class="col-12 col-md-3">
                <div class="form-group border rounded p-2 col-add shadow-sm bgwhite">
                    <ul id="categ">
                        <? foreach ($categories as $cat) { ?>
                            <li class=""  id="<?=$cat->ads_cat_id?>" ><?=$cat->ads_cat_name?></li>
                        <? } ?>
                        
                        <input type="text" name="categ" hidden>
                    </ul>
                  </div>                
            </div>

            <!-- categorias secondarias -->
            <div class="col-12 col-md-3 d-none colsub">
                <div class="form-group border rounded p-2 col-add shadow-sm bgwhite">
                    <ul id="subcateg">
                        <? foreach($categoriafilho as $filho){ ?>
                            <li class="d-none" data-parent="<?=$filho->ads_cat_parent?>"  id="<?=$filho->ads_cat_id?>" ><?=$filho->ads_cat_name?></li>
                        <? } ?>
                    
                        <input type="text" name="subcateg" hidden>
                    </ul>
                  </div>                
            </div>

            <!-- categorias terciarias -->
            <div class="col-12 col-md-3 tertiary d-none">
                <div class="form-group border rounded p-2 col-add shadow-sm tertiary bgwhite">
                    <ul id="tertiary">
                        <? foreach($categ_tertiary as $tertiary){ ?>
                            <li class="d-none" data-parent="<?=$tertiary->ads_cat_parent?>"  id="<?=$tertiary->ads_cat_id?>" ><?=$tertiary->ads_cat_name?></li>
                        <? } ?>
                    
                        <input type="text" name="tertiary" hidden>
                    </ul>
                  </div>                
            </div>

            <div class="col-12 col-md-3 d-none colpronto">
                <div class="form-group border rounded p-2 col-add shadow-sm bgwhite">
                    <center>
                        <label for="ads_cat_id">
                            <font size="3">Crie um titulo para o seu anúncio.</font>
                        </label>
                    </center>
                    
                    <center class="px-2">
                        <input class="form-control" name="ads_cat_name" placeholder="Exemplo: camisa Lacoste  manga comprida" required="required">
                    </center>

                   <center> 
                        <span class="helper">
                           <font size="2">* Requerido, 6 caracteres ou mais</font>
                       </span>
                   </center>

                    <center class="mt-4">
                        <div style="height: 200px"><img width="100" style="display: none;" class="m-3" alt="pronto" src="<?= base_url( 'assets/img/vue/ready-0.png' ) ?>"> 
                        <h3 class="m-2" style="display: none;">Pronto!</h3> 
                        </div>
                        <button id="continuar" disabled type="submit" class="btn btn-primary w-75 green"> Continuar </button>
                    </center>
                </div>                
            </div>
        </div>
    </form>
</div>

<!-- ========================= SECTION ANUNCIOS POPULARES   // ========================= -->
<? require 'anuncios_populares_view.php'; ?>

<!-- ========================= SECTION LOJAS POPULARES      // ========================= -->
<? require 'lojas_populares_view.php'; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('ul#categ li').click(function(){
           var categ_id = $(this).attr('id');
            $('#categ li').removeClass('selected');
            $(this).addClass('selected');
            $("ul#subcateg li").addClass('d-none') ;
            $("div.tertiary").addClass('d-none') ;
            var cat = $("ul#subcateg").find("[data-parent='"+categ_id+"']").fadeIn(800).removeClass('d-none') ;
            if(cat.length > 0) {
                $('div.colpronto').addClass('d-none');
                $('div.colsub').fadeIn(800).removeClass('d-none');
            }else{
                $('div.colsub').fadeIn(800).addClass('d-none');
                $('div.colpronto').fadeIn(800).removeClass('d-none');
            }
           // adicionar este id em imput hidden
            $('ul#categ input').attr('value', categ_id);
        });

        $('#subcateg li').click(function(){
            var subcateg_id = $(this).attr('id');
            $('#subcateg li').removeClass('selected');
            $(this).addClass('selected');
            $("ul#tertiary li").addClass('d-none');

            // verifica se esta sub possui subs
            var tertiary = $("ul#tertiary").find("[data-parent='"+subcateg_id+"']").fadeIn(800).removeClass('d-none') ;

            if (tertiary.length > 0) {
                $('div.colpronto').addClass('d-none');
                $('div.tertiary').fadeIn(800).removeClass('d-none');
            }else{
                $('div.tertiary').fadeOut(800).addClass('d-none');
                $('div.colpronto').fadeIn(800).removeClass('d-none');
            }

           // adicionar este id em imput hidden
            $('ul#categ input').attr('value', subcateg_id);
        });

        $('ul#tertiary li').click(function(){
           var categ_id = $(this).attr('id');
            $('#tertiary li').removeClass('selected');
            $(this).addClass('selected');
            $('div.colpronto').fadeIn(800).removeClass('d-none');
            $('.ads-step-start').animate({scrollLeft:'+=500'},500);

           // adicionar este id em imput hidden
            $('ul#categ input').attr('value', categ_id);
        });

        $('[name = ads_cat_name]').keyup(function(){
            if(this.value.length > 5) {
                $('[alt = pronto]').fadeIn(800);
                $('.colpronto h3').fadeIn(800);
                $('#continuar').removeAttr('disabled');
            } else if(this.value.length < 6) {
                $('[alt = pronto]').fadeOut(800);
                $('.colpronto h3').fadeOut(800);
                $('#continuar').attr('disabled', 'true');
            }
        })
    });
</script>