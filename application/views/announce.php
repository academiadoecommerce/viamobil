<script src="<?= base_url('assets/js/jquery.maskMoney.js') ?>"></script>
<?php if ($this->session->flashdata('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php } ?>

<!-- <script src="<?= base_url('assets/js/vendor/dropzone.js') ?>"></script> -->
<div class="container px-0-mobile" style="position: relative;top: 23px;">
    <div class="row d-block">
        <div class="medium-12 columns mb-4">
            <div class="announce-insert" id="announce-insert">
                <div class="row">
                    <div class="medium-12 large-12 columns">
                        <div class="announce__header">
                            <span>É hora de vender :)</span>
                            <p>Preencha tudo certinho pra atrair mais visitantes</p>
                        </div>

                        <center>
                            <h1>Dados do Anúncio</h1>
                            <p class="announce_infor">As informações marcadas com asterisco (*) são obrigatórias</p>
                        </center>

                        <div class="container p-5">
                            <div class="row px-5">

                                <form enctype="multipart/form-data" method="POST" accept-charset="utf-8"
                                      action="<?= base_url('announce/insert') ?>" id="ai-form"
                                      class="form form-simple ai-form dropzone w-100">
                                    <input type="hidden" name="hash" id="ai-hash" value="<?= $hash ?>">
                                    <input type="hidden" name="img_order" id="img_order" value="">

                                    <!-- Categories -->
                                    <input type="hidden" name="category" value="<?php echo $id_categoria; ?>">

                                    <div class="col-12">

                                        <!-- titulo -->
                                        <div class="row">
                                            <label>
                                                Título:
                                                <span class="required">*</span>
                                            </label>
                                            <input type="text"
                                                   value="<?php if (isset($nome_categoria)) {
                                                       echo $nome_categoria;
                                                   } ?>"
                                                   required name="title" id="ai-title" placeholder="Título do Anúncio"
                                                   maxlength="70">
                                        </div>

                                        <!-- descricap varejo -->
                                        <div class="row">
                                            <label>
                                                Descrição:
                                                <span class="required">*</span>
                                            </label>
                                            <textarea style="height: 120px" required name="desc" id="ai-desc" rows="6"
                                                      placeholder="Descrição do anúncio para varejo"></textarea>
                                        </div>

                                        <br>

                                        
                                        <!-- preços -->
                                        <div class="row pt-3">

                                            <!-- preco varejo -->
                                            <div class="col-12 col-md-3">
                                                <label>
                                                    Preço:
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">R$</span>
                                                    </div>
                                                    <input type="text" required class="form-control input-money"
                                                           name="price" id="price1" placeholder="00,00"
                                                           data-original-title="Informe um valor em R$"
                                                           data-toggle="tooltip" data-placement="top" title="">
                                                </div>
                                            </div>

                                            <!-- quantidade maxima -->
                                            <div class="col-12 col-md-3">
                                                <label>
                                                    Quantidade:
                                                    <span class="required">*</span>
                                                </label>

                                                <div class="input-group mb-3">
                                                    <input style="background-color: #F5FFFA;" type="number" required
                                                           class="form-control" name="qtdmax"
                                                           id="qtdmax" min="1" placeholder="1">
                                                </div>
                                            </div>

                                            <!-- altura -->
                                            <div class="col-12 col-md-3">
                                                <label>
                                                    Altura (cm):
                                                    <span class="required">*</span>
                                                </label>

                                                <div class="input-group mb-3">
                                                    <input type="number" required class="form-control" name="altura"
                                                           id="altura" min="2" max="105" placeholder="105">
                                                </div>
                                            </div>

                                        </div>


                                        <div class="row pt-3">


                                            <!-- largura -->
                                            <div class="col-12 col-md-3">
                                                <label>
                                                    Largura (cm):
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="input-group mb-3">
                                                    <input type="number" required class="form-control" name="largura"
                                                           id="largura" min="11" max="105" placeholder="105">
                                                </div>
                                            </div>

                                            <!-- comprimento -->
                                            <div class="col-12 col-md-3">
                                                <label>
                                                    Comprimento (cm):
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="input-group mb-3">
                                                    <input type="number" required class="form-control"
                                                           name="comprimento" id="comprimento" min="16" max="160"
                                                           placeholder="160">
                                                </div>
                                            </div>

                                            <!-- peso -->
                                            <div class="col-12 col-md-3">
                                                <label>
                                                    Peso (kg):
                                                    <span class="required">*</span>
                                                </label>
                                                <div class="input-group mb-3">

                                                    <input type="text" required class="form-control" name="peso"
                                                           id="peso" placeholder="1,58"><!--data-mask="00,00"-->
                                                </div>
                                            </div>


                                        </div>

                                        <!-- variacoes -->
                                        <div class="row pt-2">
                                            <div class="col-12">
                                                <label>
                                                    Variações:
                                                    <button class="btn btn-info btn-sm" id="add_variacao"><i
                                                                class="fas fa-plus"></i> Adicionar
                                                    </button>
                                                </label>
                                                <div class="card m-0 p-2" id="variacoes" style="display: none;">

                                                </div>

                                            </div>
                                        </div>
                                        <style>
                                            <?php foreach($variacoes_cores as $cor): ?>
                                            .icon_option_<?=$cor->variacoes_nome?>{
                                                color: <?=$cor->variacoes_hexadecimal?>;
                                            }
                                            <?php endforeach; ?>
                                            .bootstrap-select {width:100% !important;}
                                            .bootstrap-select .text{float: unset}
                                        </style>


                                        <script>
                                            let item_variacao = `<div class="row">
                                                        <div class="col-2">
                                                            <label for="">Cor:</label><br>
                                                            <select class="selectpicker" data-icon-base="fontawesome" data-style="btn-default btn-block w-100" name="variacoes[cor][]" required >
                                                              <?php foreach($variacoes_cores as $cor):?>
                                                              <option data-icon="fas fa-circle icon_option_<?=$cor->variacoes_nome?>"><?=$cor->variacoes_nome?></option>
                                                              <?php endforeach; ?>
                                                            </select>
                                                        </div>

                                                        <div class="col-2">
                                                            <label>Tamanho:</label>
                                                            <input type="text" required class="form-control "
                                                                   name="variacoes[tamanho][]" placeholder="Tamanho">
                                                        </div>
                                                        <div class="col-4">
                                                            <label>Imagem:</label>
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input imgvariacaoinput"
                                                                       id="imgvariacao" name="variacoes[][img]" >
                                                                <label class="custom-file-label" for="imgvariacao">Selecione</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-3">
                                                            <label></label>
                                                            <div class="custom-file">
                                                                  <img id="myimg" class="imgvariacaoadicionada" width="50px" height="50px" style="display: none;">
                                                            </div>
                                                        </div>

                                                    </div>`;

                                            var valor = 1;
                                            $('#add_variacao').click(function (e) {
                                                 
                                                if(valor <= 12){
                                                    valor = valor + 1
                                                    e.preventDefault();
                                                    $('#variacoes').fadeIn().append(item_variacao);
                                                    $('.selectpicker').selectpicker();
                                                }else{
                                                    alert("As variações são limitadas a 12 unidades")
                                                }


                                            });

                                            $(document).on('change', '.imgvariacaoinput', function(){

                                                var input = this;

                                                let fileName = $(input).val().split('\\').pop();

                                                var nomeFile = fileName.substr(0,20)
                                                
                                                if(fileName.length > 20){
                                                    nomeFile += '...'
                                                    nomeFile += fileName.substring(fileName.length - 3)
                                                }

                                                $(input).next('.custom-file-label').addClass("selected").html(nomeFile);

                                                if (input.files && input.files[0]) {
                                                    var reader = new FileReader();

                                                    reader.onload = function(e) {
                                                        var imgv = $(input).parent().parent().parent().find('img.imgvariacaoadicionada');
                                                        imgv.attr('src', e.target.result);
                                                        document.getElementById("myimg").style.display = "inline";
                                                        document.getElementById("myimg").style.border = "1px solid black";
                                                    };
                                                    reader.readAsDataURL(input.files[0]);
                                                }
                                            });
                                        </script>


                                        <div class="row d-block pt-2">

                                            <div class="col-12">
                                                <label >
                                                    Frete a combinar?
                                                    <span data-toggle="tooltip" data-placement="top" title="Ao selecionar SIM, será possível adicionar fretes por meio do seguinte caminho Minha Conta -> Meus Anúncios -> Frete" style="padding: 0 6px; margin-top: 0" class="btn btn-help hidden-xs">
                                                            <i style="font-size: 12px" class="fa fa-question"></i>
                                                        </span>
                                                </label>
                                                <br>

                                                <!-- Default unchecked -->
                                                <div class="custom-control custom-radio d-inline">
                                                    <input type="radio" value="sim" onclick="check(this.value)" class="custom-control-input"
                                                           id="defaultChecked" name="defaultExampleRadios">
                                                    <label class="custom-control-label" for="defaultChecked">Sim</label>
                                                </div>

                                                <!-- Default checked -->
                                                <div class="custom-control custom-radio d-inline">
                                                    <input type="radio" value="nao" onclick="check(this.value)" class="custom-control-input"
                                                           id="defaultUnchecked" name="defaultExampleRadios" checked>
                                                    <label class="custom-control-label"
                                                           for="defaultUnchecked">Não</label>
                                                </div>
                                            </div>
                                            <br>
                                            <button class="btn btn-info btn-sm" id="add_frete" style="display: none;">
                                                <i class="fas fa-plus">
                                                    Adicionar
                                                </i> 
                                            </button>
                                            <br>
                                        <div class="row" id="freteCombinar" style="display: none;">

                                        </div>

                                        </div>


                                        <!-- imagens upload -->
                                        <div class="row d-block">
                                            <label>
                                                Imagens:
                                                <span class="required">*</span>
                                            </label>
                                            <div style="clear: both;"></div>

                                            <button type="button" class="btn btn-m-bottom-medium"
                                                    id="images-upload-button">
                                                <i class="fa fa-picture-o"></i> Adicionar <br>Imagens
                                            </button>

                                            <div style="display: contents;" class="dropzone" id="images-upload"></div>

                                            <div id="image-preview-template" style="display: none">
                                                <div class="dz-preview dz-file-preview">
                                                    <div class="dz-remove-file" style="display: none"><span
                                                                style="padding: 0px 0px 0px 0px;" title="Apagar Imagem"
                                                                data-dz-remove><i class="fa fa-fw fa-trash"></i></span>
                                                    </div>
                                                    <div class="dz-image">
                                                        <img class="gimg" data-dz-thumbnail/>
                                                    </div>
                                                    <div class="dz-progress"><span class="dz-upload"
                                                                                   data-dz-uploadprogress></span></div>
                                                    <div class="dz-success-mark"><span><i
                                                                    class="fa fa-fw fa-check"></i></span></div>
                                                    <div class="dz-error-mark"><span><i
                                                                    class="fa fa-fw fa-close"></i></span></div>
                                                    <div class="dz-error-message"><span data-dz-errormessage></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                            <div class="alert alert-subtitle hide-for-small-only desbug-alert"
                                                 id="msgImg" style="display: none;">
                                                <ul>
                                                    <li><span style="font-size: 15px;">Atenção:</span> <span
                                                                style="color: red; font-size: 14px;"><strong>Altura
                                                                e largura mínima:</strong> 350x260px</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="alert alert-subtitle hide-for-small-only desbug-alert">
                                                <ul>
                                                    <li><strong>Tamanho máximo:</strong> 2MB</li>
                                                    <li><strong>Altura e largura mínima:</strong> 350x260px
                                                    </li>
                                                    <li><strong>Máximo de imagens permitidas:</strong> 6</li>
                                                    <li><strong>Tipos arquivo permitido:</strong> JPG, JPEG e
                                                        PNG
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <!-- video -->
                                        <div class="row">
                                            <label>Vídeo:</label>
                                            <input type="text" name="video" placeholder="Link Vídeo do Produto"
                                                   data-original-title="Só aceitamos videos do Youtube, caso coloque de outro site, não garantimos o funcionamento."
                                                   data-toggle="tooltip" data-placement="top" title="">
                                        </div>

                                    </div>

                            </div>

                            <div class="row mt-3 text-center">
                                <div class="col-12">
                                    <div class="checkbox-custom">
                                        <input type="checkbox" required id="ai-terms">
                                        <label for="ai-terms">Concordo com os <a
                                                    href="<?= base_url('ajuda/termos-de-uso') ?>" target="_blank">Termos
                                                do
                                                <?php echo NOME_SITE ?></a>.</label>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="small-12 columns" align="center">
                                    <div class="ai-f-actions">
                                        <!-- <button type="button" id="preview-button" class="btn btn-secondary hide-for-small-only"><i class="fa fa-eye"></i> Pré-Visualizar</button> -->
                                        <?php if (!isset($form_disabled)): ?>
                                            <button type="submit" id="publish-button" disabled="disabled"
                                                    class="btn btn-primary"><i class="fa fa-reply-all"></i>
                                                Publicar
                                            </button>
                                        <?php else: ?>
                                            <button type="button" class="btn btn-default"
                                                    onclick="modal('<?= $modal_alert ?>')"><i
                                                        class="fa fa-reply-all"></i>
                                                Publicar
                                            </button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
</div>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

<style type="text/css">
.ImageBorder
{
    border-width: 1px;
    border-color: Black;
}
</style>

<script>

     /*let item_frete = `<div class="row">

                            <div class="col-4">
                                <label>Descrição:</label>
                                <input type="text" required class="form-control "
                                       name="fretes[descricao][]" placeholder="Descrição">
                            </div>

                            <div class="col-2">
                                <label>Valor:</label>
                                <input type="text" required class="form-control "
                                       name="fretes[valor][]" placeholder="Valor">
                            </div>

                        </div>`;


    $('#add_frete').click(function (e) {
                                                 
        if(valor <= 12){
            valor = valor + 1
            e.preventDefault();
            $('#freteCombinar').fadeIn().append(item_frete);
        }else{
            alert("As variações são limitadas a 12 unidades")
        }


    });

    function check(browser) {
        if(browser == "sim"){
            document.getElementById("freteCombinar").style.display = "inline";
            document.getElementById("add_frete").style.display = "inline";

        }else{
            document.getElementById("freteCombinar").style.display = "none";
            document.getElementById("add_frete").style.display = "none";
        }
    }*/

    function desabilitar1(valor) {
        var status = document.getElementById('price1').disabled;

        if (valor == 'sim' && !status) {
            document.getElementById('price1').disabled = true;
        } else {
            document.getElementById('price1').disabled = false;
        }
    }

    function desabilitar2(valor) {
        var status = document.getElementById('price2').disabled;

        if (valor == 'sim' && !status) {
            document.getElementById('price2').disabled = true;
        } else {
            document.getElementById('price2').disabled = false;
        }
    }


    function desabilitar3(valor) {
        var status = document.getElementById('price3').disabled;

        if (valor == 'sim' && !status) {
            document.getElementById('price3').disabled = true;
        } else {
            document.getElementById('price3').disabled = false;
        }
    }

    function desabilitar4(valor) {
        var status = document.getElementById('qtdmin').disabled;

        if (valor == 'sim' && !status) {
            document.getElementById('qtdmin').disabled = true;
        } else {
            document.getElementById('qtdmin').disabled = false;
        }
    }


    $(function () {
        $("#ai-adote").click(function () {

            if ($('#ai-yes-adote').is(':checked')) {
                $('#ai-price').prop('disabled', false);
                $('#ai-yes-adote').prop('checked', false);
                $('#not_preco').show();
                $('#not_desktop_preco').show();
                $('#service').show();
                $('#trade').show();
                $('#bloco').addClass('small-12 medium-6 large-6 columns');
                $('#bloco').removeAttr('style');
            } else {
                $('#bloco').removeClass('small-12 medium-6 large-6 columns');
                $('#bloco').css({
                    "margin-left": "135px",
                    "padding-bottom": "60px"
                });
                $('#service').hide();
                $('#trade').hide();
                $('#not_preco').hide();
                $('#not_desktop_preco').hide();
                $('#ai-price').prop('disabled', true);
                $('#ai-yes-adote').prop('checked', true);
            }
        });
    });

    $("#ai-f-c-parent").on('click', 'li', function (event) {
        var btn = $(this);
        var cat = btn.attr('data-id');
        event.preventDefault();
        if (cat == 1) {
            $('#msg_titulo').show();
            $('#adote').show();

        } else {
            $('#msg_titulo').hide();
            $('#adote').hide();
        }
        if (cat == 114) {
            $('#ai-price').prop('disabled', true);
            $('#desativa').hide();

        } else {
            $('#ai-price').prop('disabled', false);
            $('#desativa').show();
        }

    });
</script>
<script src="<?= base_url('assets/js/vendor/dropzone.js') ?>"></script>
<script src="<?= base_url('assets/js/custom/announce.js') ?>"></script>
<!-- <?php if ($modal_alert): ?>
<script>
    $(document).ready(function() {
        modal('<?= $modal_alert ?>');
    });
</script>
<?php endif; ?> -->
<script type="text/javascript">
    var hash = $("#ai-hash").val();
    var previewTemplate = document.getElementById("image-preview-template").innerHTML;

    var myDropzone = new Dropzone("div#images-upload", {
        url: base_url + "/announce/images_upload/" + hash,
        method: 'POST',
        maxFiles: 6,
        maxFilesize: 2,
        acceptedFiles: "image/jpeg,image/png,image",
        clickable: "#images-upload-button",
        previewTemplate: previewTemplate
    });

    myDropzone.on('addedfile', function (event) {
        $(".dz-message").remove();
    });

    myDropzone.on("success", function (file) {
        var box = file.previewElement;
        var response = file.xhr.response;
        $('#msgImg').hide();
        if (response == 'error') {
            myDropzone.removeFile(file);
            $('#msgImg').show();
            setTimeout(function () {
                $('#msgImg').hide();
            }, 5000);
        } else {
            $('#publish-button').prop('disabled', false);
            box.id = "i" + response;
            setTimeout(function () {
                $("#i" + response).children('.dz-remove-file').show();
            }, 500);
        }

        //console.log(response);


    });

    myDropzone.on("complete", function (file) {
        var status = file.status;

        if (status == "error") {
            var timer = (Math.floor((Math.random() * 10) + 1)) * 500;

            setTimeout(function () {
                myDropzone.removeFile(file);
            }, timer);
        }
    });

    myDropzone.on('removedfile', function (file) {

        if (file.status == "success") {
            var response = file.xhr.response;

            $.ajax({
                url: base_url + '/announce/images_remove',
                type: 'POST',
                data: {
                    image: response
                },
            }).done(function () {
                if ($('.dz-image').length == 1) {
                    $('#publish-button').prop('disabled', true);
                }
                console.clear();
            }).fail(function () {
                console.log("image delete: error");
            });

        }
    });
</script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(document).ready(function () {

        $(document).on('mousedown', '.dz-preview', function () {
            id = $(this).attr('id')
            id = id.replace('-', '')
            id = id.replace('i', '')
            $(this).attr('id', 'i-' + id)
        })

        $('#images-upload').on('mouseover', function () {
            $('.dz-started').sortable({
                update: function (event, ui) {
                    var data = $(this).sortable('serialize')

                    // POST to server using $.post or $.ajax
                    $.ajax({
                        data: data,
                        type: 'POST',
                        url: 'reorder_img'
                    })
                }
            })
        })
    })
</script>