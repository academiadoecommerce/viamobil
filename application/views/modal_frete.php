<!-- MODEL DO LOGIN  -->
    <div class="modal fade" id="modal-frete" tabindex="-1" role="dialog">
        <div class="modal-dialog login1"  style="margin-top: 130px;">
            <div class="modal-content">
                <div class="user-box">
                    <div class="page-header" style="margin: 0 0 20px;">
                        <h3>Formas de entrega</h3>
                    </div>
                    <h5>Você poderá ver custos e prazos de entrega precisos em tudo que procurar.</h5>
					<div class="modal-frete-form">
						<div class="form-group row">
						  <div class="col-12">
							<input class="form-control" type="text" required name="cep" id="input-cep" placeholder="00000-000" data-mask="99999-999">
						  </div>
						</div>
						<div class="form-group row">
						  <div class="col-12">
						  	<button class="form-control" id="btn_calcular">Calcular</button>
						  </div>
						</div>
					</div>
					<div class="modal-frete-result d-none">

						<div class="row">
							<div class="col-md-12">
								<article class="box" data-dismiss="modal" id="getfretevalue" style="cursor: pointer;">
									<figure class="itemside" style="border: none;">
										<div class="aside align-self-center">
											<span class="icon-wrap icon-md round bg-success">
												<i class="fa fa-truck white"></i>
											</span>
										</div>
										<figcaption class="text-wrap">
										<h5 class="title">Entrega no seu endereço</h5>
										<p class="text-muted">Chegará entre os dias </p> <center><span class="frete-prazo"></span></center>
										<span class="float-right pull-right frete-preco" style="position: absolute;right: 7px;top: 50%;transform: translateY(-50%);font-size: 22px;">R$ 20</span>
										</figcaption>
									</figure>
								</article>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    	$(document).ready(function(){
    		$('#btn_calcular').on('click', function(event){
    			event.preventDefault();
		    	var btntxt = $(this).text();
		    	var btn_calcular = $(this);
		    	$(btn_calcular).text("Aguarde...");
		    	$(btn_calcular).attr('disabled', true);
		        var base_url = '<?=base_url()?>';
		        var cep_destino = $("#input-cep").val();
		        var cep_origem = '<?php echo $ad->use_cep ?>';
		        var comprimento = '<?php echo $ad->ad_comprimento ?>';
		        var largura = '<?php echo $ad->ad_largura ?>';
		        var altura = '<?php echo $ad->ad_altura ?>';
		        var diametro = '<?php echo $ad->ad_diametro ?>';
		        var peso = '<?php echo $ad->ad_peso ?>';
		        var quantidade = document.getElementById("quantidade");
			    qtd = quantidade.value;
		        $.post(base_url+'/Correios/frete',{cep_destino, comprimento, altura, largura, diametro, peso, cep_origem, qtd},
	        	function(data){
	        		//alert("apos o DATA")
	        		$(btn_calcular).text(btntxt);
	        		$('.modal-frete-form').fadeOut()
	        		$(btn_calcular).attr('disabled', false)
	        		var info = $.parseJSON(data)
	        		let result = $('.modal-frete-result')
	        		$(result).fadeIn().removeClass('d-none')
	        		$('.frete-prazo').text(info.prazo)
	        		$('.frete-preco').text('R$ '+info.valor)
					var frete_val = info.preco;
					$('#frete_val').val(frete_val)
					//alert(info.valor)
	        	})
	    	})

	    	$('#getfretevalue').click(function(){
	    		//alert("metodo GET")
	    		var prazo = $(this).find('.frete-prazo').text()
	    		var preco = $(this).find('.frete-preco').text()
	    		console.log([prazo, preco])
	    		$('.info-frete').text('Valor: '+preco+'. Chegará entre os dias '+prazo)
	    		$('.modal-backdrop').fadeOut()
	    	})
    	})
    </script>
