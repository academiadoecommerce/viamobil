<!-- MODEL DO LOGIN  -->
    <div class="modal fade" id="chat-required" tabindex="-1" role="dialog">
        <div class="modal-dialog login1"  style="margin-top: 130px;">
            <div class="modal-content">
                <div class="user-box">
                    <div class="page-header" style="margin: 0 0 20px;">
                        <h3>Opa, você precisa efetuar o login</h3>
                    </div>

					<div class="form-group row">
						<div class="col-lg-12">
							<p align="center">Para executar essa ação, você precisa entrar com sua conta ou se cadastrar em nosso site.</p>
							<br><br>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-6">							
							<a href="<?=base_url('registrar')?>" class="btn btn-primary btn-full myBtn" style="margin-bottom: 10px;"><i class="fa fa-user-plus" aria-hidden="true"></i>Cadastrar-se</a>
						</div>
						<div class="col-lg-6">							
							<a href="#" data-toggle="modal" data-target="#modal-login" class="btn btn-secondary btn-full myBtn"><i class="fa fa-sign-in" aria-hidden="true"></i>Entrar</a>
						</div>
					</div>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
	
<script>
    // Hide the Modal
    $(".myBtn").click(function(){
        $("#chat-required").modal("hide");
    });
</script>	


