<style>
	@media (max-width: 640px) {
		#grupo-public { display: none; }
		#grupo-cat { display: none; }
	}
	.card-body {
		margin-bottom: 15px;
		border: 1px solid #CDC9C9;
	}
</style>

<div class="card-body p-0 shadow-none border-0" id="grupo-cat">
	<aside style="width: 100%;">
		<h5 class="text-uppercase ml-3 my-2 h6">Categorias</h5>

		<?php
          $i = 0;
          $r = 8;
          $limite = $count_categories - $r; // Total das categorias PAI - 8
             //  Exibe es categorias PAI e Filho na pagina principal
             if($categories){
            echo '<ul class="menu-category second-menu" style="width: 100%;">';       
                foreach ($categories as $key => $cat) { 
                    echo '
                        <li> <a class="p-0 py-1 pl-4" href="'.base_url('shops?category='.$cat->ads_cat_id).'">'.$cat->ads_cat_name.' </a></li>';               
                    $i++;
                    if($i == $r) break;
                } ?>
		<li>
			<a href="javascript:void();" data-toggle="modal" data-target=".modal-ver-mais-categ">
				Ver mais
			</a>
		</li>
		</ul>
		<br>
		<?php } ?>
	</aside>
</div>

<!-- Espaço para Publicidades -->
<div class="" id="grupo-public">
	<div class="card-body border m-2 bgwhite">
		<aside>
			<div style="height:200px;">
				<figure class="itemside border-0" style="height: 100%;">
					<a href="<?= $img_publicidade[0]->adv_url?>">
						<img class="" src="<?= base_url('uploads/publicidade/'.$img_publicidade[0]->adv_img)?>">
					</a>
				</figure>
			</div>
		</aside>
	</div>

	<div class="card-body border m-2 bgwhite">
		<aside>
			<div style="height:200px;">
				<figure class="itemside border-0" style="height: 100%;">
					<a href="<?= $img_publicidade[1]->adv_url?>">
						<img class="" src="<?= base_url('uploads/publicidade/'.$img_publicidade[1]->adv_img)?>">
					</a>
				</figure>
			</div>
		</aside>
	</div>
</div>

<!-- =============== FIM DO ESPAÇO PARA PUBLICIDADES ======================== -->

<!-- modal todas as categorias -->
<div class="modal fade modal-ver-mais-categ" tabindex="-1" role="dialog" aria-labelledby="ModalVermaisCateg"
	aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content p-5">
			<div class="modal-header py-0 border-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<h5 class="ml-4 h6">Escolher Categorias</h5>

				<?php
                $c = 0;
                $r = 0;
                if($categories){ ?>

				<div class="row">
					<div class="col-12 col-sm-6 col-lg-4">
						<ul class="menu-category second-menu">
						<?php foreach ($categories as $key => $cat) { 
								echo '
									<li> <a class="p-0 py-1 pl-4" href="'.base_url('anuncios/?categoria='.$cat->ads_cat_id).'">'.$cat->ads_cat_name.' </a></li>';     
								$c++;
								if($c == 4){ ?>
						</ul>
					</div>
					<div class="col-12 col-sm-6 col-lg-4">
						<ul class="menu-category second-menu">
							<?php $c=0; } } ?>
						</ul>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>