    <section class="breadcrumbs row hide-for-small-only">
        <?php
        if (@$breadcrumbs) {
            echo '
            <div class="row hide-for-small-only">
                <div class="medium-12 columns">
                    <div class="breadcrumbs hide-for-small-only">
                       <ul>
                          <li><a href="' . base_url() . '">Início</a></li>';

                         foreach ($breadcrumbs as $key => $bc) {
                            if (array_filter($bc)) {
                                echo '<li>' . ((@$bc['link']) ? '<a href="' . $bc['link'] . '" target="_self">' . $bc['name'] . '</a>' : $bc['name']) . '</li>';
                            }
                        }
                        echo '
                       </ul>
                    </div>
                </div>
            </div>'; }
                ?>
    </section>