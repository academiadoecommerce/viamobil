<div class="row" id="vueApp">
        <main id="subcategories">
            <div class="columns">
                <div class="column">
                    <div class="container">
                        <h5>{{ activeCategorie.name }}</h5>
                        <img :src="'assets/img/vue/' + activeCategorie.icon + '.png'">
                    </div>
                </div>
                <div class="column level">
                    <div id="subcategoriesWrapper" class="container">
                        <ul v-for="(subcategorie, index) in subcategories" v-if="subcategorie" :key="index">
                            <li @click="subsubcategories = sub" :class="[{ __active: (subsubcategories && sub.ads_cat_id === subsubcategories.ads_cat_id) },'subcategorie']" v-for="(sub, index) in subcategorie">{{ sub.ads_cat_name }}</li>
                        </ul>
                    </div>
                </div>
                <div v-if="subsubcategories && subsubcategories.filhos" class="column level">
                    <div id="subcategoriesWrapper" class="container">
                        <ul>
                            <li @click="subsubsubcategories = sub" :class="[{ __active: (subsubsubcategories && sub.ads_cat_id === subsubsubcategories.ads_cat_id) },'subcategorie']" v-for="(sub, index) in subsubcategories.filhos">{{ sub.ads_cat_name }}</li>
                        </ul>
                    </div>
                </div>
                <div v-if="subsubsubcategories && subsubsubcategories.filhos" class="column level">
                    <div id="subcategoriesWrapper" class="container">
                        <ul>
                            <li @click="subsubsubsubcategories = sub" :class="[{ __active: (subsubsubsubcategories && sub.ads_cat_id === subsubsubsubcategories.ads_cat_id) },'subcategorie']" v-for="(sub, index) in subsubsubcategories.filhos">{{ sub.ads_cat_name }}</li>
                        </ul>
                    </div>
                </div>
                <div class="column level" v-if="lastLevel && lastLevel.filhos === null">
                    <div id="other" class="container">
                        <form action="Announce/anunciar_prox" method="post">
                            <input class="hidden" name="ads_cat_id" v-model="lastLevel.ads_cat_id">
                            <label for="ads_cat_id">Crie uma titulo para o seu anúncio.</label>
                            <input v-model="newCategorieName" name="ads_cat_name" placeholder="Exemplo: camisa Lacoste  manga comprida" required>
                            <span class="helper">
                                * Requerido, 6 caracteres ou mais
                            </span>
                            <center v-if="newCategorieName.length >= 6">
				<img width="100" src="<?php echo base_url('assets/img/vue/checl2.ico');?>" >
                                <h3>Pronto!</h3>
                                <button type="submit" class="btn btn-info btn-lg green"> Continuar </button>
                            </center>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>

<script>
var app = new Vue({
  el: '#vueApp',
  data: {
	newCategorieName: '',
	// activeCategorie: undefined,
	subcategories: undefined,
	subsubcategories: undefined,
	subsubsubcategories: undefined,
	subsubsubsubcategories: undefined,
	activeCategorie: {
		id: 4,
		name: 'Produtos e outros',
		icon: 'shopping-bag',
		map:'outros'
	}
  },
  computed: {
	subsubcategoriesChildren () {
		return this.subsubcategories ? this.subsubcategories.filhos : this.subsubcategories
	},
	subsubsubcategoriesChildren () {
		return this.subsubsubcategories ? this.subsubsubcategories.filhos : this.subsubsubcategories
	},
	subsubsubsubcategoriesChildren () {
		return this.subsubsubsubcategories ? this.subsubsubsubcategories.filhos : this.subsubsubsubcategories
	},
	lastLevel () {
		return this.subsubsubsubcategories || this.subsubsubcategories || this.subsubcategories
	},
	megedData () {
	}
  },
  beforeMount () {
	var that = this
	axios.get('https://soatacado.com/Announce/puxar_dados').then(function (response) {
		that.subcategories = response.data
	})
  },
  watch: {
	subsubcategories (newVal) {
		this.subsubsubcategories = undefined
		this.subsubsubsubcategories = undefined
	},
	subcategories (newVal) {
		this.subsubcategories = undefined
		this.subsubsubcategories = undefined
		this.subsubsubsubcategories = undefined
	},
	activeCategorie (newVal) {
		this.activeCategorie.subcategories = this.subcategories[newVal.map]
		this.subsubcategories = undefined
		this.subsubsubcategories = undefined
		this.subsubsubsubcategories = undefined
	},
	lastLevel (newVal) {
		setTimeout(function () {
			var nodelist = document.querySelectorAll('.column.level')
			if (nodelist.length) nodelist[nodelist.length - 1].scrollIntoView()
		}, 300)
	}
  },
  methods: {
	parseSubcategories (categorie) {
		return this.activeCategorie ? this.subcategories[categorie] : []
	},
	phpPath (path) {
		return "<?php base_url("+ path + "); ?>"
	}
  }
})
</script>
