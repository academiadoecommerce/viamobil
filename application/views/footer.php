<style>
    #lnk-ativo {
        text-decoration:none;
        color: #FFFFFF;
        font-size: 16px;
        text-align: left;
    } 
</style>
<!-- ========================= FOOTER ========================= -->
<footer class="section-footer m-0 p-3 mt-4">
	<div class="container">
	
			<div class="row col-lg-12" style="margin: 0 auto;" id="col-footer">			
				<div class="col-lg-2 p-0 col-logo">
					<a href="<?= base_url();?>"><img src="<?= base_url();?>/assets/img/logo_via_mobil_site.png" title="Viamobil"></a>
				</div>
				<div class="col-lg-2">
					<h5 class="title"><a id="lnk-ativo" href="<?= base_url();?>contato">Ajuda e Contato</a></h5>
				</div>		
				<div class="col-lg-2" style="font-size: 14px;">
					<h5 class="title"><a id="lnk-ativo" href="<?= base_url();?>ajuda/quem-somos">Sobre Nós</a></h5>
				</div>
				
				<div class="col-lg-2">
					<h5 class="title"><a id="lnk-ativo" href="<?= base_url();?>depoimentos">Depoimentos</a></h5>
				</div>							
	
				<div class="col-lg-2">
					<h5 class="title"><a id="lnk-ativo" href="<?= base_url();?>ajuda/termos-de-uso">Termos e Privacidade</a></h5>
				</div>								

				<?php if (!$this->session->userdata('login')){?>
				<div class="col-lg-2 bordere">
					<h5 class="title"><a id="lnk-ativo" href="#" data-toggle="modal" data-target="#modal-login">Login</a> | <a id="lnk-ativo" href="#" data-toggle="modal" data-target="#cadastro_ask_modal">Cadastre-se </a></h5>
				</div>
			<?php }?>
			</div> 
		
			<div class="row" style="margin: 0 auto; color: #FFFFFF; font-size: 13px;">
				
				<div class="col-lg-9" style="text-align: center;"> <br>
					<p>
					Rua projeta, s/n - bairro modelo, 1300, 10º andar, São Paulo - SP - Brasil | Todos os direitos reservados. Copyright 2018
					</p>
				</div>
				
				<div class="col-lg-3" style="text-align: center;">
			
						<div class="btn-group white">
							<a class="btn btn-facebook" id="lnk-ativo" title="Facebook" target="_blank" href="https://soatacado.com"><i class="fab fa-facebook-f  fa-fw"></i></a>
							<a class="btn btn-youtube" id="lnk-ativo" title="Instagram" target="_blank" href="https://soatacado.com"><i class="fab fa-instagram  fa-fw"></i></a>
							<a class="btn btn-instagram" id="lnk-ativo" title="Youtube" target="_blank" href="https://soatacado.com"><i class="fab fa-youtube  fa-fw"></i></a>
							<a class="btn btn-twitter" id="lnk-ativo" title="Twitter" target="_blank" href="https://soatacado.com"><i class="fab fa-twitter  fa-fw"></i></a>
						</div>
				
				</div>	
			</div>
		</div>			
	
</footer>
<!-- ========================= FOOTER END // ========================= -->

        <!-- modal | begin -->
	<div class="app-modal" id="app-modal">
		<div>
			<!-- modal content -->
		</div>
	</div>
	<!-- modal | end -->

	<style>
		#sidr{
			margin-top: 65px;
			border: 1px solid #ddd;
			padding: 4px;
			border-radius: 6px;
		}
	</style>

    <!-- NOVOS SCRIPTS JAVASCRIPT -->
	<script src="<?= base_url('assets/js/vendor/jquery.paginate.js')?>"></script>

	<script src="<?= base_url('assets/js/vendor/jquery.scrollUp.js') ?>"></script>
	<script src="<?= base_url('assets/js/vendor/pace.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/vendor/what-input.js') ?>"></script>
	<script src="<?= base_url('assets/js/vendor/foneMascara.js') ?>"></script>
	<script src="<?= base_url('assets/js/vendor/jquery.mask.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/vendor/jquery.sidr.min.js') ?>"></script>

	<script src="<?= base_url('assets/js/jquery.maskedinput-1.1.4.pack.js')?>" type="text/javascript"></script>
	<script src="<?= base_url('assets/js/bootstrap.bundle.min.js')?>" type="text/javascript"></script>
	<script src="<?= base_url('assets/plugins/fancybox/fancybox.min.js')?>" type="text/javascript"></script>
	<script src="<?= base_url('assets/plugins/owlcarousel/owl.carousel.min.js')?>"></script>
	<script src="<?= base_url('assets/js/script.js')?>" type="text/javascript"></script>
	<!-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script> -->
	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
	<script src="<?= base_url('assets/js/app.js')?>" type="text/javascript"></script> 
    <!-- FINAL DOS NOVOS SCRIPTS  -->

    <script type="text/javascript" src="<?= base_url('assets/js/jquery.touchSwipe.min.js') ?>"></script>
    <script>
        $(function() {
            //delizamento do touch menu
            $("#sidr").swipe({
                swipeRight: function(event, direction, distance, duration, fingerCount, fingerData) {
                    $('#black-wall').trigger('click');
                }
            });
        });

        var owl = $('.owl-carousel');
            owl.owlCarousel({
                items:1,
                loop:true,
                margin:10,
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                nav: true
            });     
    </script>

    <!-- fecha alert box após 5s -->
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function(){ 
            setTimeout(function() {
                $("#msg-box").fadeOut(800);
                $("#alert-box").fadeOut(800);
            }, 5000);
        }, false);
    </script>

    <!-- menu mobile - begin -->          
    <div id="sidr" style="right: 10px;"></div>
    <!-- menu mobile - end -->
 </div>  

<?php if($this->session->flashdata('register_modal')){ ?>
    <script type="text/javascript" src="<?=base_url('assets/js/custom/modal_register_alert.js')?>"></script>
<?php } ?>

<?php //includes modais
$this->load->view('modal/cadastro_ask_modal');
include_once ("modal_login.php");
include_once ("login_required.php"); 
// include_once ("lojista_required.php"); 
// include_once ("chat_required.php");
include_once ("modal_completar_lojista.php");
?>

<style type="text/css">
	.side-menu-wrapper { /* style menu wrapper */
	background: #fff;
	padding: 0;
	position: fixed; /* Fixed position */
	top: 0;
	right: 0; /* Sidebar initial position. "right" for right positioned menu */
	height: 100%;
	z-index: 100;
	transition: 0.5s; /* CSS transition speed */
	width: 320px;
	/*font: 20px "Courier New", Courier, monospace;*/
	box-sizing: border-box;
}
.side-menu-wrapper > ul{ /* css ul list style */
	list-style:none;
	padding:0;
	margin:0;
    overflow-y: auto; /* enable scroll for menu items */
	height:95%;
}
.side-menu-wrapper li{ /* css ul list style */
	padding: 5px 20px;
}
.side-menu-wrapper-header{
	background: url(https://shopbras.net/assets/img/menu_mobile_bg.PNG);
    background-color: #2cc17b;
}
.side-menu-wrapper-header a{
	color: white;
}
.side-menu-wrapper-header i{
	color: white;
    border: 1px solid white;
    margin: 0px;
    border-radius: 39px;
    padding: 12px;
}

.side-menu-wrapper > ul > li > a { /* links */
    display: block;
    border-bottom: 1px solid #eaeaea;
    padding: 6px 4px 6px 4px;
    color: #989898;
    transition: 0.3s;
	text-decoration: none;
}
.side-menu-wrapper > a.menu-close { /* close button */
    padding: 8px 0 4px 23px;
    color: #6B6B6B;
    display: block;
    margin: -10px 12px;
    font-size: 35px;
    text-decoration: none;
    position: absolute;
    right: 0px;
}

.menu-overlay { /* overlay */
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 99;
    top: 0;
    left: 0;
    background-color: rgba(0,0,0,.7);
    overflow-y: auto;
    overflow-x: hidden;
    text-align: center;
    opacity: 0;
    transition: opacity 1s;
}
</style>

<script type="text/javascript">
	var slide_wrp 		= ".side-menu-wrapper"; //Menu Wrapper
var open_button 	= ".menu-open"; //Menu Open Button
var close_button 	= ".menu-close"; //Menu Close Button
var overlay 		= ".menu-overlay"; //Overlay

//Initial menu position
$(slide_wrp).hide().css( {"right": -$(slide_wrp).outerWidth()+'px'}).delay(50).queue(function(){$(slide_wrp).show()}); 

$(open_button).click(function(e){ //On menu open button click
	e.preventDefault();
	$(slide_wrp).css( {"right": "0px"}); //move menu right position to 0
	setTimeout(function(){
		$(slide_wrp).addClass('active'); //add active class
	},50);
	$(overlay).css({"opacity":"1", "width":"100%"});
});

$(close_button).click(function(e){ //on menu close button click
	e.preventDefault();
	$(slide_wrp).css( {"right": -$(slide_wrp).outerWidth()+'px'}); //hide menu by setting right position 
	setTimeout(function(){
		$(slide_wrp).removeClass('active'); // remove active class
	},50);
	$(overlay).css({"opacity":"0", "width":"0"});
});

$(document).on('click', function(e) { //Hide menu when clicked outside menu area
	if (!e.target.closest(slide_wrp) && $(slide_wrp).hasClass("active")){ // check menu condition
		$(slide_wrp).css( {"right": -$(slide_wrp).outerWidth()+'px'}).removeClass('active');
		$(overlay).css({"opacity":"0", "width":"0"});
	}
});
</script>
</body>
</html>
