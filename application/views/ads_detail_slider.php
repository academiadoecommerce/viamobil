<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/engine1/style.css" />
<!-- <script type="text/javascript" src="<?=base_url()?>assets/engine1/jquery.js"></script>     -->
<script src="https://www.youtube.com/iframe_api"></script>




<style type="text/css">
    .thumb-ytb{
        width: 100% !important;
        margin: 0 !important;
    }
    .ws_controls{display: none !important;}


    .tiles {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  .tile {
    position: absolute;
    float: left;
    width: 100%;
    height: 100%;
    overflow: hidden;
  }

  .photo {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-repeat: no-repeat;
    background-position: center;
    background-size: contain;
    transition: transform .5s ease-out;
  }


</style>

<?
function id_ytb_video($url){
    $tmp = explode('v=', $url);
    return array_pop($tmp);
}
?>

<div id="wowslider-container1" style="max-height: 600px;">
    <div class="ws_images">

        <ul>

        <?foreach ($images as $value) { ?>
            <li class="border-grey">

                <img
                src="" alt="banner-request"
                title="banner-request" class="tile"
                id="wows1_1" />
                <div class="tile" data-scale="2.4" 
                data-image="<?=thumbnail($value->ads_img_file, "ads", 1200, 1200, 2)?>">
                </div>
            </li>

        <? } 

        if ($ad->ad_video) { ?>

            <li>
                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?=id_ytb_video($ad->ad_video)?>?autoplay=0&rel=0&enablejsapi=1&playerapiid=ytplayer&wmode=transparent" frameborder="0" allowfullscreen></iframe>
                
                <img class="thumb-ytb" src="https://img.youtube.com/vi/<?=id_ytb_video($ad->ad_video)?>/hqdefault.jpg" id="wows1_0"/>
            </li>

        <? } ?>
        </ul>
    </div>

    <div class="ws_thumbs">
        <!-- <div> -->
        <? foreach ($images as $key => $value){ ?>

            <a class="border-grey" href="#wows1_1" title="banner-request">
                <img src="<?=thumbnail($value->ads_img_file, "ads", 60, 60, 2)?>" alt="" />
            </a>

        <? }https://
        if ($ad->ad_video) { ?>

            <a href="#wows1_0" title="Moinho Vermelho Buffet">
                <img src="https://img.youtube.com/vi/<?=id_ytb_video($ad->ad_video)?>/3.jpg" alt="" />
                <!-- <img src="<?=base_url()?>assets/data1/tooltips/yubfnai6b7a.png" alt="" /> -->
            </a>

        <? } ?>
        <!-- </div> -->

    </div>

    <div class="ws_shadow"></div>

</div>  

<script type="text/javascript" src="<?=base_url()?>assets/engine1/wowslider.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/engine1/script.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.ws_cover').addClass('d-none');
        $('.tile').on('mouseover', function(){
            $('img#wows1_1').css({'opacity': '0'});
            $(this).children('.photo').css({'transform': 'scale('+ $(this).attr('data-scale') +')'});
        })
        .on('mouseout', function(){
            $('#wows1_1').css({'opacity': '1'});
            $(this).children('.photo').css({'transform': 'scale(1)'});
        })
        .on('mousemove', function(e){
          $(this).children('.photo').css({'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +'%'});
        })
        // tiles set up
        .each(function(){
          $(this)
            // add a photo container
            .append('<div class="photo"></div>')
            // set up a background image for each tile based on data-image attribute
            .children('.photo').css({'background-image': 'url('+ $(this).attr('data-image') +')'});
        });
    })
</script>

