<style>
	hr {width: 80%; float: left; margin-top: 0.10px;}
	.answer{
		padding: 10px;
    background: rgb(243, 243, 243);
	}
</style>

<h3><?php echo $p->page_name ?></h3>
<hr><br/>

<?php
	echo $p->page_content;

	if($faq){
		echo '<div class="faq-accordion accordion">';
		$i = 0;

		foreach ($faq as $key => $item) {
			echo '
				<div class="fa-item" id="'.$i.'" data-status="0" >
					<div class="fa-i-question" data-toggle="collapse" data-target="#'.$item->page_faq_id.'" aria-expanded="true" aria-controls="'.$item->page_faq_id.'">
						<span>'.$item->page_faq_question.'</span>
						<span><i class="fa fa-fw fa-plus"></i></span>
					</div>
					<div id="'.$item->page_faq_id.'" class="collapse answer" aria-labelledby="headingOne" data-parent="#'.$i.'" >'.nl2br($item->page_faq_answer).'</div>
				</div>
			';
			$i++;
		}

		echo '</div>';
	}
?>