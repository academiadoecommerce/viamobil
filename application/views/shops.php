<style>
	li {list-style-type:none;}	
	.book-tab-dec {background-color: #ffffff; position: relative; float: right; border: 1px solid #E6E6E6; border-radius: 5px;}
</style>

<div class="container ">
    
    
    <?php
    include_once('returns.php');

    if (@$breadcrumbs) {
        echo '
        <div class="row hide-for-small-only d-block">
            <div class="medium-12 columns my-3">
                <div class="breadcrumbs">
                   <ul>
                      <li><a href="' . base_url() . '">Início</a></li>';

                     foreach ($breadcrumbs as $key => $bc) {
                       echo '<li>' . ((@$bc['link']) ? '<a href="' . $bc['link'] . '" target="_self">' . $bc['name'] . '</a>' : $bc['name']) . '</li>';
                    }
              echo '
                   </ul>
                </div>
            </div>
        </div>'; 
    } ?>
</div>


<div class="container d-flex bgwhite rounded border p-0 pt-2">
    <div class="row m-0 d-flex w-100">


	    <div class="col-md-2 p-0">
	        <?php include_once('shops_sidebar.php'); ?>

	        <div class="hide-for-small-only">
			<?=$this->main_model->advertisingBox('side', '266px', '600px')?>
			</div>
	    </div>

		
	    <div class="col-md-10">  
	        <?= $this->main_model->advertisingBox('top', '100%', '90px') ?>

	        <div class="row">
                <div class="col">
                    <a href="javascript:void();" data-toggle="modal" data-target=".modal-ver-mais-categ" class="float-right px-4"><i class="fa fa-bars"></i> Categorias </a>
                </div>
            </div>

	        <div class="ads-shops">
	            <?php
	            if ($shops) {		
						echo '<ul class="row no-gutters ">';
						foreach ($shops as $key => $shop) {
							echo "<li class='col-sm-6 col-md-3 border-0'>".$this->shops_model->shop_item($shop)."</li>";
						}
						echo '</ul>';						
					
	            } else {
	                echo '<div align="center"><strong>Opss!<br>Nenhuma loja encontrada!
	            <br>
	            Tente buscar uma palavra diferente, ou use os filtros!</strong></div>';
	            } ?>

	            <div class="card-body border-0">
					<aside>
						<!-- <h6 class="title-bg bg-secondary">Publicidade</h6> -->
						<div style="height:130px;">
							<figure class="itemside has-bg border-bottom p-2" style="height: 100%;">
								<a class="w-100 h-100" target="_blank" href="<?=$img_publicidade[2]->adv_url ?>">
									<div class="imgbgclass" style="background-image: url(<?= base_url('uploads/publicidade/'.$img_publicidade[2]->adv_img)?>);"></div>
									<!-- <img class="img-bg" src="<?= base_url('uploads/publicidade/'.$img_publicidade[2]->adv_img)?>" height="80"> -->
								</a>
								<!-- <figcaption class="p-2">
									<h6 class="title">Espaço para publicidade </h6>
								</figcaption> -->
							</figure>
						</div>
					</aside>
				</div>

	            <?
	            echo paginacao()->exibirPaginacao(paginacao()->getPagina(), paginacao()->getTotalPagina($total), 'lojas', $total, FALSE);
	            ?>
	        </div>
	        
	    </div>
	</div>
</div>

<script>
    $(".shopshover").hover(function() {
    
    $(this).find('h3').css("text-decoration", "underline");

}, function() {
     $(this).find('h3').css("text-decoration", "none");
});

</script>