<div class="container px-0-mobile">
	<section class="breadcrumbs">
	<?php
	include_once('returns.php');

	if (@$breadcrumbs) {
		echo '
		<div class="row hide-for-small-only">
			<div class="medium-12 columns">
				<div class="breadcrumbs">
					<ul>
						<li><a href="' . base_url() . '">Início</a></li>';

						foreach ($breadcrumbs as $key => $bc) {
						echo '<li>' . ((@$bc['link']) ? '<a href="' . $bc['link'] . '" target="_self">' . $bc['name'] . '</a>' : $bc['name']) . '</li>';
					}
				echo '
					</ul>
				</div>
			</div>
		</div>'; }
	?>
	</section>
	<div class="medium-12 columns px-0-mobile">
		<div class="ads-page border-grey rounded-0 px-0-mobile">
			<div class="row">

				<div class="d-none d-lg-block col-lg-3 p-0">
					<nav class="menu-dashboard">
						<h5 class="color-grey"><i class="fas fa-bars ml-4"></i>Minha conta</h5>
						<ul class="ml-0">

							<?php if ($this->session->userdata('login_tipo') =='lojista') { ?>

							<li>
								<a href="<?=base_url('cliente/painel')?>"
									<?=(($page == "dashboard") ? 'class="active"' : '')?> target="_self">
									<span><i class="fa fa-fw fa-tags"></i></span>
									<span>Meus Anúncios</span>
								</a>
							</li>

							<li>
								<a href="<?=base_url('vendas')?>"
									<?=(($page == "view_vendas") ? 'class="active"' : '')?> target="_self">
									<span><i class="fa fa-credit-card"></i></span>
									<span>Minhas Vendas</span>
								</a>
							</li>


							<li>
								<a href="https://connect.wirecard.com.br" target="_blank">
									<span><i class="fa fa-at"></i></span>
									<span>Acessar Wirecard</span>
								</a>
							</li>

							<li>
								<a href="<?=base_url('duvidas')?>"
									<?=(($page == "view_duvidas") ? 'class="active"' : '')?> target="_self">
									<span><i class="fa fa-question"></i></span>
									<span>Perguntas Recebidas</span>
								</a>
							</li>


							<?php } ?>
							
							<li>
								<a href="<?=base_url('cliente/detalhes')?>"
									<?=(($page == "details") ? 'class="active"' : '')?> target="_self">
									<span><i class="fa fa-fw fa-cogs"></i></span>
									<span>Meu Cadastro</span>
								</a>
							</li>

							<li>
								<a href="<?=base_url('duvidas')?>"
									<?=(($page == "view_duvidas") ? 'class="active"' : '')?> target="_self">
									<span><i class="fa fa-question"></i></span>
									<span>Perguntas Feitas</span>
								</a>
							</li>

							<li>
								<a href="<?=base_url('pedidos')?>"
									<?=(($page == "view_pedidos") ? 'class="active"' : '')?> target="_self">
									<span><i class="fas fa-box"></i></span>
									<span>Meus Pedidos</span>
								</a>
							</li>


							<?php if(count($minhas_recl) > 0): ?>
							<li>
								<a href="<?=base_url('minhas_reclamacoes')?>"
									<?=(($this->uri->segment(1) == "minhas_reclamacoes") ? 'class="active"' : '')?>
									target="_self">
									<span><i class="fa fa-comment"></i></span>
									<span>Minhas Reclamações</span>
								</a>
							</li>
							<?php endif; ?>

							<?php if ($this->session->userdata('login_tipo') =='lojista') { ?>
							
							<?php if(count($recebida_recl) > 0): ?>
							<li>
								<a href="<?=base_url('reclamacoes_recebidas')?>"
									<?=(($this->uri->segment(1) == 'reclamacoes_recebidas') ? 'class="active"' : '')?>
									target="_self">
									<span><i class="fa fa-credit-card"></i></span>
									<span>Reclamações Recebidas</span>
								</a>
							</li>
							<?php endif; ?>

							<li>
								<a href="<?=base_url('cliente/loja')?>"
									<?=(($page == "shop" || $page == "shop_denied" || $page == "shop_create") ? 'class="active"' : '')?>
									target="_self">
									<span><i class="fa fa-fw fa-shopping-cart"></i></span>
									<span>Minha Loja</span>
								</a>
							</li>

							<?php } ?>

							<li>
								<a href="<?=base_url('cliente/favoritos')?>"
									<?=(($page == "favorites") ? 'class="active"' : '')?> target="_self">
									<span><i class="fa fa-fw fa-heart"></i></span>
									<span>Favoritos</span>
								</a>
							</li>

							<li>
								<a href="<?=base_url('cliente/sair')?>" target="_self">
									<span><i class="fa fa-fw fa-times"></i></span>
									<span>Sair</span>
								</a>
							</li>
						</ul>
					</nav>

					<div class="profile-infos hide-for-small-only ml-3">
						<?php
						if ($this->session->userdata('login_tipo') == 'lojista') {
							$products_published = $this->user_model->adsCount(2);
							$evaluating_products = $this->user_model->adsCount(1);
							$products_sold = $this->user_model->info('use_ads_sales');
						?>
						<ul>
							<li><i class="fa fa-fw fa-tags"></i>Você possui <strong><?=$products_published?></strong>
								anúncio(s) publicado(s).</li>
							<li><i class="fa fa-fw fa-eye"></i>Existe <strong><?=$evaluating_products?></strong>
								anúncio(s) à ser analisado.</li>
							<li><i class="far fa-handshake"></i>Você já vendeu <strong><?=$products_sold?></strong>
								produto(s).</li>
						</ul>
						<?php } else{ ?>
						<center>
							<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#form-lojista">Quero
								ser um lojista</a>
						</center>
						<?php } ?>
					</div>


					<div class="hide-for-small-only">
						<!-- Oculta os anúncios do Google  -->
						<?= $this->main_model->advertisingBox('side', '266px', '600px')?>
					</div>
				</div>

				<!-- Inclui as Views no Corpo do Perfil do Usuário  -->

				<div class="col-lg-9">
					<!-- Oculta os anúncios do Google  -->
					<?= $this->main_model->advertisingBox('top', '100%', '90px'); ?>

					<?php
						$subFolder = 'profile/';
						if($page == 'chat'){
							$subFolder = '';
						}
						include_once($subFolder.$page.".php");
					?>
				</div>
			</div>
		</div>
	</div>
</div>
