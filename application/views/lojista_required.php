<!-- MODEL DO LOGIN  -->
    <div class="modal fade" id="modal-lojista" tabindex="-1" role="dialog">
        <div class="modal-dialog login1"  style="margin-top: 130px;">
            <div class="modal-content">
                <div class="user-box">
                    <div class="page-header" style="margin: 0 0 20px;">
                        <h3 align="center">Ops, Seu cadastro está incompleto</h3>
                    </div>

					<div class="form-group row">
						<div class="col-lg-12">
							<p align="center">Para fazer isto você precisa antes completar o seu cadastro em nosso site :)</p>
                            <br><br>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-6 center">							
							<a href="#" data-toggle="modal" data-target="#form-lojista" class="btn btn-primary btn-full myBtn" style="margin-bottom: 10px;"><i class="fa fa-user-plus" aria-hidden="true"></i>Completar cadastro</a>
						</div>
					</div>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
	
<script>
    // Hide the Modal
    $(".myBtn").click(function(){
        $("#modal-lojista").modal("hide");
    });
</script>	


