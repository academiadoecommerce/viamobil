
<!-- ========================= SECTION MAIN ========================= -->
<style>
	li {list-style-type:none;}
</style>

<section class="section-main bg padding-y-sm">
	<div class="container">
		<div class="card-body">
			<div class="row">
				<aside class="col-md-12">
					<h3 class="text-uppercase"><i class="fa fa-bars"></i> Categorias do ViamoBil </h3>
					<hr>
					<div class="main-categories">
					   <div class="mc-container container" data-status="0"> 

					   
							<?php
							
							  //  Exibe es categorias PAI e Filho na pagina principal
							  if($ads_categories){
								 $i = 1;
								 $r = 1;

								 //echo '<div class="row" id="mc-row-'.$r.'">';


								 foreach ($ads_categories as $key => $cat) {

								 	

									$a = 2;
									
									/* Conta total das Categorias pai */
									$data['count_categories'] = $this->ads_model->adscategories(0);  // Select das categorias Pai.									
									
									$sub_categories = $this->ads_model->categories($cat->ads_cat_id);  // Pesquisa pela sub-categoria, passando o campo 'ads_cat_id' como parametro.
									$sub_count = count($sub_categories) / 3;

									if ($sub_categories > 0 ) {

										

									echo '
									<div class="row ">
										  <div class="mc-box w-100">
										  <a href="'.base_url('anuncios/?categoria='.$cat->ads_cat_id).'">
											 <h4><i class="fa '.$cat->ads_cat_icon.'"></i>'.$cat->ads_cat_name.'</h4> </a>
											 <ul> <div class="row">
									';

									if($sub_categories){
										$a = 12; $b = 0;
									   foreach ($sub_categories as $key => $sub_cat) {
										   
										// Conta o total de anuncios por sub-categoria.
										$categ_filho = $this->ads_model->getCategoriaFilho($sub_cat->ads_cat_id);
										$ids = array();
										foreach ($categ_filho as $key => $value) {
											$ids[] = $value->ads_cat_id;
										}
										
										// echo '<pre>';
										// print_r($ids);
										// die;

										// $this->db->where('ads_cat_id', $sub_cat->ads_cat_id);
										$this->db->where_in('ads_cat_id', $ids);
										// $this->db->or_where('ads_cat_id', $categ_ter);
										$contagem = $this->db->count_all_results('ads');


										//echo ' <div class="col-3">'; 
											
										   										  
										  if($a > 0){ 
											echo '<li class="col-12 col-md-4 col-lg-3"><a class="fluid-container" targer="_self" href="'.base_url('anuncios/?categoria='.$sub_cat->ads_cat_id).'">'.$sub_cat->ads_cat_name.' ('.$contagem.')</a></li>';
											$a--;
										
										   }


									    

										 //   if($a > 0){ 
											// echo '<li><a targer="_self" href="'.base_url('anuncios/?categoria='.$sub_cat->ads_cat_id).'">'.$sub_cat->ads_cat_name.'</a><span class="badge badge-secondary"> '.$contagem.'</span></li>';
											// $a--;
										 //   }
										}
									}

									echo '		</div>
											 </ul>
										  </div>
									   </div>
									';

									if($i % 4 == 0) {
									   $r++;

									  // echo '</div><div class="row" id="mc-row-'.$r.'">';
									}

									$i++;
									} else{

										// print_r($cat);
										$sem_sub[] = $cat;
									}
									
								}

								foreach ($sem_sub as $categ) {
									echo '
									   <div class="row">
										  <div class="mc-box">
										  <a href="'.base_url('anuncios/?categoria='.$categ->ads_cat_id).'">
											 <h4><i class="fa '.$categ->ads_cat_icon.'"></i>'.$categ->ads_cat_name.'</h4> </a>
											 <ul>
											 </div> </div>
									';
								}

								 echo '</div>';
							  }
						   ?>

	
				</aside> <!-- col.// -->
			</div> 
		</div>
	</div> 
</section>

