	
<!-- MODEL DO CADASTRO   -->
    <div class="modal fade" id="form-lojista" tabindex="-1" role="dialog">
        <div class="modal-dialog login1"  style="margin-top: 30px;">
            <div class="modal-content">
                <div class="user-box">
                    <div class="page-header" style="margin: 0 0 20px;">
                        <h3>Quero ser um lojista</h3>
                    </div>
					
					<form method="POST" action="<?= base_url('register/lojista') ?>" id="form-register" class="form form-simple">

						<div class="form-group row my-1">
							<div class="col-12">
								<input class=" my-1 form-control" type="text" name="cpfCnpj" id="cpfCnpj" maxlength="14" onkeyup="somenteNumeros(this);" placeholder="Número do CPF/CNPJ" required>
							</div>							
						</div>				

						<div class="form-group row my-1">
							<div class="col-12 mb-1">
								<input class="form-control my-1" type="text" name="urlSite" id="urlSite" placeholder="URL do Site">
								<small id="emailHelp " class="form-text text-muted"> Obrigatório para logistas.</small>
							</div>
						</div>

						<div class="form-group row my-1">
		                    <div class="col-lg-12">
		                        <div class="checkbox-custom">
		                            <input type="checkbox" required="" id="ai-terms">
		                            <label for="ai-terms">Concordo com os <a href="<?=base_url()?>/ajuda/termos-de-uso" target="_blank">Termos do <?php echo NOME_SITE ?></a>.</label>
		                        </div>
		                    </div>
	                    </div> 
						
						<!-- <div class="form-group row my-1">
						  <div class="col-12">
							<input class="form-control my-1" type="password" required onkeyup="passwordStrength(this.value)" id="senha" name="password" placeholder="Senha (escolha um senha forte)">
							  <div class="passwordForce">
								  <div class="no-strength"></div>
							  </div>
						  </div>
						</div> -->

						<div id="register-return"></div>

						<div class="form-group row my-1">
							<div class="col-lg-12">
								<button type="submit" class="btn btn-primary  btn-lg btn-block" id="cadastrar"> <i class="fa fa-user-plus" aria-hidden="true"></i> Confirmar</button>
							</div>
						</div>						
						
					</form>
					
					<script src="<?= base_url("assets/js/custom/register.js") ?>"></script>	
					<script src="<?= base_url("assets/js/custom/modal_registro.js") ?>"></script>
															
                </div>
            </div>           
        </div>
    </div>

