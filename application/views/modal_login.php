		
<!-- MODEL DO LOGIN  -->
    <div class="modal fade" id="modal-login" tabindex="-1" role="dialog">
        <div class="modal-dialog login1"  style="margin-top: 130px;">
            <div class="modal-content">
                <div class="user-box">
                    <div class="page-header" style="margin: 0 0 20px;">
                        <h3>Acesse sua Conta</h3>
                    </div>
					
					<form method="POST" action="<?=base_url('login/in')?>" id="form-login" class="form form-simple">
					
						<div class="form-group row">
						  <div class="col-12">
							<input class="form-control" type="email" required name="email" id="input-email" placeholder="E-mail (exemplo@exemplo.com.br)">
						  </div>
						</div>

						<div class="form-group row">
						  <div class="col-12">
							<input class="form-control" type="password" required name="password" id="input-password" placeholder="Senha">
						  </div>
						</div>

						<div class="form-group row">
						  <div class="col-12" style="text-align: right;">
							<a  href="#" data-toggle="modal" data-target="#modal-password" class="modal-open myBtn color-36">Esqueceu a senha?</a>							
						  </div>
						</div>						

						<div id="login-return"></div>

						<div class="form-group row">
							<div class="col-lg-12 text-right">
								<button type="button" id="logar" class="btn btn-primary btn-lg btn-block"><i class="fa fa-home" aria-hidden="true"></i> Entrar</button>
							</div>
						</div>
				
					</form>
					<script src="<?=base_url('assets/js/custom/login.js')?>"></script>
                </div>
            </div>            
        </div>
    </div>
	<?php include_once("modal_password.php");?>
	
<script>
    // Hide the Modal
    $(".myBtn").click(function(){
        $("#modal-login").modal("hide");
    });
</script>	