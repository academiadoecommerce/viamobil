$(document).ready(function(){

    $("#logar, .btn-logar").click(function(event) {
		 
		//URL BASE
		var base_url = window.location.origin;
	
        var action = $("#form-login").attr('action');
        var submit = $("#logar");
        var submit_text = submit.html();
        var email = $("#input-email").val();
        var password = $("#input-password").val();
		
        if (email == '') {
            $("#login-return").html('<div class="row"><div class="col-lg-12"><div class="alert alert-warning alert-center alert-small">Preencha o campo E-mail</div></div></div>');
            setTimeout(function() {
                    $("#login-return").html('');
                    }, 5000);
        } else if (password == '') {
            $("#login-return").html('<div class="row"><div class="col-lg-12"><div class="alert alert-warning alert-center alert-small">Preencha o campo Senha</div></div></div>');
            setTimeout(function() {
                    $("#login-return").html('');
                    }, 5000);
        } else if (email != '' && password != '') {
            submit.html('<i class="fa fa-fw fa-cog fa-spin"></i>').attr('disabled', '');
           $.post(action, {email: email, password: password}, function(data, textStatus, xhr) {
            /* nothing */
            }).done(function(data) { 

                if (data == "login_success") {

                    window.location.reload();
                    // window.location.replace(base_url + "/profile");
				} else if (data == "login_error_status") {
                    $('#modal-login').modal('hide');
                    $('#fundo_modal').fadeIn();
                    
                    $("#logar").html(submit_text).removeAttr('disabled', '');
                    // $("#login-return").html('<div class="row"><div class="col-lg-12"><div class="alert alert-warning alert-center alert-small">Conta aguardando aprovação do administrador.</div></div></div>');

                    setTimeout(function() {
                        // $("#login-return").html('');
                    }, 5000);
					
                } else if (data == "login_error") {
                    /*$("#login-return").html('<div class="row"><div class="col-lg-12"><div class="alert alert-warning alert-center alert-small">E-mail e senha não coicidem.<br>Tente novamente.</div></div></div>');
                    $("#logar").html(submit_text).removeAttr('disabled', '');

                    setTimeout(function() {
                        $("#login-return").html('');
                    }, 5000);*/
                    window.location.reload();
                }
            }).error(function() {

            }).always(function() {
                submit.html(submit_text).removeAttr('disabled', '');

                console.clear();
            });
        }

        return false;
    });
});

var modal_estilos = 'display: block;'
+'width: 85%; max-width: 400px;'
+'background: #fff; padding: 15px;'
+'border-radius: 5px;'
+'-webkit-box-shadow: 0px 6px 14px -2px rgba(0,0,0,0.75);'
+'-moz-box-shadow: 0px 6px 14px -2px rgba(0,0,0,0.75);'
+'box-shadow: 0px 6px 14px -2px rgba(0,0,0,0.75);'
+'position: fixed;'
+'top: 50%; left: 50%;'
+'transform: translate(-50%,-50%);'
+'z-index: 99999999; text-align: center';

var fundo_modal_estilos = 'top: 0; right: 0;'
+'bottom: 0; left: 0; position: fixed;'
+'background-color: rgba(0, 0, 0, 0.6); z-index: 99999999;'
+'display: none;';

var meu_modal = '<div id="fundo_modal" style="'+fundo_modal_estilos+'">'
+'<div id="meu_modal" class="modal-login-alert" style="'+modal_estilos+'">'
   +'<h5><i class="fas fa-exclamation-triangle"></i>Conta aguardando aprovação <br>do administrador</h5><br />'
   +'<p>Para mais informações entre em contato através <a style="color:#c58104;" href="https://viamobil.com.br/contato">deste link.</a></p>'
   +'<button type="button" class="close" style="top: 5px; right: 10px; position: absolute; cursor: pointer;"><span>&times;</span></button>'
+'</div></div>';

$("body").append(meu_modal);

$("#fundo_modal, .close").click(function(){ $("#fundo_modal").hide(); });
$("#meu_modal").click(function(e){ e.stopPropagation(); });