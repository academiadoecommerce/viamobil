var qtd = $('#quantidade').val();
var qtd_min = $('#quantidade').data('atacado');
var qtd_atacado = $('#qtd_atacado').val();

function sliderControls() {
    var sliderHeight = $("#ap-i-slider").height();
    var controls = $(".ap-images .ap-i-controls");

    controls.css('margin-bottom', '-' + sliderHeight + 'px');
    controls.children('.ap-ic-btn').height(sliderHeight);
}

// acao btn comprar em atacado da página
$('.comprar_atacado').on('click', function () {
    $('#modal-atacado').modal('toggle');
})

// acao botao comprar atacado do modal
$(document).on('click', '#comprar-atacado', function () {
    CompraDireta();
});

// listener de quantidade
$('#quantidade').on('input', function () {
    qtd = $(this).val();
    if (qtd >= qtd_min) {
        qtd = qtd_min;
        $(this).val(qtd);
    }
})

// listener de quantidade atacado
$('#qtd_atacado').on('input', function () {
    qtd_atacado = 1;
    if (qtd_atacado < qtd_min) {
        qtd_atacado = qtd_min;
        $(this).val(qtd_atacado);
        console.log(qtd_atacado);
    }
});

// manipula btn adicionar ao carrinho
$('.btn-add-cart').click(async function () {
    if (qtd_min >= 1) {
        let cor = $('input[name="cor[]"]:checked').val();
        let tamanho = $('input[name="tamanho[]"]:checked').val();
        let url = $(this).data('add');
        await $.get(url, {qtd, cor, tamanho}, (res) => {
            console.log('adicionado');
        });
        window.location.href = `${base_url}/cart`;
        return false;
    }
});

// alterna a visualização do botão compartilhar
$('.social-toggle').on('click', function () {
    $('.social-networks').toggleClass('open-menu');
});

$(window).resize(function () {
    sliderControls();
});

// executa a compra direta para compras no varejo ou atacado
function CompraDireta() {
    let cor = $('input[name="cor[]"]:checked').val();
    let tamanho = $('input[name="tamanho[]"]:checked').val();
    var quantidade = 1;
    id = $('#produto_id').text();
    var frete = document.getElementById("fretePost").value;
    var vendedor = document.getElementById("vendedor").value;

    $.post(base_url + 'checkout/pedidodireto', {id, quantidade, frete, vendedor, cor, tamanho}, function (data) {
        window.location.href = data;
    });
}