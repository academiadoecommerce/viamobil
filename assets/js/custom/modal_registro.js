
   $("#ai-cep").keyup(function() {
       
        // Determina o caminho da URL do site.
        var base_url = window.location.origin;
       
        var cep_code = $(this).val();
        cep_code = cep_code.replace('_', '');
        if (cep_code.length == 9) {
        
            $.getJSON(base_url + '/address/getbycep/' + cep_code, function(data) {

                if (data.states != null) {
                    $("#ai-state").empty();
                    $('#ai-state').append('<option>Selecione seu Estado</option>');
                    $.each(data.states, function(index, value) {
                        if (value.state_id == data.state_id) {
                            $('#ai-state').append('<option value="' + value.state_id + '" selected="selected">' + value.name + '</option>');
                        } else {
                            $('#ai-state').append('<option value="' + value.state_id + '">' + value.name + '</option>');
                        }
                    });
                }

                if (data.cities != null) {
                    $("#ai-city").empty();
                    $('#ai-city').append('<option>Selecione um município</option>');
                    $.each(data.cities, function(index, value) {
                        if (value.city_id == data.city_id) {
                            $('#ai-city').append('<option value="' + value.city_id + '" selected="selected">' + value.name + '</option>');
                        } else {
                            $('#ai-city').append('<option value="' + value.city_id + '">' + value.name + '</option>');
                        }
                    });
                }

                if (data.neighborhoods != null) {
                    $.each(data.neighborhoods, function(index, value) {
                        if (value.neighborhood_id == data.neighborhood_id) {
                            $("#box-neighborhood").show();
                            $('#label-neighborhood').html('<b>Bairro:</b><br>' + value.name);
                            $('#ai-neighborhood').val(value.name);
                        }
                    });
                }

                if (data.regions != null) {
                    $("#ai-region").empty();
                    $('#ai-region').append('<option>Região da Cidade</option>');
                    $.each(data.regions, function(index, value) {
                        if (value.region_id == data.region_id) {
                            $('#ai-region').append('<option value="' + value.region_id + '" selected="selected">' + value.name + '</option>');
                        } else {
                            $('#ai-region').append('<option value="' + value.region_id + '">' + value.name + '</option>');
                        }
                    });
                }

                if (data.addressText != null) {
                    $("#box-address").show();
                    $("#label-address").html('<b>Endereço:</b><br>' + data.addressText);
                    $("#ai-address").val(data.addressText);
                }

            });
        }
    }); 

		
	
   $("#c-cep").keyup(function() {
	   
		// Determina o caminho da URL do site.
		var base_url = window.location.origin;
	   
        var cep_code = $(this).val();
	
        if (cep_code.length == 9) {
            $.getJSON(base_url + '/address/getbycep/' + cep_code, function(data) {

                if (data.states != null) {
                    $("#c-state").empty();
                    $('#c-state').append('<option>Selecione seu Estado</option>');
                    $.each(data.states, function(index, value) {
                        if (value.state_id == data.state_id) {
                            $('#c-state').append('<option value="' + value.state_id + '" selected="selected">' + value.name + '</option>');
                        } else {
                            $('#c-state').append('<option value="' + value.state_id + '">' + value.name + '</option>');
                        }
                    });
                }

                if (data.cities != null) {
                    $("#c-city").empty();
                    $('#c-city').append('<option>Selecione um município</option>');
                    $.each(data.cities, function(index, value) {
                        if (value.city_id == data.city_id) {
                            $('#c-city').append('<option value="' + value.city_id + '" selected="selected">' + value.name + '</option>');
                        } else {
                            $('#c-city').append('<option value="' + value.city_id + '">' + value.name + '</option>');
                        }
                    });
                }

                if (data.neighborhoods != null) {
                    $.each(data.neighborhoods, function(index, value) {
                        if (value.neighborhood_id == data.neighborhood_id) {
                            $("#box-neighborhood").show();
                            $('#label-neighborhood').html('<b>Bairro:</b><br>' + value.name);
                            $('#c-neighborhood').val(value.name);
                        }
                    });
                }

                if (data.regions != null) {
                    $("#c-region").empty();
                    $('#c-region').append('<option>Região da Cidade</option>');
                    $.each(data.regions, function(index, value) {
                        if (value.region_id == data.region_id) {
                            $('#c-region').append('<option value="' + value.region_id + '" selected="selected">' + value.name + '</option>');
                        } else {
                            $('#c-region').append('<option value="' + value.region_id + '">' + value.name + '</option>');
                        }
                    });
                }

                if (data.addressText != null) {
                    $("#box-address").show();
                    $("#label-address").html('<b>Endereço:</b><br>' + data.addressText);
                    $("#c-address").val(data.addressText);
                }
            });
        }
    });	

    $('#ai-state').change( function() {
        let state_id = $(this).val();
        $.getJSON( `${base_url}/address/getbycep/${state_id}/regions`, (data) => {
            console.log(data);
            var region_array = [];
            $.each(data.regions, function(index, value) {
                region_array[index] = value.region_id;
            });
            $.post(`${base_url}/address/getbycep/0/state-city`, {region_array: region_array}, (data) => {
                console.log('retorno2 = '+ data);
                $("#ai-city").empty();
                $('#ai-city').append('<option>Selecione um município</option>');
                $.each(data.cities, function(index, value) {
                    if (value.city_id == data.city_id) {
                        $('#ai-city').append('<option value="' + value.city_id + '" selected="selected">' + value.name + '</option>');
                    } else {
                        $('#ai-city').append('<option value="' + value.city_id + '">' + value.name + '</option>');
                    }
                });
            });
        });
    });
    $('#c-state').change( function() {
        let state_id = $(this).val();
        $.getJSON( `${base_url}/address/getbycep/${state_id}/regions`, (data) => {
            console.log(data);
            var region_array = [];
            $.each(data.regions, function(index, value) {
                region_array[index] = value.region_id;
            });
            $.post(`${base_url}/address/getbycep/0/state-city`, {region_array: region_array}, (data) => {
                console.log('retorno2 = '+ data);
                $("#c-city").empty();
                $('#c-city').append('<option>Selecione um município</option>');
                $.each(data.cities, function(index, value) {
                    if (value.city_id == data.city_id) {
                        $('#c-city').append('<option value="' + value.city_id + '" selected="selected">' + value.name + '</option>');
                    } else {
                        $('#c-city').append('<option value="' + value.city_id + '">' + value.name + '</option>');
                    }
                });
            });
        });
    });
