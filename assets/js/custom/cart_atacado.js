function calcular(){
    alert('funcao calcular')
    $("#retorno").html("Calculando...");
    var cep_cliente = $("#cep_destino").val();
    $.post(base_url+'index.php/Cart/calcular',{cep_cliente},function(data){
        $("#retorno-frete").html(data);
        $("#valor_frete").val(data);
    });
}

// função acionada pelo botao finalizar pedido para checkout
function pedido(){
    var frete = $("#valor_frete").val().replace(',','.');
    frete = frete.substr(3);
    $('.btn-checkout').text( 'Processando...' )
    $.post(base_url+'index.php/Checkout/criar_pedido',{frete},function(data){
        window.location.href = data;
        //alert(data);
        //console.log(data);
    });
}

// ação ao alterar a quantidade de um produto do carrinho
$(document).on('change', '#quantidade-item',function(){
    var tipo = $(this).data('tipo');
    var minimo = $(this).data('minimo');

    var quantidade_item = $(this).val();

    if(tipo.trim() == "atacado" && quantidade_item >= minimo){
        $(this).val(minimo - 1); 
        quantidade_item = minimo - 1;
    }
    
    var btn_qtd = $(this)
    $(btn_qtd).attr('disabled', true)
    var rowid = $(this).next().val()
    var json_req = {quantidade_item:quantidade_item, rowid:rowid}
    $.post(base_url+'Cart/atualizar',{json_req}, function(data){
       if(data.status == 'success'){
          $('#total_item_r').html(data.total_qtd)
          $('#valor_total').html("R$ "+data.valor_total)
          if (data.cep) {
              $("#retorno-frete").html(' Atualizando...')
              calc_frete_req(data.cep)
          }
       }
       $(btn_qtd).removeAttr('disabled')
    })
 })

 // ação do botão 'calcular'
$(document).ready(function(){
    $('#btn_calcular').on('click', function(event){
        event.preventDefault();
        var btntxt = $(this).text();
        var btn_calcular = $(this);
        $('#retorno-frete').text("  Aguarde...");
        $(btn_calcular).attr('disabled', true);
        var cep_cliente = $("#input-cep").val();
        calc_frete_req(cep_cliente)
        setTimeout(() => {
            $(btn_calcular).attr('disabled', false);
        }, 4000);
    })
})

// busca o valor do frete pelo cep em formato 00000-00 com os produtos da session cart
function calc_frete_req(cep_cliente){
    $.post(base_url+'index.php/Cart/calcular',{cep_cliente}, (data) => {
        if ( parseFloat(data) > 5 ) {
            $("#retorno-frete").html(" R$ "+data);
            $("#valor_frete").val(" R$ "+data);
            $('.modal-backdrop, .modal').fadeOut()
            return;
        }

        setAlert( 'danger', 'Não foi possível processar sua solicitação, tente novamente mais tarde ou entre em contato com o vendedor!' );
    });
}

function validaQtd(minimo, tipo){

    var qtd = $("#quantidade-item").val();
    if(tipo == "atacado" && qtd >= minimo){
        document.getElementById("quantidade-item").value = minimo - 1; 
    }
    console.log( 'a quantidade é' + qtd)

}