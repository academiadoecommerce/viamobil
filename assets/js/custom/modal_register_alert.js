
var modal_estilos = 'display: block;'
	+'width: 85%; max-width: 400px;'
	+'background: #fff; padding: 15px;'
	+'border-radius: 5px;'
	+'-webkit-box-shadow: 0px 6px 14px -2px rgba(0,0,0,0.75);'
	+'-moz-box-shadow: 0px 6px 14px -2px rgba(0,0,0,0.75);'
	+'box-shadow: 0px 6px 14px -2px rgba(0,0,0,0.75);'
	+'position: fixed;'
	+'top: 50%; left: 50%;'
	+'transform: translate(-50%,-50%);'
	+'z-index: 99999999; text-align: center';

	var fundo_modal_estilos = 'top: 0; right: 0;'
	+'bottom: 0; left: 0; position: fixed;'
	+'background-color: rgba(0, 0, 0, 0.6); z-index: 99999999;'
	+'display: none;';

var meu_modal = '<div id="register_modal" style="'+fundo_modal_estilos+'">'
	+'<div id="meu_modal_register" class="modal-login-alert" style="'+modal_estilos+'">'
	   +'<h5><i class="fas fa-exclamation"></i>Você acaba de se cadastrar no Via Móbil Classificados.</h5><br />'
	   +'<p>Seu cadastro será moderado. O prazo de análize pela nossa equipe é de 24 horas.<br> Você receberá um e-mail informando.<br>'
	   +'Para mais informações entre em contato através <a style="color:#c58104;" href="http://viamobil.com.br/contato">deste link.</a></p>'
	   +'<button type="button" class="close" style="top: 5px; right: 10px; position: absolute; cursor: pointer;"><span>&times;</span></button>'
	+'</div></div>';

$("body").append(meu_modal);

$("#register_modal, .close").click(function(){ $("#register_modal").hide(); });
$("#meu_modal_register").click(function(e){ e.stopPropagation(); });
$(document).ready(function(){
	$('#register_modal').fadeIn();
})
      