<?php
date_default_timezone_set('America/Sao_Paulo');

define("NOME_SITE","ViamoBil");

define("CI_STATUS", "production"); // development | testing | production

/** enable https protocol */
define('USE_HTTPS', true);

/* App Config */
define("APP_PATH", "/");
define("APP_URL", "https://viamobil.com.br".APP_PATH);
define("ADMIN_PATH", APP_URL.'admin');
define("APP_ADMIN", ADMIN_PATH);

/* Files */
define("THUMBNAIL", APP_PATH."/assets/php/image.php");
/* Dirs */
define("UPLOADS", APP_ADMIN."/uploads");

/* Database Config */
define("DB_HOSTNAME", "viamobil.com.br");
// define("DB_HOSTNAME", "localhost");
define("DB_USERNAME", "viamobil_admin");
define("DB_PASSWORD", "g~szu5m,CRfP");
define("DB_DATABASE", "viamobil_loja");

define("DB_DBDRIVER", "mysqli");
define("DB_DBPREFIX", "panamerico_");

/** Wirecard Setting */
//sandbox
// define('app_id', 'APP-432G3PGWOLGV');
// define('accessToken', '47b71c41eebd405d941daba340285460_v2');
// define('secret', '463c0e7ee3db4b34b95ba648bd2a2b55');
// define('redirect_moip', 'https://shopbras.net/teste/redirect_teste');


// production
define('accessToken', '169e25f57d034b91b80a7b2442e160c5_v2');
define('secret', 'a531638c3e744c05b0f80aa8be6bd183');
define('app_id', 'APP-R4GBZ4X8YE7L');
define('redirect_moip', 'https://www.viamobil.com.br/checkout/redirect');


/** Força https em ambiente de produção */
if( USE_HTTPS && $_SERVER["HTTPS"] != "on" ) {
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}
