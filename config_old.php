<?php
date_default_timezone_set('America/Sao_Paulo');

define("CI_STATUS", "production"); // development | testing | production

/** enable https protocol */
define('USE_HTTPS', true);

/* App Config */
define("APP_PATH", "/");
define("APP_URL", "https://v4.soatacado.com".APP_PATH);
define("ADMIN_PATH", APP_URL.'admin');
define("APP_ADMIN", ADMIN_PATH);

/* Files */
define("THUMBNAIL", APP_PATH."/assets/php/image.php");
/* Dirs */
define("UPLOADS", APP_ADMIN."/uploads");

/* Database Config */
define("DB_HOSTNAME", "soatacado.com");
// define("DB_HOSTNAME", "localhost");
define("DB_USERNAME", "soatacad_admin");
define("DB_PASSWORD", "[7](88FXlu1uQH7%g#");
define("DB_DATABASE", "soatacad_v4");

define("DB_DBDRIVER", "mysqli");
define("DB_DBPREFIX", "panamerico_");

/** Wirecard Setting */
//sandbox
// define('app_id', 'APP-432G3PGWOLGV');
// define('accessToken', '47b71c41eebd405d941daba340285460_v2');
// define('secret', '463c0e7ee3db4b34b95ba648bd2a2b55');
// define('redirect_moip', 'https://shopbras.net/teste/redirect_teste');


// production
define('accessToken', 'ba8ea3bbd1f846a194757306b9c3b7e0_v2');
define('secret', '887b8094b07a46fbb27126b933eb2f75');
define('app_id', 'APP-8L5XTWCKN7YZ');
define('redirect_moip', 'https://www.shopbras.net/checkout/redirect');


/** Força https em ambiente de produção */
if( USE_HTTPS && $_SERVER["HTTPS"] != "on" ) {
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}
